package com.example.demo.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.security.Principal;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import org.apache.poi.openxml4j.util.ZipSecureFile;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.util.IOUtils;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.example.demo.CrearResumenLote;
import com.example.demo.Errores;
import com.example.demo.ErroresBalance;
import com.example.demo.GuardarErroresLote;
import com.example.demo.RegistrosLog;
import com.example.demo.ValidarDatosParametria;
import com.example.demo.ValidarEscruturaLote;
import com.example.demo.ValorDivisa;
import com.example.demo.Entity.Area;
import com.example.demo.Entity.Balance;
import com.example.demo.Entity.CentroCostos;
import com.example.demo.Entity.CuentasExceptuadas;
import com.example.demo.Entity.DebitoCredito;
import com.example.demo.Entity.FechaControlContable;
import com.example.demo.Entity.Fuente;
import com.example.demo.Entity.HorasCorte;
import com.example.demo.Entity.Lote;
import com.example.demo.Entity.LoteUsuario;
import com.example.demo.Entity.Mensaje;
import com.example.demo.Entity.Moneda;
import com.example.demo.Entity.NumeroCuenta;
import com.example.demo.Entity.Plano;
import com.example.demo.Entity.ResumenLote;
import com.example.demo.Entity.Sucursal;
import com.example.demo.Entity.TRM;
import com.example.demo.Entity.Usuarios;
import com.example.demo.Entity.ValorCuentasPYG;
import com.example.demo.Paginador.Paginador;

import com.example.demo.service.BalanceService;
import com.example.demo.service.CargarArchivoServer;
import com.example.demo.service.CentroCostosService;
import com.example.demo.service.CuentasExceptuadasService;
import com.example.demo.service.CuentasService;
import com.example.demo.service.DebitoCreditoService;
import com.example.demo.service.FechaControlContableService;
import com.example.demo.service.LoteUsuerioService;
import com.example.demo.service.MensajeService;
import com.example.demo.service.MonedaService;
import com.example.demo.service.ResumenLoteService;
import com.example.demo.service.SucursalService;
import com.example.demo.service.TrmServices;
import com.example.demo.service.UsuariosService;
import com.example.demo.service.ValorCuentasPYGService;
import com.example.demo.service.fuenteService;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@Controller
public class ControllerLote {

	
	@Autowired
	private CargarArchivoServer cargarArchivo;
	
	@Autowired
	private ValorCuentasPYGService valorCuentasPyGservice;

	@Autowired
	private CentroCostosService serviceCentroCostos;

	@Autowired
	private CuentasService serviceCuenta;

	@Autowired
	private fuenteService fuenteservice;

	@Autowired
	private SucursalService sucursalService;

	@Autowired
	private MonedaService monedaService;

	@Autowired
	private DebitoCreditoService debitoCreditoService;

	@Autowired
	private FechaControlContableService fechacontableservice;

	@Autowired
	private UsuariosService usuarioService;

	@Autowired
	private LoteUsuerioService loteUsuarioService;
	
	@Autowired
	private ResumenLoteService resumenLoteService;
	
	@Autowired
	public MensajeService mensajeService;
	
	
	@Autowired
	private BalanceService balanceService;
	
	@Autowired
	private UsuariosService usuarioservice;
	
	@Autowired
	private ValorCuentasPYGService CuentasPYGservice;
	
	@Autowired
	private TrmServices trmService;
	
	@Autowired
	private CuentasExceptuadasService cuentasservice;
	

	private String notificacion = "";

	private int Contador = 0;
	
	private RegistrosLog registrolog = new RegistrosLog();

	@RequestMapping(value = "/lote" , method = RequestMethod.GET)
	public String VistaCargarLote(@RequestParam(name = "page" , defaultValue = "0")  int page , Model model, RedirectAttributes redirectAttributes, ModelMap mp , Principal principal) {


		String NombreUsuarioLogeado = principal.getName();
		
		Usuarios usuario = usuarioService.buscarUsuario(NombreUsuarioLogeado);
		
		String Rol = usuario.getRol().get(0).getName();
		
		mp.put("Area", usuario.getArea().getArea());
		
		mp.put("rol", Rol);
		
		String notificacion = (String) model.asMap().get("notificacion");
		
		String idlote = (String) model.asMap().get("idlote");
		
		String NombreUsuario = principal.getName();
		
		Usuarios Usuario = usuarioservice.buscarUsuario(NombreUsuario);
		
		String nombreAnalista = Usuario.getNombre();
		
		System.out.println("Nombre de persona logeada " + nombreAnalista);

		List<Errores> listaErrores = (List<Errores>) model.asMap().get("listaErrores");
		
		List<ErroresBalance> listaErroresBalance = (List<ErroresBalance>) model.asMap().get("listaErroresBalance");
		
		Pageable pageRequest = PageRequest.of(page, 5);
		
		Page <LoteUsuario> lotes = loteUsuarioService.listaLotesUsuario(nombreAnalista, pageRequest);
		
		Paginador <LoteUsuario> pageRender = new Paginador<>("/lote" , lotes);
		
		//List<LoteUsuario> lotesUsuario = loteUsuarioService.MostrarLotesUsuario(nombreAnalista);
	
		LoteUsuario lote = new LoteUsuario();
		
		//System.out.println("El tamaño de la lista es de :  " + lotes.getSize()); 
		
		if (idlote != null) {
			
			LoteUsuario loteErrores = loteUsuarioService.buscarLoteId(idlote);
			
			mp.addAttribute("loteErrores", loteErrores);
		}
		
		mp.put("tipoLote", lote);
		mp.put("lote", lotes);
		mp.put("listaErrores", listaErrores);
		mp.put("listaErroresBalance", listaErroresBalance);
		mp.put("notificacion", notificacion);
		mp.put("page", pageRender);
		mp.put("idlote", idlote);
		model.addAttribute("cantidadMensa", mensajeService.ListaMisMensajes(NombreUsuarioLogeado).size());

		DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

		String date = dateFormat.format(Calendar.getInstance().getTime());
				
		String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ Guarda fecha contable automatico correctamente ]";
								
		registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);

		return "CargarLote";
	}
	
	
	@RequestMapping(value = "/EliminarLote/{id}", method = RequestMethod.GET)
	public String EliminarDebitoCredito(@PathVariable("id") String id, HttpServletRequest request, ModelMap mp , RedirectAttributes redirectAttributes , Principal principal) {

		
		
		List<ResumenLote> CuentasLote = resumenLoteService.mostrarResumen(id);
		
		LoteUsuario loteEliminar = loteUsuarioService.buscarLoteId(id);
		
		String rutaLoteEliminar = loteEliminar.getRutaArchivo();
		
		String NombreUsuarioLogeado = principal.getName();

		
		if (CuentasLote.isEmpty()) {
			
			loteUsuarioService.EliminarLote(id);

			notificacion = "El lote " + id + " ha sido eliminado correctamente";
			
			redirectAttributes.addFlashAttribute("notificacion",notificacion);
			
			File archive = new File (rutaLoteEliminar);

			DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

			String date = dateFormat.format(Calendar.getInstance().getTime());

			String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ Elimino el lote correctamente con el id : " + id + "]";

			registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);
			
			
			
	        if (archive.exists()){
	            
	        	//System.out.println("Archive in the Inbound folder");

	            //Elimina archivo
	            archive.delete();

	        }else {
	            //System.out.println("Waiting Trigger from IPS-I");
	            while ((archive.exists()) != true ){}
	        }
			
			return "redirect:/lote";
		}
		

   
		for (int i = 0 ; i < CuentasLote.size() ; i++) {
		
				long idResumenCuenta = CuentasLote.get(i).getId();

				resumenLoteService.EliminarCuenta(idResumenCuenta);
				
				DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

				String date = dateFormat.format(Calendar.getInstance().getTime());

				String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ Elimino el resumen de el lote de manera correcta ]";

				registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);

		}
		
		
		String rutaErrores = "..\\HistorialErrores\\" + id + ".txt";
		
		File archive = new File (rutaLoteEliminar);
		
		File archive2 = new File (rutaErrores);
		
        if (archive.exists()){
            //System.out.println("El archivo existe ");

            //Elimina archivo
            archive.delete();
            
            archive2.delete();
            

        }else {
            //System.out.println("El archivo No existe ");
            while ((archive.exists()) != true ){}
        }
		
		

		notificacion = "El lote " + id + " ha sido eliminado correctamente";
		
		redirectAttributes.addFlashAttribute("notificacion",notificacion);
		
		DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

		String date = dateFormat.format(Calendar.getInstance().getTime());

		String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ Elimino el lote correctamente con el id : " + id + "]";

		registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);
		
		return "redirect:/lote";
	}

	// validamos que el lote cumpla con la fecha de proceso que se establecio en
	// parametria

	@RequestMapping("/validarFechaProceso")
	public String ValidarTipoCarga(HttpServletRequest request, RedirectAttributes redirectAttributes , LoteUsuario lote , Principal principal , Model model) throws ParseException {
		
		String NombreUsuarioLogeado = principal.getName();
		
		DateFormat dateFormat1 = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

		String date1 = dateFormat1.format(Calendar.getInstance().getTime());
		
		String TipoLote = (String) model.asMap().get("TipoLote");
		
		String NombreArchivo = (String) model.asMap().get("NombreArchivo");
		
		String rutaArchivo  = (String) model.asMap().get("rutaArchivo");
		
		String IdLote = NombreArchivo.replaceAll("[lote.xlsx]", "");

		//System.out.println("El nombre del archivo es : " + NombreArchivo + " el id del archivo es " + IdLote);
		
		LoteUsuario LoteParaValidar = loteUsuarioService.buscarLoteId(IdLote);
		
		if (LoteParaValidar != null) {
			
			notificacion = "EL lote " + IdLote + "ya fue validado intenta con otro lote";
			
			redirectAttributes.addFlashAttribute("notificacion", notificacion);
			
			String tarea =  "Fecha Y hora [" + date1 + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ " + notificacion + "]";
			
			registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);
			
			File archive = new File (rutaArchivo);
			
	        if (archive.exists()){
	            //System.out.println("El archivo existe ");

	            //Elimina archivo
	            archive.delete();

	        }else {
	            //System.out.println("El archivo No existe ");
	            while ((archive.exists()) != true ){}
	        }
			
			return "redirect:/lote";
		}

		List<FechaControlContable> listafechascontables = fechacontableservice.buscarFechasActivas("Activo");
		
		List<LoteUsuario> listaLote = loteUsuarioService.MostrarLotesUsuario(NombreUsuarioLogeado);

		Date date = new Date();

		DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

		String horaEjecucion = dateFormat.format(date); // hora en la que se carga el lote para validar
		
		if (listafechascontables.isEmpty()) {
			
			notificacion = "No hay ninguna fecha de proceso registrada o habilitada por favor registre o habilite una fecha de proceso";
			
			redirectAttributes.addFlashAttribute("notificacion",notificacion);
			
			String tarea =  "Fecha Y hora [" + date1 + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ " + notificacion + "]";
			
			registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);
			
			File archive = new File (rutaArchivo);
			
	        if (archive.exists()){
	            //System.out.println("El archivo existe ");

	            //Elimina archivo
	            archive.delete();

	        }else {
	            //System.out.println("El archivo No existe ");
	            while ((archive.exists()) != true ){}
	        }
			
			return "redirect:/lote";
		}
		
		
		if (listaLote.isEmpty() == false) {
			
			for(int i = 0 ; i < listaLote.size() ; i++) {
				
				String EstadoLote = listaLote.get(i).getEstadoLote();
				
				if (EstadoLote.contains("Rechazado por parametro")) {
					
					notificacion = "Por favor elimine el lote " + IdLote + "para validar nuevamente";
					
					redirectAttributes.addFlashAttribute("notificacion",notificacion);
					
					String tarea =  "Fecha Y hora [" + date1 + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ " + notificacion + "]";
					
					registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);
					
					File archive = new File (rutaArchivo);
					
			        if (archive.exists()){
			            //System.out.println("El archivo existe ");

			            //Elimina archivo
			            archive.delete();

			        }else {
			            //System.out.println("El archivo No existe ");
			            while ((archive.exists()) != true ){}
			        }
					
					return "redirect:/lote";
					
					
				}
			}
			
		
		}

		String HoraMaximaEjecucion = listafechascontables.get(0).getHoraMaxima(); // hora maxima de validar lote

		String string = horaEjecucion; // 10:36:25 // hh:mm:ss
		String[] parts = string.split(":");
		String HoraLoteEjecucion = parts[0]; // 10
		String MinutosLoteEjecucion = parts[1]; // 36

		String Hora = HoraMaximaEjecucion; // 10:36 // hh:mm
		String[] parts2 = Hora.split(":");
		String HoraMaximaEje = parts2[0]; // 10
		String MinutosLoteEjecucionMaxi = parts2[1]; // 36

		int HoraEjecucionDb = Integer.parseInt(HoraMaximaEje);

		int HoraEjecucionExcel = Integer.parseInt(HoraLoteEjecucion);

		int MinutosEjecucionDb = Integer.parseInt(MinutosLoteEjecucionMaxi);

		int MinutosLoteEjecucionExcel = Integer.parseInt(MinutosLoteEjecucion);

		//System.out.println("Hora de ejecucion del lote es  : " + HoraEjecucionExcel + ":" + MinutosLoteEjecucionMaxi + " y la hora maxima es : " + HoraEjecucionDb + ":" + MinutosEjecucionDb);

		if (HoraEjecucionExcel <= HoraEjecucionDb && MinutosLoteEjecucionExcel <= MinutosEjecucionDb || HoraEjecucionExcel < HoraEjecucionDb && MinutosLoteEjecucionExcel >= MinutosEjecucionDb) {

				

				redirectAttributes.addFlashAttribute("TipoLote", TipoLote);
				redirectAttributes.addFlashAttribute("IdLote", IdLote);
				redirectAttributes.addFlashAttribute("NombreArchivo", NombreArchivo);
				
				String tarea =  "Fecha Y hora [" + date1 + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ " + "el usuario puede cargar lotes asi que paso el lote a la siguiente etapa de validacion" + "]";
				
				registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);

				return "redirect:/validarlote";
				
				
				//return "redirect:/valdiarOptimizado";

		}
		
		else  {
			
			//System.out.println("la ruta del archivo es " + rutaArchivo);
			
			File archive = new File (rutaArchivo);
			
	        if (archive.exists()){
	            //System.out.println("El archivo existe ");

	            //Elimina archivo
	            archive.delete();

	        }else {
	            //System.out.println("El archivo No existe ");
	            while ((archive.exists()) != true ){}
	        }

			notificacion = "Ya no puedes cargar más lotes la fecha máxima de proceso es a las " + HoraMaximaEjecucion;

			redirectAttributes.addFlashAttribute("notificacion", notificacion);

			//System.out.print("Ya no puedes cargar mas lotes la fecha maxima de proceso es a las " + HoraMaximaEjecucion);
			
			String tarea =  "Fecha Y hora [" + date1 + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ " + notificacion + "]";
			
			registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);
			
			return "redirect:/lote";

		}

		

	}
	
	@GetMapping("/valdiarOptimizado")
	public String validarOptimizado(RedirectAttributes redirectAttributes, Model model , LoteUsuario lote , Principal principal) {
		
		ValidarDatosParametria validarParametria = new ValidarDatosParametria(); 
		
		CrearResumenLote crearResumen = new CrearResumenLote();
		
		int ContadorErrores = 0;
		
		List<Errores> listaErrores = new ArrayList<Errores>();
		
		 long inicio = System.currentTimeMillis();
		
		String NombreArchivo = (String) model.asMap().get("NombreArchivo");
		
		LocalDate Fechacargue = LocalDate.now();
		
		String rutaArchivoExcel = "..\\Lotes\\" + "lote" + Fechacargue + "\\" + NombreArchivo;
		
		String IdLote = (String) model.asMap().get("IdLote");
		
		String TipoLote = (String) model.asMap().get("TipoLote");

		Lote datoslote = new Lote();
		
		lote.setEstadoLote("Activo");
		
		lote.setTipoLote(TipoLote);
		
		lote.setIdLote(IdLote);
		
		lote.setNombreArchivo(NombreArchivo);

		lote.setFechaProceso(Fechacargue); // guardo la fecha de proceso del lote para la tabla en db
		
		//lote.setAnalista(NombreAnalista);
		
		int ContadorRegistros = 0 ;
		
		double contadorPesos = 0 ;
		
		double contadorCredito = 0;
		
		double contadorDebito = 0 ;
		
		double ContadorCreditoDivisa = 0 ;
		
		double ContadorDebitoDivisa = 0;

		IOUtils.setByteArrayMaxOverride(1000000000);
		
		try {

            FileInputStream inputStream = new FileInputStream(new File(rutaArchivoExcel));
            
            Workbook workbook = new XSSFWorkbook(inputStream);
            
            Sheet firstSheet = workbook.getSheetAt(0);
            
            Iterator<Row> iterator = firstSheet.iterator();
            
            DataFormatter formatter = new DataFormatter();
            
            while (iterator.hasNext()) {
            	
            	Row nextRow = iterator.next();
            	
            	int numeroFila  =nextRow.getRowNum();
   
            	Iterator<Cell> cellIterator = nextRow.cellIterator();
                
            	while(cellIterator.hasNext()) {

            		Cell cell = cellIterator.next();
            		
            		if (numeroFila!= 0) {

            			int Numerocolumna = cell.getColumnIndex();
            			
            			//System.out.println("EL NUMERO DE COLUMNA ES : " + Numerocolumna);
            			
            			if(Numerocolumna == 0) { // FUENTE REQUIERE PARAMETRIA 
            				
            					String fuente = formatter.formatCellValue(cell);
            					
            					//System.out.println("FUENTE " + fuente);
								
								if (fuenteservice.BuscarFuente(fuente) == null) {
									
									ContadorErrores = ContadorErrores + 1;

									Errores error = new Errores();

									error.setDatoErroneo(fuente);
									error.setDescripError("la fuente " + fuente + " no Existe en parametria");
									error.setNumeroError(ContadorErrores);
									error.setTipoError("Parametrico");
									error.setPosicionError("[ A" + " : " + numeroFila + "]");

									listaErrores.add(error);
									
								}

								
							}
            			
            				if(Numerocolumna == 2) { // REQUIERE PARAMETRIA NO CUENTA
            					       					
            					String Cuenta = formatter.formatCellValue(cell);
            					
            					crearResumen.resumenLote(Cuenta, numeroFila, Numerocolumna, IdLote);
            					
            					//System.out.println("Numero de cuenta " + Cuenta);

            					if (cuentasservice.TraerCuentaExcep(Cuenta) == null) {
									
									ContadorErrores = ContadorErrores + 1;

									Errores error = new Errores();

									error.setDatoErroneo(Cuenta);
									error.setDescripError("la fuente " + Cuenta + " no Existe en parametria");
									error.setNumeroError(ContadorErrores);
									error.setTipoError("Parametrico");
									error.setPosicionError("[ A" + " : " + numeroFila + "]");

									listaErrores.add(error);
									
								}
            					
            					
            				}
            				
            				if (Numerocolumna == 3) { // COLUMNA SUCURSAL  ( REQUIERE PARAMETRIA )
            					
            					String Sucursal = formatter.formatCellValue(cell);
            					
            					//System.out.println("SUCURSAL : " + Sucursal);
            					
            					if (sucursalService.TraerSucursal(Integer.parseInt(Sucursal)) == null) {
									
									ContadorErrores = ContadorErrores + 1;

									Errores error = new Errores();

									error.setDatoErroneo(Sucursal);
									error.setDescripError("la sucursal " + Sucursal + " no Existe en parametria");
									error.setNumeroError(ContadorErrores);
									error.setTipoError("Parametrico");
									error.setPosicionError("[ A" + " : " + numeroFila + "]");

									listaErrores.add(error);
									
								}
            					
            					
	
            					
            				}
            				
            				if (Numerocolumna == 4) { // PARAMETRIA CENTROD DE COSTOS
            					
            					String centroCostos = formatter.formatCellValue(cell);
            					
            					//System.out.println("CENTRO DE COSTOS : " + centroCostos);
            					
            					if (serviceCentroCostos.CentroCostosBuscar(Integer.parseInt(centroCostos)) == null) {
									
									ContadorErrores = ContadorErrores + 1;

									Errores error = new Errores();

									error.setDatoErroneo(centroCostos);
									error.setDescripError("la sucursal " + centroCostos + " no Existe en parametria");
									error.setNumeroError(ContadorErrores);
									error.setTipoError("Parametrico");
									error.setPosicionError("[ A" + " : " + numeroFila + "]");

									listaErrores.add(error);
									
								}
            				
            				}
            				
            				if (Numerocolumna == 6) {// PARAMETRO MONEDA
            					
            					String TipoMoneda = formatter.formatCellValue(cell);
            					
            					if(monedaService.buscarMoneda(Integer.parseInt(TipoMoneda)) == null){
            						
            						ContadorErrores = ContadorErrores + 1;

									Errores error = new Errores();

									error.setDatoErroneo(TipoMoneda);
									error.setDescripError("la sucursal " + TipoMoneda + " no Existe en parametria");
									error.setNumeroError(ContadorErrores);
									error.setTipoError("Parametrico");
									error.setPosicionError("[ A" + " : " + numeroFila + "]");

									listaErrores.add(error);
            						
            						
            					}
            					
            					
            				}
            				
            				if (Numerocolumna == 8) { //PARAMETRO D/C
            					
            					String debitoCredito = formatter.formatCellValue(cell);
            					
            					if(debitoCreditoService.traerDebitoCredito(Integer.parseInt(debitoCredito)) == null){
            						
            						ContadorErrores = ContadorErrores + 1;

									Errores error = new Errores();

									error.setDatoErroneo(debitoCredito);
									error.setDescripError("la sucursal " + debitoCredito + " no Existe en parametria");
									error.setNumeroError(ContadorErrores);
									error.setTipoError("Parametrico");
									error.setPosicionError("[ A" + " : " + numeroFila + "]");

									listaErrores.add(error);
            						
            						
            					}
            					
            				}
            			
            				
       

            			
            		}else {
            			
            			//System.out.println("Cabezera del archivo");
            			
            		}
            		                  
            		
                }
                
            }
        } catch (Exception e) {
            
        	e.printStackTrace();
        }
		
		redirectAttributes.addFlashAttribute(notificacion, "el proceso a terminado");
		
		long fin = System.currentTimeMillis();
        
        double tiempo = (double) ((fin - inicio)/1000);
         
        System.out.println(tiempo +" segundos");
		
		return "redirect:/lote";
	}
	

	// controlador para validar los campos del lote y comparalos con lo datos
	// parametricos
	@GetMapping("/validarlote")
	public String ValidarCamposLote(Model model, HttpServletResponse response, RedirectAttributes redirectAttributes, LoteUsuario lote , Principal principal) {

		// DEPENDIENDO EL AREA EXTRAIGO LA FECHA DE CARGUE MENSUAL PARAMETRIZADA ASI
		// COMO LA FECHA DE CARGUE DIARIO MAXIMO
		
		long inicio = System.currentTimeMillis();
		
		long fin = System.currentTimeMillis();
		
		DateFormat dateFormat1 = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

		String date1 = dateFormat1.format(Calendar.getInstance().getTime());
		
		String NombreUsuarioLogeado = principal.getName();
		
		Usuarios usuario = usuarioservice.buscarUsuario(NombreUsuarioLogeado);
		
		String NombreAnalista = usuario.getNombre();
		
		String CuentaActual ;
		
		//List<LoteUsuario> loteCargado = loteUsuarioService.listaLoteUsuario();
		
		String NombreArchivo = (String) model.asMap().get("NombreArchivo");
		
		String IdLote = (String) model.asMap().get("IdLote");
		
		String TipoLote = (String) model.asMap().get("TipoLote");

		LocalDate Fechacargue = LocalDate.now();
		
		Lote datoslote = new Lote();
		
		GuardarErroresLote guardarErrores = new GuardarErroresLote();
		
		lote.setEstadoLote("Activo");
		
		lote.setTipoLote(TipoLote);
		
		lote.setIdLote(IdLote);
		
		lote.setNombreArchivo(NombreArchivo);

		lote.setFechaProceso(Fechacargue); // guardo la fecha de proceso del lote para la tabla en db
		
		lote.setAnalista(NombreAnalista);
		
		int ContadorErrores = 0;
		
		int ContadorRegistros = 0 ;
		
		double contadorPesos = 0 ;
		
		double contadorCredito = 0;
		
		double contadorDebito = 0 ;
		
		double ContadorCreditoDivisa = 0 ;
		
		double ContadorDebitoDivisa = 0;
		
		// C:\Users\andnei662\eclipse-workspace\Lotes\lote2023-05-04\lote100120230504.xlsx

		// Creamos el ArrayList
		List<Errores> listaErrores = new ArrayList<Errores>();
		
		ValorDivisa valordivisa = new ValorDivisa();

		String ruta = "..\\Lotes\\" + "lote" + Fechacargue + "\\" + NombreArchivo;
		
		//System.out.println("la ruta del archivo es :" + ruta);
		
		String tarea =  "Fecha Y hora [" + date1 + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ la ruta del lote a validar es : " + ruta + "]";
		
		registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);

		//String ruta = "C:\\Users\\andnei662\\eclipse-workspace\\lote" + Fechacargue + "\\" + NombreArchivo;

		FileInputStream file;
		
		IOUtils.setByteArrayMaxOverride(1000000000);

		try {

			file = new FileInputStream(new File(ruta));
			Workbook workbook = new XSSFWorkbook(file);

			org.apache.poi.ss.usermodel.Sheet sheet = workbook.getSheetAt(0);
			ZipSecureFile.setMinInflateRatio(0);

			
			
			for (Row row : sheet) {

				int contador = 0;
				
				long ValorDivisa = 0 ;
				
				//System.out.println( "la ultima columna es : " + row.getLastCellNum());

				//System.out.println("Numero de fila: " + row.getRowNum());

				String NumeroFila = String.valueOf(row.getRowNum() + 1);

				//System.out.println("");
				
				ResumenLote resumenLote = new ResumenLote();
				
				Moneda moneda = new Moneda();
				
				

				if (row.getRowNum() == 0) {

					//System.out.println("saltar fila ya que es la cabezera del archivo ");
					//System.out.println("");

				} else {

					for (Cell cell : row) {

						if (cell.getCellType() != CellType.BLANK) {
							
							ContadorRegistros = row.getRowNum() + 1;
						}
		
						contador = contador + 1;
						
						
						//System.out.println("la columna es : " + contador);

						switch (cell.getCellType()) {
						
						

						case STRING:

							if (contador == 1) { // columna fuente (tiene parametria )

								String fuente = cell.getRichStringCellValue().getString();

								//System.out.print("columna Fuente :" + fuente);
								//System.out.println("");
								
								boolean CumpleEstructura = true ;
								
								int LongitudFuente = fuente.length();
								
								if (LongitudFuente > 3) {
									
									CumpleEstructura = false ;
									
								}

								if (CumpleEstructura == false) {

									//System.out.println("la fuente " + fuente + " no cumple la estrutura");

									Errores error = new Errores();

									ContadorErrores = ContadorErrores + 1;

									error.setDatoErroneo(fuente);
									error.setDescripError("la fuente " + fuente + " no cumple la estrutura");
									error.setNumeroError(ContadorErrores);
									error.setTipoError("Estructura");
									error.setPosicionError("[ A" + " : " + NumeroFila + "]");

									listaErrores.add(error);

									// Agregamos un elemento a la lista

								}

								List<Fuente> FuentesActivas = fuenteservice.listaFuentesActivas("Activo"); // primer valido que el parametro se va a validar 
								
								if (FuentesActivas.isEmpty()) {
									
									//System.out.println("el parametro FUENTES no se tendra en cuenta para validacion");
								
								}else {
									
									
									List<Fuente> fuenteExiste = fuenteservice.encontrarFuente(fuente); // valido que la fuente exista 
									
									if (fuenteExiste.isEmpty()) {
										
										ContadorErrores = ContadorErrores + 1;

										Errores error = new Errores();

										error.setDatoErroneo(fuente);
										error.setDescripError("la fuente " + fuente + " no Existe en parametria");
										error.setNumeroError(ContadorErrores);
										error.setTipoError("Parametrico");
										error.setPosicionError("[ A" + " : " + NumeroFila + "]");

										listaErrores.add(error);
										
									}

									
								}
	
								
								// System.out.println("la fuente : " + fuente + " es correcta"); */

							}

							if (contador == 14) { // COLUMNA FACTURA (No tiene parametria)

								String Factura = cell.getRichStringCellValue().getString();
								
								//System.out.println("el valor de la factura es de : " + Factura);


							}

							if (contador == 15) { // COLUMNA REFERENCIA (No tiene parametria)

								String Referencia = cell.getRichStringCellValue().getString();

								//System.out.println("el valor referencia es de : " + Referencia);

							}

							break;
						case NUMERIC:

							if (contador == 2) { // COLUMNA CONSECUTIVO

								double dato1 = cell.getNumericCellValue();
								//System.out.print("columna consecutivo:");
								datoslote.EliminarPuntos(dato1);
								//System.out.println("");

							}
							
							

							if (contador == 3) { // COLUMNA NUMERO DE CUENTA (REQUIERE PARAMETRIA)
	
								double Nocuenta = cell.getNumericCellValue();

								String NumeroCuenta = datoslote.LimpiarNumeroCuenta(Nocuenta);
		
								//System.out.println("numero de cuenta " + NumeroCuenta);
								
								List<NumeroCuenta>  ParametroActivo = serviceCuenta.cuentasActivadas("Activo");
								
								if (ParametroActivo.isEmpty()) {
									
									//System.out.println("El parametro Cuentas esta desabilitado");
									
								}
								
								else {
									
									boolean CumpleEstrutura = ValidarEscruturaLote.validarEstructuraCuenta(NumeroCuenta);

									if (CumpleEstrutura == false) {

										ContadorErrores = ContadorErrores + 1;

										Errores error = new Errores();

										error.setDatoErroneo(NumeroCuenta);
										error.setDescripError("El" + NumeroCuenta + "no se encuentra en el sistema");
										error.setNumeroError(ContadorErrores);
										error.setTipoError("Estructura");
										error.setPosicionError("[ C" + " : " + NumeroFila + "]");

										listaErrores.add(error);

									}
									
									List<NumeroCuenta> Cuenta = serviceCuenta.BuscarCuenta(NumeroCuenta);
									
									//List<NumeroCuenta> Cuenta = serviceCuenta.listaCuentasHabilitadas("Activo", NumeroCuenta);

									if (Cuenta.isEmpty()) {

										ContadorErrores = ContadorErrores + 1;

										Errores error = new Errores();

										error.setDatoErroneo(NumeroCuenta);
										error.setDescripError("La cuenta : " + NumeroCuenta + " No existe en parametria");
										error.setNumeroError(ContadorErrores);
										error.setTipoError("parametria");
										error.setPosicionError("[ C" + " : " + NumeroFila + "]");

										listaErrores.add(error);

									}
									
								}

					
	
								ResumenLote ResumenCuenta = resumenLoteService.TraerCuentaResumen(NumeroCuenta, IdLote); // valdiar si existe el objeto

								resumenLote.setNoCuenta(NumeroCuenta); // variable temporal
								
								CuentaActual = resumenLote.getNoCuenta();
								
								//System.out.println("numero de cuenta " + CuentaActual);
								
								if (ResumenCuenta == null) {
									
									//System.out.println("numero de cuenta " + NumeroCuenta);
									
									resumenLote.setLote(lote);
		
									resumenLoteService.guardarResumenCuenta(resumenLote);
									
								}
								
							}
							

							if (contador == 4) { // COLUMNA SUCURSAL (REQUIERE PARAMETRIA)

								double dato1 = cell.getNumericCellValue();
								
								
								int Sucursal = datoslote.LimpiarDatoNumerico(dato1);
								
								//System.out.println("columna sucursal:" + Sucursal);
								
								List<Sucursal> listaSucursalesActivas = sucursalService.listaSucursalesActivas("Activo");
								
								if (listaSucursalesActivas.isEmpty()) {
									
									//System.out.println("Las sucursales no estan siendo validadas en estos momentos");
									
								}
								else {
									
									List<Sucursal> SucursalRegistrada = sucursalService.BuscarSucursal(Sucursal);

									if (SucursalRegistrada.isEmpty()) {

										ContadorErrores = ContadorErrores + 1;

										String SucursalErronea = String.valueOf(Sucursal);

										Errores error = new Errores();

										error.setDatoErroneo(SucursalErronea);
										error.setDescripError("La sucursal : " + Sucursal + " No existe en parametria");
										error.setNumeroError(ContadorErrores);
										error.setTipoError("Estructura");
										error.setPosicionError("[ D" + " : " + NumeroFila + "]");

										listaErrores.add(error);

									}
									
								}

							

							}

							if (contador == 5) { // COLUMNA CENTRO DE COSTOS (REQUIERE PARAMETRIA)

								double dato1 = cell.getNumericCellValue();
								
								int CentroCostos = (int) dato1;

								//System.out.println("columna centroCostos: " + CentroCostos);
								
								List<CentroCostos> listaCentroCostos = serviceCentroCostos.listaCentrocostosActivos("Activo");
								
								if (listaCentroCostos.isEmpty()) {
									
									//System.out.println("este parametro no se esta validando");
									
								}
								
								else {
									
									List<CentroCostos> CentroCostosParametria = serviceCentroCostos.BuscarCentroCostos(CentroCostos);
									
									//List<CentroCostos> CentroCostosParametria = serviceCentroCostos.listaCentroCostosHabilitados("Activo",CentroCostos);

									if (CentroCostosParametria.isEmpty()) {

										//System.out.println("Centro de costos " + CentroCostos + "No existe en parametria"); // almacenar
																															// en
										ContadorErrores = ContadorErrores + 1;

										String CentroCostosErroneo = String.valueOf(CentroCostos);

										Errores error = new Errores();

										error.setDatoErroneo(CentroCostosErroneo);
										error.setDescripError("Centro de costos : " + CentroCostosErroneo + " No existe en parametria");
										error.setNumeroError(ContadorErrores);
										error.setTipoError("parametria");
										error.setPosicionError("[ E" + " : " + NumeroFila + "]");

										listaErrores.add(error);

									}
									
								}
								
								


							}

							if (contador == 6) { // COLUMNA PESOS (SUMAR EL TOTAL DEL LOTE )

								
								double ValorPesos =  cell.getNumericCellValue();

								lote.setPesos(ValorPesos);

								contadorPesos = contadorPesos + ValorPesos;
								
								//System.out.println("columna valorPesos: " + ValorPesos);
								
								//System.out.println("");

							}
							
							if (contador == 7) { // COLUMNA MODENA (REQUIERE PARAMETRIA)

								
								double dato1 = cell.getNumericCellValue();

								int Moneda = (int) dato1;
								
								moneda.setMoneda(Moneda);

								//System.out.println("columna Moneda:" + Moneda);

								List<Moneda> listaMonedasActivas = monedaService.listaMonedasActivas("Activo");
								
								if (listaMonedasActivas.isEmpty()) {
									
									
									//System.out.println("Este parametro no se esat validando moneda");
									
								}else {
									
									List<Moneda> ListaMonedas = monedaService.BuscarMoneda(Moneda);
									
									//List<Moneda> ListaMonedas = monedaService.listaMonedaHabilitadas("Activo",Moneda);


									if (ListaMonedas.isEmpty()) {

										ContadorErrores = ContadorErrores + 1;

										String MonedaErronea = String.valueOf(Moneda);

										Errores error = new Errores();

										error.setDatoErroneo(MonedaErronea);
										error.setDescripError("El valor de moneda : " + MonedaErronea + " No existe en parametria");
										error.setNumeroError(ContadorErrores);
										error.setTipoError("Estructura");
										error.setPosicionError("[ G" + " : " + NumeroFila + "]");

										listaErrores.add(error);
									}
									
								}
								

							}

							if (contador == 8) { // COLUMNA VALOR EN DIVISAS (FATA MODULO DIVISAS PARA VALIDAR COLUMNA )

								// metodo personalizado

								long Divisa = (long) cell.getNumericCellValue();
								
								ValorDivisa = Divisa;
								
								//System.out.println("valor de la columna divisa " + Divisa);
								
								//System.out.println("el valor del la columna peso es :" + lote.getPesos());
								
								//System.out.println("el valor de la columna moneda es de  " + moneda.getMoneda());
								
								if (moneda.getMoneda() == 0 && Divisa == 0) {
									
									//System.out.println("el valor de la columna esta bien ");
									
								}
								
								if (moneda.getMoneda() == 0 && Divisa != 0) {
									
									ContadorErrores = ContadorErrores + 1;

									String valorCeldaDivisa = String.valueOf(Divisa);

									Errores error = new Errores();

									error.setDatoErroneo(valorCeldaDivisa);
									error.setDescripError("El valor del divisa : " + valorCeldaDivisa + "Tiene valor");
									error.setNumeroError(ContadorErrores);
									error.setTipoError("Estructura");
									error.setPosicionError("[ H" + " : " + NumeroFila + "]");

									listaErrores.add(error);
								}
								
								if(moneda.getMoneda() == 1 && Divisa != 0) {
									
									double valorPeso = lote.getPesos();
									
									TRM TRMDB = trmService.buscarTrmActiva("Activo");
									
									if(TRMDB == null) {
										
										//System.out.println("No se esta validando TRM en este momento ");
									}
									
									else {
										
										double TRMexcel = valorPeso / Divisa;
										
										DecimalFormat formatea = new DecimalFormat("###.###");
										
										String resultado = formatea.format(TRMexcel);

										//System.out.println("el resultado de la TRM de la celda es : " + resultado);
										
										if ( TRMexcel <=  TRMDB.getValortecho() && TRMexcel >= TRMDB.getValorpiso()) {
											
											//System.out.println("cumple");
											
										}else {
											
											ContadorErrores = ContadorErrores + 1;

											String valorCeldaDivisa = String.valueOf(Divisa);

											Errores error = new Errores();

											error.setDatoErroneo(resultado);
											error.setDescripError("El valor del divisa : " + resultado + " Tiene valor no cumple con el rango TRM");
											error.setNumeroError(ContadorErrores);
											error.setTipoError("Estructura");
											error.setPosicionError("[ H" + " : " + NumeroFila + "]");

											listaErrores.add(error);
											
										}
										
										
									}
									
									
									
								}


							}

							if (contador == 9) { // COLUMNA CREDITO / DEBITO (requiere parametria)

								CuentaActual = resumenLote.getNoCuenta();
								
								double ValorPesos = lote.getPesos();

								double dato1 = cell.getNumericCellValue();

								int DebitoCredito = (int) dato1;

								//System.out.println("el valor de la celda debito/credito es :"  + DebitoCredito);
								
								List<DebitoCredito> litaDebitoCredito = debitoCreditoService.buscarDebitoCredito(DebitoCredito);
								
								//List<DebitoCredito> litaDebitoCredito = debitoCreditoService.buscarDebitoCredito(DebitoCredito);
								
								if (litaDebitoCredito.isEmpty()) {
									
									ContadorErrores = ContadorErrores + 1;

									String DebitoCreditoError = String.valueOf(DebitoCredito);

									Errores error = new Errores();

									error.setDatoErroneo(DebitoCreditoError);
									error.setDescripError("El valor del debito/credito : " + DebitoCreditoError + "No existe en parametria");
									error.setNumeroError(ContadorErrores);
									error.setTipoError("Estructura");
									error.setPosicionError("[ I" + " : " + NumeroFila + "]");

									listaErrores.add(error);
									
									
								}
								
								ResumenLote ResumenCuenta = resumenLoteService.TraerCuentaResumen(CuentaActual, IdLote); // valdiar si existe el objeto

								DecimalFormat formatea = new DecimalFormat("###,###.##");
								
								
								if (DebitoCredito == 1) {
									
									ContadorDebitoDivisa = ContadorDebitoDivisa + ValorDivisa;
									
									String ValorDebitoDivisa = formatea.format(ContadorDebitoDivisa);
									
									//System.out.println("Valor Divisa debito " + ValorDebitoDivisa);
									
									valordivisa.setValorDebitoDivisa(ValorDebitoDivisa);
									
									valordivisa.setValorDebitoDivisaNumero(ContadorDebitoDivisa);
									
									contadorDebito = contadorDebito + ValorPesos;

									//System.out.println("Valor debito es " + contadorDebito);
	
			
									if (ResumenCuenta != null ) {
										
										double valorDebito = ResumenCuenta.getDebito();
										
										valorDebito = valorDebito + ValorPesos;

										String ValorDebitoCuenta = formatea.format(valorDebito);
										
										//System.out.println("Valor debito es " + ValorDebitoCuenta);
										
										ResumenCuenta.setDebito(valorDebito);
			
										ResumenCuenta.setResumenDebito(ValorDebitoCuenta);

										resumenLoteService.guardarResumenCuenta(ResumenCuenta);
										
									}
									
								}

								if (DebitoCredito == 2) {
									
									
									ContadorCreditoDivisa = ContadorCreditoDivisa + ValorDivisa;
									
									String ValorCreditoDivisa = formatea.format(ContadorCreditoDivisa);
									
									//System.out.println("valor Debito Divisa " + ValorCreditoDivisa);
									
									valordivisa.setValorcreditoDivisa(ValorCreditoDivisa);
									
									valordivisa.setValorCreditoDivisaNumero(ContadorCreditoDivisa);
									
									contadorCredito = contadorCredito + ValorPesos;
									
									//String ValorDebitolote = formatea.format(contadorCredito);
									
									//System.out.println("valor credito " + ValorDebitolote);
									
									if (ResumenCuenta != null ) {

										double valorCredito = ResumenCuenta.getCredito();
										
										valorCredito = valorCredito + ValorPesos;
	
										String ValorCreditoCuenta = formatea.format(valorCredito);
										
										//System.out.println("El valor del credito cuenta " + valorCredito + resumenLote.getNoCuenta());
										
										ResumenCuenta.setCredito(valorCredito);
			
										ResumenCuenta.setResumenCredito(ValorCreditoCuenta);

										resumenLoteService.guardarResumenCuenta(ResumenCuenta);
										
									}
								}
								
								
								double totalCuenta = ResumenCuenta.getDebito() - ResumenCuenta.getCredito();
								
								String ValorCreditoCuenta = formatea.format(totalCuenta);
								
								ResumenCuenta.setTotalGeneral(ValorCreditoCuenta);
								ResumenCuenta.setValorTotal(totalCuenta);
								
								ResumenCuenta.setLote(lote);
				
								resumenLoteService.guardarResumenCuenta(ResumenCuenta);
	

							}

							if (contador == 11) { // COLUMNA FECHA DE PROCESO (REQUIERE PARAMETRIA)

								// metodo personalizado
								
								
				
								int MesActual = Fechacargue.getMonthValue();
								
								double dato1 = cell.getNumericCellValue();
								
								String fecha = datoslote.limpiarErroresE(dato1); // limpio la fecha 
								
								String FechaEstrcuturada = datoslote.EstructurarFecha(fecha); // escruturo la fecha
								
								//System.out.println("columna fecha Proceso: " + fecha);
								
								Date FechaProceso = datoslote.EstablecerFormatoFechaProcesoLote(FechaEstrcuturada); // doy formato dd/mm/yyyy
			
								List<FechaControlContable> fechaProceso = fechacontableservice.buscarFechasActivas("Activo");
								
								int DiasProceso = fechaProceso.get(0).getDias();
								
								Date FechaInicial  = fechaProceso.get(0).getFecha_Inicial();
								
								Date FechaFinal = fechaProceso.get(0).getFecha_final();
								
								Date FechaCierre = fechaProceso.get(0).getFecha_Cierre();
								
								
								
								int diaProcesoLote = FechaProceso.getDate();
								
								int mesFechaProcesoLote = FechaProceso.getMonth() + 1;

								//System.out.println("MES FECHA PROCESO " + mesFechaProcesoLote);
								
								if (DiasProceso == 0 && FechaCierre == null && TipoLote.contains("Lote Diario")) {

									//System.out.println("la fecha de proceso es manual");
									
									//System.out.println("fecha de proceso Inicial " + FechaInicial);
									
									//System.out.println("fecha de proceso final " + FechaFinal);
									
									
	
									boolean resultado = datoslote.validarfechaLoteDiarioManual(FechaProceso,FechaInicial ,FechaFinal);
									
									//System.out.print(resultado);

									if (resultado == false) {
						
										ContadorErrores = ContadorErrores + 1;

										Errores error = new Errores();
										
										String FechaErronea = FechaEstrcuturada.replace("/", "");

										error.setDatoErroneo(FechaErronea);
										error.setDescripError("La fecha  : " + FechaErronea + " No cumple con el rango de fechas en parametria");
										error.setNumeroError(ContadorErrores);
										error.setTipoError("Estructura");
										error.setPosicionError("[ k" + " : " + NumeroFila + "]");

										listaErrores.add(error);
									
									}
									
									
								}
								
								if (DiasProceso > 0 && TipoLote.contains("Lote Diario")) {
		
									//System.out.println("la fecha de proceso es Automatica");
									
									//System.out.println("Dias de cargue: " + DiasProceso);
									
									boolean FechaProcesoCorrecta  = datoslote.validarfechaLoteDiarioAuto(DiasProceso , diaProcesoLote , mesFechaProcesoLote);
									
									//System.out.print(FechaProcesoCorrecta);

									if (FechaProcesoCorrecta == false) {
						
										ContadorErrores = ContadorErrores + 1;

										Errores error = new Errores();
										
										String FechaErronea = FechaEstrcuturada.replace("/", "");

										error.setDatoErroneo(FechaErronea);
										error.setDescripError("La fecha  : " + FechaErronea + " No cumple con el rango de fechas en parametria");
										error.setNumeroError(ContadorErrores);
										error.setTipoError("Estructura");
										error.setPosicionError("[ k" + " : " + NumeroFila + "]");

										listaErrores.add(error);
									
									}
								}
								
								if (FechaCierre != null) { // fecha de proceso mensual es la fecha de cierre
									
									if (TipoLote.contains("Lote Diario")) {
										
										//System.out.println("la fecha de proceso es cierre mensual");
										
										//System.out.println("fecha de proceso Inicial " + FechaInicial);
										
										//System.out.println("fecha de proceso Final " + FechaFinal);
										
										Date FechaInicialDiario = FechaInicial;
										
										Date FechaFinalDiario = FechaFinal;
										
										boolean FechaProcesoCorrecta = datoslote.ValidarFechaLoteDiarioCierre(FechaInicialDiario , FechaFinalDiario , FechaProceso);
										
										if (FechaProcesoCorrecta == false) {
											
											ContadorErrores = ContadorErrores + 1;

											Errores error = new Errores();
											
											String FechaErronea = FechaEstrcuturada.replace("/", "");

											error.setDatoErroneo(FechaErronea);
											error.setDescripError("La fecha  : " + FechaErronea + " No cumple con el rango de fechas en parametria");
											error.setNumeroError(ContadorErrores);
											error.setTipoError("Estructura");
											error.setPosicionError("[ k" + " : " + NumeroFila + "]");

											listaErrores.add(error);
										
										}
									}
									
									if(TipoLote.contains("Extenporaneo")) {
										
										//System.out.println("a este lote no se aplica filtros de fecha");
									}
									
									else {
										
										//System.out.println("fecha de proceso cierre " + FechaCierre);
										
										Date FechaFinalMensual = FechaCierre;
										
										boolean FechaProcesoCorrecta = datoslote.ValidarFechaLoteMensualCierre(FechaFinalMensual , FechaProceso);
										
										if (FechaProcesoCorrecta == false) {
											
											ContadorErrores = ContadorErrores + 1;

											Errores error = new Errores();
											
											String FechaErronea = FechaEstrcuturada.replace("/", "");


											error.setDatoErroneo(FechaErronea);
											error.setDescripError("La fecha  : " + FechaErronea + " No cumple con el rango de fechas en parametria");
											error.setNumeroError(ContadorErrores);
											error.setTipoError("Estructura");
											error.setPosicionError("[ k" + " : " + NumeroFila + "]");

											listaErrores.add(error);
										
										}
									}
				
								}

								
							}
							
							if (contador == 10) { // COLUMNA FECHA EFECTIVA

								double dato1 = cell.getNumericCellValue();
								
								String fecha = datoslote.limpiarErroresE(dato1); // limpio la fecha 
								
								String FechaEstrcuturada = datoslote.EstructurarFecha(fecha);

								//System.out.print("columna fecha efectiva " + FechaEstrcuturada);
								
								//System.out.println("");

							}

							if (contador == 12) { // COLUMNA TIPO DE IDENTIFICACION

								double dato1 = cell.getNumericCellValue();

								int TipoIdentificacion = (int) dato1;
								
								//System.out.print("columna TipoId: " + TipoIdentificacion);
								
								//System.out.println("");

							}

							if (contador == 13) { // COLUMNA NIT

								long dato1 = (long) cell.getNumericCellValue();
						
								//System.out.print("columna Nit:" + dato1);
								//System.out.println("");

							}

					

						case BLANK:
							
							/*System.out.println("la columna " + contador + " fila " + NumeroFila);
							
							String PosicionError = "la columna " + contador + " fila " + NumeroFila;
							
							ContadorErrores = ContadorErrores + 1;

							Errores error = new Errores();
	
							error.setDatoErroneo("campo en blanco");
							error.setDescripError("el campo esta vacio");
							error.setNumeroError(ContadorErrores);
							error.setTipoError("Estructura");
							error.setPosicionError(PosicionError);

							listaErrores.add(error);*/

							break;

						case ERROR:

							break;

						case FORMULA:

							break;

						default:

							//System.out.println("el dato tiene erroe no se puede leer");

						}

					}
					
					

				}
				

				//System.out.println(listaErrores.toString());
				
				

			}
			
			

			workbook.close();

		} catch (FileNotFoundException e) {

			redirectAttributes.addFlashAttribute("notificacion", notificacion);
			
			String tarea2 =  "Fecha Y hora [" + date1 + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ la ruta del lote a validar no se encuentra : " + ruta + "]";
			
			registrolog.guardarRegistro(NombreUsuarioLogeado, tarea2);

			return "redirect:/lote";

		} catch (IOException e) {
			
			String tarea2 =  "Fecha Y hora [" + date1 + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ la ruta del lote a validar no se encuentra : " + ruta + "]";
			
			registrolog.guardarRegistro(NombreUsuarioLogeado, tarea2);

			e.printStackTrace();
		}
		
		


		notificacion = "El lote " + NombreArchivo + " se a validado correctamente, el número de errores encontrados en el lote fue de : " + listaErrores.size();
		
		String tarea2 =  "Fecha Y hora [" + date1 + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ " + notificacion + "]";
		
		registrolog.guardarRegistro(NombreUsuarioLogeado, tarea2);

		DecimalFormat formatea = new DecimalFormat("###,###.##");
		
		DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss"); 
		  
		String HoraSistema = dateFormat.format(new Date());

		double restaDebitosCreditos = lote.getValorDebito() - lote.getValorCredito();
		
		String ValorTotalLote = formatea.format(restaDebitosCreditos);
		
		String ValorTotalDebito = formatea.format(contadorDebito);
		
		String ValorTotalCredito = formatea.format(contadorCredito);
		
		int numeroErrores = listaErrores.size();

		lote.setIdLote(IdLote);
		lote.setValorPesosNumerico(ValorTotalLote);
		lote.setValorCreditoNumerico(ValorTotalCredito);
		lote.setValorDebitoNumerico(ValorTotalDebito);
		lote.setValorCredito(contadorCredito);
		lote.setValorDebito(contadorDebito);
		lote.setPesos(restaDebitosCreditos);
		lote.setCatidadaregistros(ContadorRegistros);
		lote.setNumeroErrores(numeroErrores); // guardo numero de erroes para la tabla en db
		lote.setRutaArchivo(ruta);
		lote.setUsuario(usuario);
		lote.setHoraCargue(HoraSistema);
		
		
		double restaDebitosCreditosDivisa = valordivisa.getValorDebitoDivisaNumero() - valordivisa.getValorCreditoDivisaNumero();
		
		//System.out.println("VALOR CREDITO DIVISA " + valordivisa.getValorcreditoDivisa());
		
		//System.out.println("VALOR DEBITO DIVISA " + valordivisa.getValorDebitoDivisa());
		
		String ValorRestaLote = formatea.format(restaDebitosCreditos);
		
		String ValorRestaDivisas = formatea.format(restaDebitosCreditosDivisa);

		//System.out.println("el resultado de la resta debito/credito es de : " + ValorRestaLote);
		
		//System.out.println("el resultado de la resta debitos/credito divisa es de : " + ValorRestaDivisas);
		
		
		
		if (restaDebitosCreditos != 0.0) {
			
			ContadorErrores = ContadorErrores + 1;
			
			numeroErrores = ContadorErrores;

			Errores error = new Errores();

			error.setDatoErroneo(ValorRestaLote);
			error.setDescripError(" La resta de débitos y créditos  = : " + ValorRestaLote + " es ≠ 0 ");
			error.setNumeroError(ContadorErrores);
			error.setTipoError("El total débitos/créditos ≠ 0");
			error.setPosicionError("");

			listaErrores.add(error);
			
			int Errores = listaErrores.size();
			
			lote.setNumeroErrores(Errores); // guardo numero de erroes para la tabla en db
			
			//System.out.println("el lote esta mal la resta dio diferente de 0 ");
			
			String tarea3 =  "Fecha Y hora [" + date1 + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ " + "La resta de debitos y creditos  dio como resultado : " + ValorRestaLote + " es ≠ 0" + "]";
			
			registrolog.guardarRegistro(NombreUsuarioLogeado, tarea3);
			
		}
		
		if (restaDebitosCreditosDivisa != 0.0) {
			
			ContadorErrores = ContadorErrores + 1;
			
			numeroErrores = ContadorErrores;

			Errores error = new Errores();

			error.setDatoErroneo(" Resultado de resta : " +ValorRestaDivisas);
			error.setDescripError(" Valor del crédito Divisa : " + valordivisa.getValorcreditoDivisa() + " - " + "Valor debito divisa " + valordivisa.getValorDebitoDivisa() + " es ≠ 0");
			error.setNumeroError(ContadorErrores);
			error.setTipoError("El total débitos y créditos es ≠ 0");
			error.setPosicionError("");

			listaErrores.add(error);
			
			int Errores = listaErrores.size();
			
			lote.setNumeroErrores(Errores); // guardo numero de erroes para la tabla en db
			
			//System.out.println("el lote esta mal la resta dio diferente de 0 ");
			
			String tarea4 =  "Fecha Y hora [" + date1 + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ " + "La resta de debitos y creditos de divisas dio como resultado : " + ValorRestaDivisas + " es ≠ 0" + "]";
			
			registrolog.guardarRegistro(NombreUsuarioLogeado, tarea4);
			
		}
		

		if (ContadorErrores > 0 ) {
			
			String idLote = lote.getIdLote();
			
			//System.out.println("El lote presenta errores ");
			
			
			notificacion = "El lote " + idLote + " se ha validado correctamente, el número de errores encontrados en el lote fue de : " + listaErrores.size();
			
			lote.setEstadoLote("Rechazado por parametro");
			
			lote.setComentario("Rechazado por parametro");
			
			loteUsuarioService.guardarLote(lote);// cambio el estado del lote en db
			
			String tarea5 =  "Fecha Y hora [" + date1 + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ " + notificacion + "]";
			
			registrolog.guardarRegistro(NombreUsuarioLogeado, tarea5);
			
			lote.setNumeroErrores(numeroErrores);
			
		}
		
		loteUsuarioService.guardarLote(lote);
		
		
		if (listaErrores.isEmpty()) {
			
			redirectAttributes.addFlashAttribute("idlote",IdLote);
			redirectAttributes.addFlashAttribute("contadorErrores", listaErrores.size());
			
			
			String tarea6 =  "Fecha Y hora [" + date1 + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ " + "el lote no tiene nungun error de parametria pasa a la etapa de validar cuentas " + "]";
			
			registrolog.guardarRegistro(NombreUsuarioLogeado, tarea6);
			
			return "redirect:/ValidarCuentas";
			
			
		}
		
		else {
			
			
			
			notificacion = "El lote tiene un total de " + numeroErrores + " errores";
			
			guardarErrores.GuardarErroresTxt(listaErrores, IdLote);

			redirectAttributes.addFlashAttribute("idlote",IdLote);
			redirectAttributes.addFlashAttribute("listaErrores", listaErrores);
			redirectAttributes.addFlashAttribute("contadorErrores", listaErrores.size());
			redirectAttributes.addFlashAttribute("notificacion" , notificacion);
			
			String tarea7 =  "Fecha Y hora [" + date1 + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ " + notificacion + "]";
			
			registrolog.guardarRegistro(NombreUsuarioLogeado, tarea7);
			
			
			return "redirect:/lote";
		}
		
		

	}
	

	// --------------------------------------- paso 2 validar las cuentas frente a el balance no genenere saldos contrarios -------------- //
	
	@GetMapping("/ValidarCuentas")
	public String ValidarCunentasBalance (RedirectAttributes redirectAttributes , Model model , Principal principal) {
		
		String NombreUsuarioLogeado = principal.getName();

		DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

		String date = dateFormat.format(Calendar.getInstance().getTime());
				
		String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ el lote inicio la validacion de cuentas ]";
								
		registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);
		
		String Idlote = (String) model.asMap().get("idlote");
		
		int ContadorErrores = (int) model.asMap().get("contadorErrores");
		
		
		//System.out.println("el numero de errores de parametria es : " + ContadorErrores);
		
		
		int contadorErrores = ContadorErrores;
		
		List<ErroresBalance> listaErroresBalance = new ArrayList<ErroresBalance>();
		
		List<Errores> listaErrores = new ArrayList<Errores>();

		List<ResumenLote> ResumenCuentasLote = resumenLoteService.mostrarResumen(Idlote);
		
		LoteUsuario loteActual = loteUsuarioService.buscarLoteId(Idlote);
		
		for (int i = 0 ; i < ResumenCuentasLote.size() ; i ++) {
			
			String NumeroCuentaLote = ResumenCuentasLote.get(i).getNoCuenta();
			
			String SaldoFinalCuentaLote = ResumenCuentasLote.get(i).getTotalGeneral();
			
			String SaldoLote = SaldoFinalCuentaLote;
			
			SaldoFinalCuentaLote = SaldoFinalCuentaLote.replace(",", "");
			
			SaldoFinalCuentaLote = SaldoFinalCuentaLote.replace(".", "");
			
			long SaldoFinallote = Long.parseLong(SaldoFinalCuentaLote);
			
			/*System.out.println();
			
			System.out.println("la cuenta lote : " + NumeroCuentaLote);
			
			System.out.println("saldo final cuenta lote " + SaldoLote);*/
			
			List<CuentasExceptuadas> cuentasExcepActivas = cuentasservice.listaCuentasActivas("Activo");
			
			if (cuentasExcepActivas.isEmpty()) {
				
				//System.out.println("EL parametro cuentas exceptuadas no se esta validando");
				
			}else {
				
				List<CuentasExceptuadas> CuentaExcep = cuentasservice.listaCuentasExcepHabilitadas("Activo",NumeroCuentaLote);

				if (CuentaExcep == null  || CuentaExcep.isEmpty()) {
					
					List<Balance> CuentaBalance = balanceService.BuscarCuentaBalance(NumeroCuentaLote);

					
					if (CuentaBalance.isEmpty()) {
						
						ErroresBalance erroresBalance = new ErroresBalance();
						
						ContadorErrores = ContadorErrores + 1;

						Errores error = new Errores();

						error.setDatoErroneo(NumeroCuentaLote);
						error.setDescripError(" la cuenta  : " + NumeroCuentaLote + " No existe en el balance ");
						error.setNumeroError(ContadorErrores);
						error.setTipoError("Parametria");
						error.setPosicionError("");

						listaErrores.add(error);
						
						listaErroresBalance.add(erroresBalance);
						
						loteActual.setNumeroErrores(contadorErrores);
						
						loteActual.setComentario("Rechazado por parametro");
						
						loteActual.setEstadoLote("Rechazado por parametro");
						
						loteUsuarioService.guardarLote(loteActual);
						
						String tarea2 =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ " + "la cuenta no fue encontrada en el balance " + NumeroCuentaLote + " esta mal" + " ]";

						registrolog.guardarRegistro(NombreUsuarioLogeado, tarea2);
						
						notificacion = "El lote " + Idlote + " se a validado correctamente , el numero de errores encontrados en el lote fue de : " + contadorErrores;

						
						redirectAttributes.addFlashAttribute("idlote",Idlote);
						redirectAttributes.addFlashAttribute("listaErrores",listaErrores);
						redirectAttributes.addFlashAttribute("notificacion", notificacion);
						
						return "redirect:/lote";
						
					}
					
					

					String SaldoFinalCuentaBalance = CuentaBalance.get(0).getSaldoFinal();
					
					//System.out.println("saldo final balance " + SaldoFinalCuentaBalance);
					
					String NumeroCuentaBalance = CuentaBalance.get(0).getCuenta();
					
					SaldoFinalCuentaBalance = SaldoFinalCuentaBalance.replace(",", "");
					
					SaldoFinalCuentaBalance = SaldoFinalCuentaBalance.replace(".", "");
					
					long SaldoFinalBalance = Long.parseLong(SaldoFinalCuentaBalance);
					
					char Digito = NumeroCuentaBalance.charAt(0);
					
					char Digito2 = NumeroCuentaBalance.charAt(1);
					
					DecimalFormat formatea = new DecimalFormat("###,###.###");
					
					String DosDigitos = Digito  + "" + Digito2;
					
					int sumaDigitos = Integer.parseInt(DosDigitos);
					
					long Resultadototal = SaldoFinallote + SaldoFinalBalance;
					
					//System.out.println("el resultado de la suma es " + formatea.format(Resultadototal));
					
					//System.out.println("el resultado de la suma es :" + Resultadototal);
					
					int valorResul = 0 ;
					
					if(Resultadototal < 0 ) {
						
						 valorResul = 2;
					}
					
					if(Resultadototal > 0 ) {
						
						 valorResul = 1;
					}
					
					//System.out.println("el valor de para la cuenta " + NumeroCuentaLote + " es " + valorResul);
					
					if (sumaDigitos == 61 && valorResul == 2 || sumaDigitos == 64 && valorResul == 2  || sumaDigitos == 81 && valorResul == 2  || sumaDigitos == 84 && valorResul == 2 )
					{
						
						ErroresBalance erroresBalance = new ErroresBalance();
						
						contadorErrores = contadorErrores + 1 ;
						
						String SaldoBalance = formatea.format(SaldoFinalBalance);
						
						erroresBalance.setTipoError("saldo contrario");
						erroresBalance.setDatoErroneo(NumeroCuentaLote);
						erroresBalance.setDescripccion("la cuenta genera saldo contrario");
						erroresBalance.setNumeroError(contadorErrores);
						erroresBalance.setSaldoBalance(SaldoBalance);
						erroresBalance.setSaldoloteString(SaldoLote);
						
						listaErroresBalance.add(erroresBalance);
						
						loteActual.setContrario(contadorErrores);
						
						loteActual.setEstadoLote("Rechazado por parametro");
						
						loteActual.setComentario("Rechazado por parametro");
						
						loteUsuarioService.guardarLote(loteActual);
						
					}
					
					
					if (sumaDigitos == 62 && valorResul == 1 || sumaDigitos == 63 && valorResul == 1  || sumaDigitos == 83 && valorResul == 1  || sumaDigitos == 81 && valorResul == 1 )
					{
						
						ErroresBalance erroresBalance = new ErroresBalance();
						
						contadorErrores = contadorErrores + 1 ;
						
						String SaldoBalance = formatea.format(SaldoFinalBalance);
						
						erroresBalance.setTipoError("saldo contrario");
						erroresBalance.setDatoErroneo(NumeroCuentaLote);
						erroresBalance.setDescripccion("la cuenta genera saldo contrario");
						erroresBalance.setNumeroError(contadorErrores);
						erroresBalance.setSaldoBalance(SaldoBalance);
						erroresBalance.setSaldoloteString(SaldoLote);
						
						listaErroresBalance.add(erroresBalance);
						
						loteActual.setContrario(ContadorErrores);
						
						loteActual.setEstadoLote("Rechazado por parametro");
						
						loteActual.setComentario("Rechazado por parametro");
						
						loteUsuarioService.guardarLote(loteActual);
					}

					if (Digito == '1' && valorResul == 2 || Digito == '5' && valorResul == 2 ) {
						
						ErroresBalance erroresBalance = new ErroresBalance();
						
						contadorErrores = contadorErrores + 1 ;
						
						String SaldoBalance = formatea.format(SaldoFinalBalance);
						
						erroresBalance.setTipoError("saldo contrario");
						erroresBalance.setDatoErroneo(NumeroCuentaLote);
						erroresBalance.setDescripccion("la cuenta genera saldo contrario");
						erroresBalance.setNumeroError(contadorErrores);
						erroresBalance.setSaldoBalance(SaldoBalance);
						erroresBalance.setSaldoloteString(SaldoLote);
						
						listaErroresBalance.add(erroresBalance);
						
						loteActual.setContrario(contadorErrores);
						
						loteActual.setEstadoLote("Rechazado por parametro");
						
						loteActual.setComentario("Rechazado por parametro");
						
						loteUsuarioService.guardarLote(loteActual);
						
					}
					
					if (Digito == '2' && valorResul == 1 || Digito == '3' && valorResul == 1 || Digito == '4' && valorResul == 1) {
						
						ErroresBalance erroresBalance = new ErroresBalance();
						
						contadorErrores = contadorErrores + 1 ;
						
						String SaldoBalance = formatea.format(SaldoFinalBalance);
						
						erroresBalance.setTipoError("saldo contrario");
						erroresBalance.setDatoErroneo(NumeroCuentaLote);
						erroresBalance.setDescripccion("la cuenta genera saldo contrario");
						erroresBalance.setNumeroError(contadorErrores);
						erroresBalance.setSaldoBalance(SaldoBalance);
						erroresBalance.setSaldoloteString(SaldoLote);
						
						listaErroresBalance.add(erroresBalance);
						
						loteActual.setContrario(contadorErrores);
						
						loteActual.setEstadoLote("Rechazado por parametro");
						
						loteActual.setComentario("Rechazado por parametro");
						
						loteUsuarioService.guardarLote(loteActual);
						
					}
					


				}
				
				else {
					
					//System.out.println("Se encontro una cuenta que tiene excepcion " + NumeroCuentaLote);
					
					
				}
				
			}
			
	
		}
		
		if (listaErroresBalance.isEmpty()) { // cuando no existe errores de saldo contrario
			
			//System.out.println("no exiten errores en el lote");
		
			redirectAttributes.addFlashAttribute("idlote",Idlote);
			
			String tarea2 =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ " + "no exiten errores en el lote" + " ]";
			
			registrolog.guardarRegistro(NombreUsuarioLogeado, tarea2);

			return "redirect:/validarFlujoAprobacion";
			
			
			
		}
		
		else { // cuando existen errores de saldo contrario 
			
			notificacion = "El lote " + Idlote + " se ha validado correctamente contra el balance, el número de errores encontrados en el lote fue de : " + contadorErrores;
			
			redirectAttributes.addFlashAttribute("idlote",Idlote);
			redirectAttributes.addFlashAttribute("listaErroresBalance",listaErroresBalance);
			redirectAttributes.addFlashAttribute("notificacion", notificacion);
			
			String tarea2 =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ " + notificacion + " ]";
			
			registrolog.guardarRegistro(NombreUsuarioLogeado, tarea2);
			
			return "redirect:/lote";
			
		}
		

			
		
	}
	
	
	@RequestMapping(value = "/continuarFlujo/{id}", method = RequestMethod.GET)
	public String ConfirmarLote (@PathVariable("id") String id , RedirectAttributes redirectAttributes) {
		
		//System.out.println("el lote aprobasdo es " + id);
		
		String idlote = id ;
		
		redirectAttributes.addFlashAttribute("idlote",idlote);
		
		return "redirect:/validarFlujoAprobacion";
	}
	
	@RequestMapping(value = "/DenegarFlujo/{id}", method = RequestMethod.GET)
	public String negarLote (@PathVariable("id") String id , RedirectAttributes redirectAttributes) {
		
		notificacion =  "Has detenido el proceso, Tu lote no se ha enviado para autorizar";
		
		//System.out.println("el lote rechazado es : " + id);
		
		redirectAttributes.addFlashAttribute("notificacion" , notificacion );
		
		return "redirect:/lote";
	}
	
	// --------------------- validar flujo de aprobacion dependiendo el valor de la cuenta vs PYG -------------// 
	
	
	@GetMapping("/validarFlujoAprobacion")
	public String ValidarFlujoAprobacion (Model model , ModelMap mp , RedirectAttributes redirectAttributes , Principal principal) {
		
		Mensaje mensaje = new Mensaje();
		
		String NombreUsuarioLogeado = principal.getName();
		
		DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

		String date = dateFormat.format(Calendar.getInstance().getTime());
		
		Usuarios usuario = usuarioservice.buscarUsuario(NombreUsuarioLogeado);
		
		String AreaAnalista = usuario.getArea().getArea();
		
		
		Usuarios DatosCentralizador = usuarioservice.listaUsuariosPorIdRol(7);
		
		String NombreCentralizador = DatosCentralizador.getNombre();
		
		String AreaCentralizador = DatosCentralizador.getArea().getArea();
		
		String NonbreUsuario = usuario.getNombre();
		
		String Idlote = (String) model.asMap().get("idlote");
		
		LoteUsuario loteActual = loteUsuarioService.buscarLoteId(Idlote);
		
		List<ResumenLote> ResumenLoteActual = resumenLoteService.mostrarResumen(Idlote);
		
		String AutorizadorNivel1 = usuario.getArea().getAutorizadorNivel1();
		
		String AutorizadorNivel2 = usuario.getArea().getAutorizadorNivel2();
		
		Usuarios AutorizadorNivel1Datos = usuarioservice.buscarPorNombre(AutorizadorNivel1);
		
		String AreaAutorizadorNivel1 = AutorizadorNivel1Datos.getArea().getArea();
		
		//System.out.println("El autorizado 1 es " + AutorizadorNivel1);
		
		//System.out.println("El autorizado 2 es " + AutorizadorNivel2);
		
		String TipoLoteActual = loteActual.getTipoLote();

		boolean NivelAutorizacion1 = false;
		
		if (TipoLoteActual.contains("Lote Mensual") || TipoLoteActual.contains("Lote Diario")) {
		
		for (int i = 0 ; i < ResumenLoteActual.size() ; i ++) {
			
			
			String NumeroCuenta = ResumenLoteActual.get(i).getNoCuenta();
			
			char Digito = NumeroCuenta.charAt(0);
			
			int TipoCuenta = Character.getNumericValue(Digito); 

			double ValorTotalCuenta = ResumenLoteActual.get(i).getValorTotal();
			
			//String valorcuentaResumen = ResumenLoteActual.get(i).getTotalGeneral();
			
			if (TipoCuenta == 1 || TipoCuenta == 2 || TipoCuenta == 3 || TipoCuenta == 8 || TipoCuenta == 7 || TipoCuenta == 9) {
				
				NivelAutorizacion1 = true;
				
			}
			
			else {
				
				//System.out.println("se encontro una cuenta 4 o 5 o 6 ");
				
				ValorCuentasPYG CuentasExistentes = CuentasPYGservice.valorcuentaPyg("Activo",TipoCuenta);
				
				if (CuentasExistentes == null) {
					
					//System.out.println(" el valor para esta cunta no se esta trabajando");
					
					NivelAutorizacion1 = true ;
					
				}
				
				else {
					
					
					double ValorCuentaNivel1 = CuentasExistentes.getValorNivel1();
					
					//String valorCuentaNivel1PyG = CuentasExistentes.getValorNivel1Unidades();
					
					int inum =(int)ValorCuentaNivel1;  
					
					int inum2 =(int)ValorTotalCuenta;
					
					//System.out.println("el valor de la cuenta PYG parametria " + inum);
					
					//System.out.println("el valor de la cuenta lote " + inum2);
					
					//double ValorCuentaNivel2 = CuentaPYG.getValorNivel2();
					
					// ENVIO A NIVEL DE AUTORIZADOR NIVEL 2 
					
					if(inum2 > inum) {
						
						Usuarios AutorizadorNivel2Datos = usuarioservice.buscarPorNombre(AutorizadorNivel2);
						
						String AreaAutorizadorNivel2 = AutorizadorNivel2Datos.getArea().getArea();
						
						//System.out.println("se encontro una cuentas mayor ");
			
						loteActual.setNombreAutorizador(AutorizadorNivel2);
						
						loteActual.setAnalista(NonbreUsuario); // nombre se trae de DB

						loteActual.setEstadoLote("Enviado Autorizar");
						loteActual.setComentario("Ninguno");
						loteActual.setUsuario(usuario);
						loteActual.setAnalistaArea(AreaAnalista);
						loteActual.setAutorizadorArea(AreaAutorizadorNivel2);
						loteActual.setCentralizador(NombreCentralizador);
						loteActual.setCentralizadorArea(AreaCentralizador);
						
						
						loteUsuarioService.guardarLote(loteActual);
						
						notificacion = "El lote " + Idlote + " se ha enviado a autorización nivel 2";
						
						redirectAttributes.addFlashAttribute("notificacion", notificacion);
						
						String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ " + notificacion + " ]";

						registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);
						
						mensaje.setComentario("El lote " + Idlote + " ha sido validado por el sistema y cargado por el usuario " + NombreUsuarioLogeado );
						mensaje.setDestinatario(AutorizadorNivel2);
						mensaje.setFecha(date);
						mensaje.setEstado("No Leido");
						
						mensajeService.enviarMensaje(mensaje);

						return "redirect:/lote";
					}
					
					else {
						
						NivelAutorizacion1 = true ;
						
						//System.out.println("la cuenta es menor que el valor de PYG de parametria ");
						
					}
					
				}

			}

			
		}
		
		// ENVIO AUTORIZADOR NIVEL  1
		
		if (NivelAutorizacion1 = true) {
			
				loteActual.setAnalista(NonbreUsuario); // nombre se trae de DB
				
				loteActual.setNombreAutorizador(AutorizadorNivel1); // este dato es traido de base de datos 
				
				loteActual.setEstadoLote("Enviado Autorizar");
				loteActual.setComentario("Ninguno");
				loteActual.setUsuario(usuario);
				loteActual.setAnalistaArea(AreaAnalista);
				loteActual.setAutorizadorArea(AreaAutorizadorNivel1);
				loteActual.setCentralizador(NombreCentralizador);
				loteActual.setCentralizadorArea(AreaCentralizador);
				
				loteUsuarioService.guardarLote(loteActual);
				
				notificacion = "El lote " + Idlote + " se ha enviado a autorización nivel 1";
				
				redirectAttributes.addFlashAttribute("notificacion", notificacion);
				
				String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ " + notificacion + " ]";
	
				registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);
				
				mensaje.setComentario("El lote " + Idlote + " ha sido validado por el sistema y cargado por el usuario " + NombreUsuarioLogeado );
				mensaje.setDestinatario(AutorizadorNivel1);
				mensaje.setFecha(date);
				mensaje.setEstado("No Leido");
				
				mensajeService.enviarMensaje(mensaje);
				
				return "redirect:/lote";
		
			
			
			}
		
		}

			Usuarios DirectorContable = usuarioservice.listaUsuariosPorIdRol(8);
			
			// ENVIO A DIRECTOR CONTABLE 
			
			
			if (DirectorContable == null) {
				
				notificacion = "Aun no hay un director contable registrado";
				
				redirectAttributes.addFlashAttribute("notificacion", notificacion);
				
				String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ " + notificacion + " ]";

				registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);
				
				return "redirect:/lote";
				
			}
			
			String NombreDirector = DirectorContable.getNombre();
			
			System.out.println(DirectorContable.getNombre());
			
			String AreaDirectorContable = DirectorContable.getArea().getArea();
			
			//String NomrbeAutorizadorNivel1 = usuario.getArea().getAutorizadorNivel1();
			
			loteActual.setAnalista(NonbreUsuario); // se le asigna el nombre del analista que esta cargando el lote 
			loteActual.setAnalistaArea(AreaAnalista);
			loteActual.setNombreAutorizador(NombreDirector); //  nombre se trae de DB el asigna en ese caso el del director contable 
			loteActual.setAutorizadorArea(AreaDirectorContable);
			loteActual.setEstadoLote("Enviado Autorizar");
			loteActual.setComentario("Ninguno");
			loteActual.setUsuario(usuario);
			loteActual.setCentralizador(NombreCentralizador);
			loteActual.setCentralizadorArea(AreaCentralizador);
			
			
			
			loteUsuarioService.guardarLote(loteActual);
			
			notificacion = "el lote " + Idlote + " se a enviado a el director contable";
			
			redirectAttributes.addFlashAttribute("notificacion", notificacion);
			
			String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ " + notificacion + " ]";

			registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);
			
			mensaje.setComentario("El lote " + Idlote + " ha sido validado por el sistema  y cargado por el usuario " + NombreUsuarioLogeado );
			mensaje.setDestinatario(NombreDirector);
			mensaje.setFecha(date);
			mensaje.setEstado("No Leido");
			
			mensajeService.enviarMensaje(mensaje);
			
			return "redirect:/lote";
			
	}


	@RequestMapping(value = "/MostrarResumen/{id}", method = RequestMethod.GET)
	public String MostrarDetallesLote(@PathVariable("id") String id, HttpServletRequest request, ModelMap mp , Principal principal) {
		
		List<ResumenLote> ListaCuentaLote = resumenLoteService.mostrarResumen(id);
		
		//System.out.println("el tamaño de cuentas es de : " + ListaCuentaLote.size());
		
		mp.put("resumen", ListaCuentaLote);
		
		String NombreUsuarioLogeado = principal.getName();
		
		Usuarios usuario = usuarioservice.buscarUsuario(NombreUsuarioLogeado);
		
		String Rol = usuario.getRol().get(0).getName();
		
		mp.put("Area", usuario.getArea().getArea());
		
		mp.put("rol", Rol);

		DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

		String date = dateFormat.format(Calendar.getInstance().getTime());

		String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ accedio al resumen del lote con id :" + id + " ]";

		registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);
		
		return "CargarLoteResume";
	}
	
	
	
	
	@GetMapping("/pasarNombre")
	public String PasarNombre(Principal principal , RedirectAttributes redirectAttributes) {
		
		String usuarioAnalista = principal.getName();
		
		Usuarios usuario = usuarioservice.buscarUsuario(usuarioAnalista);
		
		String NombreAnalista = usuario.getNombre();
		
		//System.out.println("el nombre del analista es " + NombreAnalista);
		
		redirectAttributes.addFlashAttribute("NombreAnalista", NombreAnalista);
		
		List<LoteUsuario> lotesRechazados = loteUsuarioService.BuscarEstadoLote("Rechazado por parametro", NombreAnalista);
		
		int NumeroLotesRechazados = lotesRechazados.size();
		
		//System.out.println("numero de lotes rechazados " + lotesRechazados.size());
		
		//System.out.println("el numero de lotes rechazados por parametria es de : " + NumeroLotesRechazados);
		
		if (NumeroLotesRechazados > 0) {
			
			notificacion = "Tienes lotes rechazados por parametria elimínalos y vuelve a intentar validar un nuevo lote";
			
			redirectAttributes.addFlashAttribute("notificacion",notificacion);
			
			return "redirect:/lote";
			
		}
		
		else {
			
			redirectAttributes.addFlashAttribute("NombreAnalista", NombreAnalista);
			
			return "redirect:/descargarPlantilla";
			
		}
		
	}
	

	// controlador descargar plantilla y alojarla en carpeta

	@GetMapping("/descargarPlantilla")
	public String downloadFile(HttpServletResponse response, Model model , Principal principal) throws Exception {
		
		String NombreUsuarioLogeado = principal.getName();

		DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

		String date = dateFormat.format(Calendar.getInstance().getTime());

		String NombreAnalista = (String) model.asMap().get("NombreAnalista");
		
		List<LoteUsuario> lotesUsuario = loteUsuarioService.MostrarLotesUsuario(NombreAnalista);
		
		LocalDate Fechacargue = LocalDate.now();

		Fechacargue.toString();

		String FechaProceso = Fechacargue.toString();

		FechaProceso = FechaProceso.replaceAll("-", "");

		int Digitoincremental = 1000;

	

		// ---------------- El archivo con esa fecha y id no exista en el directorio // -----------//


			List<LoteUsuario> lotesNoAutorizados = loteUsuarioService.BuscarEstadoLote("No Autorizado", NombreAnalista);
			
			List<LoteUsuario> lotesAutorizados = loteUsuarioService.BuscarEstadoLote("Autorizado" , NombreAnalista );
			
			List<LoteUsuario> loteEnviadosAutorizar = loteUsuarioService.BuscarEstadoLote("Enviado Autorizar", NombreAnalista);
			
			List<LoteUsuario> loteProcesado = loteUsuarioService.BuscarEstadoLote("Procesado", NombreAnalista);
			
			List<LoteUsuario> LoteRechazado = loteUsuarioService.BuscarEstadoLote("Rechazado", NombreAnalista);
			
			Pageable pageRequest = PageRequest.of(0, 5);
			
			Page <LoteUsuario> lotes = loteUsuarioService.listaLotesUsuario(NombreAnalista, pageRequest);

				
			if (lotesAutorizados.isEmpty() == false || lotesNoAutorizados.isEmpty() == true || loteEnviadosAutorizar.isEmpty() == false  || lotes.isEmpty() == true || lotes.isEmpty() == false || loteProcesado.isEmpty() == false || LoteRechazado.isEmpty() == true) {

				//System.out.println("el usuario tiene lotes autorizados o no autorizados");
				
					
					if (Contador < 9999) {

						Contador = Contador + 1;

						Digitoincremental = Digitoincremental + Contador;

						//System.out.println("valor del digito :" + Digitoincremental);


					} 
					
						
					else {

						Digitoincremental = 1000;

						Contador = 0;

						//System.out.println("valor del digito :" + Contador + Digitoincremental);

					}

					
					
					
					
					File file = new File("src\\main\\resources\\static\\Plantilla\\Fto_Lote2023.xlsx");
					
					//File file = new File("..\\webapps\\CargaLotesMasivosAppweb-0.0.1-SNAPSHOT\\WEB-INF\\classes\\static\\Plantilla\\Fto_Lote2023.xlsx");

					// Llevando objeto de entrada
					FileInputStream fis = new FileInputStream(file);

					// Establecer el formato relevante
					response.setContentType("application/force-download");

					String NombreArchivo = "lote" + Digitoincremental + FechaProceso + ".xlsx";

					// Establecer el nombre y el encabezado del archivo descargado
					response.addHeader("Content-disposition", "attachment;fileName=" + NombreArchivo);
					
					String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ descargo la plantilla " + NombreArchivo + " ]";

					registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);

					// Crear objeto de salida
					OutputStream os = response.getOutputStream();
					// operación normal
					byte[] buf = new byte[1024];
					int len = 0;
					while ((len = fis.read(buf)) != -1) {
						os.write(buf, 0, len);
					}

					fis.close();

					return null;
					
				}
			
				
				
			else {
				
				
				
				return "redirect:/notificacion";
			}
				

	
		}
	
	@GetMapping("/notificacion")
	public String NotificacionPlantilla (Model model , RedirectAttributes redirectAttributes , Principal principal) {
		
		notificacion = "Para descargar otra plantilla primero debe eliminar todos los lotes con estado rechazado por parametria";
		
		redirectAttributes.addFlashAttribute("notificacion", notificacion);
		
		String NombreUsuarioLogeado = principal.getName();

		DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

		String date = dateFormat.format(Calendar.getInstance().getTime());

		String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ " + notificacion + "]";

		registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);
		
		return "redirect:/lote";
	}
	
	@GetMapping("/notificacion2")
	public String notificacionCargarLote (Model model , RedirectAttributes redirectAttributes) {
		
		notificacion = "";
		
		redirectAttributes.addFlashAttribute("notificacion", notificacion);
		
		return "redirect:/lote";
	}
	
	
	@GetMapping("/actualizarEstado")
	public String ActualizarEstadoLotes(Principal principal) {
		
		return "redirect:/lote";
	}
	
	@GetMapping("/actualizarEstadoAutorizar")
	public String ActualizarEstadoLotesAuto(Principal principal) {
		
		return "redirect:/EstadoPlanosAutorizador";
	}
	
	@GetMapping("/actualizarEstadoCentrali")
	public String ActualizarEstadoLotesCentra(Principal principal) {
		
		return "redirect:/EstadoPlanos";
	}
	
	@GetMapping("/actualirEstadoLoteAdmin")
	public String ActualizarEstadoLoteAdm (RedirectAttributes redirectAttributes) {
		
		
		return "redirect:/EstadoPlanos";
	}
	
	
	@GetMapping("/historialLotes")
	public String HistorialLotes(Model model , ModelMap mp) {
		
		
		
		return "historialLotes";
	}
	
	
	 @PostMapping("/upload")
	  public String handleFileUpload(@RequestParam(value = "dato", required = false) String dato ,@RequestParam("file") MultipartFile file , RedirectAttributes redirectAttributes , LoteUsuario lote , Principal principal){
		 
		 /* validar que ningun parametro este vacio */
		 
		 	String NombreUsuarioLogeado = principal.getName();
		 	
		 	DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");
		 	
		 	String date1 = dateFormat.format(Calendar.getInstance().getTime());
			
			List<NumeroCuenta> listaCuentasParametria = serviceCuenta.listaCuentas();
			
			List<Balance> balanceDelDia = balanceService.ListaCuentasBalance();
			
			List<DebitoCredito> ListaDebitoCreditoParametria = debitoCreditoService.listaDebitoCredito();
			
			List<Moneda> listaMonedaParametria = monedaService.listaMonedas();
			
			List<ValorCuentasPYG> listaValorCuentasPyg = valorCuentasPyGservice.listaCuentasPYG();
			
			List<Fuente> listaFuentesParametria = fuenteservice.listarFuente();
			
			List<Sucursal> listaSucursalParametria = sucursalService.listaSucursales();
			
			List<CentroCostos> listaCentroCostosParametria = serviceCentroCostos.listaCostos();
			
			List<TRM> listaTRM = trmService.listaValoresTrm();
			
			List<CuentasExceptuadas> listaCuentasExcep = cuentasservice.listaCuentasExcep();
			
			 String TipoLote = lote.getTipoLote();
			 
			 if (TipoLote == null) {
				 
				 notificacion = "Por favor selecciona un tipo de lote antes de cargar y validar";
				 
				 redirectAttributes.addFlashAttribute("notificacion", notificacion);
				 
				 String tarea =  "Fecha Y hora [" + date1 + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ " + notificacion + " ]";

				 registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);
				 
				 return "redirect:/lote";
				 
			 }

			
			if (listaCuentasParametria.isEmpty()){
				
				notificacion = "Las cuentas están siendo actualizadas, vuelva a intentar cargar el lote en unos minutos";
				
				redirectAttributes.addFlashAttribute("notificacion",notificacion);
				
				String tarea =  "Fecha Y hora [" + date1 + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ " + notificacion + "]";
				
				registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);
				
				return "redirect:/lote";
				
			}
			
			if (balanceDelDia.isEmpty()) {
				
				notificacion = "El balance está siendo actualizado, vuelva a intentar cargar el lote en unos minutos";
				
				redirectAttributes.addFlashAttribute("notificacion",notificacion);
				
				String tarea =  "Fecha Y hora [" + date1 + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ " + notificacion + "]";
				
				registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);
				
				return "redirect:/lote";
				
			}
			
			if (listaFuentesParametria.isEmpty()) {
				
				
				notificacion = "El parámetro fuentes está siendo actualizado, vuelva a intentar cargar el lote en unos minutos";
				
				redirectAttributes.addFlashAttribute("notificacion",notificacion);
				
				String tarea =  "Fecha Y hora [" + date1 + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ " + notificacion + "]";
				
				registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);
				
				return "redirect:/lote";
			}
			
			
			if (ListaDebitoCreditoParametria.isEmpty() ) {
				
				notificacion = "El parámetro débitos y créditos está siendo actualizado, vuelva a intentar cargar el lote en unos minutos";
				
				redirectAttributes.addFlashAttribute("notificacion",notificacion);
				
				String tarea =  "Fecha Y hora [" + date1 + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ " + notificacion + "]";
				
				registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);
				
				return "redirect:/lote";
			}
			
			if (listaMonedaParametria.isEmpty()) {
				
				notificacion = "El parámetro Moneda está siendo actualizado, vuelva a intentar cargar el lote en unos minutos";
				
				redirectAttributes.addFlashAttribute("notificacion",notificacion);
				
				String tarea =  "Fecha Y hora [" + date1 + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ " + notificacion + "]";
				
				registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);
				
				return "redirect:/lote";
			}
					
			if (listaSucursalParametria.isEmpty()) {
				
				notificacion = "El parámetro Sucursal está siendo actualizado, vuelva a intentar cargar el lote en unos minutos";
				
				redirectAttributes.addFlashAttribute("notificacion",notificacion);
				
				String tarea =  "Fecha Y hora [" + date1 + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ " + notificacion + "]";
				
				registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);
				
				return "redirect:/lote";
			}
					
			if (listaCentroCostosParametria.isEmpty()) {
				
				notificacion = "El parámetro centro de costos está siendo actualizado, vuelva a intentar cargar el lote en unos minutos";
				
				redirectAttributes.addFlashAttribute("notificacion",notificacion);
				
				String tarea =  "Fecha Y hora [" + date1 + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ " + notificacion + "]";
				
				registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);
				
				return "redirect:/lote";
			}
			
			if (listaValorCuentasPyg.isEmpty()) {
				
				
				
				notificacion = "El parámetro cuentas PYG está siendo actualizadas, vuelva a intentar cargar el lote en unos minutos";
				
				redirectAttributes.addFlashAttribute("notificacion",notificacion);
				
				String tarea =  "Fecha Y hora [" + date1 + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ " + notificacion + "]";
				
				registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);
				
				return "redirect:/lote";
			}
			
			if (listaTRM.isEmpty()) {
				
				notificacion = "El parámetro TRM está siendo actualizado, vuelva a intentar cargar el lote en unos minutos";
				
				redirectAttributes.addFlashAttribute("notificacion",notificacion);
				
				String tarea =  "Fecha Y hora [" + date1 + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ " + notificacion + "]";
				
				registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);
				
				return "redirect:/lote";
				
			}
			
			if (listaCuentasExcep.isEmpty()) {
				
				notificacion = "El parámetro cuentas Exceptuadas está siendo actualizado, vuelva a intentar cargar el lote en unos minutos";
				
				redirectAttributes.addFlashAttribute("notificacion",notificacion);
				
				String tarea =  "Fecha Y hora [" + date1 + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ " + notificacion + "]";
				
				registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);
				
				return "redirect:/lote";
				
			}
			

		 
		 String NombreArchivo = file.getOriginalFilename();
		 
		 String IdLote = NombreArchivo.replaceAll("[lote.xlsx]", "");
		 
		 LoteUsuario LoteAsubir = loteUsuarioService.buscarLoteId(IdLote);
		 
		 LocalDate Fechacargue = LocalDate.now();
		 
		 String FechaHoy = Fechacargue.toString();
		 
		 FechaHoy = FechaHoy.replace("-", "");
		 
		 String ruta = "..\\Lotes\\" + "lote" + Fechacargue;
		 
		 //System.out.println("la fecha del dia de hoy es : " + FechaHoy);
		 
		 String rutacompleta = "..\\Lotes\\" + "lote" + Fechacargue + "\\" + NombreArchivo;
		 
		 
		 
		 if (LoteAsubir == null && NombreArchivo.contains(FechaHoy)) {
			 
			 cargarArchivo.init(ruta);
			 
			 cargarArchivo.save(file,ruta);
			 
			 redirectAttributes.addFlashAttribute("TipoLote", TipoLote);
			 
			 redirectAttributes.addFlashAttribute("NombreArchivo", NombreArchivo);
			 
			 redirectAttributes.addFlashAttribute("rutaArchivo", rutacompleta);
			 
			 String tarea =  "Fecha Y hora [" + date1 + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ cargo el lote con la ruta " + ruta + " ]";

			 registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);
			 
			 return "redirect:/validarFechaProceso";
			 
		 }
		 
		 else {
			 
			 notificacion = "El lote " + IdLote + " ya ha sido validado por favor seleccione otro lote o el lote No tiene fecha del día de hoy ";
			 
			 redirectAttributes.addFlashAttribute("notificacion", notificacion);
			 
			 String tarea =  "Fecha Y hora [" + date1 + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ " + notificacion + " ]";

			 registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);
			 
			 return "redirect:/lote";
		 }
		

	 }
	 
	 
	 @RequestMapping(value = "/FiltrarLotesFecha" , method = RequestMethod.POST)
	 public String FiltrarLotesFecha (@RequestParam(name = "page" , defaultValue = "0")  int page, @RequestParam(value = "fecha1", required = false) String fecha1 , @RequestParam(value = "fecha2", required = false) String fecha2,  Model model, RedirectAttributes redirectAttributes, ModelMap mp , Principal principal) {
		 
			String fechaInicial = fecha1;
			 
			String fechaFinal = fecha2;
			
			//System.out.println(" la fecha 1 es " + fechaInicial);
			
			//System.out.println(" la fecha final 2 es " + fechaFinal);
			 
			String NombreUsuarioLogeado = principal.getName();
			
			Usuarios usuario = usuarioService.buscarUsuario(NombreUsuarioLogeado);
				
			String Rol = usuario.getRol().get(0).getName();
				
			mp.put("Area", usuario.getArea().getArea());
				
			mp.put("rol", Rol);

			Pageable pageRequest = PageRequest.of(page, 5);
			
			Page <LoteUsuario> lotes = loteUsuarioService.LotesFecha(fecha1, fecha2, pageRequest);
			
			Paginador <LoteUsuario> pageRender = new Paginador<>("/FiltrarLotesFecha" , lotes);
			
			mp.put("lote", lotes);
			
			mp.put("page", pageRender);
			
			mp.put("fechaInicial", fechaInicial);
			
			mp.put("fechaFinal", fechaFinal);
			
			if(lotes.isEmpty()) {
				
				mp.put("notificacion", "No hay lotes registrados entre las fechas " + fecha1 + " Y " + fecha2);
				
				 return "CargarLoteFiltroFecha";
			}
			
	
		 
		 return "CargarLoteFiltroFecha";
	 }
	 
	 
	 @RequestMapping(value = "/verdetallesLote/{id}", method = RequestMethod.GET)
	 public String verdetallesLote (@PathVariable("id") String id , ModelMap mp , Principal principal) {
		 
		 String NombreUsuarioLogeado = principal.getName();
		 
		 Usuarios usuario = usuarioService.buscarUsuario(NombreUsuarioLogeado);

		String Rol = usuario.getRol().get(0).getName();
				
		mp.put("Area", usuario.getArea().getArea());
				
		mp.put("rol", Rol);
		 
		LoteUsuario DetallesLote = loteUsuarioService.buscarLoteId(id);
		 
		mp.put("lotes", DetallesLote); 
		 
		 return "CargarLoteDetalles";
	 }
	 
	 @RequestMapping(value = "/descargarErrores/{id}")
	 public void DescargarErroresLote (@PathVariable("id") String id , HttpServletResponse response, RedirectAttributes redirectAttributes ) throws Exception {
		 
		 	
			String rutaPlano = "..\\HistorialErrores\\" + id + ".txt";
			
				
				File file = new File(rutaPlano);

				// File file = new File("..\\Planos\\siig11ajc.txt");
				// Llevando objeto de entrada
				FileInputStream fis = new FileInputStream(file);
				// Establecer el formato relevante
				response.setContentType("application/force-download");
				// Establecer el nombre y el encabezado del archivo descargado
				response.addHeader("Content-disposition", "attachment;fileName=" + "Errores" + id + ".txt");
				// Crear objeto de salida
				OutputStream os = response.getOutputStream();
				// operación normal
				byte[] buf = new byte[1024];
				int len = 0;
				while ((len = fis.read(buf)) != -1) {
					os.write(buf, 0, len);
				}
				fis.close();

		 
	 }
	 
	 

	 
	

}
