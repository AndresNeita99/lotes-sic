package com.example.demo.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.security.Principal;
import java.text.DateFormat;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import java.util.TimerTask;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.apache.poi.ss.formula.functions.Count;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.example.demo.RegistrosLog;
import com.example.demo.TiempoEspera;
import com.example.demo.TrasformarLoteAplano;
import com.example.demo.Entity.HorasCorte;
import com.example.demo.Entity.LoteUsuario;
import com.example.demo.Entity.Mensaje;
import com.example.demo.Entity.Plano;
import com.example.demo.Entity.Usuarios;
import com.example.demo.Paginador.Paginador;
import com.example.demo.service.HorasCorteService;
import com.example.demo.service.LoteUsuerioService;
import com.example.demo.service.MensajeService;
import com.example.demo.service.PlanoService;
import com.example.demo.service.UsuariosService;
import com.example.demo.service.emailEnviar;

import jakarta.servlet.http.HttpServletResponse;

@Controller
public class ControllerPlanos {

	ScheduledExecutorService executor = null;
	
	@Autowired
	private MensajeService mensajeservice;
	
	@Autowired
	private emailEnviar emailSender;

	@Autowired
	private UsuariosService usuarioService;

	@Autowired
	private HorasCorteService horasCorteService;

	@Autowired
	private LoteUsuerioService loteUsuarioService;

	@Autowired
	private PlanoService planoservice;

	private String notificacion = "";

	int count = 0;

	int contador2 = 0;
	
	int count2 = 1 ;

	private boolean procesoDetendio = false;

	private RegistrosLog registrolog = new RegistrosLog();

	@RequestMapping(value = "/listaLotesAutorizados", method = RequestMethod.GET)
	public String ListaLotesAutorizados(@RequestParam(name = "page", defaultValue = "0") int page, Model model,
			ModelMap mp, Principal principal) {

		String notificacion = (String) model.asMap().get("notificacion");

		String NombreUsuarioLogeado = principal.getName();

		Usuarios usuario = usuarioService.buscarUsuario(NombreUsuarioLogeado);

		String Rol = usuario.getRol().get(0).getName();

		mp.put("Area", usuario.getArea().getArea());

		mp.put("rol", Rol);

		Pageable pageRequest = PageRequest.of(page, 5);

		Page<LoteUsuario> lotes = loteUsuarioService.LostesAprobados("Autorizado", pageRequest);

		Paginador<LoteUsuario> pageRender = new Paginador<>("/listaLotesAutorizados", lotes);

		TiempoEspera minutosEspera = new TiempoEspera();

		mp.put("total", lotes.getTotalElements());
		mp.put("notificacion", notificacion);
		mp.put("minutosEspera", minutosEspera);
		mp.put("lotesAutorizados", lotes);
		mp.put("page", pageRender);

		DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

		String date = dateFormat.format(Calendar.getInstance().getTime());

		String tarea = "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea"
				+ " [ Accedio al la bandeja de lote autorizados ]";

		registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);

		return "PlanosBandejaEntrada";
	}

	@GetMapping("/CrearCapetaPlanos")
	public String downloadFile(HttpServletResponse response, Model model, RedirectAttributes redirectAttributes,
			Principal principal) throws Exception {

		String NombreUsuarioLogeado = principal.getName();

		DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

		String date = dateFormat.format(Calendar.getInstance().getTime());

		Usuarios usuario = usuarioService.buscarUsuario(NombreUsuarioLogeado);

		String RolUsuario = usuario.getRol().get(0).getName();

		horasCorteService.listasHorasCorte();

		if (horasCorteService.listasHorasCorte().isEmpty()) {

			notificacion = "No hay ninguna hora de corte registrada por favor registrar las 3 fechas de corte mínimas";

			redirectAttributes.addFlashAttribute("notificacion", notificacion);

			if (RolUsuario.contains("Administrador")) {

				redirectAttributes.addFlashAttribute("notificacion", notificacion);

				return "redirect:/listaHorasCorte";
			}

			return "redirect:/listaLotesAutorizados";
		}

		String ruta = "..\\Planos";

		// esta carpeta se creara cada vez que se descargue y se llene un plano
		File directorio = new File(ruta);

		if (!directorio.exists()) {
			if (directorio.mkdirs()) {
				// System.out.println("Directorio creado" + ruta);

				String tarea = "Fecha Y hora [" + date + "]  Usuario [" + "sistema" + "] " + "Tarea" + " [ "
						+ "creo la carpeta planos" + " ]";

				registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);

				downloadFile(response, model, redirectAttributes, principal);

			}

			return "redirect:/IniciarProcesoAutomatico";

		} else {

			// System.out.println("Error del directorio ya existe");

			return "redirect:/IniciarProcesoAutomatico";

		}

	}

	@GetMapping("/IniciarProcesoAutomatico")
	public String InciarProcesoAuto(RedirectAttributes redirectAttributes, HttpServletResponse response, Model model, Principal principal) {

		String NombreUsuarioLogeado = principal.getName();

		procesoDetendio = false;

		DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

		String date = dateFormat.format(Calendar.getInstance().getTime());

		Usuarios usuario = usuarioService.buscarUsuario(NombreUsuarioLogeado);

		String RolUsuario = usuario.getRol().get(0).getName();

		// System.out.println("Rol del usuario" + RolUsuario);

		List<LoteUsuario> lotesAutorizados = loteUsuarioService.LotesAutorizado("Autorizado");

		List<HorasCorte> horasCorte = horasCorteService.listasHorasCorte();

	

			String ruta = "..\\HistorialPLanos";

			// esta carpeta se creara cada vez que se descargue y se llene un plano
			File directorio = new File(ruta);

			String tarea = "Fecha Y hora [" + date + "]  Usuario [" + "sistema" + "] " + "Tarea" + " [ "
					+ "creo la carpeta Historial planos" + " ]";

			registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);

			if (!directorio.exists()) {
				if (directorio.mkdirs()) {
					// System.out.println("Directorio creado" + ruta);

					InciarProcesoAuto(redirectAttributes, response, model, principal);

				}
			}

			File folder = new File("../Planos");

			File[] listOfFiles = folder.listFiles();

			int NumeroPlanosEnCarpeta = listOfFiles.length;

			if (NumeroPlanosEnCarpeta == 0 && procesoDetendio == false) {

				executor = Executors.newScheduledThreadPool(1);
				executor.scheduleAtFixedRate(tiempoEjecucion(), 1 /* Retardo inicial */, 1, TimeUnit.SECONDS); // Cada 1
																												// segundo
																												// se
																												// esat
																												// psando
																												// un
																												// lote
																												// de
																												// xlsx
																												// a txt

				notificacion = "Proceso automatico iniciado";

				String tarea2 = "Fecha Y hora [" + date + "]  Usuario [" + "sistema" + "] " + "Tarea" + " [ " + notificacion + " ]";

				registrolog.guardarRegistro(NombreUsuarioLogeado, tarea2);

				redirectAttributes.addFlashAttribute("notificacion", notificacion);

				if (RolUsuario.contains("Administrador")) {
					

					redirectAttributes.addFlashAttribute("notificacion", notificacion);

					return "redirect:/listaHorasCorte";
				}

				return "redirect:/listaLotesAutorizados";

			}

			if (horasCorte.isEmpty() == true) {

				notificacion = "No hay horas de corte creadas";

				redirectAttributes.addFlashAttribute("notificacion", notificacion);

				String tarea2 = "Fecha Y hora [" + date + "]  Usuario [" + "sistema" + "] " + "Tarea" + " [ " + notificacion + " ]";

				registrolog.guardarRegistro(NombreUsuarioLogeado, tarea2);

				return "redirect:/listaLotesAutorizados";

			}

			else {

				notificacion = "Por favor elimine el plano que se cargo a AS400";

				redirectAttributes.addFlashAttribute("notificacion", notificacion);

				String tarea2 = "Fecha Y hora [" + date + "]  Usuario [" + "sistema" + "] " + "Tarea" + " [ " + notificacion + " ]";

				registrolog.guardarRegistro(NombreUsuarioLogeado, tarea2);

				return "redirect:/listaLotesAutorizados";

			}

		}

	

	public TimerTask tiempoEjecucion() {

		TrasformarLoteAplano tradformarLotePlano = new TrasformarLoteAplano();

		List<HorasCorte> horasCorte = horasCorteService.listasHorasCorte(); // consulta de las horas activadas en DB

		int HorasCorteRegistradas = horasCorteService.NumeroHorasActivas("Activo");

		// System.out.println("La cantiodad de fechas de corte son : " +
		// HorasCorteRegistradas);

		TimerTask _timerTask;

		_timerTask = new TimerTask() {

			int contador2 = 0;

			@Override
			public void run() {

				Plano plano = new Plano();

				DateFormat dateFormat = new SimpleDateFormat("HH:mm");

				String FechaSistema = dateFormat.format(new Date()); // trae la fecha del sistema

				String[] FechaSistemaDividida = FechaSistema.split(":");

				String HoraSistema = FechaSistemaDividida[0];

				String MinutosSistema = FechaSistemaDividida[1];

				int horasistema = Integer.parseInt(HoraSistema);

				int minutosistema = Integer.parseInt(MinutosSistema);

				String HoraCorte = horasCorte.get(count).getHoraCorte(); // traer la hora de corte guardada en base de
																			// datos

				String[] HoraCorteDividida = HoraCorte.split(":");

				String HoraCortedb = HoraCorteDividida[0];

				String MinutosCorteDb = HoraCorteDividida[1];

				int horacorte = Integer.parseInt(HoraCortedb);

				int minutocorte = Integer.parseInt(MinutosCorteDb);

				/*System.out.println("La fecha del sistema es : " + FechaSistema);

				System.out.println("La hora de corte es " + HoraCorte);*/

				List<LoteUsuario> lotesAutorizados = loteUsuarioService.TraerLotesAutorizadosHora("Autorizado");

				if (lotesAutorizados.size() == 0) {

					executor.shutdown();

					synchronized (executor) {

						try {

							System.out.println("El proceso de va detener ");

							count = count + 1;

							Thread.sleep(10000);

							System.out.println("contador " + count);
							
							
							
							if (count == HorasCorteRegistradas  && lotesAutorizados.size() == 0) {

								count = 0;

								System.out.println("el contador de horas de cargue se reinicio " + count);
							}

						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();

						}

					}

				}

				if (horasistema == horacorte && minutosistema == minutocorte && lotesAutorizados.isEmpty() == false) {

					System.out.println("lotes autorizados " + lotesAutorizados.size());

					LoteUsuario loteActual = lotesAutorizados.get(0); // cambiarlo por el contador ya que simepre va
																		// ahber mas de un plano

					String RutaLoteActual = loteActual.getRutaArchivo();

					String NombreUsuario = loteActual.getAnalista();

					String NombreAutorizador = loteActual.getNombreAutorizador();

					Usuarios usuarioAnalista = usuarioService.buscarPorNombre(NombreUsuario);

					String UsuarioAna = usuarioAnalista.getUsername();

					System.out.println("el nombre del analista es :" + UsuarioAna);

					Usuarios UsuarioAutorizador = usuarioService.buscarPorNombre(NombreAutorizador);

					String UsuarioAuto = UsuarioAutorizador.getUsername();

					System.out.println("el nombre del autorizador es :" + UsuarioAuto);

					System.out.println("la ruta del archivo es : " + RutaLoteActual);

					LocalDate Fechacargue = LocalDate.now();
					
					List<Plano> ListaPlanos = planoservice.listaPlanos();
					
					int numeroPlanos = ListaPlanos.size();

					String idPlano = Fechacargue.toString();

					idPlano = idPlano.replace("-", "");

					idPlano = numeroPlanos + idPlano;

					boolean TerminoTrasformarLote = tradformarLotePlano.TrasformarExcelTxt(RutaLoteActual, contador2, UsuarioAna, UsuarioAuto, idPlano);

					System.out.println("El lote se trasformo correctamente " + TerminoTrasformarLote);

					if (TerminoTrasformarLote == true) {

						// System.out.println("el lote a sido procesado");

						loteActual.setEstadoLote("Procesando");

						if (planoservice.buscarPlano(idPlano) == null) {

							String ruta = "..\\HistorialPLanos\\" + idPlano + ".txt";

							plano.setId(idPlano);
							plano.setRutaPlano(ruta);
							plano.setEstado("Generado");

							planoservice.GuardarPlano(plano);

							loteActual.setPlano(plano);

							loteUsuarioService.guardarLote(loteActual);

							contador2 = contador2 + 1;

						} else {

							plano = planoservice.buscarPlano(idPlano);

							loteActual.setPlano(plano);

							loteUsuarioService.guardarLote(loteActual);

						}

					}

					System.out.println(FechaSistema + " numero de lotes cargados A AS400 " + contador2);

					System.out.println("el contador es " + count);
					
					

					

				}

			}

		};

		return _timerTask;

	}

	@GetMapping("/IniciarProcesoManual")
	public String IniciarProcesoManual(Model model, RedirectAttributes redirectAttributes , Principal principal) {
		
		String NombreUsuarioLogeado = principal.getName();

		procesoDetendio = false;

		DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

		String date = dateFormat.format(Calendar.getInstance().getTime());

		Usuarios usuario = usuarioService.buscarUsuario(NombreUsuarioLogeado);


		List<LoteUsuario> lotesAutorizados = loteUsuarioService.LotesAutorizado("Autorizado");

		
		String rutaHistorial = "..\\HistorialPLanos";

		// esta carpeta se creara cada vez que se descargue y se llene un plano
		File directorio = new File(rutaHistorial);

		String tarea = "Fecha Y hora [" + date + "]  Usuario [" + "sistema" + "] " + "Tarea" + " [ " + "creo la carpeta Historial planos" + " ]";

		registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);

		if (!directorio.exists()) {
			if (directorio.mkdirs()) {
				
				 //System.out.println("Directorio ya fue creado " + rutaHistorial);

				

			}
		}

		String rutaPlano = "..\\Planos";

		// esta carpeta se creara cada vez que se descargue y se llene un plano
		File directorio2 = new File(rutaPlano);

		if (!directorio2.exists()) {
			if (directorio2.mkdirs()) {
				// System.out.println("Directorio creado" + ruta);

				String tarea2 = "Fecha Y hora [" + date + "]  Usuario [" + "sistema" + "] " + "Tarea" + " [ " + "creo la carpeta planos" + " ]";

				registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);

				//System.out.println("se creo la carpeta planos");

			}

		}

		List<LoteUsuario> NumeroLotes = loteUsuarioService.TraerLotesAutorizadosHora("Autorizado");
		
		if (NumeroLotes.isEmpty()) {
			
			redirectAttributes.addFlashAttribute("notificacion", " No hay lotes autorizados aun ");
			
			return "redirect:/listaLotesAutorizados";
		}
		
		

		TrasformarLoteAplano tradformarLotePlano = new TrasformarLoteAplano();

		Plano plano = new Plano();
		
		LocalDate Fechacargue = LocalDate.now();

		String idPlano = Fechacargue.toString();

		idPlano = idPlano.replace("-", "");
		
		List<Plano> ListaPlanos = planoservice.listaPlanos();
		
		int numeroPlanos = ListaPlanos.size();

		idPlano = numeroPlanos  + idPlano;
		
		if (planoservice.buscarPlano(idPlano) == null) {

			String ruta = "..\\HistorialPLanos\\" + idPlano + ".txt";

			plano.setId(idPlano);
			plano.setRutaPlano(ruta);
			plano.setEstado("Generado");

			planoservice.GuardarPlano(plano);


		}
		
		int contador = 0 ;

		for (int i = 0; i < NumeroLotes.size(); i++) {
	
				/*System.out.println("El id del plano es : " + idPlano);
	
				System.out.println("lotes autorizados " + lotesAutorizados.size());*/
			
				Mensaje mensaje = new Mensaje();
				
				Mensaje mensaje2 = new Mensaje();
	
				LoteUsuario loteActual = lotesAutorizados.get(i); // cambiarlo por el contador ya que simepre va ahber mas
				
				String idlote = loteActual.getIdLote(); // de un plano
	
				String RutaLoteActual = loteActual.getRutaArchivo();
	
				String NombreUsuario = loteActual.getAnalista();
	
				String NombreAutorizador = loteActual.getNombreAutorizador();
	
				Usuarios usuarioAnalista = usuarioService.buscarPorNombre(NombreUsuario);
	
				String UsuarioAna = usuarioAnalista.getUsername();
	
				//System.out.println("el nombre del analista es :" + UsuarioAna);
	
				Usuarios UsuarioAutorizador = usuarioService.buscarPorNombre(NombreAutorizador);
	
				String UsuarioAuto = UsuarioAutorizador.getUsername();
	
				//System.out.println("el nombre del autorizador es :" + UsuarioAuto);
	
				//System.out.println("la ruta del archivo es : " + RutaLoteActual);
	
				contador = contador + 1 ;
	
				//boolean TerminoTrasformarLote = tradformarLotePlano.TrasformarExcelTxt(RutaLoteActual, contador, UsuarioAna, UsuarioAuto, idPlano);
	
				boolean TerminoTrasformarLote = tradformarLotePlano.traformarloteOptimizado(RutaLoteActual, contador, UsuarioAna, UsuarioAuto, idPlano);

				
	
				if (TerminoTrasformarLote == true) {
					
					//System.out.println("El lote se trasformo correctamente " + TerminoTrasformarLote);
	
					loteActual.setEstadoLote("Procesando");
	
					plano = planoservice.buscarPlano(idPlano);
	
					loteActual.setPlano(plano);
	
					loteUsuarioService.guardarLote(loteActual);
					
					mensaje.setComentario("El plano del lote " + idlote + " Fue generado");
					mensaje.setDestinatario(NombreUsuario);
					mensaje.setEstado("No Leido");
					mensaje.setFecha(date);
					
					
					
					mensaje2.setComentario("El Plano del lote  " + idlote + " Fue generado");
					mensaje2.setDestinatario(NombreAutorizador);
					mensaje2.setEstado("No Leido");
					mensaje2.setFecha(date);
					
					mensajeservice.enviarMensaje(mensaje2);
					mensajeservice.enviarMensaje(mensaje);
	
					}

			}

			//System.out.println(" numero de lotes cargados A AS400 " + contador);

			redirectAttributes.addFlashAttribute("notificacion", "El plano se ha generado correctamente");
			
			return "redirect:/listaLotesAutorizados";

	}

	@GetMapping("/DetenerProcesoAutomatico")
	public String DetenerProcesoFormaManual(Model model, RedirectAttributes redirectAttributes, TiempoEspera minutosEspera, Principal principal) {

		String NombreUsuarioLogeado = principal.getName();

		DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

		String date = dateFormat.format(Calendar.getInstance().getTime());

		String Tiempo = minutosEspera.getTiempoEspera();

		String FechaSistema = dateFormat.format(new Date());

		// System.out.println("el timepo para deternse es de : " + Tiempo + " Y la decha
		// del sistema es de : " + FechaSistema);

		Usuarios usuario = usuarioService.buscarUsuario(NombreUsuarioLogeado);

		String RolUsuario = usuario.getRol().get(0).getName();

		//System.out.println("Rol del usuario " + RolUsuario);

		String ruta = "../Planos/siig11ajc.txt";

		File file = new File(ruta); // Si el archivo no existe es creado


		synchronized (executor) {

			try {

				executor.shutdownNow();

				//System.out.println("El proceso de va detener ");

				Thread.sleep(10000);

				// 30000 = 30s

				procesoDetendio = true;

				notificacion = "el proceso automatico se a detenido ";

				redirectAttributes.addFlashAttribute("notificacion", notificacion);

				if (RolUsuario.contains("Administrador")) {

					return "redirect:/listaHorasCorte";

				}

				return "redirect:/listaLotesAutorizados";

			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();

				System.out.println("el error es : " + e);

				return "redirect:/listaLotesAutorizados";

			}

		}

	}

	@RequestMapping(value = "download/{id}")
	public void download(@PathVariable("id") String id, HttpServletResponse response, Principal principal) throws Exception {
		// Dirección del archivo, el entorno real se almacena en la base de datos

		String NombreUsuarioLogeado = principal.getName();

		DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

		String date = dateFormat.format(Calendar.getInstance().getTime());

		Plano planoDescargar = planoservice.buscarPlano(id);

		String rutaPlano = planoDescargar.getRutaPlano();

		File file = new File(rutaPlano);

		String tarea = "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea"
				+ " [ descargo el plano con la ruta " + rutaPlano + " ]";

		registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);

		// File file = new File("..\\Planos\\siig11ajc.txt");
		// Llevando objeto de entrada
		FileInputStream fis = new FileInputStream(file);
		// Establecer el formato relevante
		response.setContentType("application/force-download");
		// Establecer el nombre y el encabezado del archivo descargado
		response.addHeader("Content-disposition", "attachment;fileName=" + "siig11ajc.txt");
		// Crear objeto de salida
		OutputStream os = response.getOutputStream();
		// operación normal
		byte[] buf = new byte[1024];
		int len = 0;
		while ((len = fis.read(buf)) != -1) {
			os.write(buf, 0, len);
		}
		fis.close();
	}

	@RequestMapping(value = "/CargarPlano/{id}", method = RequestMethod.GET)
	public String CargarLoteAs400(@PathVariable("id") String id, RedirectAttributes redirectAttributes, Principal principal) throws IOException {

		Plano PlanoSubir = planoservice.buscarPlano(id);

		PlanoSubir.setEstado("CargadoAS400");

		planoservice.GuardarPlano(PlanoSubir);

		List<LoteUsuario> lotesDelPlano = loteUsuarioService.TrearLotePlano(id);

		String NombreUsuarioLogeado = principal.getName();

		DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

		String date = dateFormat.format(Calendar.getInstance().getTime());

		String tarea = "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ cargo el plano a As400 ]";

		registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);

		for (int i = 0; i < lotesDelPlano.size(); i++) {
			
			Mensaje mensaje = new Mensaje();
			
			Mensaje mensaje2 = new Mensaje();

			LoteUsuario lotePlano = new LoteUsuario();

			lotePlano = lotesDelPlano.get(i);
			
			String Analista= lotePlano.getAnalista();
			
			String Autorizador = lotePlano.getAutorizador();
			
			Usuarios TraerCorreoAnalista = usuarioService.buscarPorNombre(Analista);
			
			Usuarios TraerCorreoAutoriza = usuarioService.buscarPorNombre(Autorizador);
			
			String CorreoAnalista = TraerCorreoAnalista.getCorreo();
			
			String CorreAutorizador = TraerCorreoAutoriza.getCorreo();
			
			usuarioService.buscarPorNombre(Autorizador);
			
			emailSender.sendEmail("Estado de Lote SIC", "Test Message : El lote  : " + id + " Ha sido cargado AS-400", "administracionlotes@pichincha.com.co" , CorreAutorizador /* PONER del nuevo usuario */ , true );

			
			emailSender.sendEmail("Estado de Lote SIC", "Test Message : El lote  : " + id + " Ha sido cargado AS-400", "administracionlotes@pichincha.com.co" , CorreoAnalista /* PONER del nuevo usuario */ , true );

			

			lotePlano.setEstadoLote("Procesado");

			loteUsuarioService.guardarLote(lotePlano);
			
			mensaje.setComentario("El lote " + lotePlano.getIdLote() + " Fue cargado a AS400");
			mensaje.setDestinatario(lotePlano.getAutorizador());
			mensaje.setEstado("No Leido");
			mensaje.setFecha(date);
			
			
			
			mensaje2.setComentario("El lote " + lotePlano.getIdLote() + " Fue cargado a AS400");
			mensaje2.setDestinatario(lotePlano.getAnalista());
			mensaje2.setEstado("No Leido");
			mensaje2.setFecha(date);
			
			mensajeservice.enviarMensaje(mensaje2);
			mensajeservice.enviarMensaje(mensaje);

		}

		File directory = new File("../Planos");
		
		FileUtils.cleanDirectory(directory);

		//System.out.println("el plano a sido eliminado para otro cargue :# ");

		notificacion = "El plano ha sido cargado en AS400, Si no descargaste el plano puedes hacer la descargar del plano " + id + " en historial planos ";

		redirectAttributes.addAttribute("notificacion", notificacion);

		String tarea2 = "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ " + notificacion + " ]";

		registrolog.guardarRegistro(NombreUsuarioLogeado, tarea2);

		return "redirect:/listaLotesAutorizados";

	}

	@GetMapping("/ListaPlanos")
	public String ListaPlanos(ModelMap mp, Model model, RedirectAttributes redirectAttributes, Principal principal) {

		String Estado = "Generado";

		String NombreUsuarioLogeado = principal.getName();

		Usuarios usuario = usuarioService.buscarUsuario(NombreUsuarioLogeado);

		String Rol = usuario.getRol().get(0).getName();

		mp.put("Area", usuario.getArea().getArea());

		mp.put("rol", Rol);

		DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

		String date = dateFormat.format(Calendar.getInstance().getTime());

		List<Plano> PlanoGenerado = planoservice.BuscarPlanoEstado(Estado);

		if (PlanoGenerado.isEmpty()) {

			notificacion = "No se ha generado un plano aun";

			redirectAttributes.addFlashAttribute("notificacion", notificacion);

			String tarea2 = "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ "
					+ notificacion + " ]";

			registrolog.guardarRegistro(NombreUsuarioLogeado, tarea2);

			return "redirect:/listaLotesAutorizados";

		}

		String tarea2 = "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ " + "accedio a la lista de planos genearos para cargue" + " ]";

		registrolog.guardarRegistro(NombreUsuarioLogeado, tarea2);

		mp.put("Planos", PlanoGenerado);

		return "PlanosListaGenerado";
	}

	@RequestMapping(value = "/ListaPlanosCargados", method = RequestMethod.GET)
	public String ListaPlanosCargados(@RequestParam(name = "page", defaultValue = "0") int page, ModelMap mp, Model model, Principal principal) {

		String NombreUsuarioLogeado = principal.getName();

		DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

		String date = dateFormat.format(Calendar.getInstance().getTime());

		Usuarios usuario = usuarioService.buscarUsuario(NombreUsuarioLogeado);

		String Rol = usuario.getRol().get(0).getName();

		mp.put("Area", usuario.getArea().getArea());

		mp.put("rol", Rol);

		Pageable pageRequest = PageRequest.of(page, 5);

		Page<Plano> planos = planoservice.TraerPlanosCrgardos("CargadoAS400", pageRequest);

		Paginador<Plano> pageRender = new Paginador<>("/ListaPlanosCargados", planos);

		long Totalplanos = planos.getTotalElements();

		//System.out.print("la cantidad de datos es :" + planos.getTotalElements());

		mp.put("planos", planos);
		mp.put("total", Totalplanos);
		mp.put("page", pageRender);

		String tarea = "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ Accedio al la lista de planos cargados As400 ]";

		registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);

		return "PlanosListaCargados";
	}
	
	

}
