package com.example.demo.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.Principal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.poi.openxml4j.util.ZipSecureFile;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.example.demo.RegistrosLog;
import com.example.demo.Dao.FuenteParametroDao;
import com.example.demo.Entity.Area;
import com.example.demo.Entity.Balance;
import com.example.demo.Entity.CuentasExceptuadas;
import com.example.demo.Entity.Fuente;
import com.example.demo.Entity.SolicitudesCrearUsuario;
import com.example.demo.Entity.Usuarios;
import com.example.demo.Paginador.Paginador;
import com.example.demo.service.CargarArchivoServer;
import com.example.demo.service.SolicitudesCrearUsuarioService;
import com.example.demo.service.UsuariosService;
import com.example.demo.service.fuenteService;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;


@Controller
public class ControllerFuente {
	
	@Autowired
	private UsuariosService usuarioService;
	
	@Autowired
	private SolicitudesCrearUsuarioService SolicitudService;
	
	@Autowired
	private fuenteService serviceFuente;
	
	@Autowired
	private FuenteParametroDao fuenteParametroDao;
	
	@Autowired
	private CargarArchivoServer cargarArchivo;
	
	private String notificacion = "";

	private RegistrosLog registrolog = new RegistrosLog();
	// ---------------------------- controller Fuente // -------------------------------- //

	
		@GetMapping("/CrearFuente")
		public String CrearFuente(ModelMap mp , Principal principal) {
			
			mp.put("fuentes", new Fuente());
			
			String NombreUsuarioLogeado = principal.getName();

			DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

			String date = dateFormat.format(Calendar.getInstance().getTime());
					
			String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ ingreso a formulario crear fuente ]";
									
			registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);

			return "ParametroFuenteCrear";
		}
		
		
		
		@RequestMapping("/guardarFuente")
		public String guardarFuente(Model model, Fuente fuentes , Principal principal) {
			
			//System.out.print("la longitud de la fuente es de : " + fuentes.getFuenteParametro().length());
			
			if(fuentes.getFuenteParametro().length() == 3) {
				
				fuentes.setEstado("activo");
				fuenteParametroDao.save(fuentes);
				
				//System.out.print("La fuente " + fuentes.getFuenteParametro() + "se ha guardado correctamente ");
				
				String NombreUsuarioLogeado = principal.getName();
				
				
				DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");
				
				String date = dateFormat.format(Calendar.getInstance().getTime());
				
				String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ se guardo la fuente correctamente]";
				
				registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);
				
			}else {
				
			String NombreUsuarioLogeado = principal.getName();
			
			
			DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");
			
			String date = dateFormat.format(Calendar.getInstance().getTime());
			
			String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ no se pudo guardar la fuente por que excede el limite de longitud]";
			
			registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);
				
				//System.out.print("la longitud de la fuente  : " + fuentes.getFuenteParametro() + "Exece la longitud" + fuentes.getFuenteParametro().length());
			}
			
			
			return "redirect:/listarFuentes";
		}
		

		
		@RequestMapping(value = "/EliminarFuente/{id}" , method = RequestMethod.GET)
		public String EliminarFuente (@PathVariable("id") long id, HttpServletRequest request,ModelMap mp , Principal principal , RedirectAttributes redirectAttributes) {

			String NombreUsuarioLogeado = principal.getName();
			
			serviceFuente.eliminarFuente(id);
			
			DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

			String date = dateFormat.format(Calendar.getInstance().getTime());

			String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ Elimino la fuente con el id : " + id + "]";

			registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);
			
			redirectAttributes.addFlashAttribute("notificacion", "El parámetro fuente ha sido eliminado ");
			
			
			return "redirect:/listarFuentes";
		} 
		
		@PostMapping("/actualizarFuente")
		public String ActualizarFuente (ModelMap mp , Fuente fuentes , Principal principal) {

			//System.out.println("nuevo fuente");
			//System.out.println("fuente " + fuentes.getFuente());
			//System.out.println("Nombre fuente " + fuentes.getFuente());
			//System.out.println("Codifo fuente:  " + fuentes.getId());
			
			String NombreUsuarioLogeado = principal.getName();
			
			DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

			String date = dateFormat.format(Calendar.getInstance().getTime());
			
			String NuevoNombre = fuentes.getNombreFuente();
			String NuevaFuente = fuentes.getFuenteParametro();
			
			if(NuevoNombre.isEmpty() == true) {
				
				//System.out.println("El nombre no puede estar vacion");
				
				return "ParametroFuenteEditar";
				
			}else {
				
				fuentes.setNombreFuente(NuevoNombre);
				fuentes.setFuenteParametro(NuevaFuente);
				fuentes.setEstado("activo");
				fuenteParametroDao.save(fuentes);
				
				String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ Actualizo la fuente ] ";

				registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);
				
				//System.out.println("dato actualizado exitosamente");
				
			}

			
			return "redirect:/listarFuentes";
		} 
		
		
		
		@RequestMapping(value = "/EditarFuente/{id}" , method = RequestMethod.GET)
		public String ModificarFuente (@PathVariable("id") long id, ModelMap mp , HttpServletRequest request ,Fuente fuente , Principal principal) {
			
			Fuente fecha = serviceFuente.findOne(id);
			
			mp.put("fuentes", fecha);
			
			String NombreUsuarioLogeado = principal.getName();

			DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

			String date = dateFormat.format(Calendar.getInstance().getTime());

			String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ Edito la fuente con el id : " + id + "]";

			registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);

			return "ParametroFuenteEditar";
		}

		
		@RequestMapping(value = "/listarFuentes" , method = RequestMethod.GET)
		public String listarFuentes(@RequestParam(name = "page" , defaultValue = "0")  int page ,  ModelMap mp , Model model , Principal principal) {
			
			
			String NombreUsuarioLogeado = principal.getName();
			
			Usuarios usuario = usuarioService.buscarUsuario(NombreUsuarioLogeado);
			
			String Rol = usuario.getRol().get(0).getName();
			
			mp.put("Area", usuario.getArea().getArea());
			
			mp.put("rol", Rol);
			
			
			Pageable pageRequest = PageRequest.of(page, 10);
			
			 Page<Fuente> fuentes = serviceFuente.ListaFuentePagina(pageRequest);
			 
			 //System.out.println("el total de elementos es " + fuentes.getTotalElements());
			 
			 Paginador <Fuente> pageRender = new Paginador<>("/listarFuentes" , fuentes);
			 
			 List<SolicitudesCrearUsuario> cantidadSolicitudes = SolicitudService.cantidadSolicitudes("Enviada");
				
				List<Usuarios> ListaUsuarioCambioCon = usuarioService.listaUsuariosCambio("Olvidada");
				
				int CatidadSolicitudes = cantidadSolicitudes.size();
				
				mp.put("CatidadSolicitudes", CatidadSolicitudes);
				
				int cambiodecontra = ListaUsuarioCambioCon.size();
				
				mp.put("CatidadSolicitudesCambio", cambiodecontra);
			 
			 long Totalfuentes = fuentes.getTotalElements();
			 
			 String notificacion = (String) model.asMap().get("notificacion");


			mp.put("totalRegistros", Totalfuentes);
			mp.put("Fuente", fuentes);
			mp.put("page", pageRender);
			mp.put("notificacion", notificacion);

			DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

			String date = dateFormat.format(Calendar.getInstance().getTime());
					
			String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ Lista de fuentes ]";
									
			registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);
			
			

			return "ParametroFuenteLista";
		}

		@RequestMapping(value = "/Desabilitar/{id}", method = RequestMethod.GET)
		public String DesabilitarFuente(@PathVariable("id") long id, HttpServletRequest request, ModelMap mp , Principal principal , RedirectAttributes redirectAttributes) {

			Fuente fuente = null;

			fuente = serviceFuente.findOne(id);

			fuente.setEstado("desabilitado");

			fuenteParametroDao.save(fuente);
			
			String NombreUsuarioLogeado = principal.getName();

			DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

			String date = dateFormat.format(Calendar.getInstance().getTime());

			String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ Desabilito el la fuente con le id : " + id + "]";

			registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);
			
			redirectAttributes.addFlashAttribute("notificacion", "El parámetro fuente ha sido deshabilitado");

			return "redirect:/listarFuentes";
		}

		@RequestMapping(value = "/habilitar/{id}", method = RequestMethod.GET)
		public String HbailitarFuente(@PathVariable("id") long id, HttpServletRequest request, ModelMap mp , Principal principal , RedirectAttributes redirectAttributes) {

			String NombreUsuarioLogeado = principal.getName();
			
			Fuente fuente = null;

			fuente = serviceFuente.findOne(id);

			fuente.setEstado("Activo");

			fuenteParametroDao.save(fuente);
			
			DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

			String date = dateFormat.format(Calendar.getInstance().getTime());

			String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ habilito la fuente con el id : " + id + "]";

			redirectAttributes.addFlashAttribute("notificacion", "El parámetro fuente ha sido habilitado");
			
			return "redirect:/listarFuentes";
		}

		@GetMapping("/cargarFuenteMasiva")
		public String CargarFuenteMasiva(Model model , RedirectAttributes redirectAttributes , Principal principal) {

			//String NombreArchivo = "Fuentes";

			String NombreArchivoFuentes = (String) model.asMap().get("NombreArchivo");
			
			String NombreUsuarioLogeado = principal.getName();
			
			//String ruta = "..\\insumos\\" + NombreArchivoFuentes;
			
			//C:\Users\andnei662\eclipse-workspace\insumos\Fuentes.xlsx
			
			String ruta = "..\\Fuentes\\" + NombreArchivoFuentes;
			
			System.out.println(ruta);

			FileInputStream file;

			try {

				file = new FileInputStream(new File(ruta));
				Workbook workbook = new XSSFWorkbook(file);

				org.apache.poi.ss.usermodel.Sheet sheet = workbook.getSheetAt(0);
				ZipSecureFile.setMinInflateRatio(0);

				for (Row row : sheet) {

					int contador = 0; // contador de columna

					//System.out.println("Numero de fila: " + row.getRowNum());
					//System.out.println("");
					

					if (row.getRowNum() == 0) {

						//System.out.println("saltar fila ya que es la cabezera del archivo ");
						//System.out.println("");

					} else {
						
						Fuente fuente = new Fuente();
						

						for (Cell cell : row) {
							
							contador = contador + 1;

							switch (cell.getCellType()) {
							
							case STRING:

								if (contador == 1) { // COLUMNA CODIGO FUENTE STRING

									String FuenteString = cell.getRichStringCellValue().getString();
									
									//System.out.println("La fuentes " + FuenteString);
									
									fuente.setFuenteParametro(FuenteString);
								

								}

								if (contador == 2) { // COLUMNA NOMBRE FUENTE

									String NombreFuente = cell.getRichStringCellValue().getString();		
									
									//System.out.print("Nombre " + NombreFuente);
									
									fuente.setNombreFuente(NombreFuente);

								}


								break;

							case NUMERIC:


								if (contador == 1) { // COLUMNA CODIGO FUENTE NUMERICA

									double FuenteNumerica = cell.getNumericCellValue();

									String Fuente = String.valueOf(FuenteNumerica);

									//System.out.println("La fuentes " + Fuente);
									
									fuente.setFuenteParametro(Fuente);
			
								}
			
							default:
								break;

							}

						}
						
						
						if (contador != 2) {
							
							notificacion = "El archivo " + NombreArchivoFuentes + " no cumple con la estructura";
							
							redirectAttributes.addFlashAttribute("notificacion",notificacion);
							
							return "redirect:/listarFuentes";
							
						}
						
						fuente.setEstado("Activo");

						/*System.out.println("Fuente");
						System.out.println("id " + fuente.getId());
						System.out.println("Fuente " + fuente.getFuenteParametro());
						System.out.println("Nombre " + fuente.getNombreFuente());
						System.out.println("Estado " + fuente.getEstado());*/
						
						serviceFuente.GuardarFuente(fuente);
					}
					

				}
				
				workbook.close();
				//System.out.println("Guardado exitosamente");

			} catch (FileNotFoundException e) {
				
				DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

				String date = dateFormat.format(Calendar.getInstance().getTime());
						
				String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ No se pudo leer la ruta " + ruta + " ]";
										
				registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);

				//System.out.print("No se a cargado ninguna lote para validar por favor seleccionar un lote");

			} catch (IOException e) {
				
				DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

				String date = dateFormat.format(Calendar.getInstance().getTime());
						
				String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ No se pudo leer la ruta " + ruta + " ]";
										
				registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);

				e.printStackTrace();
			}
			
			DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

			String date = dateFormat.format(Calendar.getInstance().getTime());
					
			String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ el archivo de fuentes con la ruta  " + ruta + " fue cargado correctamente ]";
									
			registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);
			
			redirectAttributes.addFlashAttribute("notificacion", "Fuentes cargadas correctamente");

			return "redirect:/listarFuentes";
		}
		


		@PostMapping("/ValidarExistenciaFuenteCarpeta")
		String validarExistenciaCarpeta(@RequestParam("file") MultipartFile file , RedirectAttributes redirectAttributes , Model model) throws IOException {
			
			String NombreArchivo = file.getOriginalFilename();
			
			//System.out.println("el archivo de area es : " + NombreArchivo);
			
			//String NombreArchivo =  dato.replaceAll("[xlsx]", "");

			DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

			String date = dateFormat.format(Calendar.getInstance().getTime());
			
			String ruta = "..\\Fuentes";

			// esta carpeta se creara cada vez que se descargue y se llene un plano
			File directorio = new File(ruta);
			

			// ---------------- validamos que en la carpeta areas no exista el archivo si existe se elimina para la llegada del nuevo archivo areas // -----------//
			


			if (directorio.exists()) {

				File directory = new File(ruta);
					
				FileUtils.cleanDirectory(directory);
					
				cargarArchivo.save(file,ruta);
					
				redirectAttributes.addFlashAttribute("NombreArchivo",NombreArchivo);
				
				String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + "sistema" + "] " + "Tarea" + " [ Creo la carpeta Areas dentro del servidor ]";
				
				registrolog.guardarRegistro("sistema", tarea);

				return "redirect:/ActualizarFuenteMasiva";

			}
			
			else {
				
				 cargarArchivo.init(ruta);
				 
				 cargarArchivo.save(file,ruta);
				 
				 redirectAttributes.addFlashAttribute("NombreArchivo",NombreArchivo);
				 
				String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + "sistema" + "] " + "Tarea" + " [ Creo la carpeta Areas dentro del servidor ]";
					
				registrolog.guardarRegistro("sistema", tarea);
				 
				 return "redirect:/cargarFuenteMasiva";
				
			}

		}
		
		
		@GetMapping("/notificacionPlantillaFuentes")
		public String NotificacionPlantilla (Model model , RedirectAttributes redirectAttributes) {
			
			String NombreArchivoFuentes = (String) model.asMap().get("NombreArchivo");
			
			notificacion = "El archivo " + NombreArchivoFuentes + "no existe en la ruta, por favor guardar el archivo en la carpeta insumos, para poder cargar o actualizar las fuentes";
			
			redirectAttributes.addFlashAttribute("notificacion", notificacion);
			
			return "redirect:/listarFuentes";
		}
		
		
		@GetMapping("/ActualizarFuenteMasiva")
		public String ActualizarCargaMasiva (ModelMap mp , Model model , RedirectAttributes redirectAttributes , Principal principal ) {
			
			String NombreArchivoFuentes = (String) model.asMap().get("NombreArchivo");
			
			redirectAttributes.addFlashAttribute("NombreArchivo",NombreArchivoFuentes);
			
			serviceFuente.ActualizarCargarMasivaFuente();
			
			String NombreUsuarioLogeado = principal.getName();

			DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

			String date = dateFormat.format(Calendar.getInstance().getTime());
					
			String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ actualizo las fuentes correctamente ]";
									
			registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);
			
			return "redirect:/cargarFuenteMasiva";
		}
		
		@PostMapping("/BuscarFuente")
		public String BuscarFuente(@RequestParam(value = "dato", required = false) String dato, ModelMap mp , Principal principal , RedirectAttributes redirectAttributes) {

			String NombreUsuarioLogeado = principal.getName();
			
			Usuarios usuario = usuarioService.buscarUsuario(NombreUsuarioLogeado);
			
			String Rol = usuario.getRol().get(0).getName();
			
			mp.put("Area", usuario.getArea().getArea());
			
			mp.put("rol", Rol);		

			List<Fuente> FuenteBuscar = serviceFuente.encontrarFuente(dato);
			
			if (FuenteBuscar.isEmpty()) {
				
				redirectAttributes.addFlashAttribute("notificacion", "la fuente " + dato + " No existe en el sistema");
				
				return "redirect:/listarFuentes";
			}

			mp.put("Fuente", FuenteBuscar);
			

			DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

			String date = dateFormat.format(Calendar.getInstance().getTime());

			String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ busco la fuente " + dato + "]";

			registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);

			return "ParametroFuenteBuscar";

		}
		
		@GetMapping("/activarParametroFuente")
		public String activarParametroFuente(ModelMap mp , RedirectAttributes redirectAttributes) {
			
			
			List<Fuente> Fuente = serviceFuente.listarFuente();
			
			for (int i = 0; i < Fuente.size(); i++) {
				
				long id = Fuente.get(i).getId();
				
				Fuente FuenteActual = serviceFuente.findOne(id);
				
				FuenteActual.setEstado("Activo");
				
				serviceFuente.GuardarFuente(FuenteActual);
				
			}
			
			redirectAttributes.addFlashAttribute("notificacion", "Todos los parámetros se han sido activados");
			
			return "redirect:/listarFuentes";
			
		}
		
		
		@GetMapping("/DesactivarParametroFuente")
		public String DesactivarParametroCuentaExcep(ModelMap mp , RedirectAttributes redirectAttributes) {
			
			List<Fuente> Fuente = serviceFuente.listarFuente();
			
			for (int i = 0; i < Fuente.size(); i++) {
				
				long id = Fuente.get(i).getId();
				
				Fuente FuenteActual = serviceFuente.findOne(id);
				
				FuenteActual.setEstado("desactivado");
				
				serviceFuente.GuardarFuente(FuenteActual);
				
			}
			
			redirectAttributes.addFlashAttribute("notificacion", "Todos los parámetros han sido desactivados, este parámetro no se tendrá en cuenta al momento de la validación de un lote");
			
			return "redirect:/listarFuentes";
		}


}
