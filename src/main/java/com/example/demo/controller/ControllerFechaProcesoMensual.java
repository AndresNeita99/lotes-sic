package com.example.demo.controller;

import java.sql.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.example.demo.Entity.FechaProcesoMensual;
import com.example.demo.service.FechaProcesoMensualService;
import jakarta.servlet.http.HttpServletRequest;

@Controller
public class ControllerFechaProcesoMensual {

	@Autowired
	private FechaProcesoMensualService fechaprocesoservicemensual;

	@GetMapping("/listafechasprocesoMensual")
	public String ListaFechasProcesoMensual(ModelMap mp) {

		mp.put("fechasProcesoMensual", fechaprocesoservicemensual.listarFechasProcesoMensual());

		return "ParametrofechasProcesoMenLista";

	}

	@RequestMapping("/crearfechaprocesoMensualManual")
	public String crearFechaProcesoManu(ModelMap mp, FechaProcesoMensual fechaproMensual) {

		List<FechaProcesoMensual> fechaProceso = fechaprocesoservicemensual.listarFechasProcesoMensual();

		if (fechaProceso.isEmpty()) {

			//System.out.print("No hay Ninguna fecha de proceso registrada , favor ingresar una fecha manual o auto");
			
			mp.put("fechaProcesoMensual", fechaproMensual);
			
			return "ParametrofechasProcesoMenManualCrear";

		}

		else {

			for (int i = 0; i < fechaProceso.size(); i++) {

				String Estado = fechaProceso.get(i).getEstado();
				String Modo = fechaProceso.get(i).getModo();
				
				//System.out.print("numero de filas " + fechaProceso.size());
	
			
				if (Estado == "Activo" && Modo == "Automatico") {

					//System.out.print("por favor desabilite todas las fechas automaticas");
					
					return "redirect:/listafechasprocesoMensual";
				}

			}
			
			mp.put("fechaProcesoMensual", fechaproMensual);

			return "ParametrofechasProcesoMenManualCrear";

		}

	}

	@RequestMapping("/crearfechaprocesoMensualAuto")
	public String crearFechaProcesoAuto(ModelMap mp, FechaProcesoMensual fechaproMensual) {

		List<FechaProcesoMensual> fechaProceso = fechaprocesoservicemensual.listarFechasProcesoMensual();

		if (fechaProceso.isEmpty()) {

			//System.out.print("No hay Ninguna fecha de proceso registrada , favor ingresar una fecha manual o auto");
			
			mp.put("fechaProcesoMensual", fechaproMensual);
			
			return "ParametrofechasProcesoMenAutoCrear";

		}

		else {

			for (int i = 0; i < fechaProceso.size(); i++) {

				String Estado = fechaProceso.get(i).getEstado();
				String Modo = fechaProceso.get(i).getModo();
				
				//System.out.print("numero de filas " + fechaProceso.size());
	
			
				if (Estado == "Activo" && Modo == "Manual") {

					//System.out.print("por favor desabilite todas las fechas manuales");
					
					return "redirect:/listafechasprocesoMensual";
				}

			}
		}
			
			mp.put("fechaProceso", fechaproMensual);

		return "ParametrofechasProcesoMenAutoCrear";
	}

	@RequestMapping("/GuardarFechaProcesoMensualAuto")
	public String GuardarFechaProcesoAuto(ModelMap mp, FechaProcesoMensual fechaproMensual, HttpServletRequest request) {


		fechaproMensual.setTipoCargue("Mensual");
		fechaproMensual.setModo("Automatico");
		fechaproMensual.setEstado("Activo");

		fechaprocesoservicemensual.guardarFechaProcesoMensual(fechaproMensual);

		return "redirect:/listafechasprocesoMensual";
	}

	@RequestMapping("/GuardarFechaProcesoMensualManual")
	public String GuardarFechaProceso(ModelMap mp, FechaProcesoMensual fechaproMensual, HttpServletRequest request) {

		//int Dia1 = fechaproMensual.getFecha_final().getDate();
		
		//int Dia2 = fechaproMensual.getFecha_Inicial().getDate();
		
		//int dias = Dia1 - Dia2;
	
		//fechaproMensual.setDias(dias);

		fechaproMensual.setTipoCargue("Mensual");
		fechaproMensual.setModo("Manual");
		fechaproMensual.setEstado("Activo");

		fechaprocesoservicemensual.guardarFechaProcesoMensual(fechaproMensual);

		return "redirect:/listafechasprocesoMensual";
	}

	@RequestMapping(value = "/EliminarFechaProcesoMensual/{id}", method = RequestMethod.GET)
	public String EliminarFechaProceso(@PathVariable("id") long id, HttpServletRequest request, ModelMap mp) {

		fechaprocesoservicemensual.EliminarFechaProcesoMensual(id);

		return "redirect:/listafechasprocesoMensual";
	}

	@RequestMapping(value = "/EditarFechaProcesoMensual/{id}", method = RequestMethod.GET)
	public String ModificarFuente(@PathVariable("id") long id, ModelMap mp, HttpServletRequest request, FechaProcesoMensual fechaproMensual) {

		FechaProcesoMensual fecha = fechaprocesoservicemensual.BuscarFechaProcesoMensualId(id);
		

		mp.put("fechaProcesoMensual", fecha);

		return "ParametrofechasProcesoMenEditar";
	}

	@PostMapping("/actualizarFechaProcesoMensual")
	public String ActualizarFuente(ModelMap mp, FechaProcesoMensual fechaproMensual , HttpServletRequest request) {

		String NuevaHora = fechaproMensual.getHoraMaxima();
		String Modo = fechaproMensual.getModo();
		
		//System.out.println("el valor de ultimo es " + fechaproMensual.getUltimoDia());

		if (fechaproMensual.getDias() <= 0) {

			fechaproMensual.setUltimoDia("Ultimo dia del mes");
			fechaproMensual.setDias(0);

		} else {
			
			fechaproMensual.getDias();
			fechaproMensual.setUltimoDia("");
		}
		
		if (fechaproMensual.getFecha_final() != null && fechaproMensual.getFecha_Inicial() != null) {
			
			Date NuevaFechaFinal = fechaproMensual.getFecha_final();
			Date NuevaFechaInicial = fechaproMensual.getFecha_Inicial();
			
	
			fechaproMensual.setFecha_final(NuevaFechaFinal);
			fechaproMensual.setFecha_Inicial(NuevaFechaInicial);
			
		}

		fechaproMensual.setTipoCargue("Mensual");
		fechaproMensual.setModo(Modo);
		fechaproMensual.setEstado("Activo");
		fechaproMensual.setHoraMaxima(NuevaHora);

		fechaprocesoservicemensual.guardarFechaProcesoMensual(fechaproMensual);

		//System.out.println("dato actualizado exitosamente");

		return "redirect:/listafechasprocesoMensual";
	}

	@RequestMapping(value = "/DesabilitarFechaProcesoMensual/{id}", method = RequestMethod.GET)
	public String DesabilitarFuente(@PathVariable("id") long id, HttpServletRequest request, ModelMap mp) {

		FechaProcesoMensual fechaProceso = null;

		fechaProceso = fechaprocesoservicemensual.BuscarFechaProcesoMensualId(id);

		fechaProceso.setEstado("Inactivo");

		fechaprocesoservicemensual.guardarFechaProcesoMensual(fechaProceso);

		return "redirect:/listafechasprocesoMensual";
	}

	@RequestMapping(value = "/habilitarFechaProcesoMensual/{id}", method = RequestMethod.GET)
	public String HbailitarFuente(@PathVariable("id") long id, HttpServletRequest request, ModelMap mp) {

		FechaProcesoMensual fechaProceso = null;

		fechaProceso = fechaprocesoservicemensual.BuscarFechaProcesoMensualId(id);

		fechaProceso.setEstado("Activo");

		fechaprocesoservicemensual.guardarFechaProcesoMensual(fechaProceso);

		return "redirect:/listafechasprocesoMensual";
	}
}
