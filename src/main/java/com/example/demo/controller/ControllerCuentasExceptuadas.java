package com.example.demo.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.Principal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.poi.openxml4j.util.ZipSecureFile;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.example.demo.RegistrosLog;

import com.example.demo.Entity.CuentasExceptuadas;
import com.example.demo.Entity.SolicitudesCrearUsuario;
import com.example.demo.Entity.Usuarios;
import com.example.demo.Paginador.Paginador;
import com.example.demo.service.CargarArchivoServer;
import com.example.demo.service.CuentasExceptuadasService;
import com.example.demo.service.SolicitudesCrearUsuarioService;
import com.example.demo.service.UsuariosService;

import jakarta.servlet.http.HttpServletRequest;

@Controller
public class ControllerCuentasExceptuadas {

	@Autowired
	private CuentasExceptuadasService cuentasExcepService;
	
	@Autowired
	private SolicitudesCrearUsuarioService SolicitudService;

	@Autowired
	private UsuariosService usuarioService;

	@Autowired
	private CargarArchivoServer cargarArchivo;

	private RegistrosLog registrolog = new RegistrosLog();

	private String notifiacion = "";

	@RequestMapping(value = "/listaCuentasExcep", method = RequestMethod.GET)
	public String ListaCuentasExcep(@RequestParam(name = "page", defaultValue = "0") int page, ModelMap mp, Model model, Principal principal) {

		String notificacion = (String) model.asMap().get("notificacion");

		String NombreUsuarioLogeado = principal.getName();

		Usuarios usuario = usuarioService.buscarUsuario(NombreUsuarioLogeado);

		String Rol = usuario.getRol().get(0).getName();

		mp.put("Area", usuario.getArea().getArea());

		mp.put("rol", Rol);

		Pageable pageRequest = PageRequest.of(page, 10);

		Page<CuentasExceptuadas> cuentasExep = cuentasExcepService.listaCuentasExcepci(pageRequest);

		Paginador<CuentasExceptuadas> pageRender = new Paginador<>("/listaCuentasExcep", cuentasExep);
		
		List<SolicitudesCrearUsuario> cantidadSolicitudes = SolicitudService.cantidadSolicitudes("Enviada");
		
		List<Usuarios> ListaUsuarioCambioCon = usuarioService.listaUsuariosCambio("Olvidada");
		
		int CatidadSolicitudes = cantidadSolicitudes.size();
		
		mp.put("CatidadSolicitudes", CatidadSolicitudes);
		
		int cambiodecontra = ListaUsuarioCambioCon.size();
		
		mp.put("CatidadSolicitudesCambio", cambiodecontra);


		long totalRegistros = cuentasExep.getTotalElements();

		mp.put("notificacion", notificacion);
		mp.put("totalRegistros", totalRegistros);
		mp.put("listaCuentasExep", cuentasExep);
		mp.put("page", pageRender);

		return "ParametroCuentasExcepListar";
	}

	@RequestMapping(value = "/editarCuentaExcep/{id}")
	public String editarCuentaExcep(@PathVariable("id") long id, ModelMap mp) {

		CuentasExceptuadas cuentaExcepEditar = cuentasExcepService.BuscarCuentaExep(id);

		mp.addAttribute("cuentaEditar", cuentaExcepEditar);

		return "ParametroCuentasExcepEditar";
	}
	
	@PostMapping("/guardarCambiosCuentaExcep")
	public String guardarCambiosCuentaExce (Model model , CuentasExceptuadas cuentas , RedirectAttributes redirectAttributes) {
		
		long id = cuentas.getId();
		
		CuentasExceptuadas cuenta = cuentasExcepService.BuscarCuentaExep(id);
		
		cuentasExcepService.guardarCuentaExcep(cuenta);
		
		notifiacion = "Cuentas actualizadass correctamente";
		
		redirectAttributes.addAttribute(notifiacion, redirectAttributes);
		
		return "redirect:/listaCuentasExcep";
	}

	@GetMapping("/CargarAreasMasivaCuentaExcep")
	public String CargasCuentasMasivas(Model model, ModelMap mp, RedirectAttributes redirectAttributes,
			Principal principal) {

		String NombreUsuarioLogeado = principal.getName();

		String NombreArchivo = (String) model.asMap().get("NombreArchivo");

		String ruta = "..\\CuentaExcep\\" + NombreArchivo;

		FileInputStream file;

		try {

			file = new FileInputStream(new File(ruta));
			Workbook workbook = new XSSFWorkbook(file);

			org.apache.poi.ss.usermodel.Sheet sheet = workbook.getSheetAt(0);
			ZipSecureFile.setMinInflateRatio(0);

			for (Row row : sheet) {

				CuentasExceptuadas CuentasExcep = new CuentasExceptuadas();

				int contador = 0; // contador de columna

				
				//System.out.println("Numero de fila: " + row.getRowNum());
				
				//System.out.println("");

				if (row.getRowNum() == 0) {

					// System.out.println("saltar fila ya que es la cabezera del archivo ");
					// System.out.println("");

				}

				else {

					for (Cell cell : row) {

						contador = contador + 1;

						switch (cell.getCellType()) {

						case STRING:

							if (contador == 1) { // COLUMNA CODIGO FUENTE NUMERICA

								String Cuenta = cell.getRichStringCellValue().getString();

								CuentasExcep.setCuenta(Cuenta);

								//System.out.println("Numero cuenta es : " + Cuenta);

							}

							if (contador == 2) { // Columna nombre de cuenta

								String DescripCuentaExcep = cell.getRichStringCellValue().getString();

								//System.out.println("Descripcion de la cuenta es : " + DescripCuentaExcep);

								CuentasExcep.setDescripcion(DescripCuentaExcep);

							}

							break;
						default:
							break;

						}


					}
					
					if (contador != 2) {
						
						notifiacion = "El archivo " + NombreArchivo + " no cumple con la estructura";
						
						redirectAttributes.addFlashAttribute("notificacion",notifiacion);
						
						return "redirect:/listaAreas";
						
					}
					
					CuentasExcep.setEstado("Activo");
					
					cuentasExcepService.guardarCuentaExcep(CuentasExcep);

				}

			}

			workbook.close();

		} catch (FileNotFoundException e) {

			// System.out.print("No se a cargado ninguna Archivo de area para cargar");

			DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

			String date = dateFormat.format(Calendar.getInstance().getTime());

			String tarea = "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ No se pudo cargar el archivo en la ruta " + ruta + " de manera exitosa ] ";

			registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);

		} catch (IOException e) {

			e.printStackTrace();

			DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

			String date = dateFormat.format(Calendar.getInstance().getTime());

			String tarea = "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ No se pudo cargar el archivo en la ruta " + ruta + " de manera exitosa ] ";

			registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);

		}

		DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

		String date = dateFormat.format(Calendar.getInstance().getTime());

		String tarea = "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ cargo el archivo en la ruta " + ruta + " de manera exitosa ] ";

		registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);

		notifiacion = "Cuentas Exceptuadas cargadas correctamente";

		redirectAttributes.addFlashAttribute("notificacion", notifiacion);

		return "redirect:/listaCuentasExcep";
	}

	@PostMapping("/ValidarExistenciaArchivoCuentasExcep")
	public String downloadFile(@RequestParam(value = "dato", required = false) String dato, @RequestParam("file") MultipartFile file, RedirectAttributes redirectAttributes) throws Exception {

		DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

		String date = dateFormat.format(Calendar.getInstance().getTime());

		String NombreArchivo = file.getOriginalFilename();

		// System.out.println("el archivo de area es : " + NombreArchivo);

		// String NombreArchivo = dato.replaceAll("[xlsx]", "");

		String ruta = "..\\CuentaExcep";

		// esta carpeta se creara cada vez que se descargue y se llene un plano
		File directorio = new File(ruta);

		// ---------------- validamos que en la carpeta areas no exista el archivo si
		// existe se elimina para la llegada del nuevo archivo areas // -----------//

		if (directorio.exists()) {

			File directory = new File(ruta);

			FileUtils.cleanDirectory(directory);

			cargarArchivo.save(file, ruta);

			redirectAttributes.addFlashAttribute("NombreArchivo", NombreArchivo);

			String tarea = "Fecha Y hora [" + date + "]  Usuario [" + "sistema" + "] " + "Tarea" + " [ Creo la carpeta Areas dentro del servidor ]";

			registrolog.guardarRegistro("sistema", tarea);

			return "redirect:/ActualizarCuentasExcep";

		}

		else {

			cargarArchivo.init(ruta);

			cargarArchivo.save(file, ruta);

			redirectAttributes.addFlashAttribute("NombreArchivo", NombreArchivo);

			String tarea = "Fecha Y hora [" + date + "]  Usuario [" + "sistema" + "] " + "Tarea" + " [ Creo la carpeta Areas dentro del servidor ]";

			registrolog.guardarRegistro("sistema", tarea);

			return "redirect:/CargarAreasMasivaCuentaExcep";

		}
		
		
		

	}
	
	
	@GetMapping("/ActualizarCuentasExcep")
	public String ActualizarCuentasExcep (RedirectAttributes redirectAttributes , Model model) {
		
		
		String NombreArchivo = (String) model.asMap().get("NombreArchivo");
		
		DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");
		
		String date = dateFormat.format(Calendar.getInstance().getTime());
		
		cuentasExcepService.ActualizarCuentasExcep();
		
		redirectAttributes.addFlashAttribute("NombreArchivo", NombreArchivo);

		String tarea = "Fecha Y hora [" + date + "]  Usuario [" + "sistema" + "] " + "Tarea" + " [ Creo la carpeta Areas dentro del servidor ]";

		registrolog.guardarRegistro("sistema", tarea);
		
		return "redirect:/CargarAreasMasivaCuentaExcep";
	}
	
	@RequestMapping(value = "/HabilitarCuentaExcep/{id}", method = RequestMethod.GET)
	public String HabilitarCuentaExcep(@PathVariable("id") long id, HttpServletRequest request, ModelMap mp , Principal principal , RedirectAttributes redirectAttributes) {
		
		 CuentasExceptuadas CuentaExcep = cuentasExcepService.BuscarCuentaExep(id);
		 
		 String NumeroCuenta = CuentaExcep.getCuenta();
		 
		 CuentaExcep.setEstado("Activo");
		 
		 cuentasExcepService.guardarCuentaExcep(CuentaExcep);
		 
		 notifiacion = "Cuenta " + NumeroCuenta + "habilitada correctamente";
		 
		 redirectAttributes.addFlashAttribute("notificacion", notifiacion);
		
		 
		
		return "redirect:/listaCuentasExcep";
	}
	
	@RequestMapping(value = "/DesabilitarCuentaExcep/{id}", method = RequestMethod.GET)
	public String DesabilitarCuentaExce(@PathVariable("id") long id, HttpServletRequest request, ModelMap mp , Principal principal , RedirectAttributes redirectAttributes) {
		
		
		CuentasExceptuadas CuentaExcep = cuentasExcepService.BuscarCuentaExep(id);
		 
		 String NumeroCuenta = CuentaExcep.getCuenta();
		 
		 CuentaExcep.setEstado("desabilitado");
		 
		 cuentasExcepService.guardarCuentaExcep(CuentaExcep);
		 
		 notifiacion = "Cuenta " + NumeroCuenta + "deshabilitada correctamente";
		 
		 redirectAttributes.addFlashAttribute("notificacion", notifiacion);
		
		return "redirect:/listaCuentasExcep";
	}
	
	@RequestMapping(value = "/ElimarCuentaExcep/{id}", method = RequestMethod.GET)
	public String ElimarCuentaExcep(@PathVariable("id") long id, HttpServletRequest request, ModelMap mp , Principal principal , RedirectAttributes redirectAttributes) {
		
	     cuentasExcepService.EliminarCuentaExcep(id);
		 
		 notifiacion = "Cuenta eliminada correctamente";
		 
		 redirectAttributes.addFlashAttribute("notificacion", notifiacion);
		
		return "redirect:/listaCuentasExcep";
	}
	
	
	@GetMapping("/activarParametroCuentaExcep")
	public String activarParametroCuentaExcep(ModelMap mp , RedirectAttributes redirectAttributes) {
		
		
		List<CuentasExceptuadas> cuentasExcep = cuentasExcepService.listaCuentasExcep();
		
		for (int i = 0; i < cuentasExcep.size(); i++) {
			
			long id = cuentasExcep.get(i).getId();
			
			CuentasExceptuadas CuentaActual = cuentasExcepService.BuscarCuentaExep(id);
			
			CuentaActual.setEstado("Activo");
			
			cuentasExcepService.guardarCuentaExcep(CuentaActual);
			
		}
		
		redirectAttributes.addFlashAttribute("notificacion", "Todos los parámetros han sido activados");
		
		return "redirect:/listaCuentasExcep";
		
	}
	
	
	@GetMapping("/DesactivarParametroCuentaExcep")
	public String DesactivarParametroCuentaExcep(ModelMap mp , RedirectAttributes redirectAttributes) {
		
	List<CuentasExceptuadas> cuentasExcep = cuentasExcepService.listaCuentasExcep();
			
			for (int i = 0; i < cuentasExcep.size(); i++) {
				
				long id = cuentasExcep.get(i).getId();
				
				CuentasExceptuadas CuentaActual = cuentasExcepService.BuscarCuentaExep(id);
				
				CuentaActual.setEstado("desactivado");
				
				cuentasExcepService.guardarCuentaExcep(CuentaActual);
				
			}
			
			redirectAttributes.addFlashAttribute("notificacion", "Todos los parámetros han sido desactivados , no se validarán cuentas exceptuadas al momento de validar un lote");
			
			return "redirect:/listaCuentasExcep";
	}
	
	
}
