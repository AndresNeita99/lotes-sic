package com.example.demo.controller;

import java.security.Principal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.example.demo.RegistrosLog;
import com.example.demo.Entity.Area;
import com.example.demo.Entity.Moneda;
import com.example.demo.Entity.SolicitudesCrearUsuario;
import com.example.demo.Entity.Sucursal;
import com.example.demo.Entity.Usuarios;
import com.example.demo.Paginador.Paginador;
import com.example.demo.service.SolicitudesCrearUsuarioService;
import com.example.demo.service.SucursalService;
import com.example.demo.service.UsuariosService;

import ch.qos.logback.core.model.Model;
import jakarta.servlet.http.HttpServletRequest;

@Controller
public class ControllerSucursal {
	
	@Autowired
	private UsuariosService usuarioService;
	
	@Autowired
	private SolicitudesCrearUsuarioService SolicitudService;
	
	@Autowired
	private SucursalService serviceSucusal;
	
	private RegistrosLog registrolog = new RegistrosLog();
	
	// ---------------------------- controller sucursal // -------------------------------- //

	
		@GetMapping("/CrearSucursal")
		public String crearSucursal (ModelMap mp , Principal principal) {
			

			String NombreUsuarioLogeado = principal.getName();
			
			Usuarios usuario = usuarioService.buscarUsuario(NombreUsuarioLogeado);
			
			String Rol = usuario.getRol().get(0).getName();
			
			mp.put("Area", usuario.getArea().getArea());
			
			mp.put("rol", Rol);
			
			mp.put("sucursal", new Sucursal());
			
			DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

			String date = dateFormat.format(Calendar.getInstance().getTime());

			String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ Accedio al formulario crear hora de corte ]";

			registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);
			
			return "ParametroSucursalCrear";
		}
		
		
		@RequestMapping("/guardarSucursal")
		public String guardarSucursal(Model model, Sucursal sucursal , Principal principal , ModelMap mp) {
			

				String NombreUsuarioLogeado = principal.getName();
				
				Usuarios usuario = usuarioService.buscarUsuario(NombreUsuarioLogeado);
				
				String Rol = usuario.getRol().get(0).getName();
				
				mp.put("Area", usuario.getArea().getArea());
				
				mp.put("rol", Rol);
				
				String NombreSucursal = sucursal.getNombreSucursal();
				int Sucursal = sucursal.getSucursal();
				
				sucursal.setEstado("activo");
				sucursal.setNombreSucursal(NombreSucursal);
				sucursal.setSucursal(Sucursal);
				
				serviceSucusal.GuardarSucursal(sucursal);
				
				//System.out.println("guardado exitosamente");
				
				DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

				String date = dateFormat.format(Calendar.getInstance().getTime());

				String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ guardo la sucursa de manera correcta ]";

				registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);
				
			return "redirect:/listaSucursales";
		}

		@RequestMapping(value = "/listaSucursales" , method = RequestMethod.GET)
		public String ListarSucursales(@RequestParam(name = "page" , defaultValue = "0")  int page, Model model , ModelMap mp , Principal principal) {
			
			String NombreUsuarioLogeado = principal.getName();
			
			Usuarios usuario = usuarioService.buscarUsuario(NombreUsuarioLogeado);
			
			String Rol = usuario.getRol().get(0).getName();
			
			mp.put("Area", usuario.getArea().getArea());
			
			mp.put("rol", Rol);
			
			Pageable pageRequest = PageRequest.of(page, 5);
			
			Page <Sucursal> sucursales = serviceSucusal.ListaAreaPagina(pageRequest);

			//List<Sucursal> sucursales = serviceSucusal.listaSucursales();
			
			Paginador <Sucursal> pageRender = new Paginador<>("/listaSucursales" , sucursales);
			
			List<SolicitudesCrearUsuario> cantidadSolicitudes = SolicitudService.cantidadSolicitudes("Enviada");
			
			List<Usuarios> ListaUsuarioCambioCon = usuarioService.listaUsuariosCambio("Olvidada");
			
			int CatidadSolicitudes = cantidadSolicitudes.size();
			
			mp.put("CatidadSolicitudes", CatidadSolicitudes);
			
			int cambiodecontra = ListaUsuarioCambioCon.size();
			
			mp.put("CatidadSolicitudesCambio", cambiodecontra);
			
			long Total = sucursales.getTotalElements();
			
			mp.put("Total", Total);
			mp.put("page", pageRender);
			mp.put("sucursales", sucursales);
			
			DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

			String date = dateFormat.format(Calendar.getInstance().getTime());

			String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ Accedio a la lista de sucursales ]";

			registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);


			return "ParametroSucursalLista";
		}
		
		
		
		@RequestMapping(value = "/editarSucursal/{id}" , method = RequestMethod.GET)
		public String EditarSucursal (@PathVariable("id") long id , HttpServletRequest request, ModelMap mp , Sucursal sucursal , Principal principal) {
			
			String NombreUsuarioLogeado = principal.getName();
			
			Usuarios usuario = usuarioService.buscarUsuario(NombreUsuarioLogeado);
			
			String Rol = usuario.getRol().get(0).getName();
			
			Sucursal SucuarsalEditar = serviceSucusal.findOne(id);
			
			mp.put("Area", usuario.getArea().getArea());
			
			mp.put("rol", Rol);
			
			mp.put("sucursal", SucuarsalEditar);
			
			DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

			String date = dateFormat.format(Calendar.getInstance().getTime());

			String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ " + "Edito la sucursal con el id : " + id + "]";

			registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);
			
			return "ParametroSucursalEditar";
		}
		
		@PostMapping("/actualizarSucursal")
		public String ActualizarSucursal (ModelMap mp , Sucursal sucursal , Principal principal) {

			//System.out.println("nuevo fuente");
			//System.out.println("fuente " + fuentes.getFuente());
			//System.out.println("Nombre fuente " + fuentes.getFuente());
			//System.out.println("Codifo fuente:  " + fuentes.getId());
			
			String NuevoNombre = sucursal.getNombreSucursal();
			int NuevaSucursal= sucursal.getSucursal();
			
			String NombreUsuarioLogeado = principal.getName();
			
			DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

			String date = dateFormat.format(Calendar.getInstance().getTime());
			
			if(NuevoNombre.isEmpty() == true) {
				
				//System.out.println("El nombre no puede estar vacion");
				
				return "ParametroSucursalEditar";
				
			}else {
				
				sucursal.setSucursal(NuevaSucursal);
				sucursal.setNombreSucursal(NuevoNombre);
				sucursal.setEstado("Activo");
				serviceSucusal.GuardarSucursal(sucursal);
				
				//System.out.println("dato actualizado exitosamente");
				
				String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ la sucursal a sido actualizada correctamente ]";
				
				registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);
				
			}

			
			return "redirect:/listaSucursales";
		} 
		
		
		
		@RequestMapping(value = "/EliminarSucursal/{id}" , method = RequestMethod.GET)
		public String EliminarSucursal (@PathVariable("id") long id , HttpServletRequest request, ModelMap mp , Sucursal sucursal , Principal principal) {
			
			serviceSucusal.eliminarSucursal(id);
			
			String NombreUsuarioLogeado = principal.getName();
			
			DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

			String date = dateFormat.format(Calendar.getInstance().getTime());

			String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ " + "Elimino la sucursal con el id : " + id + "]";

			registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);
			
			return "redirect:/listaSucursales";
		}
		
		

		@RequestMapping(value = "/HabilitarSucursal/{id}", method = RequestMethod.GET)
		public String HbailitarSucursal(@PathVariable("id") long id, HttpServletRequest request, ModelMap mp  , Principal principal) {

			Sucursal sucursal = null;

			sucursal = serviceSucusal.findOne(id);

			sucursal.setEstado("Activo");

			serviceSucusal.GuardarSucursal(sucursal);
			
			String NombreUsuarioLogeado = principal.getName();
			
			DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

			String date = dateFormat.format(Calendar.getInstance().getTime());

			String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ " + "Habilito la sucursal con el id : " + id + "]";

			registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);

			return "redirect:/listaSucursales";
		}

		@RequestMapping(value = "/DesabilitarSucursal/{id}", method = RequestMethod.GET)
		public String DesabilitarSucursal(@PathVariable("id") long id, HttpServletRequest request, ModelMap mp , Principal principal) {

			Sucursal sucursal = null;

			sucursal = serviceSucusal.findOne(id);

			sucursal.setEstado("desabilitado");

			serviceSucusal.GuardarSucursal(sucursal);
			
			String NombreUsuarioLogeado = principal.getName();
			
			DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

			String date = dateFormat.format(Calendar.getInstance().getTime());

			String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ " + "Desabilito la sucursal con el id : " + id + "]";

			registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);

			return "redirect:/listaSucursales";
		}
		
		
		@GetMapping("/activarParametroSucursal")
		public String activarParametroMoneda(ModelMap mp , RedirectAttributes redirectAttributes) {
			
			
			List<Sucursal> sucursal = serviceSucusal.listaSucursales();
			
			for (int i = 0; i < sucursal.size(); i++) {
				
				long id = sucursal.get(i).getId();
				
				Sucursal SucursalActual = serviceSucusal.findOne(id);
				
				SucursalActual.setEstado("Activo");
				
				serviceSucusal.GuardarSucursal(SucursalActual);
				
			}
			
			redirectAttributes.addFlashAttribute("notificacion", "Todos los parámetros se han sido activados");
			
			return "redirect:/listaSucursales";
			
		}
		
		
		@GetMapping("/DesactivarParametroSucursal")
		public String DesactivarParametroMoneda(ModelMap mp , RedirectAttributes redirectAttributes) {
			
			
			List<Sucursal> sucursal = serviceSucusal.listaSucursales();
			
			for (int i = 0; i < sucursal.size(); i++) {
				
				long id = sucursal.get(i).getId();
				
				Sucursal SucursalActual = serviceSucusal.findOne(id);
				
				SucursalActual.setEstado("desactivado");
				
				serviceSucusal.GuardarSucursal(SucursalActual);
				
			}
			
			redirectAttributes.addFlashAttribute("notificacion", "Todos los parámetros se han sido desactivados , Este parametro no se validara en el proceso de cargue lote");
			
			return "redirect:/listaSucursales";
			
		
		}

}
