package com.example.demo.controller;

import java.security.Principal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.example.demo.Entity.Mensaje;
import com.example.demo.Entity.Usuarios;
import com.example.demo.service.MensajeService;
import com.example.demo.service.UsuariosService;

import jakarta.servlet.http.HttpServletRequest;

@Controller 
public class ControllerMensaje {
	
	@Autowired
	public MensajeService mensajeService;
	
	@Autowired
	public UsuariosService usuariosService;
	
	@RequestMapping(value = "/notificaciones", method = RequestMethod.GET)
	public String listaNotificaciones(Model model ,  ModelMap mp , Principal principal , RedirectAttributes redirectAttributes , HttpServletRequest request ) {
		
		long idusuario = usuariosService.buscarUsuario(principal.getName()).getId();
		
		String NombreUsuarioLogeado = principal.getName();
		
		Usuarios usuario = usuariosService.buscarUsuario(NombreUsuarioLogeado);
		
		String nombrePersona = usuario.getNombre();
		
		String Rol = usuario.getRol().get(0).getName();
		
		mp.put("Area", usuario.getArea().getArea());
		
		mp.put("rol", Rol);
		
		System.out.println(mensajeService.ListaMisMensajes(nombrePersona).size());
		
		model.addAttribute("notificaciones", mensajeService.ListaMisMensajes(nombrePersona));
		model.addAttribute("cantidadMensajes", mensajeService.ListaMisMensajes(nombrePersona).size());
		model.addAttribute("cantidadMensa", mensajeService.listaMensajesNoLeidos(nombrePersona, "No leido").size());

		return "Notificacion";
		
	}
	
	
	@GetMapping("eliminarNotificacion/{id}")
	public String EliminarNotificacion (@PathVariable("id") long id , RedirectAttributes redirectAttributes) {
		
		mensajeService.EliminarMensaje(id);
		
		//redirectAttributes.addAttribute("notificacion", "la noti");
		
		return "redirect:/notificaciones";
		
	}
	
	@GetMapping("/marcarcomoleido/{id}")
	public String MarcarMensajeLeido(@PathVariable("id") long id) {
		
		
		
		Mensaje MensajesActual = mensajeService.BuscarMensajeActual(id);
		
		MensajesActual.setEstado("Leido");
		
		mensajeService.enviarMensaje(MensajesActual);
		
		return "redirect:/notificaciones";
	}
	
	
	@GetMapping("/MarcarMensajeLeido")
	public String MarcarTodosMensajeLeido(RedirectAttributes redirectAttributes , ModelMap mp , Principal principal) {
		
		 String nombre = usuariosService.buscarUsuario(principal.getName()).getNombre();
		
		 List<Mensaje> MensajesNoLeidos = mensajeService.listaMensajesNoLeidos(nombre, "No leido");
		
		for (int i = 0; i < MensajesNoLeidos.size(); i++) {
			
			Mensaje mensajeactual = MensajesNoLeidos.get(i);
			
			mensajeactual.setEstado("Leido");
			
			mensajeService.enviarMensaje(mensajeactual);
			
		}
		
		mp.put("notificacion", "Ninguna notificacion por leer");
	
		return "redirect:/notificaciones";
	}
	
	
	@GetMapping("VaciarBuzon")
	public String VaciarBuzon (Principal principal) {
		
		 String nombre = usuariosService.buscarUsuario(principal.getName()).getNombre();
			
		 List<Mensaje> MensajesNoLeidos = mensajeService.ListaMisMensajes(nombre);
		
		for (int i = 0; i < MensajesNoLeidos.size(); i++) {
			
			Mensaje mensajeactual = MensajesNoLeidos.get(i);
			
			mensajeService.EliminarMensaje(mensajeactual.getId());

		}
		
		
		return "redirect:/notificaciones";
	}
	
	/* tomar la fecha mas antigua de la notificaciones y aumentarle los 6 mese y obtener la fecha actual 
	 * y comparar si son iguales es ya pasaron 6 meses desde esa 1 notificacion es decir hay que eliminarla  
	
	*/
	
	
	

}
