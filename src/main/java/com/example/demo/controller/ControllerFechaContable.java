package com.example.demo.controller;
import java.security.Principal;
import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.example.demo.RegistrosLog;
import com.example.demo.Entity.Area;
import com.example.demo.Entity.FechaControlContable;
import com.example.demo.Entity.SolicitudesCrearUsuario;
import com.example.demo.Entity.Usuarios;
import com.example.demo.Paginador.Paginador;
import com.example.demo.service.FechaControlContableService;
import com.example.demo.service.SolicitudesCrearUsuarioService;
import com.example.demo.service.UsuariosService;

import jakarta.servlet.http.HttpServletRequest;



@Controller
public class ControllerFechaContable {
	
	@Autowired
	private UsuariosService usuarioService;
	
	@Autowired
	private SolicitudesCrearUsuarioService SolicitudService;
	
	@Autowired
	public FechaControlContableService fechaContableService;

	private RegistrosLog registrolog = new RegistrosLog();
	

	@RequestMapping(value = "/ListaFechasContable")
	public String ListaFechasContable (@RequestParam(name = "page" , defaultValue = "0")  int page ,ModelMap mp , Model model , Principal principal) {
		
		String NombreUsuarioLogeado = principal.getName();
	
		Usuarios usuario = usuarioService.buscarUsuario(NombreUsuarioLogeado);
		
		String Rol = usuario.getRol().get(0).getName();
		
		mp.put("Area", usuario.getArea().getArea());
		
		mp.put("rol", Rol);
		
		
		String notificacion = (String) model.asMap().get("notificacion");
		
		List<FechaControlContable> listafechasContables = fechaContableService.listaFechasContables();
		
		Pageable pageRequest = PageRequest.of(page, 5);
		
		Page <FechaControlContable> fechasContables = fechaContableService.ListaFechasContablesPagin(pageRequest);

		Paginador <FechaControlContable> pageRender = new Paginador<>("/ListaFechasContable" , fechasContables);
		
		List<SolicitudesCrearUsuario> cantidadSolicitudes = SolicitudService.cantidadSolicitudes("Enviada");
		
		List<Usuarios> ListaUsuarioCambioCon = usuarioService.listaUsuariosCambio("Olvidada");
		
		int CatidadSolicitudes = cantidadSolicitudes.size();
		
		mp.put("CatidadSolicitudes", CatidadSolicitudes);
		
		int cambiodecontra = ListaUsuarioCambioCon.size();
		
		mp.put("CatidadSolicitudesCambio", cambiodecontra);

		
		long TotalfechasContables = fechasContables.getTotalElements();
		
		
		if (listafechasContables.isEmpty()) {
			
			mp.put("notificacion", "No hay ninguna fecha registrada");
		}
		
		
		mp.put("notificacion", notificacion);
		mp.put("fechasContables", listafechasContables);
		mp.put("page", pageRender);
		mp.put("totalAreas", TotalfechasContables);
		
		DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");
		 
        String date = dateFormat.format(Calendar.getInstance().getTime());
		
		String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ ingreso a la lista de fechas contables ]";
						
		registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);
		
		return "ParametrofechasContableLista";
	}
	
	@RequestMapping("/crearFechaContableManual")
	public String CrearFechaContableManu (ModelMap mp , FechaControlContable fechaContable , RedirectAttributes redirectAttributes , Principal principal) {
		
		String NombreUsuarioLogeado = principal.getName();
		
		Usuarios usuario = usuarioService.buscarUsuario(NombreUsuarioLogeado);
		
		String Rol = usuario.getRol().get(0).getName();
		
		mp.put("Area", usuario.getArea().getArea());
		
		mp.put("rol", Rol);
		
		String notificacion = "";
		
		List<FechaControlContable> fechaProceso = fechaContableService.listaFechasContables();

		if (fechaProceso.isEmpty()) {
	
			mp.put("fechasContables", fechaContable);
			
			return "ParametrofechasContableManCrear";

		}

		else {

			for (int i = 0; i < fechaProceso.size(); i++) {

				String Estado = fechaProceso.get(i).getEstado();
				String Modo = fechaProceso.get(i).getModo();
				String Tipo = fechaProceso.get(i).getTipo();
				
				//System.out.print("numero de filas " + fechaProceso.size());
	
			
				if (Estado.contains("Activo") && Modo.contains("Automatico") && Tipo.contains("Diario") || Estado.contains("Activo") && Modo.contains("Manual") && Tipo.contains("Cierre") || Estado.contains("Activo") && Modo.contains("Manual") && Tipo.contains("Diario")) {
					
					notificacion = "Por favor deshabilite todas las fechas automáticas activas, así como las manuales y de cierre. cree una nueva fecha manual o edite la ya establecida";
					
					redirectAttributes.addFlashAttribute("notificacion",notificacion);

					//System.out.print("por favor desabilite todas las fechas automaticas");
					
					return "redirect:/ListaFechasContable";
				}

			}
			
			DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");
			 
	        String date = dateFormat.format(Calendar.getInstance().getTime());
			
			String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ ingreso a el formulario crear fecha contable manual ]";
							
			registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);
			
			mp.put("fechasContables", fechaContable);

			return "ParametrofechasContableManCrear";
		}
	}
	
	
	@RequestMapping("/GuardarFechaContableManu")
	public String GuardarFechaProceso(ModelMap mp, FechaControlContable fechaContable, HttpServletRequest request , Principal principal) {

		//int Dia1 = fechaContable.getFecha_final().getDate();
		
		//int Dia2 = fechaContable.getFecha_Inicial().getDate();
		
		//int dias = Dia1 - Dia2;
	
		//fechaContable.setDias(dias);
		
		String NombreUsuarioLogeado = principal.getName();

		fechaContable.setModo("Manual");
		fechaContable.setTipo("Diario");
		fechaContable.setEstado("Activo");

		fechaContableService.GuardarFechaContable(fechaContable);
		
		DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");
		 
        String date = dateFormat.format(Calendar.getInstance().getTime());
		
		String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ creo la fecha contable manual corretamente ]";
						
		registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);

		return "redirect:/ListaFechasContable";
	}
	
	@RequestMapping("/crearFechaContableAuto")
	public String CrearFechaContableAuto (ModelMap mp , FechaControlContable fechaContable , RedirectAttributes redirectAttributes , Principal principal) {
	
		String NombreUsuarioLogeado = principal.getName();
		
		Usuarios usuario = usuarioService.buscarUsuario(NombreUsuarioLogeado);
		
		String Rol = usuario.getRol().get(0).getName();
		
		mp.put("Area", usuario.getArea().getArea());
		
		mp.put("rol", Rol);
		
		String notificacion = "";
		
		List<FechaControlContable> fechaProceso = fechaContableService.listaFechasContables();

		if (fechaProceso.isEmpty()) {

			notificacion = "Ninguna fecha de proceso registrada";
			
			//System.out.print("No hay Ninguna fecha de proceso registrada");
			
			mp.put("notificacion", notificacion);
			mp.put("fechasContables", fechaContable);
			
			return "ParametrofechasContableAutoCrear";

		}

		else {

			for (int i = 0; i < fechaProceso.size(); i++) {

				String Estado = fechaProceso.get(i).getEstado();
				String Modo = fechaProceso.get(i).getModo();
				String Tipo = fechaProceso.get(i).getTipo();
				
				//System.out.print("numero de filas " + fechaProceso.size());
	
			
				if (Estado.contains("Activo") && Modo.contains("Manual") && Tipo.contains("Cierre") || Estado.contains("Activo") && Modo.contains("Manual") && Tipo.contains("Diario") || Estado.contains("Activo") && Modo.contains("Automatico") && Tipo.contains("Diario")) {

					notificacion = "Por favor deshabilite todas las fechas automáticas activas, así como las manuales y de cierre. cree una nueva fecha manual o edite la ya establecida";
					
					redirectAttributes.addFlashAttribute("notificacion",notificacion);
					
					//System.out.print("por favor desabilite todas las fechas automaticas");
					
					return "redirect:/ListaFechasContable";
				}

			}
			
			
			
			mp.put("fechasContables", fechaContable);
			
			DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");
			 
	        String date = dateFormat.format(Calendar.getInstance().getTime());
			
			String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ creo la fecha contable automatica corretamente ]";
							
			registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);


			return "ParametrofechasContableAutoCrear";
		}
	}
	
	@RequestMapping("/GuardarFechaContableAuto")
	public String GuardarFechaContableAuto(ModelMap mp, FechaControlContable fechaContable , HttpServletRequest request , RedirectAttributes redirectAttributes , Principal principal) {

		String NombreUsuarioLogeado = principal.getName();
		
		String notificacion = "";
		
		notificacion = "Fecha automatica guardada correctemente";
		
		fechaContable.setTipo("Diario");
		fechaContable.setModo("Automatico");
		fechaContable.setEstado("Activo");

		fechaContableService.GuardarFechaContable(fechaContable);

		redirectAttributes.addFlashAttribute("notificacion",notificacion);
		
		
		DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");
		 
        String date = dateFormat.format(Calendar.getInstance().getTime());
		
		String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ Guardo fecha contable automatico correctamente ]";
						
		registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);

		return "redirect:/ListaFechasContable";
	}
	
	
	@RequestMapping("/crearFechaContableCierre")
	public String CrearFechaContableCierre(ModelMap mp , FechaControlContable fechaContable , RedirectAttributes redirectAttributes , Principal principal) {
	
		String NombreUsuarioLogeado = principal.getName();
		
		Usuarios usuario = usuarioService.buscarUsuario(NombreUsuarioLogeado);
		
		String Rol = usuario.getRol().get(0).getName();
		
		mp.put("Area", usuario.getArea().getArea());
		
		mp.put("rol", Rol);
		
		String notificacion = "";
		
		List<FechaControlContable> fechaProceso = fechaContableService.listaFechasContables();

		if (fechaProceso.isEmpty()) {

			notificacion = "No hay Ninguna fecha de proceso registrada";
			
			//System.out.print("No hay Ninguna fecha de proceso registrada");
			
			mp.put("notificacion", notificacion);
			mp.put("fechasContables", fechaContable);
			
			DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");
			 
	        String date = dateFormat.format(Calendar.getInstance().getTime());
			
			String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ No hay ninguna fecha registrada ]";
							
			registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);

			
			return "ParametrofechasContableCierreCrear";

		}

		else {

			for (int i = 0; i < fechaProceso.size(); i++) {

				String Estado = fechaProceso.get(i).getEstado();
				String Modo = fechaProceso.get(i).getModo();
				
				//System.out.print("numero de filas " + fechaProceso.size());
	
			
				if (Estado.contains("Activo") && Modo.contains("Manual") || Estado.contains("Activo") && Modo.contains("Automatico")) {

					notificacion = "por favor deshabilite todas las fechas ";
					
					redirectAttributes.addFlashAttribute("notificacion",notificacion);
					
					//System.out.print("por favor desabilite todas las fechas automaticas");
					
					DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");
					 
			        String date = dateFormat.format(Calendar.getInstance().getTime());
					
					String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ por favor desabilite todas las fechas ]";
									
					registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);

					
					return "redirect:/ListaFechasContable";
				}

			}
			
			DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");
			 
	        String date = dateFormat.format(Calendar.getInstance().getTime());
			
			String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ ingreso al formulario crear fecha de cierre ]";
							
			registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);

			
			mp.put("fechasContables", fechaContable);

			return "ParametrofechasContableCierreCrear";
		}
	}
	
	
	@RequestMapping("/GuardarFechaContableCierre")
	public String GuardarFechaContableCierre(ModelMap mp, FechaControlContable fechaContable , HttpServletRequest request , RedirectAttributes redirectAttributes , Principal principal) {

		String notificacion = "";
		
		notificacion = "Fecha manual de cierre creada correctamente";
		
		fechaContable.setModo("Manual");
		fechaContable.setEstado("Activo");
		fechaContable.setTipo("Cierre");

		fechaContableService.GuardarFechaContable(fechaContable);

		redirectAttributes.addFlashAttribute("notificacion",notificacion);
		
		String NombreUsuarioLogeado = principal.getName();

		DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

		String date = dateFormat.format(Calendar.getInstance().getTime());
				
		String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ Guarda fecha de cierre correctamente ]";
								
		registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);

		return "redirect:/ListaFechasContable";
	}
	
	
	@RequestMapping(value = "/EditarFechaContable/{id}", method = RequestMethod.GET)
	public String ModificarFechaContable(@PathVariable("id") long id, ModelMap mp, HttpServletRequest request, FechaControlContable fechaContable , Principal principal) {

		String NombreUsuarioLogeado = principal.getName();
		
		Usuarios usuario = usuarioService.buscarUsuario(NombreUsuarioLogeado);
		
		String Rol = usuario.getRol().get(0).getName();
		
		mp.put("Area", usuario.getArea().getArea());
		
		mp.put("rol", Rol);
		
		FechaControlContable fecha = fechaContableService.buscarFechaContable(id);

		mp.put("fechasContables", fecha);
		
		DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");
		
		String date = dateFormat.format(Calendar.getInstance().getTime());
		
		String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ edito la fecha contable con el id " + id + "]";
		
		registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);

		return "ParametrofechasContableEditar";
	}
	
	

	@PostMapping("/actualizarFechaContable")
	public String ActualizarFechaContable(ModelMap mp, FechaControlContable fechaContable , RedirectAttributes redirectAttributes , Principal principal) {

		String notificacion = "";

		String NuevaHora = fechaContable.getHoraMaxima();
		//int NuevosDias = fechaContable.getDias();
		String modo = fechaContable.getModo();
		
		

		if (fechaContable.getFecha_final() != null && fechaContable.getFecha_Inicial() != null) {
			
			Date NuevaFechaFinal = fechaContable.getFecha_final();
			Date NuevaFechaInicial = fechaContable.getFecha_Inicial();

			fechaContable.setFecha_final(NuevaFechaFinal);
			fechaContable.setFecha_Inicial(NuevaFechaInicial);
			
		}


		fechaContable.setModo(modo);
		fechaContable.setEstado("Activo");
		fechaContable.setHoraMaxima(NuevaHora);

		fechaContableService.GuardarFechaContable(fechaContable);

		//System.out.println("dato actualizado exitosamente");
		
		notificacion = "Fecha actualizada exitosamente";
		
		redirectAttributes.addFlashAttribute("notificacion",notificacion);
		
		String NombreUsuarioLogeado = principal.getName();

		DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

		String date = dateFormat.format(Calendar.getInstance().getTime());
				
		String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ actualizo la fecha contable correctamente ]";
								
		registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);


		return "redirect:/ListaFechasContable";
	}
	
	
	
	@RequestMapping(value = "/EliminarFechaContable/{id}", method = RequestMethod.GET)
	public String EliminarFechaContable(@PathVariable("id") long id, HttpServletRequest request, ModelMap mp , Principal principal) {

		fechaContableService.EliminarFechaContable(id);
		
		String NombreUsuarioLogeado = principal.getName();

		DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

		String date = dateFormat.format(Calendar.getInstance().getTime());

		String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ elimino la fecha contable con el id :" + id + "]";

		registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);

		return "redirect:/ListaFechasContable";
	}

	
	@RequestMapping(value = "/DesabilitarFechaContable/{id}", method = RequestMethod.GET)
	public String DesabilitarFechaContable(@PathVariable("id") long id, HttpServletRequest request, ModelMap mp , Principal principal) {

		FechaControlContable fechaContable = null;

		fechaContable = fechaContableService.buscarFechaContable(id);

		fechaContable.setEstado("Inactivo");

		fechaContableService.GuardarFechaContable(fechaContable);
		
		String NombreUsuarioLogeado = principal.getName();

		DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

		String date = dateFormat.format(Calendar.getInstance().getTime());

		String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ Desabilito la fecha contable con el id :" + id + "]";

		registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);

		return  "redirect:/ListaFechasContable";
	}

	@RequestMapping(value = "/habilitarFechaContable/{id}", method = RequestMethod.GET)
	public String HabilitarFechaContable(@PathVariable("id") long id, HttpServletRequest request, ModelMap mp , Principal principal) {

		
		FechaControlContable fechaContable = null;

		fechaContable = fechaContableService.buscarFechaContable(id);

		fechaContable.setEstado("Activo");

		fechaContableService.GuardarFechaContable(fechaContable);
		
		String NombreUsuarioLogeado = principal.getName();

		DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

		String date = dateFormat.format(Calendar.getInstance().getTime());

		String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ Habilito la fecha contable con el id :" + id + "]";

		registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);

		return  "redirect:/ListaFechasContable";
	}
	
	@GetMapping("/desactivarFechas")
	public String DesactivarFechas (Principal principal) {
		
	List<FechaControlContable> listaFechasActivas = fechaContableService.buscarFechasActivas("Activo");
	
	String Desactivar = "Inactivo";
	
	for (int i = 0 ;  i < listaFechasActivas.size() ; i ++) {
		
		FechaControlContable fechaContable = null;
		
		fechaContable = listaFechasActivas.get(i);
		
		//String EstadoActualFecha = listaFechasActivas.get(i).getEstado();
	
		fechaContable.setEstado(Desactivar);
		
		fechaContableService.GuardarFechaContable(fechaContable);
		
	}
	
	String NombreUsuarioLogeado = principal.getName();

	DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

	String date = dateFormat.format(Calendar.getInstance().getTime());
			
	String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ Desactivo todas las fechas contables ]";
							
	registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);
		
		return  "redirect:/ListaFechasContable";
	}
}
