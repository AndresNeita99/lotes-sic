package com.example.demo.controller;




import java.security.Principal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.example.demo.RegistrosLog;
import com.example.demo.TiempoEspera;
import com.example.demo.Entity.HorasCorte;
import com.example.demo.Entity.SolicitudesCrearUsuario;
import com.example.demo.Entity.Usuarios;
import com.example.demo.Paginador.Paginador;
import com.example.demo.service.HorasCorteService;
import com.example.demo.service.SolicitudesCrearUsuarioService;
import com.example.demo.service.UsuariosService;

import jakarta.servlet.http.HttpServletRequest;

@Controller
public class ControllerHorasCorte {

	@Autowired
	private UsuariosService usuarioService;
	
	@Autowired
	private SolicitudesCrearUsuarioService SolicitudService;
	
	@Autowired
	private HorasCorteService horaCorteService;
	
	private String notificacion = "";

	private RegistrosLog registrolog = new RegistrosLog();

	@RequestMapping(value = "/listaHorasCorte")
	public String ListaFechasProceso(@RequestParam(name = "page" , defaultValue = "0")  int page ,ModelMap mp , Principal principal) {
		
		String NombreUsuarioLogeado = principal.getName();
		
		Usuarios usuario = usuarioService.buscarUsuario(NombreUsuarioLogeado);
		
		String Rol = usuario.getRol().get(0).getName();
		
		mp.put("Area", usuario.getArea().getArea());
		
		mp.put("rol", Rol);
		
		int HorasCorteRegistradas = horaCorteService.NumeroHorasActivas("Activo");
		
		Pageable pageRequest = PageRequest.of(page, 5);
		
		Page <HorasCorte> HorasCorte = horaCorteService.ListaHorasCortePagin(pageRequest);

		Paginador <HorasCorte> pageRender = new Paginador<>("/listaHorasCorte" , HorasCorte);
		
		List<SolicitudesCrearUsuario> cantidadSolicitudes = SolicitudService.cantidadSolicitudes("Enviada");
		
		List<Usuarios> ListaUsuarioCambioCon = usuarioService.listaUsuariosCambio("Olvidada");
		
		int CatidadSolicitudes = cantidadSolicitudes.size();
		
		mp.put("CatidadSolicitudes", CatidadSolicitudes);
		
		int cambiodecontra = ListaUsuarioCambioCon.size();
		
		mp.put("CatidadSolicitudesCambio", cambiodecontra);

		long TotalHorasCorte = HorasCorte.getTotalElements();
		
		if (HorasCorteRegistradas < 3) {
			
			notificacion = "Mínimo son 3 horas de corte activas por favor habilitar otra fecha";
			
			mp.put("notificacion", notificacion);
			
			//System.out.println("Minimo son 3 horas de corte activas por favor habilitar otra fecha");
		}
		
		TiempoEspera minutosEspera = new TiempoEspera();

		mp.put("minutosEspera", minutosEspera);
		
		mp.put("totalAreas", TotalHorasCorte);

		mp.put("HoraCorte", HorasCorte);
		
		mp.put("page", pageRender);
		
		DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

		String date = dateFormat.format(Calendar.getInstance().getTime());
				
		String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ Accedio a la lista de horas de corte ]";
								
		registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);

		return "ParametroHoraCorteLista";

	}

	@GetMapping("/crearHoraCorte")
	public String crearFechaProcesoManu(ModelMap mp, HorasCorte horaCorte , RedirectAttributes redirectAttributes , Principal principal) {

		String NombreUsuarioLogeado = principal.getName();
		
		Usuarios usuario = usuarioService.buscarUsuario(NombreUsuarioLogeado);
		
		String Rol = usuario.getRol().get(0).getName();
		
		mp.put("Area", usuario.getArea().getArea());
		
		mp.put("rol", Rol);
		
		int HorasCorteRegistradas = horaCorteService.NumeroHorasActivas("Activo");
		
		System.out.println(HorasCorteRegistradas);

		if (HorasCorteRegistradas < 5 ) {
			
			mp.put("horasCorte", horaCorte);

			DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

			String date = dateFormat.format(Calendar.getInstance().getTime());
					
			String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ Accedio al formulario crear hora de corte ]";
									
			registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);
			
			return "ParametroHoraCorteCrear";
			
			
		}
		
		else {
			
			DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

			String date = dateFormat.format(Calendar.getInstance().getTime());
					
			String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ para crear otra hora de corte desactive una hora o eliminela ]";
									
			registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);
			
			notificacion = "Desactive una hora o elimínela";
			
			redirectAttributes.addFlashAttribute("notificacion" , notificacion);
			
			return "redirect:/listaHorasCorte";
			
		}

	}
	
	

	@RequestMapping("/GuardarHoraCorte")
	public String GuardarFechaProcesoAuto(ModelMap mp, HorasCorte horaCorte , HttpServletRequest request , Principal principal) {
		
		String NombreUsuarioLogeado = principal.getName();

		DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

		String date = dateFormat.format(Calendar.getInstance().getTime());
				
		String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ Guardo la hora de corte correctamente ]";
								
		registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);
		
		horaCorte.setTipo("Automatico");
		horaCorte.setEstado("Activo");

		horaCorteService.GuardarHoraCorte(horaCorte);
		

		return "redirect:/listaHorasCorte";
	}

	

	@RequestMapping(value = "/EliminarHoraCorte/{id}", method = RequestMethod.GET)
	public String EliminarFechaProceso(@PathVariable("id") long id, HttpServletRequest request, ModelMap mp , Principal principal) {

		horaCorteService.EliminarHoraCorte(id);
		
		String NombreUsuarioLogeado = principal.getName();

		DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

		String date = dateFormat.format(Calendar.getInstance().getTime());

		String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ Elimino la hora de corte con el id : " + id + "]";

		registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);

		return "redirect:/listaHorasCorte";
	}

	@RequestMapping(value = "/EditarHoraCorte/{id}", method = RequestMethod.GET)
	public String ModificarFuente(@PathVariable("id") long id, ModelMap mp, HttpServletRequest request, HorasCorte horaCorte, Principal principal) {

		String NombreUsuarioLogeado = principal.getName();
		
		Usuarios usuario = usuarioService.buscarUsuario(NombreUsuarioLogeado);
		
		String Rol = usuario.getRol().get(0).getName();
		
		mp.put("Area", usuario.getArea().getArea());
		
		mp.put("rol", Rol);
		
		HorasCorte HoraCorte = horaCorteService.BuscarHoraid(id);

		mp.put("HoraCorte", HoraCorte);

		DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

		String date = dateFormat.format(Calendar.getInstance().getTime());

		String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ Accedio a el formulario editar hora de corte con el id : " + id + "]";

		registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);

		return "ParametroHoraCorteEditar";
	}

	@PostMapping("/actualizarHoraCorte")
	public String ActualizarFuente(ModelMap mp,  HorasCorte horaCorte , Principal principal) {

		horaCorte.setEstado("Activo");
		
		horaCorteService.GuardarHoraCorte(horaCorte);

		//System.out.println("dato actualizado exitosamente");
		
		String NombreUsuarioLogeado = principal.getName();

		DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

		String date = dateFormat.format(Calendar.getInstance().getTime());

		String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ Actualizo la hora de hora de corte de manera correcta]";

		registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);

		return "redirect:/listaHorasCorte";
	}

	@RequestMapping(value = "/DesabilitarHoraCorte/{id}", method = RequestMethod.GET)
	public String DesabilitarFuente(@PathVariable("id") long id, HttpServletRequest request, ModelMap mp , Principal principal) {

		HorasCorte horaProceso = null;

		horaProceso = horaCorteService.BuscarHoraid(id);

		horaProceso.setEstado("Inactivo");

		horaCorteService.GuardarHoraCorte(horaProceso);
		
		String NombreUsuarioLogeado = principal.getName();

		DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

		String date = dateFormat.format(Calendar.getInstance().getTime());

		String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ Desabilito la hora de corte con el id : " + id + "]";

		registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);

		return "redirect:/listaHorasCorte";
	}

	@RequestMapping(value = "/habilitarHoraCorte/{id}", method = RequestMethod.GET)
	public String HbailitarFuente(@PathVariable("id") long id, HttpServletRequest request, ModelMap mp , Principal principal) {

		HorasCorte horaProceso = null;

		String NombreUsuarioLogeado = principal.getName();

		horaProceso = horaCorteService.BuscarHoraid(id);

		horaProceso.setEstado("Activo");

		horaCorteService.GuardarHoraCorte(horaProceso);
		
		DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

		String date = dateFormat.format(Calendar.getInstance().getTime());

		String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ habilito la hora de corte con el id : " + id + "]";

		registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);

		return "redirect:/listaHorasCorte";
	}

}
