package com.example.demo.controller;

import java.security.Principal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import com.example.demo.RegistrosLog;
import com.example.demo.Entity.DebitoCredito;
import com.example.demo.Entity.SolicitudesCrearUsuario;
import com.example.demo.Entity.Usuarios;
import com.example.demo.Paginador.Paginador;
import com.example.demo.service.DebitoCreditoService;
import com.example.demo.service.SolicitudesCrearUsuarioService;
import com.example.demo.service.UsuariosService;

import jakarta.servlet.http.HttpServletRequest;

@Controller
public class ControllerDebitoCredito {
	
	@Autowired
	private UsuariosService usuarioService;
	
	@Autowired
	private SolicitudesCrearUsuarioService SolicitudService;

	@Autowired
	private DebitoCreditoService serviceDebitoCredito;
	
	private RegistrosLog registrolog = new RegistrosLog();
	
	private Date date = new Date();
	// ---------------------------- controller DebitoCredito // -------------------------------- //

		@RequestMapping("/crearDebitoCredito")
		private String CrearDebitoCredito (ModelMap mp , Principal principal) {
			
			
			String NombreUsuarioLogeado = principal.getName();
			
			Usuarios usuario = usuarioService.buscarUsuario(NombreUsuarioLogeado);
			
			String Rol = usuario.getRol().get(0).getName();
			
			mp.put("Area", usuario.getArea().getArea());
			
			mp.put("rol", Rol);
			
			mp.put("debitoCredito", new DebitoCredito());
			

			DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

			String date = dateFormat.format(Calendar.getInstance().getTime()); // hora en la que se carga el lote para validar
							
			String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ ingreso a el formulario crear Debito / Credito ]";
							
			registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);
			
			return "ParametroDebitoCreditoCrear";
		}
		
		@RequestMapping(value = "/listarDebitoCredito")
		private String listaDebitoCredito(@RequestParam(name = "page" , defaultValue = "0")  int page , ModelMap mp , Principal principal) {
			
			String NombreUsuarioLogeado = principal.getName();
			
			Usuarios usuario = usuarioService.buscarUsuario(NombreUsuarioLogeado);
			
			DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

			String date = dateFormat.format(Calendar.getInstance().getTime()); // hora en la que se carga el lote para validar
							
			Pageable pageRequest = PageRequest.of(page, 5);
			
			Page <DebitoCredito> debitoCredito = serviceDebitoCredito.listaDebitoCreditoPagin(pageRequest);

			Paginador <DebitoCredito> pageRender = new Paginador<>("/listarDebitoCredito" , debitoCredito);
			
			List<SolicitudesCrearUsuario> cantidadSolicitudes = SolicitudService.cantidadSolicitudes("Enviada");
			
			List<Usuarios> ListaUsuarioCambioCon = usuarioService.listaUsuariosCambio("Olvidada");
			
			int CatidadSolicitudes = cantidadSolicitudes.size();
			
			mp.put("CatidadSolicitudes", CatidadSolicitudes);
			
			int cambiodecontra = ListaUsuarioCambioCon.size();
			
			mp.put("CatidadSolicitudesCambio", cambiodecontra);

			
			long TotaldebitoCredito = debitoCredito.getTotalElements();
			
			String Rol = usuario.getRol().get(0).getName();
			
			mp.put("Area", usuario.getArea().getArea());
			
			mp.put("rol", Rol);

			mp.put("totalAreas", TotaldebitoCredito);
			mp.put("debitoCredito", debitoCredito);
			mp.put("page", pageRender);
			
			String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ lista de valores debito / credito ]";
			
			registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);


			return "ParametroDebitoCreditoLista";
		}

		@RequestMapping("/GuardarDebitoCredito")
		private String GuardarDebitoCredito(ModelMap mp, DebitoCredito debitoCredito , Principal principal) {

			String NombreUsuarioLogeado = principal.getName();

			DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

			String date = dateFormat.format(Calendar.getInstance().getTime()); // hora en la que se carga el lote para validar
							
			String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ Guardo el el valor del debito / credito en base de datos ]";
							
			registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);
			
			debitoCredito.setEstado("Activo");
			serviceDebitoCredito.guardarDebitoCredito(debitoCredito);

			return "redirect:/listarDebitoCredito";
		}
		
		@RequestMapping(value = "/EditarCreditoDebito/{id}", method = RequestMethod.GET)
		public String EditarDebitoCredito(@PathVariable("id") long id, HttpServletRequest request, ModelMap mp , Principal principal) {
			
			String NombreUsuarioLogeado = principal.getName();
			
			Usuarios usuario = usuarioService.buscarUsuario(NombreUsuarioLogeado);
			
			DebitoCredito credito = serviceDebitoCredito.encontrarId(id);
			
			String Rol = usuario.getRol().get(0).getName();
			
			mp.put("Area", usuario.getArea().getArea());
			
			mp.put("rol", Rol);
			
			mp.put("credito", credito);
			
			DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

			String date = dateFormat.format(Calendar.getInstance().getTime()); // hora en la que se carga el lote para validar

			String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ editor el valor del Debito/Credit con el id : " + id + "]";

			registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);
			
			return "ParametroDebitoCreditoEditar";
		}
		
		@PostMapping("/actualizarDebtioCredito")
		public String ActualizarDebitoCredito(ModelMap mp , DebitoCredito credito , Principal principal) {

			//System.out.println("nuevo fuente");
			//System.out.println("fuente " + fuentes.getFuente());
			//System.out.println("Nombre fuente " + fuentes.getFuente());
			//System.out.println("Codifo fuente:  " + fuentes.getId());
			
			String NombreUsuarioLogeado = principal.getName();

			DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

			String date = dateFormat.format(Calendar.getInstance().getTime()); // hora en la que se carga el lote para validar
			
			String NuevoNombre = credito.getNombre();
			int NuevoValor= credito.getValor();
			
			if(NuevoNombre.isEmpty() == true) {
				
				//System.out.println("El nombre no puede estar vacion");
				
				return "ParametroCentroCostosEditar";
				
			}else {
				
				credito.setEstado("Activo");
				credito.setNombre(NuevoNombre);
				credito.setValor(NuevoValor);

				serviceDebitoCredito.guardarDebitoCredito(credito);
				
				String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ Actualizo el Debito/Credito ]";
				
				registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);
				
				//System.out.println("dato actualizado exitosamente");
				
			}

			
			return "redirect:/listarDebitoCredito";
		} 
		
		@RequestMapping(value = "/EliminarDebitoCredito/{id}", method = RequestMethod.GET)
		public String EliminarDebitoCredito(@PathVariable("id") long id, HttpServletRequest request, ModelMap mp , Principal principal) {
			
			String NombreUsuarioLogeado = principal.getName();
			
			serviceDebitoCredito.EliminarDebitoCredito(id);
			
			DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

			String date = dateFormat.format(Calendar.getInstance().getTime()); // hora en la que se carga el lote para validar

			String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ elimino el debito/credito con el id : " + id + "]";


			registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);
			
			return "redirect:/listarDebitoCredito";
		}
		
		

		@RequestMapping(value = "/HabilitarDebitoCredito/{id}", method = RequestMethod.GET)
		public String HabilitarDebitoCredito(@PathVariable("id") long id, HttpServletRequest request, ModelMap mp , Principal principal) {

			
			DebitoCredito debitoCredito = null;

			debitoCredito = serviceDebitoCredito.encontrarId(id);

			debitoCredito.setEstado("Activo");

			serviceDebitoCredito.guardarDebitoCredito(debitoCredito);
			
			String NombreUsuarioLogeado = principal.getName();

			DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

			String date = dateFormat.format(Calendar.getInstance().getTime()); // hora en la que se carga el lote para validar

			String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ Habilito debito/credito con el id : " + id + "]";

			registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);

			return "redirect:/listarDebitoCredito";
		}

		@RequestMapping(value = "/DesabilitarDebitoCredito/{id}", method = RequestMethod.GET)
		public String DesabilitarDebitoCredito(@PathVariable("id") long id, HttpServletRequest request, ModelMap mp , Principal principal) {

			DebitoCredito debitoCredito = null;

			debitoCredito = serviceDebitoCredito.encontrarId(id);

			debitoCredito.setEstado("Inactivo");

			serviceDebitoCredito.guardarDebitoCredito(debitoCredito);
			
			String NombreUsuarioLogeado = principal.getName();

			DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

			String date = dateFormat.format(Calendar.getInstance().getTime()); // hora en la que se carga el lote para validar

			String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ Desabilito el debito/credito con el id : " + id + "]";


			registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);

			return "redirect:/listarDebitoCredito";
		}

	
}
