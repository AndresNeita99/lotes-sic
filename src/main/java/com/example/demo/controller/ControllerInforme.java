package com.example.demo.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;
import java.security.Principal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.example.demo.Errores;
import com.example.demo.RegistrosLog;
import com.example.demo.Entity.Log;
import com.example.demo.Entity.LoteUsuario;
import com.example.demo.Entity.Usuarios;
import com.example.demo.Paginador.Paginador;
import com.example.demo.service.CuentasService;
import com.example.demo.service.LogService;
import com.example.demo.service.LoteUsuerioService;
import com.example.demo.service.UsuariosService;

import jakarta.servlet.http.HttpServletResponse;

@Controller
public class ControllerInforme {
	
	@Autowired
	private LoteUsuerioService loteservice;
	
	@Autowired
	private UsuariosService usuarioservice;
	
	private String Analista = "";
	
	private String Fecha1 = "";
	
	private String Fecha2 = "";
	
	private String Autorizador = "";
	
	@Autowired
	private LogService logservice;
	
	private RegistrosLog registrolog = new RegistrosLog();
	
	LoteUsuario lote = new LoteUsuario();
	
	@GetMapping("/listaLotesInforme")
	public String listaLotesInforme (@RequestParam(name = "page" , defaultValue = "0")  int page , ModelMap mp , Principal principal , Model model) {
		
		Analista = "";
		
		Fecha1 = "";
		
		Fecha2 = "";
		
		Autorizador = "";
		
		Pageable pageRequest = PageRequest.of(page, 5);
		
		Page <LoteUsuario> lotes = loteservice.listaLotes(pageRequest);
		
		Paginador <LoteUsuario> pageRender = new Paginador<>("/listaLotesInforme" , lotes);
		
		String NombreUsuarioLogeado = principal.getName();
		
		Usuarios usuario = usuarioservice.buscarUsuario(NombreUsuarioLogeado);
		
		String Rol = usuario.getRol().get(0).getName();
		
		mp.put("Area", usuario.getArea().getArea());
		
		mp.put("rol", Rol);
		
		mp.put("lote", lotes);
		
		mp.put("page", pageRender);
		
		
		return "informesListaLotes";
		
	}
	
	@RequestMapping(value = "/GuardarDatosBusqueda" , method = RequestMethod.GET)
	public String guardarDatosBusqueda (@RequestParam(value = "analista", required = false) String analista , @RequestParam(value = "autorizador", required = false) String autorizador , @RequestParam(value = "fecha1", required = false) String fecha1 , @RequestParam(value = "fecha2", required = false) String fecha2, ModelMap mp , Principal principal , RedirectAttributes redirectAttributes) {
		

		Analista = analista;
		
		Autorizador = autorizador;
		
		Fecha1 = fecha1;
		
		Fecha2 = fecha2;

		
		return "redirect:/filtroInforme";
		
	}
	
	
	@RequestMapping(value = "/filtroInforme" , method = RequestMethod.GET)
	public String FiltrarInformacion( @RequestParam(name = "page" , defaultValue = "0")  int page , ModelMap mp , Principal principal , Model model , RedirectAttributes redirectAttributes ) {
		

		/*System.out.println("el anlaista es : " + Analista);
		System.out.println("en autorizador es : " + Autorizador);
		System.out.println("la fecha 1 es : " + Fecha1);
		System.out.println("la fecha 2 es :" + Fecha2);*/

		
		Pageable pageRequest = PageRequest.of(page, 5);
		
		Page <LoteUsuario> lotes = loteservice.loteusuarioFitral(Analista, Autorizador, Fecha1, Fecha2, pageRequest);
		
		if (lotes.isEmpty()) {
			
			redirectAttributes.addFlashAttribute("notificacion", "No existen lotes con esas características, por favor revisar los datos ingresados en los filtros");
			
			return "redirect:/listaLotesInforme";
		}
		
		Paginador <LoteUsuario> pageRender = new Paginador<>("/filtroInforme" , lotes);
		
		String NombreUsuarioLogeado = principal.getName();
		
		Usuarios usuario = usuarioservice.buscarUsuario(NombreUsuarioLogeado);
		
		String Rol = usuario.getRol().get(0).getName();
		
		mp.put("Area", usuario.getArea().getArea());
		
		mp.put("analista", Analista);
		mp.put("Autorizador", Autorizador);
		mp.put("fecha1", Fecha1);
		mp.put("fecha2", Fecha2);
		mp.put("rol", Rol);
		mp.put("lote", lotes);
		mp.put("page", pageRender);
	
		return "informes";
	}
	
	@RequestMapping(value = "/logs" ,  method = RequestMethod.GET)
	public String logs(@RequestParam(name = "page" , defaultValue = "0")  int page , ModelMap mp , RedirectAttributes redirectAttributes , Principal principal) {
		
		String NombreUsuarioLogeado = principal.getName();
		
		Usuarios usuario = usuarioservice.buscarUsuario(NombreUsuarioLogeado);
		
		String Rol = usuario.getRol().get(0).getName();
		
		mp.put("Area", usuario.getArea().getArea());
		
		mp.put("rol", Rol);
		
		
		Pageable pageRequest = PageRequest.of(page, 5);
		
		Page <Log> logs = logservice.listaLosg(pageRequest);

		Paginador <Log> pageRender = new Paginador<>("/logs" , logs);
		
		int total = logs.getNumberOfElements();
		
		mp.put("logs", logs);
		
		mp.put("total", total);
		
		mp.put("page", pageRender);
		
		return "informeLogs";
		
	}
	

	
	
	@RequestMapping(value = "downloadlog/{id}")
	public void downloadlog(@PathVariable("id") String id ,  HttpServletResponse response , Principal principal) throws Exception {
		// Dirección del archivo, el entorno real se almacena en la base de datos
		
		
		String NombreUsuarioLogeado = principal.getName();

		DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

		String date = dateFormat.format(Calendar.getInstance().getTime());
		
		Log logDescargar = logservice.BuscarLog(id);
		
		String NombreLog = logDescargar.getNombre();
		
		String rutaPlano = logDescargar.getRuta();
		
		File file = new File(rutaPlano);
		
		String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ descargo el log con la ruta " + rutaPlano + " ]";

		registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);
		
		//File file = new File("..\\Planos\\siig11ajc.txt");
		 // Llevando objeto de entrada
		FileInputStream fis = new FileInputStream(file);
		 // Establecer el formato relevante
		response.setContentType("application/force-download");
		 // Establecer el nombre y el encabezado del archivo descargado
		response.addHeader("Content-disposition", "attachment;fileName=" + NombreLog);
		 // Crear objeto de salida
		OutputStream os = response.getOutputStream();
		 // operación normal
		byte[] buf = new byte[1024];
		int len = 0;
		while((len = fis.read(buf)) != -1) {
			os.write(buf, 0, len);
		}
		fis.close();
	}
	
	
	
	
	

}
