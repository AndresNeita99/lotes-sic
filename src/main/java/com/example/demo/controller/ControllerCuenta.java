package com.example.demo.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.Principal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.poi.openxml4j.util.ZipSecureFile;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.example.demo.RegistrosLog;
import com.example.demo.Entity.CentroCostos;
import com.example.demo.Entity.Lote;
import com.example.demo.Entity.NumeroCuenta;
import com.example.demo.Entity.SolicitudesCrearUsuario;
import com.example.demo.Entity.Usuarios;
import com.example.demo.Paginador.Paginador;
import com.example.demo.service.CargarArchivoServer;
import com.example.demo.service.CuentasService;
import com.example.demo.service.SolicitudesCrearUsuarioService;
import com.example.demo.service.UsuariosService;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;


@Controller
public class ControllerCuenta {
	
	@Autowired
	private UsuariosService usuarioService;
	
	@Autowired
	private SolicitudesCrearUsuarioService SolicitudService;
	
	@Autowired
	private CuentasService serviceCuenta;
	
	@Autowired
	private CargarArchivoServer cargarArchivo;
	
	private String notificacion = "";
	
	private RegistrosLog registrolog = new RegistrosLog();
	
	private Date date = new Date();
	
	// ---------------------------- controller No Cuentas // -------------------------------- //

		@GetMapping("/crearCuenta")
		public String ListaCuentas (ModelMap mp) {
			
			mp.put("cuentas", new NumeroCuenta());
			
			return "ParametroCuentaCrear";
		}
		
		
		@RequestMapping("/guardarCuenta")
		public String guardarCuenta(Model model, NumeroCuenta cuentas) {
			
			cuentas.setEstado("Activo");
			
			if (cuentas.getNoCuenta().length() == 10 && cuentas.getNoCuenta().isEmpty() == false) {
				
				//System.out.print("dato guardado correctamente ");
			
			}if (cuentas.getNoCuenta().length() > 10 || cuentas.getNoCuenta().length() < 10) {
				
				//System.out.print("el dato No tiene 10 digitos");
				
			}else {
				
				
			}

			serviceCuenta.GuardarCuenta(cuentas);

			return "redirect:/listarCuentas";
		}


		@RequestMapping(value = "/listarCuentas" , method = RequestMethod.GET)
		public String listarCuentas(@RequestParam(name = "page" , defaultValue = "0")  int page ,  ModelMap mp , Model model , Principal principal) {

			String NombreUsuarioLogeado = principal.getName();
			
			DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

			String date = dateFormat.format(Calendar.getInstance().getTime()); // hora en la que se carga el lote para validar
							
			String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ lista de cuentas  ]";
							
			registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);
			
			Usuarios usuario = usuarioService.buscarUsuario(NombreUsuarioLogeado);
			
			String Rol = usuario.getRol().get(0).getName();
			
			mp.put("Area", usuario.getArea().getArea());
			
			mp.put("rol", Rol);
			
			String notificacion = (String) model.asMap().get("notificacion");
			
			Pageable pageRequest = PageRequest.of(page, 10);
			
			Page <NumeroCuenta> cuentas = serviceCuenta.ListaCuentasPagin(pageRequest);

			Paginador <NumeroCuenta> pageRender = new Paginador<>("/listarCuentas" , cuentas);
			
			List<SolicitudesCrearUsuario> cantidadSolicitudes = SolicitudService.cantidadSolicitudes("Enviada");
			
			List<Usuarios> ListaUsuarioCambioCon = usuarioService.listaUsuariosCambio("Olvidada");
			
			int CatidadSolicitudes = cantidadSolicitudes.size();
			
			mp.put("CatidadSolicitudes", CatidadSolicitudes);
			
			int cambiodecontra = ListaUsuarioCambioCon.size();
			
			mp.put("CatidadSolicitudesCambio", cambiodecontra);
			
			//System.out.print("la cantidad de datos es :" + cuentas.getTotalElements());
			
			serviceCuenta.listaCuentas().size();
			
			
			mp.put("totalRegistros", serviceCuenta.listaCuentas().size());
			mp.put("notificacion", notificacion);
			mp.put("Cuenta", cuentas);
			mp.put("page", pageRender);

			return "ParametroCuentaLista";
		}
		
		@RequestMapping(value = "/editarCuenta/{id}" , method = RequestMethod.GET)
		public String EditarCuenta(@PathVariable("id")  long id , ModelMap mp , NumeroCuenta cuenta) {

			mp.put("cuenta", cuenta);
					
			return "ParametroCuentaEditar";
		}
		
		@RequestMapping(value = "/EliminarCuenta/{id}" , method = RequestMethod.GET)
		public String EliminarCuenta(@PathVariable("id")  long id , ModelMap mp , Principal principal) {
			
			String NombreUsuarioLogeado = principal.getName();

			DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

			String date = dateFormat.format(Calendar.getInstance().getTime()); // hora en la que se carga el lote para validar
							
			String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ Elimino el area " + id + "]";

			registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);
			
			serviceCuenta.EliminarCuenta(id);
			
			
			
			return "redirect:/listarCuentas";
		}
		
		
		
		@PostMapping("/actualizarCuenta")
		public String ActualizarCuenta (ModelMap mp , NumeroCuenta cuenta) {

			//System.out.println("nuevo fuente");
			//System.out.println("fuente " + fuentes.getFuente());
			//System.out.println("Nombre fuente " + fuentes.getFuente());
			//System.out.println("Codifo fuente:  " + fuentes.getId());
			
			String NuevoNombre = cuenta.getNombreCuenta();
			String NuevaCuenta = cuenta.getNoCuenta();
			
			if(NuevoNombre.isEmpty() == true) {
				
				//System.out.println("El nombre no puede estar vacion");
				
				return "ParametroCuentaEditar";
				
			}else {
				
				cuenta.setNoCuenta(NuevaCuenta);
				cuenta.setNombreCuenta(NuevoNombre);
				cuenta.setEstado("activo");
				serviceCuenta.GuardarCuenta(cuenta);
				
				//System.out.println("dato actualizado exitosamente");
				
			}

			
			return "redirect:/listarCuentas";
		} 

		@RequestMapping(value = "/DesabilitarCuenta/{id}", method = RequestMethod.GET)
		public String DesabilitarrCuenta(@PathVariable("id") long id, HttpServletRequest request, ModelMap mp, Principal principal) {

			String NombreUsuarioLogeado = principal.getName();

			DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

			String date = dateFormat.format(Calendar.getInstance().getTime()); // hora en la que se carga el lote para validar
			
			NumeroCuenta cuenta = null;

			cuenta = serviceCuenta.findOne(id);

			cuenta.setEstado("inabilitada");

			serviceCuenta.GuardarCuenta(cuenta);
			
			String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ Desabilito la cuenta " + id + "]";
			
			registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);

			return "redirect:/listarCuentas";
		}

		@RequestMapping(value = "/HabilitarCuenta/{id}", method = RequestMethod.GET)
		public String HabilitarCuenta(@PathVariable("id") long id, HttpServletRequest request, ModelMap mp , Principal principal , RedirectAttributes redirectAttributes) {

			String NombreUsuarioLogeado = principal.getName();
			
			DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

			String date = dateFormat.format(Calendar.getInstance().getTime());
			
			NumeroCuenta cuenta = null;

			cuenta = serviceCuenta.findOne(id);

			cuenta.setEstado("Activo");

			serviceCuenta.GuardarCuenta(cuenta);
			
			String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ Habilito la cuenta con id " + id + "]";
			
			registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);
			
			redirectAttributes.addFlashAttribute("notificacion", "La cuenta ha sido habilitada");

			return "redirect:/listarCuentas";
		}

		@PostMapping("/BuscarCuenta")
		public String BuscarCuenta(@RequestParam(value = "dato", required = false) String dato ,ModelMap mp , Principal principal , RedirectAttributes redirectAttributes) {
			
			String NombreUsuarioLogeado = principal.getName();
			
			DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

			String date = dateFormat.format(Calendar.getInstance().getTime());
			
			Usuarios usuario = usuarioService.buscarUsuario(NombreUsuarioLogeado);
			
			String Rol = usuario.getRol().get(0).getName();
			
			mp.put("Area", usuario.getArea().getArea());
			
			mp.put("rol", Rol);
			
			List<NumeroCuenta> Cuenta = serviceCuenta.BuscarCuenta(dato);

			if (Cuenta.isEmpty()) {
				
				redirectAttributes.addFlashAttribute("notificacion", "La cuenta " + dato + " No existe en el sistema");
				
				return "redirect:/listarCuentas";
				
			}
			
			mp.put("Cuenta", Cuenta);
			
			String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ busco la cuenta : " + dato + "]";
			
			registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);


			return "ParametroCuentaBuscar";

		}
		
		
		@GetMapping("/CargarCuentasMasivas")
		public String CargasCuentasMasivas (Model model ,ModelMap mp , RedirectAttributes redirectAttributes , Principal principal) {
	
			Lote lote = new Lote();
			
			String NombreUsuarioLogeado = principal.getName();
			
			DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

			String date = dateFormat.format(Calendar.getInstance().getTime()); // hora en la que se carga el lote para validar
			
			String NombreArchivoCuentas = (String) model.asMap().get("NombreArchivo");
			
			String ruta = "..\\CuentasContables\\" + NombreArchivoCuentas;


			FileInputStream file;

			try {

				file = new FileInputStream(new File(ruta));
				Workbook workbook = new XSSFWorkbook(file);

				org.apache.poi.ss.usermodel.Sheet sheet = workbook.getSheetAt(0);
				ZipSecureFile.setMinInflateRatio(0);

				for (Row row : sheet) {

					int contador = 0; // contador de columna

					//System.out.println("Numero de fila: " + row.getRowNum());
					//System.out.println("");
					

					if (row.getRowNum() == 0) {

						//System.out.println("saltar fila ya que es la cabezera del archivo ");
						//System.out.println("");

					} else {
						
						NumeroCuenta cuenta = new NumeroCuenta();
						

						for (Cell cell : row) {
							
							contador = contador + 1;

							switch (cell.getCellType()) {
							
							case STRING:


								if (contador == 3) { // Columna nombre de cuenta

									String NombreCuenta = cell.getRichStringCellValue().getString();
									
									//System.out.println("Nombre de cuenta " + NombreCuenta);
									
									cuenta.setNombreCuenta(NombreCuenta);

								}
								
								if (contador == 28) {
									
									String signoCuenta = cell.getRichStringCellValue().getString();
									
									//System.out.println("signo cuenta " + signoCuenta);
									
									cuenta.setSignoCuenta(signoCuenta);
									
									
								}
								
								if (contador == 2) {
									
									String NumeroCuenta = cell.getRichStringCellValue().getString();
									
									//System.out.println("Numero cuenta lote" + NumeroCuenta);
									
									cuenta.setNoCuenta(NumeroCuenta); 
									
								}
		
								break;

							case NUMERIC:


								if (contador == 1) { // COLUMNA CODIGO FUENTE NUMERICA

									double NumeroCuenta = cell.getNumericCellValue();
									
									String Cuenta = lote.LimpiarNumeroCuenta(NumeroCuenta);

									//System.out.println("Numero cuenta lote: " + NumeroCuenta + " Numero cuenta traformada " + Cuenta + "Longitud cuenta " + Cuenta.length());
									
									cuenta.setNoCuenta(Cuenta); 
			
								}
			
							default:
								break;

							}

						}
						
						if (contador != 40) {
							
							notificacion = "El archivo " + NombreArchivoCuentas + " no cumple con la estructura";
							
							redirectAttributes.addFlashAttribute("notificacion",notificacion);
							
							return "redirect:/listarCuentas";
							
						}
						
						cuenta.setEstado("Activo");

						//System.out.println("Cuenta");
						//System.out.println("id " + cuenta.getId());
						//System.out.println("Numero Cuenta " + cuenta.getNoCuenta());
						//System.out.println("Nombre " + cuenta.getNombreCuenta());
						//System.out.println("Estado " + cuenta.getEstado());
						
						serviceCuenta.GuardarCuenta(cuenta);
						
						workbook.close();
					}
					

				}
				
				//System.out.println("Guardado exitosamente");

			} catch (FileNotFoundException e) {
				
				String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ el archvio con la ruta  " + ruta + " no se encuentra]";

				registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);

				//System.out.print("No se a cargado ninguna lote para validar por favor seleccionar un lote");
				
				return "redirect:/NotificacionArchivo";

			} catch (IOException e) {

				e.printStackTrace();
				
				String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ el archvio con la ruta  " + ruta + " no se encuentra]";

				registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);

			}
			
			
			String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ Cargo el archivo  " + ruta + " de maenra exitosa ]";
			
			registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);
			
			return "redirect:/notificacionCuentasCargadas";
		}
		
		
		
		
		
		@PostMapping("/ValidarExistenciaCuentaCarpeta")
		String validarExistenciaCarpeta(@RequestParam("file") MultipartFile file,RedirectAttributes redirectAttributes , Model model , Principal principal) throws IOException {
			
			String NombreArchivo = file.getOriginalFilename();
			
			String NombreUsuarioLogeado = principal.getName();

			DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

			String date = dateFormat.format(Calendar.getInstance().getTime());
			
			//System.out.println("el archivo de area es : " + NombreArchivo);
			
			//String NombreArchivo =  dato.replaceAll("[xlsx]", "");

			String ruta = "..\\CuentasContables";

			// esta carpeta se creara cada vez que se descargue y se llene un plano
			File directorio = new File(ruta);
			

			// ---------------- validamos que en la carpeta areas no exista el archivo si existe se elimina para la llegada del nuevo archivo areas // -----------//
			


			if (directorio.exists()) {

				File directory = new File(ruta);
					
				FileUtils.cleanDirectory(directory);
					
				cargarArchivo.save(file,ruta);
					
				redirectAttributes.addFlashAttribute("NombreArchivo",NombreArchivo);
				
				String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ cargo el archivo " + NombreArchivo + " al servidor ]";

				registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);

				return "redirect:/ActualizarCuentaMasiva";

			}
			
			else {
				
				 cargarArchivo.init(ruta);
				 
				 cargarArchivo.save(file,ruta);
				 
				 redirectAttributes.addFlashAttribute("NombreArchivo",NombreArchivo);
				 
					String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ cargo el archivo " + NombreArchivo + " al servidor]";

					registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);
				 
				 return "redirect:/CargarCuentasMasivas";
				
			}
		}
		
		
		@GetMapping("/notificacionPlantillaCuentas")
		public String NotificacionPlantilla (Model model , RedirectAttributes redirectAttributes) {
			
			
			String NombreArchivocuentas = (String) model.asMap().get("NombreArchivocuentas");
			
			notificacion = "El archivo no existe" + NombreArchivocuentas + "en la ruta, por favor guardar el archivo en la carpeta insumos, para poder cargar o actualizar las cuentas";
			
			redirectAttributes.addFlashAttribute("notificacion", notificacion);
			
			return "redirect:/listarCuentas";
		}
		
		@GetMapping("/notificacionCuentasCargadas")
		public String NotificacionCuentasCargadas (Model model , RedirectAttributes redirectAttributes) {
			
			notificacion = "cuentas contables cargadas exitosamente";
			
			redirectAttributes.addFlashAttribute("notificacion", notificacion);
			
			return "redirect:/listarCuentas";
		}
		

	
		@GetMapping("/ActualizarCuentaMasiva")
		public String ActualizarCargaMasivaCuentas (ModelMap mp , Model model , RedirectAttributes redirectAttributes , Principal principal) {
		
		String NombreArchivo = (String) model.asMap().get("NombreArchivo");		
		
		serviceCuenta.ActualizarCuentaCargaMasiva();
			
		redirectAttributes.addFlashAttribute("NombreArchivo",NombreArchivo);
		
		String NombreUsuarioLogeado = principal.getName();

		DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

		String date = dateFormat.format(Calendar.getInstance().getTime()); // hora en la que se carga el lote para validar
						
		String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ Actualizo los datos de cuentas ]";
						
		registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);
			
			return "redirect:/CargarCuentasMasivas";
		}
		
		@GetMapping("/activarParametroCuenta")
		public String activarParametroCuenta(ModelMap mp , RedirectAttributes redirectAttributes) {
			
			
			List<NumeroCuenta> cuentas = serviceCuenta.listaCuentas();
			
			for (int i = 0; i < cuentas.size(); i++) {
				
				long id = cuentas.get(i).getId();
				
				NumeroCuenta CuentaActual = serviceCuenta.findOne(id);
				
				CuentaActual.setEstado("Activo");
				
				serviceCuenta.GuardarCuenta(CuentaActual);
				
			}
			
			redirectAttributes.addFlashAttribute("notificacion", "Todos los parámetros  han sido activados");
			
			return "redirect:/listarCuentas";
			
		}
		
		
		@GetMapping("/DesactivarParametroCuenta")
		public String DesactivarParametroCuenta(ModelMap mp , RedirectAttributes redirectAttributes) {
			
			
			List<NumeroCuenta> cuentas = serviceCuenta.listaCuentas();
			
			for (int i = 0; i < cuentas.size(); i++) {
				
				long id = cuentas.get(i).getId();
				
				NumeroCuenta CuentaActual = serviceCuenta.findOne(id);
				
				CuentaActual.setEstado("desactivado");
				
				serviceCuenta.GuardarCuenta(CuentaActual);
				
			}
			
			redirectAttributes.addFlashAttribute("notificacion", "Todos los parámetros  han sido desactivados");
			
			return "redirect:/listarCuentas";
		}
		

}
