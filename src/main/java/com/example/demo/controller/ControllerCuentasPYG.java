package com.example.demo.controller;

import java.security.Principal;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.example.demo.RegistrosLog;
import com.example.demo.Entity.Area;
import com.example.demo.Entity.NumeroCuenta;
import com.example.demo.Entity.Usuarios;
import com.example.demo.Entity.ValorCuentasPYG;
import com.example.demo.Paginador.Paginador;
import com.example.demo.service.UsuariosService;
import com.example.demo.service.ValorCuentasPYGService;



@Controller
public class ControllerCuentasPYG {
	
	@Autowired
	private UsuariosService usuarioService;
	
	@Autowired
	private ValorCuentasPYGService cuentaPYGservice;
	
	private String notificacion = "";
	
	private RegistrosLog registrolog = new RegistrosLog();
	
	@GetMapping("/crearCuentaPYG")
	public String CuentasPYG (ModelMap mp , Model model , Principal principal , RedirectAttributes redirectAttributes) {
		
		ValorCuentasPYG cuentaPYG = new ValorCuentasPYG();
		
		String NombreUsuarioLogeado = principal.getName();
		
		Usuarios usuario = usuarioService.buscarUsuario(NombreUsuarioLogeado);
		
		String notificacion = (String) model.asMap().get("notificacion");
		
		String Rol = usuario.getRol().get(0).getName();
		
		mp.put("Area", usuario.getArea().getArea());
		
		mp.put("rol", Rol);
		
		mp.put("notificacion", notificacion);
		mp.put("cuentaPYG", cuentaPYG);
		
		DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

		String date = dateFormat.format(Calendar.getInstance().getTime()); // hora en la que se carga el lote para validar
						
		String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ ingreso formulario crear cuenta PYG ]";
						
		registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);
		
		return "ParametroValorCuentasAutorizacionCrear";
	}
	
	@PostMapping("/GuardarCuentaPYG")
	public String GuardarCuentaPYG (ModelMap mp , ValorCuentasPYG cuentaPYG , RedirectAttributes redirectAttributes , Principal principal) {
		
		String NombreUsuarioLogeado = principal.getName();

		DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

		String date = dateFormat.format(Calendar.getInstance().getTime());
		
		//System.out.println("El valor de la cuenta " + cuentaPYG.getCuenta());
		
		int NumeroCuenta = cuentaPYG.getCuenta();
		
		if (cuentaPYGservice.BuscarCuentaPYG(NumeroCuenta) != null && cuentaPYGservice.BuscarCuentaPYG(NumeroCuenta).getEstado().contains("Activo")) {
			
			notificacion = "Cuenta registrada, por favor intente con otros datos";
			
			redirectAttributes.addFlashAttribute("notificacion" , notificacion);
			
			String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ no pudo registrar una cuenta PYG que ya existe en Base de datos ]";
							
			registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);
			
			return "redirect:/crearCuentaPYG";
		}
		
		if (NumeroCuenta == 0) {
			
			notificacion = "Cuenta con valor 0";
			
			redirectAttributes.addFlashAttribute("notificacion" , notificacion);
			
			String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ no pudo registrar una cuenta PYG que ya existe en Base de datos ]";
							
			registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);
			
			return "redirect:/crearCuentaPYG";
			
		}
		
		
		
		double ValorCuentaNivel1 = cuentaPYG.getValorNivel1();
		
		double ValorCuentaNivel2 = cuentaPYG.getValorNivel2();
		
		DecimalFormat formatea = new DecimalFormat("###,###.##");
		
		String ValorCuentaNivel1Unidades = formatea.format(ValorCuentaNivel1);
		 
		String ValorCuentasNivel2Unidades = formatea.format(ValorCuentaNivel2);
		 
		cuentaPYG.setValorNivel1Unidades(ValorCuentaNivel1Unidades);
		
		cuentaPYG.setValorNivel2Unidades(ValorCuentasNivel2Unidades);
		
		cuentaPYG.setCuenta(NumeroCuenta);
		
		cuentaPYG.setEstado("Activo");
		
		cuentaPYGservice.GuardarCuentaPYG(cuentaPYG);
		
		notificacion = "Cuenta creada correctamente";
		
		redirectAttributes.addFlashAttribute("notificacion",notificacion);
		
		
		String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ registro en valor de la cuenta PYG de manera exitosa]";
						
		registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);
		
		return "redirect:/listarCuentasPYG";
	}
	
	
	@RequestMapping(value = "/listarCuentasPYG")
	public String ListaCuentasPYG (@RequestParam(name = "page" , defaultValue = "0")  int page , ModelMap mp , Model model , Principal principal) {
		
		String notificacion = (String) model.asMap().get("notificacion");
		
		Pageable pageRequest = PageRequest.of(page, 5);
		
		
		Page<ValorCuentasPYG> listaCuentasPYG = cuentaPYGservice.listaCuentasPYpPagin(pageRequest);
		
		Paginador <ValorCuentasPYG> pageRender = new Paginador<>("/listarCuentasPYG" , listaCuentasPYG);
		
		List<Usuarios> ListaUsuarioCrear = usuarioService.listaPeticionesCrearUsuario("No registrado");
		
		List<Usuarios> ListaUsuarioCambioCon = usuarioService.listaUsuariosCambio("Olvidada");
		
		int CatidadSolicitudes = ListaUsuarioCrear.size();
		
		mp.put("CatidadSolicitudes", CatidadSolicitudes);
		
		int cambiodecontra = ListaUsuarioCambioCon.size();
		
		mp.put("CatidadSolicitudesCambio", cambiodecontra);
		
		long TotalCuentas = listaCuentasPYG.getTotalElements();
		
		String NombreUsuarioLogeado = principal.getName();
		
		DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

		String date = dateFormat.format(Calendar.getInstance().getTime()); // hora en la que se carga el lote para validar
		
		Usuarios usuario = usuarioService.buscarUsuario(NombreUsuarioLogeado);
		
		String Rol = usuario.getRol().get(0).getName();
		
		mp.put("Area", usuario.getArea().getArea());
		
		mp.put("rol", Rol);
		
		mp.put("notificacion", notificacion);
		mp.put("cuentasPYG", listaCuentasPYG);
		mp.put("page", pageRender);
		mp.put("totalAreas", TotalCuentas);
		
		
		String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ Lista de valores cuentas PYG ]";
		
		registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);
		
		return "ParametroValorCuentasAutorizacionLista";
	}
	
	@RequestMapping(value = "/EliminarCuentaPYG/{id}" , method = RequestMethod.GET)
	public String EliminarCuenta(@PathVariable("id")  long id , ModelMap mp , Principal principal) {

		String NombreUsuarioLogeado = principal.getName();

		DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

		String date = dateFormat.format(Calendar.getInstance().getTime()); // hora en la que se carga el lote para validar
						

		String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ Elimino la cuenta PYG con el id : " + id + "]";
		
		registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);
		
		cuentaPYGservice.eliminarCuentaPYG(id);
		
		
		return "redirect:/listarCuentasPYG";
	}
	
	@RequestMapping(value = "/EditarCuentaPYG/{id}" , method = RequestMethod.GET)
	public String EditarCuenta(@PathVariable("id")  long id , ModelMap mp , ValorCuentasPYG cuentaPYG , Principal principal) {

		
		String NombreUsuarioLogeado = principal.getName();
		
		DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

		String date = dateFormat.format(Calendar.getInstance().getTime()); // hora en la que se carga el lote para validar
						
		String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ Edito la cuenta con el id" + id + "]";
		
		registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);
		
		Usuarios usuario = usuarioService.buscarUsuario(NombreUsuarioLogeado);
		
		String Rol = usuario.getRol().get(0).getName();
		
		mp.put("Area", usuario.getArea().getArea());
		
		mp.put("rol", Rol);
		
		ValorCuentasPYG CuentaBYG = cuentaPYGservice.BuscarCuentaPYG(id);
		
		mp.put("cuentaPYG", CuentaBYG);

		return "ParametroValorCuentasAutorizacionEditar";
	}
	
	
	@RequestMapping(value = "/HabilitarCuentaPyG/{id}" , method = RequestMethod.GET)
	public String HabilitarCuentaPyG(@PathVariable("id")  long id , ModelMap mp , Principal principal) {

		String NombreUsuarioLogeado = principal.getName();

		DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

		String date = dateFormat.format(Calendar.getInstance().getTime()); // hora en la que se carga el lote para validar
						
		ValorCuentasPYG CuentaPyG = cuentaPYGservice.BuscarCuentaPYG(id);
		
		CuentaPyG.setEstado("Activo");

		String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ Elimino la cuenta PYG con el id : " + id + "]";
		
		registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);
		
		cuentaPYGservice.GuardarCuentaPYG(CuentaPyG);
		
		return "redirect:/listarCuentasPYG";
	}
	
	
	@RequestMapping(value = "/DesabilitarCuentaPyG/{id}" , method = RequestMethod.GET)
	public String DesabilitarCuentaPyG(@PathVariable("id")  long id , ModelMap mp , Principal principal) {

		String NombreUsuarioLogeado = principal.getName();

		DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

		String date = dateFormat.format(Calendar.getInstance().getTime()); // hora en la que se carga el lote para validar
						
		ValorCuentasPYG CuentaPyG = cuentaPYGservice.BuscarCuentaPYG(id);
		
		CuentaPyG.setEstado("desactivado");

		String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ Elimino la cuenta PYG con el id : " + id + "]";
		
		registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);
		
		cuentaPYGservice.GuardarCuentaPYG(CuentaPyG);
		
		return "redirect:/listarCuentasPYG";
	}
	
	@PostMapping("/actualizarCuentaPYG")
	public String ActualizarCuentaPYG (Model model , ValorCuentasPYG cuentaPYG , RedirectAttributes redirectAttributes , Principal principal) {
		
		double ValorCuentaNivel1 = cuentaPYG.getValorNivel1();
		
		double ValorCuentaNivel2 = cuentaPYG.getValorNivel2();
		
		int cuenta = cuentaPYG.getCuenta();
		
		String Estado = cuentaPYG.getEstado();
		
		DecimalFormat formatea = new DecimalFormat("###,###.##");
		
		String ValorCuentaNivel1Unidades = formatea.format(ValorCuentaNivel1);
		 
		String ValorCuentasNivel2Unidades = formatea.format(ValorCuentaNivel2);
		 
		cuentaPYG.setValorNivel1Unidades(ValorCuentaNivel1Unidades);
		
		cuentaPYG.setValorNivel2Unidades(ValorCuentasNivel2Unidades);
		
		cuentaPYG.setCuenta(cuenta);
		
		cuentaPYG.setEstado("Activo");
		
		cuentaPYGservice.GuardarCuentaPYG(cuentaPYG);
		
		notificacion = "Cuenta creada correctamente";
		
		redirectAttributes.addFlashAttribute("notificacion",notificacion);
		
		cuentaPYGservice.GuardarCuentaPYG(cuentaPYG);
		
		String NombreUsuarioLogeado = principal.getName();

		DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

		String date = dateFormat.format(Calendar.getInstance().getTime()); // hora en la que se carga el lote para validar
						
		String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ Actualizo cuenta PYG ]";
						
		registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);
		
		return "redirect:/listarCuentasPYG";
	}
	
	@GetMapping("/activarParametroPyG")
	public String activarParametroPyG(ModelMap mp , RedirectAttributes redirectAttributes) {
		
		
		List<ValorCuentasPYG> cuentasPyg = cuentaPYGservice.listaCuentasPYG();
		
		for (int i = 0; i < cuentasPyg.size(); i++) {
			
			long id = cuentasPyg.get(i).getId();
			
			ValorCuentasPYG CuentaPyGActual = cuentaPYGservice.BuscarCuentaPYG(id);
			
			CuentaPyGActual.setEstado("Activo");
			
			cuentaPYGservice.GuardarCuentaPYG(CuentaPyGActual);
			
		}
		
		redirectAttributes.addFlashAttribute("notificacion", "Todos los parámetros se han sido activados");
		
		return "redirect:/listarCuentasPYG";
		
	}
	
	
	@GetMapping("/DesactivarParametroPyG")
	public String DesactivarParametroPyG(ModelMap mp , RedirectAttributes redirectAttributes) {
		
	List<ValorCuentasPYG> cuentasPyg = cuentaPYGservice.listaCuentasPYG();
			
			for (int i = 0; i < cuentasPyg.size(); i++) {
				
				long id = cuentasPyg.get(i).getId();
				
				ValorCuentasPYG CuentaPyGActual = cuentaPYGservice.BuscarCuentaPYG(id);
				
				CuentaPyGActual.setEstado("desactivado");
				
				cuentaPYGservice.GuardarCuentaPYG(CuentaPyGActual);
				
			}
			
			redirectAttributes.addFlashAttribute("notificacion", "Todos los parámetros han sido desactivados , este parametro no se validara en el proceso cargar lote");
			
			return "redirect:/listarCuentasPYG";
		}
	
	}
