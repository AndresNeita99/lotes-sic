package com.example.demo.controller;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.security.Principal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.openxml4j.util.ZipSecureFile;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.example.demo.RegistrosLog;
import com.example.demo.Entity.Area;
import com.example.demo.Entity.LoteUsuario;
import com.example.demo.Entity.SolicitudesCrearUsuario;
import com.example.demo.Entity.Usuarios;
import com.example.demo.Paginador.Paginador;
import com.example.demo.service.AreaService;
import com.example.demo.service.CargarArchivoServer;
import com.example.demo.service.SolicitudesCrearUsuarioService;
import com.example.demo.service.UsuariosService;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;


@Controller
public class ControllerArea {

	@Autowired
	private AreaService areaService;

	@Autowired
	private UsuariosService usuarioService;
	
	@Autowired
	private CargarArchivoServer cargarArchivo;
	
	@Autowired
	private SolicitudesCrearUsuarioService SolicitudService;
	
	private String notificacion = "";
	
	private RegistrosLog registrolog = new RegistrosLog();
	
	private String DatoBusqueada = "" ;

	@RequestMapping(value = "/listaAreas" , method = RequestMethod.GET)
	public String listaAreas(@RequestParam(name = "page" , defaultValue = "0")  int page ,  ModelMap mp , Model model , Principal principal) {

		String NombreUsuarioLogeado = principal.getName();
		
		Usuarios usuario = usuarioService.buscarUsuario(NombreUsuarioLogeado);
		
		String Rol = usuario.getRol().get(0).getName();
		
		mp.put("Area", usuario.getArea().getArea());
		
		mp.put("rol", Rol);
		
		
		String notificacion = (String) model.asMap().get("notificacion");
		
		Area area = new Area();
		
		Pageable pageRequest = PageRequest.of(page, 10);
		
		Page <Area> areas = areaService.ListaAreaPagina(pageRequest);

		Paginador <Area> pageRender = new Paginador<>("/listaAreas" , areas);
		
		long TotalAreas = areas.getTotalElements();
		
		//System.out.print("la cantidad de datos es :" + areas.getTotalElements());
		
		List<SolicitudesCrearUsuario> cantidadSolicitudes = SolicitudService.cantidadSolicitudes("Enviada");
		
		List<Usuarios> ListaUsuarioCambioCon = usuarioService.listaUsuariosCambio("Olvidada");
		
		int CatidadSolicitudes = cantidadSolicitudes.size();
		
		mp.put("CatidadSolicitudes", CatidadSolicitudes);
		
		int cambiodecontra = ListaUsuarioCambioCon.size();
		
		mp.put("CatidadSolicitudesCambio", cambiodecontra);


		DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

		String date = dateFormat.format(Calendar.getInstance().getTime());
				
		String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ accedio a lista de areas ]";
				
		registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);
		
		mp.put("notificacion", notificacion);
		mp.put("totalAreas", TotalAreas);
		mp.put("listaAreas", areas);
		mp.put("page", pageRender);
		mp.put("area", area);

		return "Arealista";
	}


	@PostMapping("/guardarArea")
	public String GuardarArea(ModelMap mp, Area area , RedirectAttributes redirectAttributes , Principal principal) {

		notificacion = "El área se a guardado correctamente";
		
		String NombreUsuarioLogeado = principal.getName();
		
		area.setCantidadUsuarios(0);
		area.setEstado("Activo");
		areaService.GuardarArea(area);
		
		redirectAttributes.addFlashAttribute("notificacion",notificacion);
		
		DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

		String date = dateFormat.format(Calendar.getInstance().getTime());
				
		String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ Guardo área correctamente ]";
								
				
		registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);

		return "redirect:/listaAreas";
	}

	@GetMapping("/crearArea")
	public String CrearArea(ModelMap mp , Principal principal) {
		

		String NombreUsuarioLogeado = principal.getName();
		
		Usuarios usuario = usuarioService.buscarUsuario(NombreUsuarioLogeado);
		
		String Rol = usuario.getRol().get(0).getName();
		
		mp.put("Area", usuario.getArea().getArea());
		
		mp.put("rol", Rol);
		
		DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

		String date = dateFormat.format(Calendar.getInstance().getTime());
				
		String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ ingreso a el formulario crear área ]";
				
		registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);
		
		Area area = new Area();
		
		mp.put("area", area);
		

		return "AreaCrear";
	}


	@PostMapping("/actualizarArea")
	public String ActualizarArea(ModelMap mp, Area area , RedirectAttributes redirectAttributes) {

		areaService.GuardarArea(area);
		
		notificacion = "El área a sido actualizada correctamente";

		redirectAttributes.addFlashAttribute("notificacion",notificacion);
		
		//System.out.println("dato actualizado exitosamente");

		return "redirect:/listaAreas";
	}

	@RequestMapping(value = "/DesabilitarArea/{id}", method = RequestMethod.GET)
	public String DesabilitarArea(@PathVariable("id") long id, HttpServletRequest request, ModelMap mp, Principal principal) {

		Area area = null;

		area = areaService.buscarAreaId(id);

		area.setEstado("desabilitado");

		areaService.GuardarArea(area);
		
		String NombreUsuarioLogeado = principal.getName();

		DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");
		
		String date = dateFormat.format(Calendar.getInstance().getTime());
		
		String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ Desabilito el área " + id + "]";
		
		registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);

		return "redirect:/listaAreas";
	}

	@RequestMapping(value = "/habilitarArea/{id}", method = RequestMethod.GET)
	public String HabilitarArea(@PathVariable("id") long id, HttpServletRequest request, ModelMap mp , Principal principal) {

		Area area = null;

		area = areaService.buscarAreaId(id);

		area.setEstado("Habilitado");

		areaService.GuardarArea(area);
		
		String NombreUsuarioLogeado = principal.getName();

		DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");
		
		String date = dateFormat.format(Calendar.getInstance().getTime());
		
		String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ Habilito el área " + id + "]";
		
		registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);


		return "redirect:/listaAreas";
	}

	
	@RequestMapping(value = "/EditarArea/{id}", method = RequestMethod.GET)
	public String EditarArea(@PathVariable("id") long id, HttpServletRequest request, ModelMap mp , Principal principal) {

		Area area = null;

		area = areaService.buscarAreaId(id);
		
		 List<Usuarios> UsuariosAutorizadorNivel1 = usuarioService.ListaDeAutorizadoresNivel1(2);
		 
		 List<Usuarios> UsuariosAutorizadorNivel2 = usuarioService.ListaDeAutorizadoresNivel1(3);

		mp.put("area", area);
		mp.put("autorizadores1", UsuariosAutorizadorNivel1);
		mp.put("autorizadores2", UsuariosAutorizadorNivel2);
		
		String NombreUsuarioLogeado = principal.getName();
		
		DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");
		
		String date = dateFormat.format(Calendar.getInstance().getTime());
		
		String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ Edito el area " + id + "]";
		
		registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);

		return "AreaEditar";
	}
	
	@RequestMapping(value = "/EliminarArea/{id}", method = RequestMethod.GET)
	public String EliminarArea(@PathVariable("id") long id, ModelMap mp , RedirectAttributes redirectAttributes , Principal principal) {


		List<Usuarios> ListaUsuariosArea = usuarioService.listaUsuariosPorAreaLista(id);
		
		if (ListaUsuariosArea.isEmpty()) {
			
			areaService.eliminarArea(id);
			
			notificacion = "El área se ha eliminado correctamente";
			
			redirectAttributes.addFlashAttribute("notificacion",notificacion);
			
			String NombreUsuarioLogeado = principal.getName();

			DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");
			
			String date = dateFormat.format(Calendar.getInstance().getTime());
			
			String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ Elimino el área " + id + "]";
			
			registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);
			
			return "redirect:/listaAreas";
			
		}
		
		else {
			
			notificacion = "Existen usuarios registrados en el área , por favor asígnalos a otra área";
			
			redirectAttributes.addFlashAttribute("notificacion",notificacion);
			
			String NombreUsuarioLogeado = principal.getName();

			DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");
			
			String date = dateFormat.format(Calendar.getInstance().getTime());
			
			String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ elimino el area " + id + "]";
			
			registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);
			
			return "redirect:/listaAreas";
			
		}

	}


	@GetMapping("/CargarAreasMasiva")
	public String CargasCuentasMasivas(Model model , ModelMap mp , RedirectAttributes redirectAttributes , Principal principal) {
		
		String NombreUsuarioLogeado = principal.getName();
		
		String NombreArchivo = (String) model.asMap().get("NombreArchivo");
		
		long contador2 = 0 ;
		
		String ruta = "..\\Areas\\" + NombreArchivo;

		FileInputStream file;

		try {

			file = new FileInputStream(new File(ruta));
			Workbook workbook = new XSSFWorkbook(file);

			org.apache.poi.ss.usermodel.Sheet sheet = workbook.getSheetAt(0);
			ZipSecureFile.setMinInflateRatio(0);

			for (Row row : sheet) {

				Area area = new Area();
				
				int contador = 0; // contador de columna

				//System.out.println("Numero de fila: " + row.getRowNum());
				//System.out.println("");


				if (row.getRowNum() == 0 ) {

					//System.out.println("saltar fila ya que es la cabezera del archivo ");
					//System.out.println("");

				} 
				
				else {


					for (Cell cell : row) {
						
						contador = contador + 1;
						
						//System.out.println("el contador es : " + contador);

						switch (cell.getCellType()) {
						

						case STRING:

							if (contador == 2) { // Columna nombre de cuenta
								
								String Viceprecidencia = cell.getRichStringCellValue().getString();

								if (Viceprecidencia != "" || Viceprecidencia != null) {

									area.setPresidencia(Viceprecidencia);

								} else {

									//System.out.print("El dato es null " + Viceprecidencia);

								}

								 //System.out.println("Nombre de cuenta " + Viceprecidencia);

							}

							if (contador == 4) { // Columna nombre de cuenta

								String Gerencia = cell.getRichStringCellValue().getString();

								if (Gerencia != "" || Gerencia != null) {

									area.setDireccion(Gerencia);

								} else {

									//System.out.print("El dato es null " + Gerencia);
								}

								 //System.out.println("Nombre de vicepredincia " + Gerencia);

							}

							if (contador == 6) { // Columna nombre de cuenta

								String Area = cell.getRichStringCellValue().getString();
								
								//System.out.println("El area es : " + Area);

								if (Area != "" || Area != null) {

									area.setArea(Area);

								} else {

									//System.out.print("El dato es null " + Area);
								}

								//System.out.println("Nomrbe area " + Area);

							}

							if (contador == 7) {

								String Autorizador1 = cell.getRichStringCellValue().getString();
								
								//System.out.println("El autorizador nivel 1 es : " + Autorizador1);

								if (Autorizador1 != "" || Autorizador1 != null) {

									area.setAutorizadorNivel1(Autorizador1);
								}

								else {

									//System.out.print("El dato es null " + Autorizador1);
								}
							}

							if (contador == 8) {

								String Autorizador2 = cell.getRichStringCellValue().getString();
								
								//System.out.println("El autorizador nivel 2 es : " + Autorizador2);

								if (Autorizador2 != "" || Autorizador2 != null) {

									area.setAutorizadorNivel2(Autorizador2);
								}

								else {

									//System.out.println("El dato es null " + Autorizador2);
								}
							}
							
		
							break;
							
							

						case NUMERIC:

							if (contador == 1) { // COLUMNA CODIGO FUENTE NUMERICA

								double codVP = cell.getNumericCellValue();

								int inum = (int) Math.round(codVP);

								//System.out.println("Numero cuenta lote: " + codVP + " codigo vicepresidencia: " + inum);

								area.setCodVice(inum);

							}

							if (contador == 3) { // COLUMNA CODIGO FUENTE NUMERICA

								double CodDir = cell.getNumericCellValue();

								int inum = (int) Math.round(CodDir);

								 //System.out.println("Numero cuenta lote: " + CodDir + " codigo Direccion " + inum);

								area.setCodDirec(inum);

							}

							if (contador == 5) { // COLUMNA CODIGO FUENTE NUMERICA

								double CodCoor = cell.getNumericCellValue();

								int inum = (int) Math.round(CodCoor);

								 //System.out.println("Numero cuenta lote: " + inum);

								area.setCodCoor(inum);

							}
							
						

						default:
							break;

						}
						
						
						
						
						

					}
					
					
					
					if (contador != 8) {
						
						notificacion = "El archivo " + NombreArchivo + " no cumple con la estructura ";
						
						redirectAttributes.addFlashAttribute("notificacion",notificacion);
						
						return "redirect:/listaAreas";
						
					}
					
					
						List<Area> AreasGuardadas = areaService.listaAreas();
						
						long TotalAreas = AreasGuardadas.size();
			
					
						if (area.getArea() == null || area.getAutorizadorNivel1() == null || area.getAutorizadorNivel2() == null || area.getDireccion() == null || area.getPresidencia() == null || area.getCodDirec() == 0 || area.getCodCoor() == 0 || area.getCodVice() == 0) {
	
							//System.out.println("algun campo es vacio");
	
						}

						
						//Area AreaBaseDat = areaService.BuscarAreaAntigua(Area);
						
						if (area.getArea() != null || area.getAutorizadorNivel1() != null || area.getAutorizadorNivel2() != null || area.getDireccion() != null || area.getPresidencia() != null || area.getCodDirec() != 0 || area.getCodCoor() != 0 || area.getCodVice() != 0)  {
							
								
								contador2 = contador2 + 1;
								
								//System.out.println("la fila actual es : " + contador);
								
								Area AreaBaseDat = areaService.buscarAreaId(contador2);
								
								if (AreaBaseDat != null) {
									
									String areadb = AreaBaseDat.getArea();
									
									String viceprecidendiaDb = AreaBaseDat.getDireccion();
									
									String precidenciaDb = AreaBaseDat.getPresidencia();
									
									String AutrizadorNievel1Db = AreaBaseDat.getAutorizadorNivel1();
									
									String AutorizadorNievel2Db = AreaBaseDat.getAutorizadorNivel2();
									
									int codviDb = AreaBaseDat.getCodVice();
									
									int CodPreDb = AreaBaseDat.getCodDirec();
									
									int CodAreaDb = AreaBaseDat.getCodCoor();
									
									
									
									//-------------------------- datos de excel vs datos base de datos -------- // 
								
									String AreaExcel = area.getArea();
									
									String VicepresidenciaExcel = area.getDireccion();
									
									String presidenciaExcel = area.getPresidencia();
									
									String AutorizadorNivel1Excel = area.getAutorizadorNivel1();
									
									String AutorizadorNivel2Excel = area.getAutorizadorNivel2();
											
									int codViExcel = area.getCodVice();
									
									int CodAreaExcel = area.getCodCoor();
									
									int CodDireciExcel = area.getCodDirec();
									
								
									
									if (AreaExcel.contains(areadb) == false ) {
										
										AreaBaseDat.setArea(AreaExcel);
										
										areaService.GuardarArea(AreaBaseDat);
										
									}
									
									if (VicepresidenciaExcel.equals(viceprecidendiaDb) == false) {
										
										AreaBaseDat.setDireccion(VicepresidenciaExcel);
										
										areaService.GuardarArea(AreaBaseDat);
										
										
										
									}
									
									if (presidenciaExcel.equals(precidenciaDb) == false ) {
										
										AreaBaseDat.setPresidencia(presidenciaExcel);
										
										areaService.GuardarArea(AreaBaseDat);
										
									}
									
									if (AutorizadorNivel1Excel.equals(AutrizadorNievel1Db) == false ) {
										
										AreaBaseDat.setAutorizadorNivel1(AutorizadorNivel1Excel);
										
										areaService.GuardarArea(AreaBaseDat);
									}
									
									if (AutorizadorNivel2Excel.equals(AutorizadorNievel2Db) == false) {
										
										AreaBaseDat.setAutorizadorNivel2(AutorizadorNivel2Excel);
										
										areaService.GuardarArea(AreaBaseDat);
										
										
									}
									
									if (CodAreaExcel != CodAreaDb ) {
									
										AreaBaseDat.setCodCoor(CodAreaExcel);
										
										areaService.GuardarArea(AreaBaseDat);
										
									}
									
									if (CodDireciExcel != CodPreDb) {
										
										AreaBaseDat.setCodDirec(CodDireciExcel);
										
										areaService.GuardarArea(AreaBaseDat);
									}
									
									if(codViExcel != codviDb) {
										
										AreaBaseDat.setCodVice(codViExcel);
										
										areaService.GuardarArea(AreaBaseDat);
									}
									
							
									
								}
								
								else {
									
									TotalAreas = TotalAreas + 1 ;
									
									area.setIdarea(TotalAreas);
									
									/*System.out.println("Total de areas guardadas " + TotalAreas);

									System.out.println("el area No registrada");

									System.out.println("Area");
									System.out.println("area " + area.getArea());
									System.out.println("presidencia " + area.getPresidencia());
									System.out.println("gerencia " + area.getDireccion());
									System.out.println("id area " + area.getIdarea());*/
									

									area.setEstado("Activo");

									areaService.GuardarArea(area);
									
								}

						}

					}

					workbook.close();
				

			}
			
			

			//System.out.println("Guardado exitosamente");

		} catch (FileNotFoundException e) {

			//System.out.print("No se a cargado ninguna Archivo de area para cargar");
			
			DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

			String date = dateFormat.format(Calendar.getInstance().getTime());

			String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ No se pudo cargar el archivo en la ruta " + ruta + " de manera exitosa ] ";

			registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);

		} catch (IOException e) {

			e.printStackTrace();
			

			DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");
			
			String date = dateFormat.format(Calendar.getInstance().getTime());
			
			String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ No se pudo cargar el archivo en la ruta " + ruta + " de manera exitosa ] ";
			
			registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);
			
		}
		

		DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");
		
		String date = dateFormat.format(Calendar.getInstance().getTime());
		
		String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ cargo el archivo en la ruta " + ruta + " de manera exitosa ] ";
		
		registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);
		
		notificacion = "Áreas cargadas correctamente";
		
		redirectAttributes.addFlashAttribute("notificacion",notificacion);

		return "redirect:/listaAreas";
	}

	@GetMapping("/ActualizarAreaCargaMasiva")
	public String ActualizarCargaMasivaArea(ModelMap mp , Principal principal) {
		
		String NombreUsuarioLogeado = principal.getName();

		DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");
		
		String date = dateFormat.format(Calendar.getInstance().getTime());
				
		String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ Guarda fecha contable automatico correctamente ]";
								
		registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);
		
		return "redirect:/ValidarExistenciaArchivoArea";
	}

	@RequestMapping(value = "/BuscarArea", method = RequestMethod.GET)
	public String BuscarArea(@RequestParam(name = "page" , defaultValue = "0")  int page , @RequestParam(value = "dato", required = false) String dato , HttpServletRequest request, Model model, ModelMap mp, RedirectAttributes redirectAttributes , Principal principal) {
		
		Pageable pageRequest = PageRequest.of(page, 5);
		
		Page <Area> areas = areaService.FiltrarPorArea(DatoBusqueada, pageRequest);

		Paginador <Area> pageRender = new Paginador<>("/BuscarArea" , areas);
		
		String NombreUsuarioLogeado = principal.getName();
		
		Usuarios usuario = usuarioService.buscarUsuario(NombreUsuarioLogeado);
		
		String Rol = usuario.getRol().get(0).getName();
		
		mp.put("Area", usuario.getArea().getArea());
		
		mp.put("rol", Rol);

		DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");
			
		String date = dateFormat.format(Calendar.getInstance().getTime());
			
		String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ busco el area " + dato + "]";
			
		registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);
			
		mp.put("ListaArea", areas);
		mp.put("page", pageRender);
		mp.put("totalAreas", areas.getTotalElements());
		

		return "ArealistaPorArea";
	}
	
	@GetMapping("/barrabusquedaArea")
	public String barrabusquedaArea (ModelMap mp , Principal principal) {
		
		String NombreUsuarioLogeado = principal.getName();
		
		Usuarios usuario = usuarioService.buscarUsuario(NombreUsuarioLogeado);
		
		String Rol = usuario.getRol().get(0).getName();
		
		mp.put("Area", usuario.getArea().getArea());
		
		mp.put("rol", Rol);
		
		Area area = new Area ();
		
		mp.put("area", area);
		
		return "ArealistaPorAreaBarra";
	}
	
	@GetMapping("/guardarDatoArea")
	public String guardardatoBarraArea (@RequestParam(value = "dato", required = false) String dato) {
		
		DatoBusqueada = dato;
		
		return "redirect:/BuscarArea";
		
	}
	
	@GetMapping("/barrabusquedapresidencia")
	public String barrabusquedapresidencia (ModelMap mp , Principal principal) {
		
		String NombreUsuarioLogeado = principal.getName();
		
		Usuarios usuario = usuarioService.buscarUsuario(NombreUsuarioLogeado);
		
		String Rol = usuario.getRol().get(0).getName();
		
		mp.put("Area", usuario.getArea().getArea());
		
		mp.put("rol", Rol);
		
		Area area = new Area ();
		
		mp.put("area", area);
		
		return "ArealistaPorPresidesBarra";
	}
	
	@GetMapping("/guardarDato")
	public String guardardatoBarraPresidencia (@RequestParam(value = "dato", required = false) String dato) {
		
		DatoBusqueada = dato;
		
		return "redirect:/BuscarPresidencia";
		
	}
	

	@RequestMapping(value = "/BuscarPresidencia", method = RequestMethod.GET)
	public String BuscarPresidencia(@RequestParam(name = "page" , defaultValue = "0")  int page , @RequestParam(value = "dato", required = false) String dato, HttpServletRequest request, Model model, ModelMap mp, RedirectAttributes redirectAttributes , Principal principal) {
		
				
		//System.out.println("la presidencia es : " + DatoBusqueada);
		
		String NombreUsuarioLogeado = principal.getName();
		
		Pageable pageRequest = PageRequest.of(page, 5);
		
		Page <Area> areas = areaService.FiltrarPorPresidencia(DatoBusqueada, pageRequest);

		Paginador <Area> pageRender = new Paginador<>("/BuscarPresidencia" , areas);
		
		Usuarios usuario = usuarioService.buscarUsuario(NombreUsuarioLogeado);
		
		String Rol = usuario.getRol().get(0).getName();
		
		mp.put("Area", usuario.getArea().getArea());
		
		mp.put("rol", Rol);
	
		//System.out.println("el dato a buscar es :" + DatoBusqueada);
	
		DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");
		
		String date = dateFormat.format(Calendar.getInstance().getTime());
		
		String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ Busco la presidencia " + dato + "]";
		
		registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);
		
		mp.put("ListaPresidencia", areas);
		mp.put("page", pageRender);
		mp.put("totalAreas", areas.getTotalElements());
		

		return "ArealistaPorPreside";
	}
	
	
	
	@RequestMapping(value = "/BuscarVicepresidencia", method = RequestMethod.GET)
	public String BuscarVicepresidencia(@RequestParam(name = "page" , defaultValue = "0")  int page , @RequestParam(value = "dato", required = false) String dato ,HttpServletRequest request, Model model, ModelMap mp, RedirectAttributes redirectAttributes , Principal principal) {
		
		String NombreUsuarioLogeado = principal.getName();
		
		Pageable pageRequest = PageRequest.of(page, 5);
		
		Page <Area> areas = areaService.FiltrarPorVisepresidencia(DatoBusqueada, pageRequest);

		Paginador <Area> pageRender = new Paginador<>("/BuscarVicepresidencia" , areas);
		
		DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");
		
		String date = dateFormat.format(Calendar.getInstance().getTime());
		
		Usuarios usuario = usuarioService.buscarUsuario(NombreUsuarioLogeado);
		
		String Rol = usuario.getRol().get(0).getName();
		
		mp.put("Area", usuario.getArea().getArea());
		
		mp.put("rol", Rol);
		
		//System.out.print("el dato a buscar es :" + dato);
		
		List<Area> listavicepresidencias = areaService.BuscarPorDireccion(DatoBusqueada);

		//System.out.print("el dato a buscar es :" + listavicepresidencias.size());
		
		String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ La visepresidencia  " + dato + " se encontro con exito en base de datos]";
		
		registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);
		
		mp.put("ListaVicepresidencia", areas);
		mp.put("page", pageRender);
		mp.put("totalAreas", areas.getTotalElements());
		
		return "ArealistaPorVicepresidencia";
	}
	
	
	@GetMapping("/barrabusquedaVisepresidencia")
	public String barrabusquedaVisepresidencia (ModelMap mp , Principal principal) {
		
		String NombreUsuarioLogeado = principal.getName();
		
		Usuarios usuario = usuarioService.buscarUsuario(NombreUsuarioLogeado);
		
		String Rol = usuario.getRol().get(0).getName();
		
		mp.put("Area", usuario.getArea().getArea());
		
		mp.put("rol", Rol);
		
		Area area = new Area ();
		
		mp.put("area", area);
		
		return "ArealistaPorVisepresidenciaBarra";
	}
	
	@GetMapping("/guardarDatoVisepresidencia")
	public String guardardatoBarraVisepresidencia (@RequestParam(value = "dato", required = false) String dato) {
		
		DatoBusqueada = dato;
		
		return "redirect:/BuscarVicepresidencia";
		
	}
	
	
	

	@RequestMapping(value = "/BuscarUsuariosArea/{id}", method = RequestMethod.GET)
	public String BuscarUsuarioArea(@PathVariable("id") long id, HttpServletRequest request, ModelMap mp , Principal principal) {
		
		
		//Paginador <Usuarios> pageRender = new Paginador<>("/BuscarUsuariosArea" , ListaUsuariosArea);
		
		List<Usuarios> ListaUsuariosArea = usuarioService.listaUsuariosPorAreaLista(id);
		
		DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");
		
		String date = dateFormat.format(Calendar.getInstance().getTime());

		String NombreUsuarioLogeado = principal.getName();
		
		Usuarios usuario = usuarioService.buscarUsuario(NombreUsuarioLogeado);
		
		String Rol = usuario.getRol().get(0).getName();
		
		mp.put("Area", usuario.getArea().getArea());
		
		mp.put("rol", Rol);

		String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ Busco usuarios que corresponden a el area con id : " + id + " ]";
		
		registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);
		

		mp.put("UsuariosArea", ListaUsuariosArea);
		//mp.put("page", pageRender);

		return "ArealistaUsuariosArea";
	}
	
	@RequestMapping(value = "/ActualizarUsuarios/{id}", method = RequestMethod.GET)
	public String ActualizaUsuariosArea(@PathVariable("id") long id, HttpServletRequest request, ModelMap mp , RedirectAttributes redirectAttributes , Principal principal) {
		
		String NombreUsuarioLogeado = principal.getName();

		DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");
		
		String date = dateFormat.format(Calendar.getInstance().getTime());
						
		String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ Actualizo el numero de usuarios correctamente para el area " + id + "]" ;
						
		registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);

	 	List<Usuarios> usuariosArea = usuarioService.listaUsuarios();
	 	
	 	int contador = 0;
	 	
	 	for (int i = 0 ; i < usuariosArea.size() ; i++) {
	 		
	 		Long idArea = usuariosArea.get(i).getArea().getIdarea();
	 		
	 		if (id == idArea) {
	 			
	 			contador = contador + 1;
	 			
	 		}
	 		
	 	}
	 	
	 	Area area = areaService.buscarAreaId(id);
	 	
	 	String NombreArea = area.getArea();
	 	
	 	area.setCantidadUsuarios(contador);
	 	
	 	areaService.GuardarArea(area);
	 	
	 	notificacion =   NombreArea + " actualizada correctamente";
	 	
	 	redirectAttributes.addFlashAttribute("notificacion",notificacion);
	
		return "redirect:/listaAreas";
	}
	
	@PostMapping("/ValidarExistenciaArchivoArea")
	public String downloadFile(@RequestParam(value = "dato", required = false) String dato ,@RequestParam("file") MultipartFile file , RedirectAttributes redirectAttributes) throws Exception {
		
		DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");
		
		String date = dateFormat.format(Calendar.getInstance().getTime());
		
		String NombreArchivo = file.getOriginalFilename();
		
		//System.out.println("el archivo de area es : " + NombreArchivo);
		
		//String NombreArchivo =  dato.replaceAll("[xlsx]", "");

		String ruta = "..\\Areas";

		// esta carpeta se creara cada vez que se descargue y se llene un plano
		File directorio = new File(ruta);
		

		// ---------------- validamos que en la carpeta areas no exista el archivo si existe se elimina para la llegada del nuevo archivo areas // -----------//
		


		if (directorio.exists()) {

			File directory = new File(ruta);
				
			FileUtils.cleanDirectory(directory);
				
			cargarArchivo.save(file,ruta);
				
			redirectAttributes.addFlashAttribute("NombreArchivo",NombreArchivo);
			
			String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + "sistema" + "] " + "Tarea" + " [ Creo la carpeta Areas dentro del servidor ]";
			
			registrolog.guardarRegistro("sistema", tarea);

			return "redirect:/CargarAreasMasiva";

		}
		
		else {
			
			 cargarArchivo.init(ruta);
			 
			 cargarArchivo.save(file,ruta);
			 
			 redirectAttributes.addFlashAttribute("NombreArchivo",NombreArchivo);
			 
			 String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + "sistema" + "] " + "Tarea" + " [ Creo la carpeta Areas dentro del servidor ]";
				
			 registrolog.guardarRegistro("sistema", tarea);
			 
			 return "redirect:/CargarAreasMasiva";
			
		}
		

	}
	
	
	@GetMapping("/notificacionPlantillaArea")
	public String NotificacionPlantilla (Model model , RedirectAttributes redirectAttributes) {
		
		String NombreArchivoFuentes = (String) model.asMap().get("NombreArchivo");
		
		notificacion = "El archivo " + NombreArchivoFuentes + "no existe en la carpeta insumos, por favor guardar el archivo en la carpeta, para cargar o actualizar las áreas";
		
		redirectAttributes.addFlashAttribute("notificacion", notificacion);
		
		return "redirect:/listaAreas";
	}
	
	
	// api que llama a las area cargadas en el sistema
	@GetMapping(value = "/cargarAreas/{term}", produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody List<Area> cargarAreas(@PathVariable String term){
		
		return areaService.search(term);
	}
	
	@GetMapping(value = "/cargarViceprecidencias/{term}", produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody List<Area> cargarVicepresidencias(@PathVariable String term){
		
		return areaService.BuscarPorPresidencia(term);
	}
	
	@GetMapping(value = "/cargarDirecciones/{term}", produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody List<Area> cargarDireccion(@PathVariable String term){

		return areaService.BuscarPorDireccion(term);
	}
	
	
	
	
	
	@GetMapping(value = "/editarAreaUsu/{term}", produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody List<Area> editarAreaUsu(@PathVariable String term){

		System.out.println(term);
		
		List<Area> areas = areaService.search(term);
		
		return areaService.search(term);
	}
	
	
	
	@GetMapping("/GenerarSoporteAreas")
	public String CrearCarpetaReporteAreas(ModelMap mp , RedirectAttributes redirectAttributes) {
		
		LocalDate Fechacargue = LocalDate.now();
		 
		 String FechaHoy = Fechacargue.toString();
		 
		 FechaHoy = FechaHoy.replace("-", "");
		
		 String ruta = "..\\ReporteArea\\";
		 	 
		 cargarArchivo.init(ruta);
		 
		//Crear libro de trabajo en blanco
		
		 Workbook libro = new XSSFWorkbook();
		 
		 final String nombreArchivo = "ReporteAreas" + Fechacargue + ".xlsx";
		 
		 Sheet hoja = libro.createSheet("Hoja 1");
		 
		 List<Area> listaAreasActual = areaService.listaAreas();
		 
		 String[] encabezados = {"CODVP", "PRESIDENCIA", "COD DV" , "VICEPRESIDENCIA" , "COD COOR" , "AREA" , "# USUARIOS" , "AUTORIZADOR NIVEL 1" , "AUTORIZADOR NIVEL 2" , "ESATADO"};
		 
		 int indiceFila = 0;
		 
		 Row fila = hoja.createRow(indiceFila);
	       
		 for (int i = 0; i < encabezados.length; i++) {
			 
	            String encabezado = encabezados[i];
	            
	            Cell celda = fila.createCell(i);
	            
	            celda.setCellValue(encabezado);
	        }
		 
		 indiceFila++;
		 
	        for (int i = 0; i < listaAreasActual.size(); i++) {
	        	
	            fila = hoja.createRow(indiceFila);
	            
	            Area AreaActual = listaAreasActual.get(i);
	            
	            fila.createCell(0).setCellValue(AreaActual.getCodVice());
	            
	            fila.createCell(1).setCellValue(AreaActual.getPresidencia());
	            
	            fila.createCell(2).setCellValue(AreaActual.getCodDirec());
	            
	            fila.createCell(3).setCellValue(AreaActual.getDireccion());
	            
	            fila.createCell(4).setCellValue(AreaActual.getCodCoor());
	            
	            fila.createCell(5).setCellValue(AreaActual.getArea());
	            
	            fila.createCell(6).setCellValue(AreaActual.getCantidadUsuarios());
	            
	            fila.createCell(7).setCellValue(AreaActual.getAutorizadorNivel1());
	            
	            fila.createCell(8).setCellValue(AreaActual.getAutorizadorNivel2());
	            
	            fila.createCell(9).setCellValue(AreaActual.getEstado());
	            
	            indiceFila++;
	        }
	        
	        String rutaCompleta = ruta + nombreArchivo;
	       
	        FileOutputStream outputStream;
	        
	        try {
	        	
	            outputStream = new FileOutputStream(ruta + nombreArchivo);
	           
	            libro.write(outputStream);
	            
	            libro.close();
	            
	            //System.out.println("Libro de personas guardado correctamente");
	            
	            
		        redirectAttributes.addFlashAttribute("nombreArchivo", nombreArchivo);

		        redirectAttributes.addFlashAttribute("rutaCompleta", rutaCompleta);
	     
		        return "redirect:/DescargarSoporteAreaActual";
	       
	        } catch (FileNotFoundException ex) {
	        	
	            //System.out.println("Error de filenotfound");
	            
	    		
		        redirectAttributes.addFlashAttribute("notificacion","no se puedo generar el soporte");

	     
		        return "redirect:/listaAreas";
	        
	        } catch (IOException ex) {
	           
	        	//System.out.println("Error de IOException");
	            
	    		
		        redirectAttributes.addFlashAttribute("NombreArchivo","no se pudo generar el soporte");

	     
		        return "redirect:/listaAreas";
	        }

	}
	
	@RequestMapping(value = "/DescargarSoporteAreaActual")
	public void download( HttpServletResponse response , Principal principal , Model model , RedirectAttributes redirectAttributes) throws Exception {
		// Dirección del archivo, el entorno real se almacena en la base de datos
		
		String rutaCompleta = (String) model.asMap().get("rutaCompleta");
		
		String nombreArchivo = (String) model.asMap().get("nombreArchivo");
		
		//System.out.println("la ruat del archivo completa es " + rutaCompleta);
		
		//System.out.println("el nombre del archivo es " + nombreArchivo);
		
		String NombreUsuarioLogeado = principal.getName();

		DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");
		
		String date = dateFormat.format(Calendar.getInstance().getTime());
		
		File file = new File(rutaCompleta);
		
		String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [Desacargo el archivo " + rutaCompleta + "]";

		registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);
		
		//File file = new File("..\\Planos\\siig11ajc.txt");
		 // Llevando objeto de entrada
		FileInputStream fis = new FileInputStream(file);
		 // Establecer el formato relevante
		response.setContentType("application/force-download");
		 // Establecer el nombre y el encabezado del archivo descargado
		response.addHeader("Content-disposition", "attachment;fileName=" + nombreArchivo);
		 // Crear objeto de salida
		OutputStream os = response.getOutputStream();
		 // operación normal
		byte[] buf = new byte[1024];
		int len = 0;
		while((len = fis.read(buf)) != -1) {
			os.write(buf, 0, len);
		}
		
		fis.close();
		
	
	}
	


}
