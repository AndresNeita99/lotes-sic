package com.example.demo.controller;



import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.security.Principal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.example.demo.RegistrosLog;
import com.example.demo.Entity.Area;
import com.example.demo.Entity.SolicitudesCrearUsuario;
import com.example.demo.Entity.Usuarios;
import com.example.demo.Paginador.Paginador;
import com.example.demo.service.AreaService;
import com.example.demo.service.CargarArchivoServer;
import com.example.demo.service.RolService;
import com.example.demo.service.SolicitudesCrearUsuarioService;
import com.example.demo.service.UsuariosService;
import com.example.demo.service.emailEnviar;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@Controller
public class ControllerUsuario {

	
	@Autowired
	private CargarArchivoServer cargarArchivo;
	
	@Autowired
	private emailEnviar emailSender;
	
	@Autowired
	private SolicitudesCrearUsuarioService SolicitudService;
	
	@Autowired
	private UsuariosService usuarioservice;

	@Autowired
	private RolService rolService;

	@Autowired
	private AreaService areaService;
	
	private RegistrosLog registrolog = new RegistrosLog();

	@RequestMapping(value = "/listaUsuarios" , method = RequestMethod.GET)
	public String ListarAreas(@RequestParam(name = "page" , defaultValue = "0")  int page , Model model  , ModelMap mp , RedirectAttributes redirectAttributes , Principal principal) {

		String NombreUsuarioLogeado = principal.getName();
		
		DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

		String date = dateFormat.format(Calendar.getInstance().getTime());
		
		Usuarios usuariologueado = usuarioservice.buscarUsuario(NombreUsuarioLogeado);
		
		String Rol = usuariologueado.getRol().get(0).getName();
		
		mp.put("Area", usuariologueado.getArea().getArea());
		
		mp.put("rol", Rol);
		
		List<SolicitudesCrearUsuario> cantidadSolicitudes = SolicitudService.cantidadSolicitudes("Enviada");
		
		List<Usuarios> ListaUsuarioCambioCon = usuarioservice.listaUsuariosCambio("Olvidada");
		
		int CatidadSolicitudes = cantidadSolicitudes.size();
		
		mp.put("CatidadSolicitudes", CatidadSolicitudes);
		
		int cambiodecontra = ListaUsuarioCambioCon.size();
		
		mp.put("CatidadSolicitudesCambio", cambiodecontra);
		
		String notificacion = (String) model.asMap().get("notificacion");
		
		String notificacion2 = "";
		
		if (CatidadSolicitudes > 0 || cambiodecontra > 0) {
			
			 notificacion2 = "Tienes solicitudes pendientes";
			
		}
		
		
		
		Usuarios usuarios = new Usuarios();

		Pageable pageRequest = PageRequest.of(page, 5);
		
		Page <Usuarios> usuario = usuarioservice.listaUsuariosPagin(pageRequest);

		Paginador <Usuarios> pageRender = new Paginador<>("/listaUsuarios" , usuario);
		
		//System.out.print("la cantidad de datos es :" + usuario.getTotalElements());

		mp.put("notificacion2", notificacion2);
		mp.put("notificacion", notificacion);
		mp.put("usuario", usuario);
		mp.put("page", pageRender);
		mp.put("usuarios", usuarios);
		
		String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ Accedio al la lista de usuarios ]";

		registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);

		return "UsuariosLista";
	}


	@GetMapping("/crearUsuario")
	public String crearArea(ModelMap mp , Model model , RedirectAttributes redirectAttributes , Principal principal) {

		String NombreUsuarioLogeado = principal.getName();
		
		DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

		String date = dateFormat.format(Calendar.getInstance().getTime());
		
		Usuarios usuariologueado = usuarioservice.buscarUsuario(NombreUsuarioLogeado);
		
		String Rol = usuariologueado.getRol().get(0).getName();
		
		mp.put("Area", usuariologueado.getArea().getArea());
		
		mp.put("rol", Rol);
		
		String notificacion = (String) model.asMap().get("notificacion");
		
		mp.put("notificacion", notificacion);
		mp.put("roles", rolService.listaRoles());
		mp.put("Areas", areaService.listaAreas());
		mp.put("usuario", new Usuarios());

		String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ Accedio al formulario crear usuario ]";

		registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);
		
		return "UsuariosCrear";

	}

	@RequestMapping("/guardarUsuario")
	public String GuardarUsuario(@RequestParam(value = "dato", required = false) String dato ,ModelMap mp, Usuarios usuario , RedirectAttributes redirectAttributes , Principal principal) {

		// Vigente, En Cambio, Sin Asignar y Olvidada 
		
		//System.out.println("el dato es " + dato);
		
		String NombreUsuarioLogeado = principal.getName();

		DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

		String date = dateFormat.format(Calendar.getInstance().getTime());
		
		String notificacion = "";
		
		usuario.setEstadoUsuario("Activo");
		
		Area AREA = areaService.BuscarAreaAntigua(dato);
		
		if (AREA == null ) {
			
			redirectAttributes.addFlashAttribute("notificacion", "por favor llenar todos los campos");
			
			return "redirect:/crearUsuario";
		}
		
		AREA.getIdarea();
		
		long idArea = AREA.getIdarea();

		//System.out.println("el id de el area es : " + dato);
		
		String correoPichincha = "@pichincha.com.co";

		String Nombre = usuario.getNombre();
		
		String Correo = usuario.getCorreo();
		
		int Cedula = usuario.getDocumento();
		
		String numCedula= String.valueOf(Cedula);
		
		if (Correo.contains(correoPichincha) == false || Correo.matches(Nombre) || Correo.matches(".")) {
			
			notificacion = "Correo " + Correo + "No valido";
			
			redirectAttributes.addFlashAttribute("notificacion" , notificacion);
			
			String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ " + notificacion + " ]";

			registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);
			
			return "redirect:/crearUsuario";
			
		}
		
		String Usuario = usuario.getUsername();
		
		if (Usuario.matches("^[a-zA-Z0-9]*$") == false && Usuario.matches(numCedula) == false) {
			
			notificacion = "Nombre de usuario " + Usuario + "No valido , el usuario debe contener algun digito de la cedula ";
			
			redirectAttributes.addFlashAttribute("notificacion" , notificacion);
			
			String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ " + notificacion + " ]";

			registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);
			
			return "redirect:/crearUsuario";
			
		}

		
		if (numCedula.length() > 10 || numCedula.length() < 7 ) {
			
			notificacion = "Numero de C.C " + Cedula + " No es valido ";
			
			redirectAttributes.addFlashAttribute("notificacion" , notificacion);
			
			String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ " + notificacion + " ]";

			registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);
			
			return "redirect:/crearUsuario";
			
		}

		Usuarios correoDb = usuarioservice.buscarPorEmail(Correo);
		
		Usuarios usuarioDb = usuarioservice.buscarUsuario(Usuario); 
				
		Usuarios NombreDb = usuarioservice.buscarPorNombre(Nombre);
		
		Usuarios cedulaDb = usuarioservice.buscarPorCedula(Cedula);
		
		long idRol = usuario.getRol().get(0).getId();
		
		if (idRol == 1 || idRol == 7 || idRol == 8) {
			
			Usuarios rolexistente = usuarioservice.listaUsuariosPorIdRol(idRol);

			
			if (rolexistente != null) {
				
				notificacion = "EL rol que le estas asignando al usuario ya existe , y esta Activo porfavor cambiar el rol asignado o desabilitarlo";
				
				redirectAttributes.addFlashAttribute("notificacion" , notificacion);
				
				String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ " + notificacion + " ]";

				registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);
				
				return "redirect:/crearUsuario";
				
				
			}
			
		}
		
		
		if(usuarioDb == null && correoDb == null && NombreDb == null && cedulaDb == null) {
			
			//System.out.print("El usuario no existe en la base de datos ");

			Area AreaGuardar = areaService.buscarAreaId(idArea);
			
			int contadorUsuarios = AreaGuardar.getCantidadUsuarios();

			contadorUsuarios = contadorUsuarios + 1;
			
			if (idRol == 2) {
				
				AreaGuardar.setAutorizadorNivel1(Nombre);
				
			}
			
			if (idRol == 3) {
				
				AreaGuardar.setAutorizadorNivel2(Nombre);
			}
			
			

			AreaGuardar.setCantidadUsuarios(contadorUsuarios);

			usuario.setEstadoContra("Sin Asignar");
			usuario.setEstadoUsuario("Activo");
			usuario.setArea(AreaGuardar);

			usuarioservice.GuardarUsuario(usuario);
			
			areaService.GuardarArea(AreaGuardar);

			//System.out.print("Se a guardado usuario exitosamente");
			
			redirectAttributes.addFlashAttribute("notificacion", "El usuario se a creado correctamente");
			
			String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ " + notificacion + " ]";

			registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);
			
			emailSender.sendEmail("Usuario creado Herramienta Lote SIC", "Test Message : Usuario para la herramienta SIC : " + Correo  + "recordar ingresar en el apartado ( primer ingreso ),  con el fin de establecer todos los datos de seguiridad dentro de la herramienta", "administracionlotes@pichincha.com.co" , Correo /* PONER del nuevo usuario */ , true );
			
			return "redirect:/listaUsuarios";
			
			
		}else {
			
			notificacion = "El usuario existe en la base de datos";
			
			redirectAttributes.addFlashAttribute("notificacion" , notificacion);
			
			//System.out.print("El usuario existe en la base de datos");
			
			String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ " + notificacion + " ]";

			registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);
			
			return "redirect:/crearUsuario";
		}

	}
	
	
	
	

	@RequestMapping(value = "/editarUsuario/{id}", method = RequestMethod.GET)
	public String EditarUsuario(@PathVariable("id") long id, ModelMap mp, Usuarios usuario , Principal principal) {

		String NombreUsuarioLogeado = principal.getName();
		
		DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

		String date = dateFormat.format(Calendar.getInstance().getTime());

		Usuarios usuariologueado = usuarioservice.buscarUsuario(NombreUsuarioLogeado);
		
		Long idArea = usuariologueado.getArea().getIdarea();
		
		areaService.buscarAreaId(idArea);
		
		String Rol = usuariologueado.getRol().get(0).getName();
		
		mp.put("Area", usuariologueado.getArea().getArea());
		
		mp.put("rol", Rol);
		
		Usuarios usuarioEditar = usuarioservice.buscarUsuarioId(id);
		
		String AreaActual = usuarioEditar.getArea().getArea();
		
		
		mp.put("AreaActual", AreaActual);
		mp.put("roles", rolService.listaRoles());
		mp.put("Areas", areaService.listaAreas());
		mp.put("usuario", usuarioservice.buscarUsuarioId(id));
		
		String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ Accedio al formulario editar usuario ]";

		registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);

		return "UsuariosEditar";
	}
	
	@GetMapping("/usuariosActivos")
	public String BuscarUsuariosActivos (@RequestParam(value = "dato", required = false) String dato) {
		
		if (dato.contains("Activos")) {
			
			return "redirect:/UsuariosActivos";
			
		}else {
			
			
			return "redirect:/UsuariosActivos";
			
		}
		
		
		
	}

	@PostMapping("/actualizarUsuario")
	public String ActualizarUsuario(@RequestParam(value = "dato", required = false) String dato , ModelMap mp, Usuarios usuario , RedirectAttributes redirectAttributes , Principal principal) {
		
		String notificacion = "";
		
		String NombreUsuarioLogeado = principal.getName();

		DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

		String date = dateFormat.format(Calendar.getInstance().getTime());
		
		Long idUsuario = usuario.getId();
		
		String estadoContra = usuario.getEstadoContra();
		
		long Idrol = usuario.getRol().get(0).getId();
		
		//System.out.println("El estado de contraseña es : " + estadoContra);
		
		Usuarios usuarioGuardar = usuarioservice.buscarUsuarioId(idUsuario);
		
		String Nombre = usuarioGuardar.getNombre();
		
		String CorreoUsuario = usuarioGuardar.getCorreo();
		
		Date fechacontra = usuarioGuardar.getFechacontra();
		
		Date fechalimitecontra = usuarioGuardar.getFechalimite();
		
		String fechaNacimiento = usuarioGuardar.getFechanacimiento();
		
		String fechaexpeci = usuarioGuardar.getFechaexpedicion();
		
		Integer numerocel = usuarioGuardar.getNumcelular();
		
		String password = usuarioGuardar.getPassword();
		
		Area AreaActual = usuarioGuardar.getArea();
		
		String NombreArea = AreaActual.getArea();
		
		//System.out.println("Nombre area actual es : " + NombreArea);
		
		//System.out.println("El area nueva es : " + dato);
		
		usuario.setFechaexpedicion(fechaexpeci);
		
		usuario.setFechanacimiento(fechaNacimiento);
		
		usuario.setNumcelular(numerocel);
		
		usuario.setPassword(password);
		
		usuario.setFechacontra(fechacontra);
		
		usuario.setFechalimite(fechalimitecontra);
		
		usuario.setEstadoUsuario("Activo");
		
		usuario.setEstadoContra(estadoContra);
		
		
		if (dato.isEmpty() ) {
			
			Area AREA = areaService.BuscarAreaAntigua(NombreArea);
			
			if (Idrol == 2) {
				
				AREA.setAutorizadorNivel1(Nombre);
				
				areaService.GuardarArea(AREA);
				
			}
			
			if (Idrol == 3) {
				
				AREA.setAutorizadorNivel2(Nombre);
				
				areaService.GuardarArea(AREA);
			}
			
			usuario.setArea(AREA);
			
			usuarioservice.GuardarUsuario(usuario);
			
			notificacion = "Dato actualizado exitosamente";
			
			redirectAttributes.addFlashAttribute("notificacion" , notificacion);
			
			String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ " + notificacion + " ]";

			registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);

			return "redirect:/listaUsuarios";
			
			
		} 

		Area AREA = areaService.BuscarAreaAntigua(dato);
		
		if (Idrol == 2) {
			
			AREA.setAutorizadorNivel1(Nombre);
			
			areaService.GuardarArea(AREA);
			
		}
		
		if (Idrol == 3) {
			
			AREA.setAutorizadorNivel2(Nombre);
			
			areaService.GuardarArea(AREA);
		}
		
		AREA.getIdarea();
		
		long idArea = AREA.getIdarea();

		Area AreaGuardar = areaService.buscarAreaId(idArea);
		
		usuario.setArea(AreaGuardar);
		
		usuarioservice.GuardarUsuario(usuario);
		
		notificacion = "Datos actualizado exitosamente";
		
		redirectAttributes.addFlashAttribute("notificacion" , notificacion);
		
		String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ " + notificacion + " ]";

		registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);

		return "redirect:/listaUsuarios";

	}

	@RequestMapping(value = "/EliminarUsuario/{id}", method = RequestMethod.GET)
	public String EliminarUsuario(@PathVariable("id") long id, ModelMap mp , RedirectAttributes redirectAttributes , Principal principal) {

		String notificacion = "";
		
		String NombreUsuarioLogeado = principal.getName();
		
		Usuarios usuarioEliminar = usuarioservice.buscarUsuarioId(id);
		
		String Rol = usuarioEliminar.getRol().get(0).getName();
		
		if (Rol.contains("Administrador")) {
			
			notificacion = "Usuario que tratas de eliminar es de rol administrador, por favor cambiar de rol para continuar con el proceso";
			
			redirectAttributes.addFlashAttribute("notificacion" , notificacion);
			
			return "redirect:/listaUsuarios";
		}

		DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

		String date = dateFormat.format(Calendar.getInstance().getTime());
		
		notificacion = "Usuario eliminado correctamente";
		
		redirectAttributes.addFlashAttribute("notificacion" , notificacion);
		
		usuarioservice.eliminarUsuario(id);
		
		String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ " + "Elimino el usuario con el id : " + id + "]";

		registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);

		return "redirect:/listaUsuarios";
	}

	@RequestMapping(value = "/DesabilitarUsuario/{id}", method = RequestMethod.GET)
	public String DesabilitarUsuario(@PathVariable("id") long id, HttpServletRequest request, ModelMap mp , RedirectAttributes redirectAttributes , Principal principal) {

		String notificacion = "";
		
		String NombreUsuarioLogeado = principal.getName();

		DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

		String date = dateFormat.format(Calendar.getInstance().getTime());
		
		Usuarios usuario = null;
		
		Usuarios usuarioDesabilitar = usuarioservice.buscarUsuarioId(id);
		
		String Rol = usuarioDesabilitar.getRol().get(0).getName();
		
		if (usuarioDesabilitar.getRol().get(0).getId() == 1) {
			
			redirectAttributes.addFlashAttribute("notificacion", "El rol que tratas de deshabilitar es de administrador por favor cambiar el rol e inténtalo nuevamente");
			
			return "redirect:/listaUsuarios";
			
		}
		
		if (usuarioDesabilitar.getRol().get(0).getId() == 8) {
			
			redirectAttributes.addFlashAttribute("notificacion", "El rol que tratas de deshabilitar es de director contable por favor cambiar el rol e inténtalo nuevamente");
			
			return "redirect:/listaUsuarios";
			
		}
		
		if (usuarioDesabilitar.getRol().get(0).getId() == 7) {
			
			redirectAttributes.addFlashAttribute("notificacion", "El rol que tratas de deshabilitar es de centralizador por favor cambiar el rol e inténtalo nuevamente");
			
			return "redirect:/listaUsuarios";
			
		}
		
		usuario = usuarioservice.buscarUsuarioId(id);

		usuario.setEstadoUsuario("Inactivo");
		
		notificacion = "Estado de usuario cambiado a Inactivo";
		
		redirectAttributes.addFlashAttribute("notificacion" , notificacion);

		usuarioservice.GuardarUsuario(usuario);
		
		String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ " + "Desabilito el usuario con el id : " + id + "]";

		registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);

		return "redirect:/listaUsuarios";
	}

	@RequestMapping(value = "/HabilitarUsuario/{id}", method = RequestMethod.GET)
	public String HabilitarUsuario(@PathVariable("id") long id, HttpServletRequest request, ModelMap mp , RedirectAttributes redirectAttributes , Principal principal) {

		String NombreUsuarioLogeado = principal.getName();

		DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

		String date = dateFormat.format(Calendar.getInstance().getTime());
		
		Usuarios usuario = null;

		usuario = usuarioservice.buscarUsuarioId(id);
		
		if (usuario.getRol().get(0).getName().contains("Administrador")) {
			
			
			
		}

		usuario.setEstadoUsuario("Activo");
		
		String notificacion = "";
		
		notificacion = "Estado de usuario cambiado a Activo";
		
		redirectAttributes.addFlashAttribute("notificacion" , notificacion);

		usuarioservice.GuardarUsuario(usuario);
		
		String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ " + "Habilito el usuario con el id : " + id + "]";

		registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);

		return "redirect:/listaUsuarios";
	}

	@RequestMapping("/BuscarUsuario")
	public String BuscarUsuario(@RequestParam(value = "dato", required = false) String dato, ModelMap mp, RedirectAttributes redirectAttributes , Principal principal) {

		String NombreUsuarioLogeado = principal.getName();

		DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

		String date = dateFormat.format(Calendar.getInstance().getTime());
		
		String datoentrada = dato;

		boolean isNumeric = (datoentrada != null && datoentrada.matches("[0-9]+"));

		//System.out.println("el dato es " + datoentrada + isNumeric);

		if (isNumeric == true) {

			int cedula = Integer.parseInt(datoentrada);

			redirectAttributes.addFlashAttribute("cedula", cedula);
			
			String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ " + "busco el usuario con la cedula : " + cedula + "]";

			registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);

			return "redirect:/BuscarUsuarioCedula";
		}

		if (isNumeric == false && datoentrada.matches("^[a-zA-Z0-9]*$")) {

			String NombreUsuario = datoentrada;

			//System.out.println("Nombre de usuario a buscar es : " + NombreUsuario);

			redirectAttributes.addFlashAttribute("NombreUsuario", NombreUsuario);
			
			String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ " + "busco el usuario con el usuario : " + NombreUsuario + "]";

			registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);

			return "redirect:/BuscarUsuarioUser";
		}
		
		if (isNumeric == false && datoentrada.contains("@") && datoentrada.contains(".com")) {
			
			String correo = datoentrada;
			
			//System.out.print("el correo a bsucar es :" + correo);
			
			redirectAttributes.addFlashAttribute("correo", correo);
			
			String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ " + "busco el usuario con el correo : " + correo + "]";

			registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);
			
			return "redirect:/BuscarUsuarioCorreo";
		}

		else {
			
			String Nombre = datoentrada;

			//System.out.print("Nombre a buscar es : " + Nombre);

			redirectAttributes.addFlashAttribute("Nombre", Nombre);
			
			String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ " + "busco el usuario con el nombre: " + Nombre + "]";

			registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);

			return "redirect:/BuscarUsuarioNombre";
		}

	}

	@RequestMapping(value = "/BuscarUsuarioCedula", method = RequestMethod.GET)
	public String BuscarPorCedula(HttpServletRequest request, Model model, ModelMap mp, RedirectAttributes redirectAttributes, Usuarios usuario , Principal principal) {

		String NombreUsuarioLogeado = principal.getName();
		
		Usuarios usuariologueado = usuarioservice.buscarUsuario(NombreUsuarioLogeado);
		
		String Rol = usuariologueado.getRol().get(0).getName();
		
		mp.put("Area", usuariologueado.getArea().getArea());
		
		mp.put("rol", Rol);
		
		String notificacion = "";
		
		int cedula = (int) model.asMap().get("cedula");

		//System.out.println("la cedula a buscar es " + cedula);

		Usuarios usuariosCedula = usuarioservice.buscarPorCedula(cedula);

		if (usuariosCedula == null) {
			
			notificacion = "Usuario con el documento " + cedula + "no encontrado ";
			
			redirectAttributes.addFlashAttribute("notificacion" , notificacion);

			//System.out.print("El nombre del usuario con el documento " + cedula + "no encontrado ");

			return "redirect:/listaUsuarios";
		}

		mp.put("usuariosCedula", usuariosCedula);

		return "UsuariosListaBusquedaCedula";
	}
	
	@RequestMapping(value = "/BuscarUsuarioCorreo", method = RequestMethod.GET)
	public String BuscarPorCorreo(HttpServletRequest request, Model model, ModelMap mp, RedirectAttributes redirectAttributes, Usuarios usuario , Principal principal) {

		String notificacion = "";
		
		String NombreUsuarioLogeado = principal.getName();
		
		Usuarios usuariologueado = usuarioservice.buscarUsuario(NombreUsuarioLogeado);
		
		String Rol = usuariologueado.getRol().get(0).getName();
		
		mp.put("Area", usuariologueado.getArea().getArea());
		
		mp.put("rol", Rol);
		
		String correo = (String) model.asMap().get("correo");

		//System.out.println("el correo a buscar es " + correo);

		Usuarios usuariosCorreo = usuarioservice.buscarPorEmail(correo);

		if (usuariosCorreo == null) {
			
			notificacion = "Correo :  " + correo + "no encontrado ";
			
			redirectAttributes.addFlashAttribute("notificacion" , notificacion);

			//System.out.print("El nombre del usuario con el correo :  " + correo + "no encontrado ");

			return "redirect:/listaUsuarios";
		}

		mp.put("usuariosCorreo", usuariosCorreo);

		return "UsuariosListaBusquedaCorreo";
	}

	@RequestMapping(value = "/BuscarUsuarioNombre", method = RequestMethod.GET)
	public String BuscarPorNombre(HttpServletRequest request, Model model, ModelMap mp, RedirectAttributes redirectAttributes, Usuarios usuario , Principal principal) {

		String NombreUsuarioLogeado = principal.getName();
		
		Usuarios usuariologueado = usuarioservice.buscarUsuario(NombreUsuarioLogeado);
		
		String Rol = usuariologueado.getRol().get(0).getName();
		
		mp.put("Area", usuariologueado.getArea().getArea());
		
		mp.put("rol", Rol);
		
		String notificacion = "";
		
		String Nombre = (String) model.asMap().get("Nombre");

		Usuarios usuariosNombre = usuarioservice.buscarPorNombre(Nombre);

		if (usuariosNombre == null) {
			
			notificacion = "El Nombre " + Nombre + "no fue encontrado";
			
			redirectAttributes.addFlashAttribute("notificacion" , notificacion);

			//System.out.print("El nombre del usuario con el nombre " + Nombre + "no encontrado ");

			return "redirect:/listaUsuarios";
		}

		mp.put("usuariosNombre", usuariosNombre);

		return "UsuariosListaBusquedaNombre";
	}

	@RequestMapping(value = "/BuscarUsuarioUser", method = RequestMethod.GET)
	public String BuscarPorNombreUsuario(HttpServletRequest request, Model model, ModelMap mp, RedirectAttributes redirectAttributes, Usuarios usuario , Principal principal) {

		String NombreUsuarioLogeado = principal.getName();
		
		Usuarios usuariologueado = usuarioservice.buscarUsuario(NombreUsuarioLogeado);
		
		String Rol = usuariologueado.getRol().get(0).getName();
		
		mp.put("Area", usuariologueado.getArea().getArea());
		
		mp.put("rol", Rol);
		String notificacion = "";
		
		String NombreUsuario = (String) model.asMap().get("NombreUsuario");

		//System.out.println("el usuario  : " + NombreUsuario);

		Usuarios usuariosNombreUser = usuarioservice.buscarUsuario(NombreUsuario);

		if (usuariosNombreUser == null) {

			notificacion = "El usuario " + NombreUsuario + " no fue encontrado ";
			
			redirectAttributes.addFlashAttribute("notificacion" , notificacion);
			
			//System.out.println("El usuario " + NombreUsuario + " no fue encontrado ");

			return "redirect:/listaUsuarios";
		}

		mp.put("usuariosNombreUser", usuariosNombreUser);

		return "UsuariosListaBusquedaUsername";
	}
	
	@GetMapping("/GenerarSoporteUsuarios")
	public String CrearCarpetaReporteAreas(ModelMap mp , RedirectAttributes redirectAttributes) {
		
		LocalDate Fechacargue = LocalDate.now();
		 
		 String FechaHoy = Fechacargue.toString();
		 
		 FechaHoy = FechaHoy.replace("-", "");
		
		 String ruta = "..\\ReporteUsuarios\\";
		 	 
		 cargarArchivo.init(ruta);
		 
		//Crear libro de trabajo en blanco
		
		 Workbook libro = new XSSFWorkbook();
		 
		 final String nombreArchivo = "ReporteUsuarios" + Fechacargue + ".xlsx";
		 
		 Sheet hoja = libro.createSheet("Hoja 1");
		 
		 List<Usuarios> listaUsuarios = usuarioservice.listaUsuarios();
		 
		 String[] encabezados = {"NOMBRE COMPLETO", "USUARIO", "IDENTIFICACIÓN" , "CORREO" , "ROL" , "AREA" , "VICEPRESIDENCIA" , "ESTADO" , "CONTRASEÑA"};
		 
		 int indiceFila = 0;
		 
		 Row fila = hoja.createRow(indiceFila);
	       
		 for (int i = 0; i < encabezados.length; i++) {
			 
	            String encabezado = encabezados[i];
	            
	            Cell celda = fila.createCell(i);
	            
	            celda.setCellValue(encabezado);
	        }
		 
		 indiceFila++;
		 
	        for (int i = 0; i < listaUsuarios.size(); i++) {
	        	
	            fila = hoja.createRow(indiceFila);
	            
	            Usuarios usuariosActual = listaUsuarios.get(i);
	            

	            
	            fila.createCell(0).setCellValue(usuariosActual.getNombre());
	            
	            fila.createCell(1).setCellValue(usuariosActual.getUsername());
	            
	            fila.createCell(2).setCellValue(usuariosActual.getDocumento());
	            
	            fila.createCell(3).setCellValue(usuariosActual.getCorreo());
	            
	            fila.createCell(4).setCellValue(usuariosActual.getRol().get(0).getName());
	            
	            fila.createCell(5).setCellValue(usuariosActual.getArea().getArea());
	            
	            fila.createCell(6).setCellValue(usuariosActual.getArea().getPresidencia());
	            
	            fila.createCell(7).setCellValue(usuariosActual.getEstadoUsuario());
	            
	            fila.createCell(8).setCellValue(usuariosActual.getEstadoContra());
	            
	            
	            indiceFila++;
	        }
	        
	        String rutaCompleta = ruta + nombreArchivo;
	       
	        FileOutputStream outputStream;
	        
	        try {
	        	
	            outputStream = new FileOutputStream(ruta + nombreArchivo);
	           
	            libro.write(outputStream);
	            
	            libro.close();
	            
	            //System.out.println("Libro de personas guardado correctamente");
	            
	            
		        redirectAttributes.addFlashAttribute("nombreArchivo", nombreArchivo);

		        redirectAttributes.addFlashAttribute("rutaCompleta", rutaCompleta);
	     
		        return "redirect:/DescargarSoporteUsuarios";
	       
	        } catch (FileNotFoundException ex) {
	        	
	            //System.out.println("Error de filenotfound");
	            
	    		
		        redirectAttributes.addFlashAttribute("notificacion","Error al generar el reporte");

	     
		        return "redirect:/listaUsuarios";
	        
	        } catch (IOException ex) {
	           
	        	//System.out.println("Error de IOException");
	            
	    		
		        redirectAttributes.addFlashAttribute("NombreArchivo","Error al generar el reporte");

	     
		        return "redirect:/listaUsuarios";
	        }

	}

	@RequestMapping(value = "/DescargarSoporteUsuarios")
	public void download( HttpServletResponse response , Principal principal , Model model , RedirectAttributes redirectAttributes) throws Exception {
		// Dirección del archivo, el entorno real se almacena en la base de datos
		
		String rutaCompleta = (String) model.asMap().get("rutaCompleta");
		
		String nombreArchivo = (String) model.asMap().get("nombreArchivo");
		
		//System.out.println("la ruat del archivo completa es " + rutaCompleta);
		
		//System.out.println("el nombre del archivo es " + nombreArchivo);
		
		String NombreUsuarioLogeado = principal.getName();

		DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");
		
		String date = dateFormat.format(Calendar.getInstance().getTime());
		
		File file = new File(rutaCompleta);
		
		String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [Desacargo el archivo " + rutaCompleta + "]";

		registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);
		
		//File file = new File("..\\Planos\\siig11ajc.txt");
		 // Llevando objeto de entrada
		FileInputStream fis = new FileInputStream(file);
		 // Establecer el formato relevante
		response.setContentType("application/force-download");
		 // Establecer el nombre y el encabezado del archivo descargado
		response.addHeader("Content-disposition", "attachment;fileName=" + nombreArchivo);
		 // Crear objeto de salida
		OutputStream os = response.getOutputStream();
		 // operación normal
		byte[] buf = new byte[1024];
		int len = 0;
		while((len = fis.read(buf)) != -1) {
			os.write(buf, 0, len);
		}
		
		fis.close();
		
	
	}
	

}
