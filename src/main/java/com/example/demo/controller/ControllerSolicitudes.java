package com.example.demo.controller;


import java.security.Principal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.example.demo.RegistrosLog;
import com.example.demo.Entity.Area;
import com.example.demo.Entity.Mensaje;
import com.example.demo.Entity.Rol;
import com.example.demo.Entity.SolicitudesCrearUsuario;
import com.example.demo.Entity.Usuarios;
import com.example.demo.Paginador.Paginador;
import com.example.demo.service.AreaService;
import com.example.demo.service.MensajeService;
import com.example.demo.service.RolService;
import com.example.demo.service.SolicitudesCrearUsuarioService;
import com.example.demo.service.UsuariosService;
import com.example.demo.service.emailEnviar;

@Controller
public class ControllerSolicitudes {
	
	@Autowired
	private MensajeService mensajeservice;
	
	@Autowired
	private UsuariosService usuarioservice;
	
	@Autowired
	private RolService rolService;

	@Autowired
	private AreaService areaService;
	
	@Autowired
	private SolicitudesCrearUsuarioService SolicitudService;
	
	@Autowired
	private emailEnviar emailSender;
	
	private String notificacion = "";
	
	private RegistrosLog registrolog = new RegistrosLog();
	
	// -------------------------------- lista de solicitudes administrador ------------------------------
	
	// crear tabla con los atos de solicitud
		@GetMapping("/SolicitudUsuariosCrear")
		public String CrearUsuarioAutorizador(ModelMap mp , Principal principal) {
			
			String NombreUsuarioLogeado = principal.getName();
			
			DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

			String date = dateFormat.format(Calendar.getInstance().getTime());
			
			Usuarios usuariologueado = usuarioservice.buscarUsuario(NombreUsuarioLogeado);
			
			String Rol = usuariologueado.getRol().get(0).getName();
			
			mp.put("Area", usuariologueado.getArea().getArea());
			
			mp.put("rol", Rol);

			Usuarios usuarios = new Usuarios();
			
			mp.put("roles", rolService.listaRoles());
			mp.put("Areas", areaService.listaAreas());
			mp.put("usuario", usuarios);
			
			String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ Accedio al listado de solicitudes ]";

			registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);

			return "UsuariosSolicitudesCrearUsu";
		}
		
		@PostMapping("/guardarSolicitudCrearUsu")
		public String GuardarPeticionCrearUsu(@RequestParam(value = "nombre", required = false) String Nombre , @RequestParam(value = "usuario", required = false) String usuario , @RequestParam(value = "correo", required = false) String correo , @RequestParam(value = "identificacion", required = false) int identificacion , @RequestParam(value = "area", required = false) String NombreArea, @RequestParam(value = "nombreRol", required = false) String nombreRol, @RequestParam(value = "correoautorizador", required = false) String correoautorizador, ModelMap mp , Model model , RedirectAttributes redirectAttributes , Principal principal) {
			
			//System.out.println("el nombre del area es " + NombreArea);
			
			String NombreUsuarioLogeado = principal.getName();
			
			String correoAutorizador = usuarioservice.buscarUsuario(NombreUsuarioLogeado).getCorreo();
			
			DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

			String date = dateFormat.format(Calendar.getInstance().getTime());
			
			Area Areaguardar = areaService.BuscarAreaAntigua(NombreArea);
			
			long idarea = Areaguardar.getIdarea();
			
			String areaGuardar = Areaguardar.getArea();
			
			String Nombreu = Nombre;
			
			String NombreUsuario = usuario;
			
			String Correo = correo;
			
			String correoPichincha = "@pichincha.com.co";
			
			String numCedula= String.valueOf(identificacion);
			
			
			if (Correo.contains(correoPichincha) == false || Correo.matches(Nombre)) {
				
				notificacion = "Correo " + Correo + "No valido";
				
				redirectAttributes.addFlashAttribute("notificacion" , notificacion);
				
				String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ " + notificacion + " ]";

				registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);
				
				return "redirect:/crearUsuario";
				
			}
			
			
			if (NombreUsuario.matches("^[a-zA-Z0-9]*$") == false && NombreUsuario.matches(numCedula) == false) {
				
				notificacion = "Nombre de usuario " + NombreUsuario + "No valido ";
				
				redirectAttributes.addFlashAttribute("notificacion" , notificacion);
				
				String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ " + notificacion + " ]";

				registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);
				
				return "redirect:/crearUsuario";
				
			}
			
			String Role = nombreRol;
			
			SolicitudesCrearUsuario solicitud = new SolicitudesCrearUsuario();
			
			solicitud.setNombre(Nombreu);
			
			solicitud.setUsername(NombreUsuario);
			
			solicitud.setArea(areaGuardar);
			
			solicitud.setIdArea(idarea);
			
			solicitud.setCorreo(Correo);
			
			solicitud.setEstadoUsuario("Activo");
			
			solicitud.setEstadoContra("Sin Asignar");
			
			solicitud.setDocumento(identificacion);
			
			solicitud.setEstado("Enviada");
			
			solicitud.setTipo("Crear usuario");
			
			solicitud.setUsuario(NombreUsuarioLogeado);
			
			solicitud.setRol(nombreRol);
			
			solicitud.setCorreoAutorizador(correoAutorizador);
			
			SolicitudService.GuardarSolicitud(solicitud);	
			
			notificacion = "La solicitud se envió con éxito al administrador";
				
			redirectAttributes.addFlashAttribute("notificacion" , notificacion);
				
			String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ " + " envio la solicitud de crear usurio ]";

			registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);
				
			return "redirect:/bandejaEntradaLote";
	
			
		}
		
		// --------------------- adminstrador --------------------------------------
		
		@RequestMapping(value = "/listaSolicitudesBloquearUsu" , method = RequestMethod.POST)
		public String SolicitudesBloquearUusario (@RequestParam(name = "page" , defaultValue = "0" )int page  ,ModelMap mp , RedirectAttributes redirectAttributes , Principal principal){
			
			String NombreUsuarioLogeado = principal.getName();
			
			Usuarios usuariologueado = usuarioservice.buscarUsuario(NombreUsuarioLogeado);
			
			String Rol = usuariologueado.getRol().get(0).getName();
			
			mp.put("Area", usuariologueado.getArea().getArea());
			
			mp.put("rol", Rol);
			
			Pageable pageRequest = PageRequest.of(page, 5);
			
			Page <SolicitudesCrearUsuario> SolicitudesUsuario = SolicitudService.listaSolicitudesBloquear("Bloqueo Usuario","Enviada", pageRequest);

			Paginador <SolicitudesCrearUsuario> pageRender = new Paginador<>("/listaSolicitudesBloquearUsu" , SolicitudesUsuario);
			
			int cantidadsolisitudes = SolicitudesUsuario.getNumberOfElements();
			
			String notificacion = "";

			mp.put("CatidadSolicitudes", cantidadsolisitudes);
			mp.put("ListaUsuarioCrear", SolicitudesUsuario);
			mp.put("totalAreas", cantidadsolisitudes);
			mp.put("page", pageRender);
			
			return "UsuariosListaSolicitudesEnviadasBloqueoUsu";
		}
	

		@RequestMapping(value = "/listaSolicitudesCrearUsuario" , method = RequestMethod.POST)
		public String VerSolicitudes (@RequestParam(name = "page" , defaultValue = "0")  int page  ,ModelMap mp , RedirectAttributes redirectAttributes , Principal principal) {
			
			
			
			String NombreUsuarioLogeado = principal.getName();
			
			Usuarios usuariologueado = usuarioservice.buscarUsuario(NombreUsuarioLogeado);
			
			String Rol = usuariologueado.getRol().get(0).getName();
			
			mp.put("Area", usuariologueado.getArea().getArea());
			
			mp.put("rol", Rol);
			
			Pageable pageRequest = PageRequest.of(page, 5);
			
			Page <SolicitudesCrearUsuario> SolicitudesUsuario = SolicitudService.listaSolcitudesRechazadas("Enviada", pageRequest);

			Paginador <SolicitudesCrearUsuario> pageRender = new Paginador<>("/listaSolicitudesCrearUsuario" , SolicitudesUsuario);
			
			int cantidadsolisitudes = SolicitudesUsuario.getNumberOfElements();
			
			String notificacion = "";

			mp.put("CatidadSolicitudes", cantidadsolisitudes);
			mp.put("ListaUsuarioCrear", SolicitudesUsuario);
			mp.put("totalAreas", cantidadsolisitudes);
			mp.put("page", pageRender);
			
			
			return "UsuariosListaSolicitudesCrear";
		}
		
		@RequestMapping(value = "/verdetallesSolicitud/{id}", method = RequestMethod.GET)
		public String verdetallesSolicitud(@PathVariable("id") long id , Model model , ModelMap mp , RedirectAttributes redirectAttributes , Principal principal) {

			String NombreUsuarioLogeado = principal.getName();
			
			Usuarios usuariologueado = usuarioservice.buscarUsuario(NombreUsuarioLogeado);
			
			String Rol = usuariologueado.getRol().get(0).getName();
			
			mp.put("Area", usuariologueado.getArea().getArea());
			
			mp.put("rol", Rol);
			
			SolicitudesCrearUsuario detallesSolicitud = SolicitudService.VerDetalles(id);
			
			mp.addAttribute("detallesSolicitud", detallesSolicitud);
			
			return "UsuariosVerDetalleSolicitud";
		}
		
		@RequestMapping(value = "/AprobarUsuarioBloqueo/{id}", method = RequestMethod.GET)
		public String AprobarUsuarioBloqueo(@PathVariable("id") long id , Model model , RedirectAttributes redirectAttributes , Principal principal) {
	
		DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");
			
		String date = dateFormat.format(Calendar.getInstance().getTime());
			
		Mensaje mensaje2 = new Mensaje();	
			
		SolicitudesCrearUsuario Solicitud = SolicitudService.traerSolicitud(id);
		
		String usuario = Solicitud.getUsername();
		
		Usuarios usuarioBloquear = usuarioservice.buscarUsuario(usuario);
		
		usuarioBloquear.setEstadoUsuario("Bloqueado");
		
		usuarioservice.GuardarUsuario(usuarioBloquear);
		
		Solicitud.setEstado("Aprobada");
		
		SolicitudService.GuardarSolicitud(Solicitud);
		
		redirectAttributes.addFlashAttribute(notificacion, "El usuario " + usuario + " ha sido bloqueado");
		
		String NombreAutorizador = usuarioservice.buscarPorEmail(Solicitud.getCorreo()).getNombre();
		
		mensaje2.setComentario("Tu solicitud de bloqueo para el usuario " + Solicitud.getUsuario() + " ha sido aprobada");
		mensaje2.setDestinatario(NombreAutorizador);
		mensaje2.setEstado("No Leido");
		mensaje2.setFecha(date);
		
		mensajeservice.enviarMensaje(mensaje2);

			
		 return "redirect:/listaUsuarios";
		}
		
		
		@RequestMapping(value = "/AprobarUsuario/{id}", method = RequestMethod.GET)
		public String AprobarCrearUsuario(@PathVariable("id") long id , Model model , RedirectAttributes redirectAttributes , Principal principal) {
			
			DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");
			
			String date = dateFormat.format(Calendar.getInstance().getTime());
			
			Mensaje mensaje2 = new Mensaje();	
			
			Usuarios usuario = new Usuarios();
			
			SolicitudesCrearUsuario Solicitud = SolicitudService.traerSolicitud(id);
			
			String NombreAutorizador = Solicitud.getUsuario();
			
			String Tipo = Solicitud.getTipo();
			
			Usuarios Autorizador = usuarioservice.buscarUsuario(NombreAutorizador);
			
			if (Tipo.contains("Crear usuario")) {
				
				
				
				String CorreoAutorizador = Autorizador.getCorreo();
					
				String nombre = Solicitud.getNombre();
				
				String NombreUsuario = Solicitud.getUsername();
				
				String Correo = Solicitud.getCorreo();
				
				int documento = Solicitud.getDocumento();
				
				long idarea = Solicitud.getIdArea();
				
				String Rol = Solicitud.getRol();
				
				Area Areaguardar = areaService.buscarAreaId(idarea);
				
				List<Rol> RolGuardar = rolService.BuscarRolNombre(Rol);
				
				usuario.setNombre(nombre);
				
				usuario.setUsername(NombreUsuario);
				
				usuario.setCorreo(Correo);
				
				usuario.setDocumento(documento);
				
				usuario.setEstadoUsuario("Activo");
				
				usuario.setEstadoContra("Sin Asignar");
				
				usuario.setArea(Areaguardar);
				
				usuario.setRol(RolGuardar);
				
				if(Rol.contains("Autorizador nivel 1")) {
					
					Areaguardar.setAutorizadorNivel1(nombre);
					
					areaService.GuardarArea(Areaguardar);
				}
				
				if(Rol.contains("Autorizador nivel 2")) {
					
					Areaguardar.setAutorizadorNivel2(nombre);
					
					areaService.GuardarArea(Areaguardar);
				}

		 
				emailSender.sendEmail("Su solicitud de Crear usuario ha sido aprobada", "Test Message : para el usuario :  " + NombreUsuario , CorreoAutorizador , "administracionlotes@pichincha.com.co" /* PONER EL CORREO DEL ADMIN */ , true );
				
				
				String notificacion = "";

				usuarioservice.GuardarUsuario(usuario);
				
				Solicitud.setEstado("Aprobada");
				
				SolicitudService.GuardarSolicitud(Solicitud);
				
				notificacion = "La solicitud ha sido aprobada";
				
				redirectAttributes.addFlashAttribute("notificacion",notificacion);
				
				mensaje2.setComentario("Tu solicitud de creación para el usuario " + NombreUsuario + " ha sido aprobada");
				mensaje2.setDestinatario(Autorizador.getNombre());
				mensaje2.setEstado("No Leido");
				mensaje2.setFecha(date);
				
				mensajeservice.enviarMensaje(mensaje2);
				
				return "redirect:/listaUsuarios";
				
			} else {
				
				String usuarioBlo = Solicitud.getUsername();
				
				String CorreoAuto = Solicitud.getCorreoAutorizador();
				
				Usuarios usuarioBloquear = usuarioservice.buscarUsuario(usuarioBlo);
				
				usuarioBloquear.setEstadoUsuario("Bloqueado");
				
				usuarioservice.GuardarUsuario(usuarioBloquear);
				
				Solicitud.setEstado("Aprobada");
				
				SolicitudService.GuardarSolicitud(Solicitud);
				
				redirectAttributes.addFlashAttribute(notificacion, "El usuario " + usuario + " ha sido bloqueado");
				
				emailSender.sendEmail("Su solicitud de bloque ha sido aprobada", "Test Message : para el usuario :  " + usuarioBlo , CorreoAuto , "administracionlotes@pichincha.com.co" /* PONER EL CORREO DEL ADMIN */ , true );
					
				mensaje2.setComentario("Tu solicitud de bloqueo para el usuario " + Solicitud.getUsuario() + " ha sido aprobada");
				mensaje2.setDestinatario(Autorizador.getNombre());
				mensaje2.setEstado("No Leido");
				mensaje2.setFecha(date);
				
				mensajeservice.enviarMensaje(mensaje2);
				
				 return "redirect:/listaUsuarios";

			}
			
		
		}
		
		@RequestMapping(value = "/RechazarSolicitudUsuario/{id}", method = RequestMethod.GET)
		public String RechazrCrearUsuario(@PathVariable("id") long id, ModelMap mp , RedirectAttributes redirectAttributes , Principal principal) {

			DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");
			
			String date = dateFormat.format(Calendar.getInstance().getTime());
			
			Mensaje mensaje2 = new Mensaje();
			
			SolicitudesCrearUsuario Solicitud = SolicitudService.traerSolicitud(id);
			
			if (Solicitud.getTipo() == "Crear Usuario") {
				
				String NombreAutorizador = Solicitud.getUsuario();
				
				Usuarios Autorizador = usuarioservice.buscarUsuario(NombreAutorizador);
				
				String CorreoAutorizador = Autorizador.getCorreo();
					
				String notificacion = "";
				
				emailSender.sendEmail("Su solicitud ha sido rechazada el usuario ", "Test Message : para el usuario :   " + NombreAutorizador , CorreoAutorizador , "administracionlotes@pichincha.com.co" /* PONER EL CORREO DEL ADMIN */ , true);

				notificacion = "La solicitud de creación ha ha sido rechazada";
				
				redirectAttributes.addFlashAttribute("notificacion" , notificacion);
				
				Solicitud.setEstado("Rechazada");
				
				SolicitudService.GuardarSolicitud(Solicitud);
				
				mensaje2.setComentario("Tu solicitud de creación para el usuario " + Solicitud.getUsuario() + " ha sido rechazada");
				mensaje2.setDestinatario(Autorizador.getNombre());
				mensaje2.setEstado("No Leido");
				mensaje2.setFecha(date);
				
				mensajeservice.enviarMensaje(mensaje2);

				return "redirect:/listaUsuarios";
				
			}else {
				
				String NombreAutorizador = Solicitud.getUsuario();
				
				Usuarios Autorizador = usuarioservice.buscarUsuario(NombreAutorizador);
				
				String CorreoAutorizador = Autorizador.getCorreo();
					
				String notificacion = "";
				
				emailSender.sendEmail("Su solicitud de bloque de usuario ha sido rechazada ", "Test Message : para el usuario :   " + NombreAutorizador , CorreoAutorizador , "administracionlotes@pichincha.com.co" /* PONER EL CORREO DEL ADMIN */ , true);
				
				notificacion = "La solicitud de bloqueo de usuario ha sido rechazada";
				
				redirectAttributes.addFlashAttribute("notificacion" , notificacion);
				
				Solicitud.setEstado("Rechazada");
				
				SolicitudService.GuardarSolicitud(Solicitud);
				
				mensaje2.setComentario("Tu solicitud de bloque para el usuario " + Solicitud.getUsuario() + " ha sido rechazada");
				mensaje2.setDestinatario(Autorizador.getNombre());
				mensaje2.setEstado("No Leido");
				mensaje2.setFecha(date);
				
				mensajeservice.enviarMensaje(mensaje2);
				
				return "redirect:/listaUsuarios";
			}
			
			
		}
		
		/* lista de solicitudes rechazadas y autorizadas administrador */
		
		@RequestMapping(value = "/SolicitudesRechazadas", method = RequestMethod.GET)
		public String SolicitudesRechazadas( @RequestParam(name = "page" , defaultValue = "0")  int page , ModelMap mp , RedirectAttributes redirectAttributes , Principal principal) {
			
			String NombreUsuarioLogeado = principal.getName();
			
			Usuarios usuariologueado = usuarioservice.buscarUsuario(NombreUsuarioLogeado);
			
			String Rol = usuariologueado.getRol().get(0).getName();
			
			mp.put("Area", usuariologueado.getArea().getArea());
			
			mp.put("rol", Rol);
	
			Pageable pageRequest = PageRequest.of(page, 5);
			
			Page <SolicitudesCrearUsuario> SolicitudesUsuario = SolicitudService.listaSolcitudesRechazadas("Rechazada" ,pageRequest);

			Paginador <SolicitudesCrearUsuario> pageRender = new Paginador<>("/SolicitudesRechazadas" , SolicitudesUsuario);
			
			int cantidadsolisitudes = SolicitudesUsuario.getNumberOfElements();
			
			String notificacion = "";

			mp.put("CatidadSolicitudes", cantidadsolisitudes);
			mp.put("ListaUsuarioCrear", SolicitudesUsuario);
			mp.put("totalAreas", cantidadsolisitudes);
			mp.put("page", pageRender);
			

			return "UsuariosListaSolicitudesRechazadas";
		}
		
		@RequestMapping(value = "/SolicitudesAprobadas", method = RequestMethod.GET)
		public String SolicitudesAprobadas(@RequestParam(name = "page" , defaultValue = "0")  int page , ModelMap mp , RedirectAttributes redirectAttributes , Principal principal) {

			String NombreUsuarioLogeado = principal.getName();
			
			Usuarios usuariologueado = usuarioservice.buscarUsuario(NombreUsuarioLogeado);
			
			String Rol = usuariologueado.getRol().get(0).getName();
			
			mp.put("Area", usuariologueado.getArea().getArea());
			
			mp.put("rol", Rol);
	
			Pageable pageRequest = PageRequest.of(page, 5);
			
			Page <SolicitudesCrearUsuario> SolicitudesUsuario = SolicitudService.listaSolcitudesRechazadas("Aprobada" ,pageRequest);

			Paginador <SolicitudesCrearUsuario> pageRender = new Paginador<>("/SolicitudesAprobadas" , SolicitudesUsuario);
			
			int cantidadsolisitudes = SolicitudesUsuario.getNumberOfElements();
			
			String notificacion = "";

			mp.put("CatidadSolicitudes", cantidadsolisitudes);
			mp.put("ListaUsuarioCrear", SolicitudesUsuario);
			mp.put("totalAreas", cantidadsolisitudes);
			mp.put("page", pageRender);

			return "UsuariosListaSolicitudesAprobadas";
		}
		
	// ----------------------- lista solicitudes AUTORIZADORES ------------------------------ */
		
		@RequestMapping(value = "/listaSolicitudesCrearUsuarioAutori" , method = RequestMethod.GET)
		public String VerSolicitudesEnviadasAuto (@RequestParam(name = "page" , defaultValue = "0")  int page  ,ModelMap mp , RedirectAttributes redirectAttributes , Principal principal) {
			
			String NombreUsuarioLogeado = principal.getName();
			
			Usuarios usuariologueado = usuarioservice.buscarUsuario(NombreUsuarioLogeado);
			
			String Rol = usuariologueado.getRol().get(0).getName();
			
			mp.put("Area", usuariologueado.getArea().getArea());
			
			mp.put("rol", Rol);
			
			Pageable pageRequest = PageRequest.of(page, 5);
			
			Page <SolicitudesCrearUsuario> SolicitudesUsuario = SolicitudService.ListaSolicitudesAproUsu(NombreUsuarioLogeado ,"Enviada", pageRequest);

			Paginador <SolicitudesCrearUsuario> pageRender = new Paginador<>("/listaSolicitudesCrearUsuario" , SolicitudesUsuario);
			
			int cantidadsolisitudes = SolicitudesUsuario.getNumberOfElements();
			
			String notificacion = "";

			mp.put("CatidadSolicitudes", cantidadsolisitudes);
			mp.put("ListaUsuarioCrear", SolicitudesUsuario);
			mp.put("totalAreas", cantidadsolisitudes);
			mp.put("page", pageRender);
			
			
			return "UsuariosListaSolicitudesEnviadas";
		}
		
		@RequestMapping(value = "/SolicitudesBloquearUsuario" ,  method = RequestMethod.GET)
		public String SolicitudesBloqueoUsu (@RequestParam(name = "page" , defaultValue = "0")  int page , ModelMap mp , Model model , RedirectAttributes redirectAttributes , Principal principal) {
			
			String NombreUsuarioLogeado = principal.getName();
			
			Usuarios usuariologueado = usuarioservice.buscarUsuario(NombreUsuarioLogeado);
			
			String Rol = usuariologueado.getRol().get(0).getName();
			
			mp.put("Area", usuariologueado.getArea().getArea());
			
			mp.put("rol", Rol);
			
			String notificacion = (String) model.asMap().get("notificacion");
			
			mp.put("notificacion", notificacion);
			
			return "UsuariosSolicitudesEnviarBloquearUsu";
		}
		
		@PostMapping(value = "/guardarSolicitudBloquearUsu")
		public String guardarSOlicitudBloque(@RequestParam(value = "usuario", required = false) String usuario , @RequestParam(value = "motivo", required = false) String motivo ,  RedirectAttributes redirectAttributes , Principal principal) {
			
			
			SolicitudesCrearUsuario solicitudBloque = new SolicitudesCrearUsuario();
			
			String NombreUsuarioLogeado = principal.getName();
			
			String correoAutorizador = usuarioservice.buscarUsuario(NombreUsuarioLogeado).getCorreo();
			
			//System.out.println("usuario " + usuario);
				
			if (usuarioservice.buscarPorEmail(usuario) == null) {
				
				redirectAttributes.addFlashAttribute("notificacion", "El usuario " + usuario + " No existe en el sistema , Por favor validar el nombre del usuario ingresado.");
				
				return "redirect:/SolicitudesBloquearUsuario";
			}
			
			else {
				
				String tipo = "Bloqueo Usuario" ;
				
				solicitudBloque.setEstado("Enviada");
				
				solicitudBloque.setTipo(tipo);
				
				solicitudBloque.setUsername(usuario);
				
				solicitudBloque.setUsuario(NombreUsuarioLogeado);
				
				solicitudBloque.setCorreoAutorizador(correoAutorizador);
				
				SolicitudService.GuardarSolicitud(solicitudBloque);
				
				redirectAttributes.addFlashAttribute("notificacion", "Se ha enviado tu solicitud de bloque de usuario recuerda remitir un correo al administrador explicando el motivo del bloqueo");
				
				
				return "redirect:/listaSolicitudesCrearUsuarioAutori";
				
			}
			
			
		}
		
		
		@RequestMapping(value = "/SolicitudesRechazadasAuto", method = RequestMethod.GET)
		public String SolicitudesRechazadasAuto( @RequestParam(name = "page" , defaultValue = "0")  int page , ModelMap mp , RedirectAttributes redirectAttributes , Principal principal) {
			
			String NombreUsuarioLogeado = principal.getName();
			
			Usuarios usuariologueado = usuarioservice.buscarUsuario(NombreUsuarioLogeado);
			
			String Rol = usuariologueado.getRol().get(0).getName();
			
			mp.put("Area", usuariologueado.getArea().getArea());
			
			mp.put("rol", Rol);
	
			Pageable pageRequest = PageRequest.of(page, 5);
			
			Page <SolicitudesCrearUsuario> SolicitudesUsuario = SolicitudService.ListaSolicitudesRechazadasUsua(NombreUsuarioLogeado ,"Rechazada" ,pageRequest);

			Paginador <SolicitudesCrearUsuario> pageRender = new Paginador<>("/SolicitudesRechazadas" , SolicitudesUsuario);
			
			int cantidadsolisitudes = SolicitudesUsuario.getNumberOfElements();
			
			String notificacion = "";

			mp.put("CatidadSolicitudes", cantidadsolisitudes);
			mp.put("ListaUsuarioCrear", SolicitudesUsuario);
			mp.put("totalAreas", cantidadsolisitudes);
			mp.put("page", pageRender);
			

			return "UsuariosListaSolicitudesRechazadasAuto";
		}
		
		@RequestMapping(value = "/SolicitudesAprobadasAuto", method = RequestMethod.GET)
		public String SolicitudesAprobadasAuto(@RequestParam(name = "page" , defaultValue = "0")  int page , ModelMap mp , RedirectAttributes redirectAttributes , Principal principal) {

			String NombreUsuarioLogeado = principal.getName();
			
			Usuarios usuariologueado = usuarioservice.buscarUsuario(NombreUsuarioLogeado);
			
			String Rol = usuariologueado.getRol().get(0).getName();
			
			mp.put("Area", usuariologueado.getArea().getArea());
			
			mp.put("rol", Rol);
	
			Pageable pageRequest = PageRequest.of(page, 5);
			
			Page <SolicitudesCrearUsuario> SolicitudesUsuario = SolicitudService.ListaSolicitudesRechazadasUsua(NombreUsuarioLogeado ,"Aprobada" ,pageRequest);

			Paginador <SolicitudesCrearUsuario> pageRender = new Paginador<>("/SolicitudesAprobadas" , SolicitudesUsuario);
			
			int cantidadsolisitudes = SolicitudesUsuario.getNumberOfElements();
			
			String notificacion = "";

			mp.put("CatidadSolicitudes", cantidadsolisitudes);
			mp.put("ListaUsuarioCrear", SolicitudesUsuario);
			mp.put("totalAreas", cantidadsolisitudes);
			mp.put("page", pageRender);

			return "UsuariosListaSolicitudesAprobadasAuto";
		}
		
		
		
	// -------------------------- solicitudes cambiod de contraseña ----------------------------------------------------------
		
		 @RequestMapping(value = "/listaSolicitudesCambioCon" , method = RequestMethod.POST)
		public String VerSolicitudesCambiar (@RequestParam(name = "page" , defaultValue = "0")  int page  , ModelMap mp , RedirectAttributes redirectAttributes , Principal principal) {
			
			String NombreUsuarioLogeado = principal.getName();
			
			Usuarios usuariologueado = usuarioservice.buscarUsuario(NombreUsuarioLogeado);
			
			String Rol = usuariologueado.getRol().get(0).getName();
			
			mp.put("Area", usuariologueado.getArea().getArea());
			
			mp.put("rol", Rol);
			
			List<SolicitudesCrearUsuario> cantidadSolicitudes = SolicitudService.cantidadSolicitudes("Enviada");
			
			List<Usuarios> ListaUsuarioCambioCon = usuarioservice.listaUsuariosCambio("Olvidada");
			
			int CatidadSolicitudes = cantidadSolicitudes.size();
			
			mp.put("CatidadSolicitudes", CatidadSolicitudes);
			
			int cambiodecontra = ListaUsuarioCambioCon.size();
			
			mp.put("CatidadSolicitudesCambio", cambiodecontra);
			
			String notificacion = "";
			
			
			notificacion = "No tienes solicitudes de cambio de contraseña";
			
			Pageable pageRequest = PageRequest.of(page, 5);
			
			Page <Usuarios> listapeticionesContra = usuarioservice.listaPeticionesContra("Olvidada", pageRequest);

			Paginador <Usuarios> pageRender = new Paginador<>("/listaSolicitudesCambioCon" , listapeticionesContra);
			
			long Totalpeticiones = listapeticionesContra.getTotalElements();
			
			mp.put("CatidadSolicitudes", CatidadSolicitudes);
			
			
			mp.put("ListaUsuarioCambio", listapeticionesContra);
			mp.put("Totalpeticiones", Totalpeticiones);
			mp.put("page", pageRender);
			
			return "UsuariosListaCambiarContra";
		}
		
		@RequestMapping(value = "/CambiarEstadoContra/{id}", method = RequestMethod.GET)
		public String CambiarEstadoContra(@PathVariable("id") long id, ModelMap mp , RedirectAttributes redirectAttributes) {

			String notificacion = "";
			
			Usuarios usuario = usuarioservice.buscarUsuarioId(id);
			
			String NombreUsuario = usuario.getUsername();
			
			String correoUsuario = usuario.getCorreo();
			
			usuario.setEstadoContra("Sin Asignar");
			
			usuarioservice.GuardarUsuario(usuario);
			
			notificacion = "El estado de contraseña del usuario ha sido cambiado a sin asignar";
			
			redirectAttributes.addFlashAttribute("notificacion" , notificacion);
			
			emailSender.sendEmail("Tu estado de contraseña se ha restablecido , ingresar al apartado ( por primera vez )", "Test Message : Restablecer contraseña  " + NombreUsuario, "administracionlotes@pichincha.com.co" , correoUsuario /* PONER EL CORREO DEL usuario */ , true );
			
			return "redirect:/listaUsuarios";
		}

}
