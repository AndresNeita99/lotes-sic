package com.example.demo.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.Principal;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import org.apache.commons.io.FileUtils;
import org.apache.poi.hssf.record.crypto.Biff8EncryptionKey;
import org.apache.poi.openxml4j.util.ZipSecureFile;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;


import com.example.demo.RegistrosLog;
import com.example.demo.Dao.BalanceDao;
import com.example.demo.Entity.Balance;
import com.example.demo.Entity.FechaBalance;
import com.example.demo.Entity.Lote;
import com.example.demo.Entity.SolicitudesCrearUsuario;
import com.example.demo.Entity.Usuarios;
import com.example.demo.Paginador.Paginador;
import com.example.demo.service.BalanceService;
import com.example.demo.service.CargarArchivoServer;
import com.example.demo.service.FechaBalanceService;
import com.example.demo.service.MensajeService;
import com.example.demo.service.SolicitudesCrearUsuarioService;
import com.example.demo.service.UsuariosService;

import jakarta.servlet.http.HttpServletResponse;

@Controller
public class ControllerBalance {
	
	@Autowired
	private UsuariosService usuarioService;

	@Autowired
	private BalanceService balanceService;
	
	@Autowired
	private SolicitudesCrearUsuarioService SolicitudService;
	
	@Autowired
	private CargarArchivoServer cargarArchivo;
	
	@Autowired
	private FechaBalanceService fechaBalance;

	private String notificacion;
	
	private String NombreBalance ;
	
	
	private RegistrosLog registrolog = new RegistrosLog();


	@RequestMapping(value = "/MostrarBalance", method = RequestMethod.GET)
	public String MostrarBalance(@RequestParam(name = "page", defaultValue = "0") int page, ModelMap mp, Model model , Principal principal) {

		String NombreUsuarioLogeado = principal.getName();
		
		Usuarios usuario = usuarioService.buscarUsuario(NombreUsuarioLogeado);
		
		DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

		String date = dateFormat.format(Calendar.getInstance().getTime());
		
		String Rol = usuario.getRol().get(0).getName();
		
		mp.put("Area", usuario.getArea().getArea());
		
		mp.put("rol", Rol);
		
		String notificacion = (String) model.asMap().get("notificacion");
		
		String fechabalance = (String) model.asMap().get("fechabalance");

		
		Pageable pageRequest = PageRequest.of(page, 10);

		List<Balance> balance = balanceService.ListaCuentasBalance();

		Page<Balance> listaCuentasBalance = balanceService.ListaBalancePagin(pageRequest);

		Paginador<Balance> pageRender = new Paginador<>("/MostrarBalance", listaCuentasBalance);
		
		List<SolicitudesCrearUsuario> cantidadSolicitudes = SolicitudService.cantidadSolicitudes("Enviada");
		
		List<Usuarios> ListaUsuarioCambioCon = usuarioService.listaUsuariosCambio("Olvidada");
		
		int CatidadSolicitudes = cantidadSolicitudes.size();
		
		mp.put("CatidadSolicitudes", CatidadSolicitudes);
		
		int cambiodecontra = ListaUsuarioCambioCon.size();
		
		mp.put("CatidadSolicitudesCambio", cambiodecontra);

		//System.out.print("la cantidad de datos es :" + listaCuentasBalance.getTotalElements());


		mp.put("fechabalance", fechaBalance.MostrarFechaBalance());
		mp.put("totalRegistros", balance.size());
		mp.put("notificacion", notificacion);
		mp.put("balance", listaCuentasBalance);
		mp.put("page", pageRender);
		
		String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ Accedio a Lista de registros de balance ]";

		registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);

		return "Balance";
	}

	@GetMapping("/ActualizarBalance")
	public String ActualizarBalance(ModelMap mp , Model model , RedirectAttributes redirectAttributes , Principal principal) {
		
		String NombreUsuarioLogeado = principal.getName();
		
		DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

		String date = dateFormat.format(Calendar.getInstance().getTime());
		
		String NombreArchivo = (String) model.asMap().get("NombreArchivo");

		balanceService.eliminarBalanceAnterior();
		
		redirectAttributes.addFlashAttribute("NombreArchivo",NombreArchivo);
		
		String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ cargo el archivo " + NombreArchivo + " actualizo los datos del balance ]";
		
		registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);

		return "redirect:/CargarBalance";
		

	}

	@GetMapping("/CargarBalance")
	public String CargarBalance(HttpServletResponse response, Model model , RedirectAttributes redirectAttributes , Principal principal) {

		FechaBalance balance1 = new FechaBalance();
		
		String RutaBalance = (String) model.asMap().get("NombreArchivo");
		
		String NombreUsuarioLogeado = principal.getName();
		
		DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

		String date = dateFormat.format(Calendar.getInstance().getTime());

		boolean FormatoBalance = false;
		
		System.out.println(RutaBalance);
		
		Lote lote = new Lote();
		
		String rutaBalance = "..\\Balance\\" + RutaBalance;

		FileInputStream file;

		int contadorRegistros = 0;

		try {

			
			
			file = new FileInputStream(new File(rutaBalance));

			Biff8EncryptionKey.setCurrentUserPassword("IPCONT01"); // valdiar si siempre es la misma clave

			Workbook workbook = new XSSFWorkbook(file);

			org.apache.poi.ss.usermodel.Sheet sheet = workbook.getSheetAt(0);
			ZipSecureFile.setMinInflateRatio(0);

			for (Row row : sheet) {

				int contador = 0; // contador de columna

				
				//System.out.println("Numero de fila: " + row.getRowNum());
				//System.out.println("");

				if (row.getRowNum() == 0 || row.getRowNum() == 1) {

					
					FormatoBalance = true ;
					//System.out.println("saltar filas ya que es la cabezeras del archivo ");
					//System.out.println("");

				} else {

					Balance balance = new Balance();

					for (Cell cell : row) {

						if (cell.getCellType().BLANK != null) {

							contadorRegistros = contadorRegistros + 1;
						}

						contador = contador + 1;
						
						//System.out.println(" el numero de columnas es de " + contador);

						switch (cell.getCellType()) {

						case STRING:

							if (contador == 2) {

								String Descripcion = cell.getRichStringCellValue().getString();

								// System.out.println("Nombre de cuenta " + Descripcion);

								balance.setDescripcion(Descripcion);
							}
							
							if (contador == 4 && row.getRowNum() == 2) {
								
								NombreBalance = cell.getRichStringCellValue().getString();
								
								//System.out.println("La fecha del balance es : " + NombreBalance);
								
								FormatoBalance = true;
								
							}

							break;

						case NUMERIC:

							if (contador == 1) { // COLUMNA CODIGO FUENTE NUMERICA

								long cuentaBalance = (long) cell.getNumericCellValue();

								String Cuenta = lote.completarCeros(cuentaBalance);

								// System.out.println("Numero cuenta lote: " + Cuenta);

								balance.setCuenta(Cuenta);

							}
							
							

							if (contador == 3) { // COLUMNA CODIGO

								double SaldoAnterior = cell.getNumericCellValue();

								DecimalFormat formatea = new DecimalFormat("###,###.##");

								String ValorSaldoAnterior = formatea.format(SaldoAnterior);

								// System.out.println("Saldo anterior : " + ValorSaldoAnterior);

								balance.setSaldoAterior(ValorSaldoAnterior);

							}
							
							

							if (contador == 4) { // COLUMNA DEBITOS
								
								double debitos = cell.getNumericCellValue();

								DecimalFormat formatea = new DecimalFormat("###,###.##");

								String ValorDebito = formatea.format(debitos);

								balance.setDebitos(ValorDebito);

								// System.out.println("Debitos : " + ValorDebito);

							}

							if (contador == 5) { // COLUMNA CREDITOS

								double Creditos = cell.getNumericCellValue();

								DecimalFormat formatea = new DecimalFormat("###,###.##");

								String ValorCredito = formatea.format(Creditos);

								balance.setCreditos(ValorCredito);

								// System.out.println("Creditos : " + ValorCredito);

							}

							if (contador == 6) { // COLUMNA CODIGO FUENTE NUMERICA

								double Creditos = cell.getNumericCellValue();

								DecimalFormat formatea = new DecimalFormat("###,###.##");

								String SaldoFinal = formatea.format(Creditos);

								balance.setSaldoFinal(SaldoFinal);

								//System.out.println("Saldo final : " + SaldoFinal);

							}

						case BLANK:

							// System.out.println("Celda en blanco : ");

							break;
							
						

						}

					}
					
					if (contador != 6 && FormatoBalance == false) {
						
						notificacion = "El archivo " + RutaBalance + " no cumple con la estructura";
						
						redirectAttributes.addFlashAttribute("notificacion",notificacion);
						
						return "redirect:/MostrarBalance";
						
					}
					
					
					else {
						
						if (balance.getCuenta() != null) {

							balanceService.GuardarCuentaBalance(balance);
						}

						else {

							//System.err.println("hay datos en blanco");
						}

						workbook.close();
						
					}


				
				}

			}

			//System.out.println("Guardado exitosamente");

		} catch (FileNotFoundException e) {
			
			String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ No se a cargado ningun balance para por favor seleccionar un archivo]";
			
			registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);
			
			//System.out.println(e);

			//System.out.print("No se a cargado ningun balance para por favor seleccionar un archivo");

		} catch (IOException e) {
			
			String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ No se a cargado ningun balance para por favor seleccionar un archivo]";
			
			registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);

			//System.out.println(e);
			
			//System.out.print("No se a cargado ningun balance para por favor seleccionar un archivo");
		}
		
		catch (Exception e) {
			
			//System.out.println(e);
			
			notificacion = "El archivo " + RutaBalance + "contiene contraseña o un formato diferente, por favor guardar el archivo con formato actual y quitar la contraseña del archivo";
			
			String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ " + notificacion + "]";
			
			registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);
			
			redirectAttributes.addFlashAttribute("notificacion" , notificacion);
		}
		
		if(fechaBalance.MostrarFechaBalance().isEmpty()) {
			
			balance1.setFechaBalance(NombreBalance);
			
			fechaBalance.guardarFecha(balance1);
			
		}
		
		else {
			
			FechaBalance FechaActualizar = fechaBalance.MostrarFechaBalance().get(0);
			
			FechaActualizar.setFechaBalance(date);
			
			fechaBalance.guardarFecha(FechaActualizar);
			
		}
		
		String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ cargo el archivo " + rutaBalance + " actualizo los datos del balance ]";
		
		registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);
		
		redirectAttributes.addFlashAttribute("notificacion", "El archivo " + RutaBalance + " fue cargado correctamente");

		
		return "redirect:/MostrarBalance";
	}

	
	@PostMapping("/validarArchivoBalance")
	public String validarBalance (@RequestParam(value = "dato", required = false) String dato , @RequestParam("file") MultipartFile file , RedirectAttributes redirectAttributes , HttpServletResponse response , Model model , Principal principal) throws IOException {
		
		String NombreUsuarioLogeado = principal.getName();
		
		DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

		String date = dateFormat.format(Calendar.getInstance().getTime());
		
		String NombreArchivo = file.getOriginalFilename();
		
		//System.out.println("el archivo de area es : " + NombreArchivo);
		
		//String NombreArchivo =  dato.replaceAll("[xlsx]", "");

		String ruta = "..\\Balance";


		// esta carpeta se creara cada vez que se descargue y se llene un plano
		File directorio = new File(ruta);
		

		// ---------------- validamos que en la carpeta areas no exista el archivo si existe se elimina para la llegada del nuevo archivo areas // -----------//
		


		if (directorio.exists()) {

			File directory = new File(ruta);
				
			FileUtils.cleanDirectory(directory);
				
			cargarArchivo.save(file,ruta);
				
			redirectAttributes.addFlashAttribute("NombreArchivo",NombreArchivo);
			
			String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ cargo el archivo " + NombreArchivo + "]";

			registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);

			return "redirect:/ActualizarBalance";

		}
		
		else {
			
			 cargarArchivo.init(ruta);
			 
			 cargarArchivo.save(file,ruta);
			 
			 redirectAttributes.addFlashAttribute("NombreArchivo",NombreArchivo);
			 
			 String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ cargo el archivo " + NombreArchivo + "]";

				registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);
			 
			 return "redirect:/CargarBalance";
			
		}
		
		
		
			
	}

	@PostMapping("/BuscarCuentaBalance")
	public String BuscarCuenta(@RequestParam(value = "dato", required = false) String dato, ModelMap mp , Principal principal) {

		String NombreUsuarioLogeado = principal.getName();
		
		DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

		String date = dateFormat.format(Calendar.getInstance().getTime());
		
		Usuarios usuario = usuarioService.buscarUsuario(NombreUsuarioLogeado);
		
		String Rol = usuario.getRol().get(0).getName();
		
		mp.put("Area", usuario.getArea().getArea());
		
		mp.put("rol", Rol);
		
		//System.out.println("Numero de cuenta a buscar : " + dato);

		List<Balance> balance = balanceService.BuscarCuentaBalance(dato);

		//System.out.println("cuenta " + balance.size());

		mp.put("CuentaBalance", balance);
		
		String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ Busco la cuenta  " + dato + " en el balance del sistema]";

		registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);

		return "BalanceCuentaBuscar";

	}
	
	@GetMapping("/notificacionBalance")
	public String notificacionBalance (Model model , RedirectAttributes redirectAttributes) {
		
		notificacion = "";
		
		redirectAttributes.addFlashAttribute("notificacion", notificacion);
		
		return "redirect:/MostrarBalance";
	}
	

}
