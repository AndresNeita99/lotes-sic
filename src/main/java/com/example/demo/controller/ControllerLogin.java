package com.example.demo.controller;

import org.springframework.ui.Model;

import java.io.File;
import java.io.FileWriter;
import java.security.Principal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.example.demo.RegistrosLog;
import com.example.demo.Entity.Email;
import com.example.demo.Entity.Log;
import com.example.demo.Entity.Mensaje;
import com.example.demo.Entity.SolicitudesCrearUsuario;
import com.example.demo.Entity.Usuarios;
import com.example.demo.service.LogService;
import com.example.demo.service.MensajeService;
import com.example.demo.service.SolicitudesCrearUsuarioService;
import com.example.demo.service.UsuariosService;
import com.example.demo.service.emailEnviar;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@Controller
public class ControllerLogin {
	
	@Autowired
	private MensajeService mensajeService;


	@Autowired
	private emailEnviar emailSender;
	
	@Autowired
	private SolicitudesCrearUsuarioService SolicitudService;

	@Autowired
	private UsuariosService usuarioservice;
	
	@Autowired
	private LogService logservice;
	
	int contadorValidar = 4;
	
	private RegistrosLog registrolog = new RegistrosLog();
	

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String login(@RequestParam(value = "error", defaultValue = "false") boolean loginError ,Model model, ModelMap mp, RedirectAttributes redirectAttributes , HttpServletResponse response , HttpServletRequest request) {
	
		Log log = new Log();
		
		//Authentication auth = SecurityContextHolder.getContext().getAuthentication();

		//System.out.println(auth.getName());

		String ruta = "..\\Logs";

		// esta carpeta se creara cada vez que se descargue y se llene un plano
		File directorio = new File(ruta);

		if (!directorio.exists()) {
			if (directorio.mkdirs()) {
				
				//System.out.println("Directorio creado" + ruta);

				login(loginError , model, mp , redirectAttributes , response , request);

			}

		} else {

			//System.out.println("Error del directorio ya existe");

		}
		
		  try {

			  LocalDate Fechacargue = LocalDate.now();
			  
			  String idlog = Fechacargue.toString();
			  
			  idlog = idlog.replace("-", "");
		  		
			  String rutalog = "../Logs/log" + Fechacargue +".txt";
			  
			  String NombreArchivo = "log" + Fechacargue + ".txt";
			  
			  File file = new File(rutalog); // Si el archivo no existe es creado
			  
			  Log logActual = logservice.BuscarLog(idlog);
			  
			  File Archivolog = new File(rutalog);

			  
		  if (!file.exists() && logActual == null) {
			  
			  log.setId(idlog);
			  
			  log.setNombre(NombreArchivo);
			  
			  log.setRuta(rutalog);
			  
			  log.setDescripcion("Log generado el dia " + NombreArchivo);

			  logservice.guardarLog(log);
			  
			  file.createNewFile();
			  
		  
		  }
	  
	  } catch (Exception e) { 
		  
		  e.printStackTrace(); 
	  
	  }
		
		
		if (loginError == true ) {
			
			contadorValidar = contadorValidar - 1;
			
			String Notificacion = "Tiene " + contadorValidar + " intentos" ;
			
			mp.put("notificacion", Notificacion);
		
		}
			
		if(contadorValidar < 1) {
			
			contadorValidar = 0 ;

			return "redirect:/preguntasRestableContra";
		}
		
		
	
		String notificacion = (String) model.asMap().get("notificacion");
		
		//System.out.println("el mensaje es " + notificacion);
		
		Usuarios usuarios = new Usuarios();

		mp.put("notificacion", notificacion);
		mp.put("usuario", usuarios);

		return "login";
	}

	@RequestMapping(value = "/Home", method = RequestMethod.GET)
	public String PantallaInicio(HttpServletRequest request, Model model, ModelMap mp, RedirectAttributes redirectAttributes , Principal principal) {
		
	

		String NombreUsuarioLogeado = principal.getName();

		Usuarios usuario = usuarioservice.buscarUsuario(NombreUsuarioLogeado);
		
		String NombreUsuarioLogueado = usuario.getNombre();
		
		List<SolicitudesCrearUsuario> cantidadSolicitudes = SolicitudService.cantidadSolicitudes("Enviada");
		
		List<Usuarios> ListaUsuarioCambioCon = usuarioservice.listaUsuariosCambio("Olvidada");
		
		int numeroSolicitudes = cantidadSolicitudes.size();
		
		mp.addAttribute("numeroSolicitudes", numeroSolicitudes);
		
		int cambiodecontra = ListaUsuarioCambioCon.size();
		
		mp.put("CatidadSolicitudesCambio", cambiodecontra);
		
		if (usuario.getEstadoUsuario().contains("Inactivo"))  {
			
			redirectAttributes.addFlashAttribute("notificacion" , "El estado de tu perfil es inactivo comunícate con el administrador para habilitar tu perfil nuevamente");
			
			return "redirect:/login";
			
		}
		
		if (usuario.getEstadoUsuario().contains("Bloqueado"))  {
			
			redirectAttributes.addFlashAttribute("notificacion" , "El estado de tu perfil es Bloqueado comunícate con el administrador para habilitar tu perfil nuevamente");
			
			return "redirect:/login";
			
		}
		
		if (usuario.getEstadoContra().contains("Sin asignar"))  {
			
			redirectAttributes.addFlashAttribute("notificacion" , "El estado de tu contraseña es Sin asignar registra tu contraseña en primer ingreso");
			
			return "redirect:/login";
			
		}
		
		if (usuario.getEstadoContra().contains("Olvidada"))  {
			
			redirectAttributes.addFlashAttribute("notificacion" , "Tu usuario ha sido bloqueado por olvido de contraseña");
			
			return "redirect:/login";
			
		}
		

		if (usuario.getEstadoContra().contains("Expirada"))  {
			
			redirectAttributes.addFlashAttribute("notificacion" , "El estado de tu contraseña ha expirado registra renueva tu contraseña en olvide mi contraseña");
			
			return "redirect:/login";
			
		}
		
		
		


		Calendar calendar = Calendar.getInstance();
		
		Date fechaIngreso = calendar.getTime() ;
		
		String fechaActual = LocalDate.now().toString();
		
		//System.out.println("La fecha es : " + fechaIngreso);
		
		//System.out.println("la fecha limite del usuario es : " + usuario.getFechalimite());
		
		Date FechaLimiContra = usuario.getFechalimite();
		
		String FechaLimiCon = usuario.getFechalimite().toString();
		
		//System.out.println("Fecha limite  " + FechaLimiCon);
		
		//System.out.println("Fecha Hoy  " + fechaActual);

		
		if(FechaLimiCon.matches(fechaActual)) {
			
			String notificacion = "Su contraseña expira hoy, por favor cambiarla en el apartado recuperar contraseña";
			
			usuario.setEstadoContra("Expirada");
			
			usuarioservice.GuardarUsuario(usuario);
			
			mp.addAttribute("notificacion", notificacion);
			
			return "redirect:/login";
			
		}
		
		
		if(fechaIngreso.after(FechaLimiContra)) {
			
			usuario.setEstadoContra("Expirada");
			
			usuarioservice.GuardarUsuario(usuario);
			
			redirectAttributes.addFlashAttribute("notificacion" , "Su contraseña ha expirado por favor establecer otra contraseña en el apartado recuperar contraseña");
			
			return "redirect:/login";	
		}
		
		if(fechaIngreso.before(FechaLimiContra)) {
			
			//System.out.println("La fecha es menor aun hay ingreso ");	
		}
		
		
		String Rol = usuario.getRol().get(0).getName();
		
		mp.put("Area", usuario.getArea().getArea());
		
		mp.put("rol", Rol);
		
		String nombre = usuarioservice.buscarUsuario(principal.getName()).getNombre();
		
		List<Mensaje> MensajesNoLeidos = mensajeService.listaMensajesNoLeidos(nombre, "No leido");
		
		mp.put("cantidadMensajes", MensajesNoLeidos.size());
		
		//System.out.print("el nombre de la persona es :" + principal.getName());
		
		DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

		String date = dateFormat.format(Calendar.getInstance().getTime());

		String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ El usuario se logueo correctamente ]";

		registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);
		
		return "InicioAdmin";
	}
	

	

	@GetMapping("/preguntasSeguridad")
	public String preguntasSeguridad(ModelMap mp , Model model , RedirectAttributes redirectAttributes) {

		String notificacion = (String) model.asMap().get("notificacion");
		
		mp.put("notificacion", notificacion);
		mp.put("usuarios", new Usuarios());

		return "loginPreguntasSeguridad";

	}

	@PostMapping("/guardarpreguntas")
	public String GuardarPreguntasSeguridad(ModelMap mp, Usuarios usuarios , RedirectAttributes redirectAttributes) {

		String notificacion = "";
		
		String NombreUsuario = usuarios.getCorreo();

		String fechanacimiento = usuarios.getFechanacimiento();
		
		String fechaExpedicion = usuarios.getFechaexpedicion();
		
		int numeroCelular = usuarios.getNumcelular();
		
		String numCadena= String.valueOf(numeroCelular);

		Usuarios usuario = usuarioservice.buscarPorEmail(NombreUsuario);
		
		
		if (usuario == null ) {
			
			notificacion = "El usuario no existe vuelva a ingresar el correo del usuario ";
			
			redirectAttributes.addFlashAttribute("notificacion",notificacion);
			
			DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

			String date = dateFormat.format(Calendar.getInstance().getTime());

			String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuario + "] " + "Tarea" + " [ El usuario no existe vuelva a ingresar el correo del usuario ]";

			registrolog.guardarRegistro(NombreUsuario, tarea);
			
			return "redirect:/preguntasSeguridad";
		}
		
		if (usuario != null) {
			
			String EstadoContra = usuario.getEstadoContra();
			
			if (EstadoContra.contains("vigente")) {
				
				notificacion = "El usuario " + NombreUsuario + " ya ingreso por primera vez ";
				
				redirectAttributes.addFlashAttribute("notificacion",notificacion);
				
				DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

				String date = dateFormat.format(Calendar.getInstance().getTime());

				String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuario + "] " + "Tarea" + " [ El usuario no existe vuelva a ingresar el correo del usuario ]";

				registrolog.guardarRegistro(NombreUsuario, tarea);
				
				return "redirect:/login";
				
			}
			

			if (EstadoContra.contains("Olvidada")) {
				
				notificacion = "El estado de la contraseña para el " + NombreUsuario + " es olvidada, espere a que el administrador se ponga en contacto con usted  ";
				
				redirectAttributes.addFlashAttribute("notificacion",notificacion);
				
				DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

				String date = dateFormat.format(Calendar.getInstance().getTime());

				String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuario + "] " + "Tarea" + notificacion;

				registrolog.guardarRegistro(NombreUsuario, tarea);
				
				return "redirect:/login";
				
			}
			
		}
		
		if (numCadena.length() < 3) {
			
			notificacion = "El numero que ingresado no contiene 3 dígitos";
			
			redirectAttributes.addFlashAttribute("notificacion",notificacion);
			
			return "redirect:/preguntasSeguridad";
			
		}
		
		

		
		
		usuario.setFechaexpedicion(fechaExpedicion);
		usuario.setFechanacimiento(fechanacimiento);
		usuario.setNumcelular(numeroCelular);

		usuarioservice.GuardarUsuario(usuario);

		notificacion = "Preguntas guardadas exitosamente";
		
		redirectAttributes.addFlashAttribute("notificacion",notificacion);
		
		//System.out.print("Preguntas guardadas exitosamente");
		
		DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

		String date = dateFormat.format(Calendar.getInstance().getTime());

		String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuario + "] " + "Tarea" + " [ Preguntas guardadas exitosamente ]";

		registrolog.guardarRegistro(NombreUsuario, tarea);
		

		return "redirect:/crearcontra";
	}

	@RequestMapping(value = "/crearcontra", method = RequestMethod.GET)
	public String crearContraseña(ModelMap mp ,Model model , RedirectAttributes redirectAttributes) {
		
		String notificacion = (String) model.asMap().get("notificacion");
		
		//System.out.println("el mensaje es " + notificacion);

		mp.put("notificacion", notificacion);
		
		mp.put("usuarios", new Usuarios());

		return "loginCrearContraseña";

	}

	@RequestMapping("/guardarcontra")
	public String GuardarContraseña(ModelMap mp, Usuarios usuarios , RedirectAttributes redirectAttributes) {
		
		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		
		String notificacion = "";

		String NombreUsuario = usuarios.getCorreo();

		String password = usuarios.getPassword();

		String[] contraseñaSeparada = password.split(",");

		String Contraseña = contraseñaSeparada[0];

		String Contraseña2 = contraseñaSeparada[1];
		
		int LongitudContraseña1 = Contraseña.length();
		
		int LongitudContraseña2 = Contraseña2.length();
		
		//System.out.println("Contraseña 1 : " + Contraseña);
		//System.out.println("Contraseña 2 : " + Contraseña2);
		
		//System.out.print("la longitud de la contraseña 1 es de : " + LongitudContraseña1 + " la longitud de la contraseña 2 es : " + LongitudContraseña2);
		
		Usuarios usuario = usuarioservice.buscarPorEmail(NombreUsuario);
		
		
		if (usuario == null ) {
			
			notificacion = "El usuario no se encuentra registrado en el sistema";
			
			redirectAttributes.addFlashAttribute("notificacion", notificacion);
			
			DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

			String date = dateFormat.format(Calendar.getInstance().getTime());

			String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuario + "] " + "Tarea" + " [ El usuario no esta registrado ingrese un usuario registrado ]";

			registrolog.guardarRegistro(NombreUsuario, tarea);
			
			
			
			return "redirect:/crearcontra";
		}

		if (Contraseña.equals(Contraseña2) == false) { // validar longitud y que tenga 1 mayuscula y 1 caracter especial 

			notificacion = "las contraseñas no son iguales";

			redirectAttributes.addFlashAttribute("notificacion", notificacion);
			
			DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

			String date = dateFormat.format(Calendar.getInstance().getTime());

			String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuario + "] " + "Tarea" + " [ la contraseña No son iguales ]";

			registrolog.guardarRegistro(NombreUsuario, tarea);
			
			
			return "redirect:/crearcontra";

		} 
		
		if((LongitudContraseña1 < 8 || LongitudContraseña1 > 15) || ( LongitudContraseña2 < 8 || LongitudContraseña2 > 15 )) {
			
			
			notificacion = "No cumple con la longitud de la contraseña ";
			
			redirectAttributes.addFlashAttribute("notificacion", notificacion);
			
			DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

			String date = dateFormat.format(Calendar.getInstance().getTime());

			String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuario + "] " + "Tarea" + " [ La contraseña no cumple con las estructura ]";

			registrolog.guardarRegistro(NombreUsuario, tarea);
			
			
			
			return "redirect:/crearcontra";
			
		}
		
		if(Contraseña.matches("^[A-Z0-9]*$")) {
			
			notificacion = "La contraseña no cumple con la estructura";
			
			redirectAttributes.addFlashAttribute("notificacion", notificacion);
			
			DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

			String date = dateFormat.format(Calendar.getInstance().getTime());

			String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuario + "] " + "Tarea" + " [ La contraseña no cumple con las estructura ]";

			registrolog.guardarRegistro(NombreUsuario, tarea);
			
			
			
			return "redirect:/crearcontra";
		}
		
		if(Contraseña.matches("^[a-z0-9]*$")) {
			
			notificacion = "La contraseña no cumple con la estructura";
			
			redirectAttributes.addFlashAttribute("notificacion", notificacion);
			
			DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

			String date = dateFormat.format(Calendar.getInstance().getTime());

			String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuario + "] " + "Tarea" + " [ La contraseña no cumple con las estructura ]";

			registrolog.guardarRegistro(NombreUsuario, tarea);
			
			
			
			return "redirect:/crearcontra";
		}
		
		
			Calendar calendar1 = Calendar.getInstance();
			
			Calendar calendar2 = Calendar.getInstance();
			
			calendar1.add(Calendar.MONTH, 3);
			
			Date fechacreacion = calendar2.getTime();
			
			Date fechaLimite = calendar1.getTime();
			
			//System.out.println("La fecha es : " + fechaLimite);

			String contraseñaEncirptada = passwordEncoder.encode(Contraseña);
			
			usuario.setEstadoContra("vigente");

			usuario.setPassword(contraseñaEncirptada);
			
			usuario.setFechacontra(fechacreacion);
			
			usuario.setFechalimite(fechaLimite);

			usuarioservice.GuardarUsuario(usuario);
			
			redirectAttributes.addFlashAttribute("notificacion", "Contraseña guardada correctamente");
			
			DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

			String date = dateFormat.format(Calendar.getInstance().getTime());

			String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuario + "] " + "Tarea" + " [ contraseña guardada correctamente]";

			registrolog.guardarRegistro(NombreUsuario, tarea);
			
			

			return "redirect:/login";
		

	}

	@GetMapping("/recuperarContra")
	public String ValidarRegistro(ModelMap mp) {

		Usuarios usuario = new Usuarios();

		mp.put("usuarios", usuario);

		return "loginrecuperarcontraseña";
	}
	
	@GetMapping("/preguntasRestableContra")
	public String PreguntasRestablecerContra (ModelMap mp , Model model , RedirectAttributes redirectAttributes) {
		
		String notificacion = (String) model.asMap().get("notificacion");
		
		//System.out.println("el mensaje es " + notificacion);
		
		mp.put("notificacion", notificacion);
		
		Usuarios usuario = new Usuarios();

		mp.put("usuarios", usuario);
		
	    contadorValidar = 4 ;
		
		//System.out.println("el valor del contador " + contadorValidar);
		
		return "loginPreguntasSeguridadContra";
	}
	
	@GetMapping("/preguntasOlvidadas")
	public String PreguntasOlvidadas(ModelMap mp) {

		Usuarios usuario = new Usuarios();

		mp.put("usuarios", usuario);

		return "loginPreguntasOlvidadas";
	}
	

	@PostMapping("validarPreguntas")
	public String validarPreguntas(ModelMap mp, Usuarios usuarios ,  RedirectAttributes redirectAttributes) {

		String notificacion = "";
		
		String NombreUsuario = usuarios.getCorreo();

		String FechaExpedicion = usuarios.getFechaexpedicion();

		String fechaCumpleaños = usuarios.getFechanacimiento();

		int NumeroTel = usuarios.getNumcelular();

		Usuarios usuario = usuarioservice.buscarPorEmail(NombreUsuario);
		
		
		if (usuario == null) {
			
			notificacion = "El usuario no se encuentra registrado en el sistema";
			
			redirectAttributes.addFlashAttribute("notificacion", notificacion);
			
			//System.out.print("El usuario no existe vuelva a intentarlo");
			
			DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

			String date = dateFormat.format(Calendar.getInstance().getTime());

			String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuario + "] " + "Tarea" + " [ el usuario que ingreso no es valido  ]";

			registrolog.guardarRegistro(NombreUsuario, tarea);
			
			return "redirect:/preguntasRestableContra";
		}

		if (usuario.getFechaexpedicion().contains(FechaExpedicion) &&  usuario.getFechanacimiento().contains(fechaCumpleaños) && usuario.getNumcelular() == NumeroTel) {

			notificacion = "preguntas validadas correctamente ";
			
			redirectAttributes.addFlashAttribute("notificacion", notificacion);
			
			String UsuarioIngresado = usuario.getUsername();
			
			//System.out.print("los datos son correctos ");
			
			DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

			String date = dateFormat.format(Calendar.getInstance().getTime());

			String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuario + "] " + "Tarea" + " [ las preguntas ingresadas fueron validadas correctamente ]";

			registrolog.guardarRegistro(NombreUsuario, tarea);

			return "redirect:/crearcontra";

		}
		
		else {
			
			notificacion = "los datos no son correctos, por favor vuelva a intentar";
			
			redirectAttributes.addFlashAttribute("notificacion", notificacion);
			
			//System.out.print("los datos no son correctos");
			
			DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

			String date = dateFormat.format(Calendar.getInstance().getTime());
			
			String UsuarioIngresado = usuario.getUsername();

			String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuario + "] " + "Tarea" + " [ los datos no son correctos , porfavor vuelva a intentear ]";

			registrolog.guardarRegistro(NombreUsuario, tarea);
			
			return "redirect:/preguntasRestableContra";
		}
		
	}



	@RequestMapping("/EnviarCorreoContra")
	public String EnviarCorreoContraseña(ModelMap mp, Usuarios usuarios, Email email , RedirectAttributes redirectAttributes) {

		String notificacion = "";
		
		String NombreUsuario = usuarios.getCorreo();
		
		//String NombreUsuario = "andres@pichincha.com.co";
		Usuarios Administrador = usuarioservice.listaUsuariosPorIdRol(1);
		
		String CorreoAdministrador = Administrador.getCorreo();
		
		Usuarios usuario = usuarioservice.buscarPorEmail(NombreUsuario);
		
		String UsuarioIngresado = usuario.getUsername();
		
		usuario.setPassword("");
		
		usuario.setEstadoContra("Olvidada");
		
		usuarioservice.GuardarUsuario(usuario);
 
		emailSender.sendEmail("Cambio de contraseña", "Test Message : Se solicita cambio de contrasena al usuario : " + UsuarioIngresado, "administracionlotes@pichincha.com.co" , "andres.neita@pichincha.com.co" /* PONER EL CORREO DEL ADMIN */ , true );
		
		notificacion = "El correo se ha enviado correctamente al administrador";
		
		redirectAttributes.addFlashAttribute("notificacion", notificacion);
		
		DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

		String date = dateFormat.format(Calendar.getInstance().getTime());

		String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + UsuarioIngresado + "] " + "Tarea" + " [ Envio correo al administrador olvido de contraseña ]";

		registrolog.guardarRegistro(NombreUsuario, tarea);
		
		Mensaje mensajeNuevo =  new Mensaje ();
		
		mensajeNuevo.setComentario("el usuario " + NombreUsuario + "olvido la contraseña ");
		mensajeNuevo.setDestinatario(NombreUsuario);
		mensajeNuevo.setFecha(date);
		
		mensajeService.enviarMensaje(mensajeNuevo);

		return "redirect:/login";
	}
	
	
	@GetMapping("/error403")
	public String eror403() {
		
		return "Error403";
	}

	
}
