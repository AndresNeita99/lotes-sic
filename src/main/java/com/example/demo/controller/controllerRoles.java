package com.example.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import ch.qos.logback.core.model.Model;

@Controller
public class controllerRoles {

	@GetMapping("/listaRoles")
	public String ListarAreas (Model model) {
		
		return "Areas";
	}
	
	@GetMapping("/editarRol")
	public String editarArea (Model model) {
		
		return "redirect:/listaAreas";
		
	}
	
	@GetMapping("/eliminarRol")
	public String eliminarArea (Model model) {
		
		return "redirect:/listaAreas";
		
	}
	
	@GetMapping("/crearRol")
	public String crearArea (Model model) {
		
		return "redirect:/listaAreas";
		
	}
	
}
