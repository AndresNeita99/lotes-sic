package com.example.demo.controller;

import java.security.Principal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.example.demo.RegistrosLog;
import com.example.demo.Entity.LoteUsuario;
import com.example.demo.Entity.SolicitudesCrearUsuario;
import com.example.demo.Entity.Usuarios;
import com.example.demo.Paginador.Paginador;
import com.example.demo.service.LoteUsuerioService;
import com.example.demo.service.SolicitudesCrearUsuarioService;
import com.example.demo.service.UsuariosService;

@Controller
public class ControllerEstadoLote {
	
	@Autowired
	private UsuariosService usuarioService;

	@Autowired
	private LoteUsuerioService loteUsuarioService;
	
	@Autowired
	private SolicitudesCrearUsuarioService SolicitudService;
	
	@Autowired
	private UsuariosService usuarioservice;
	
	private String NombreAnalista = "";
	
	private RegistrosLog registrolog = new RegistrosLog();
	
	private String fechaProceso = "";
	
	
	 @RequestMapping(value = "/EstadoPlanosAutorizador" , method = RequestMethod.GET)
	 public String verEstadoPlanosAutorizador (@RequestParam(name = "page" , defaultValue = "0")  int page ,ModelMap mp , Principal principal , Model model) {
		 
		   NombreAnalista = "";
		 
		 	String notificacion = (String) model.asMap().get("notificacion");
		 
		 	String NombreUsuarioLogeado = principal.getName();
			
			Usuarios usuario = usuarioService.buscarUsuario(NombreUsuarioLogeado);
			
			String Rol = usuario.getRol().get(0).getName();
			
			mp.put("Area", usuario.getArea().getArea());
			
			mp.put("rol", Rol);
			
			String NombreUsuario = principal.getName();
			
			Usuarios Usuario = usuarioservice.buscarUsuario(NombreUsuario);
			
			String nombreAutorizador = Usuario.getNombre();
			
			//System.out.println("Nombre de persona logeada " + nombreAutorizador);
			
			Pageable pageRequest = PageRequest.of(page, 5);
			
			Page <LoteUsuario> lotes = loteUsuarioService.listaLotesAutorizadorfecha(nombreAutorizador,pageRequest);
			
			Paginador <LoteUsuario> pageRender = new Paginador<>("/EstadoPlanosAutorizador" , lotes);
			
			//List<LoteUsuario> lotesUsuario = loteUsuarioService.MostrarLotesUsuario(nombreAnalista);
		
			LoteUsuario lote = new LoteUsuario();
			
			mp.put("notificacion", notificacion);
			mp.put("lotes", lote);
			mp.put("lote", lotes);
			mp.put("page", pageRender);
			
			DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

			String date = dateFormat.format(Calendar.getInstance().getTime()); // hora en la que se carga el lote para validar
							
			String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ consulto estado de planos ]";
							
			registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);
		 
		 return "EstadoLotes";
	 }

	
	 @RequestMapping(value = "/EstadoPlanosAnalista" , method = RequestMethod.GET)
	 public String verEstadoPlanosAnalista (@RequestParam(name = "page" , defaultValue = "0")  int page ,ModelMap mp , Principal principal) {
		 
		    NombreAnalista = "";
		 
		 	String NombreUsuarioLogeado = principal.getName();
			
			Usuarios usuario = usuarioService.buscarUsuario(NombreUsuarioLogeado);
			
			String Rol = usuario.getRol().get(0).getName();
			
			mp.put("Area", usuario.getArea().getArea());
			
			mp.put("rol", Rol);
			
			String NombreUsuario = principal.getName();
			
			Usuarios Usuario = usuarioservice.buscarUsuario(NombreUsuario);
			
			String nombreAnalista = Usuario.getNombre();
			
			//System.out.println("Nombre de persona logeada " + nombreAnalista);
			
			Pageable pageRequest = PageRequest.of(page, 5);
			
			Page <LoteUsuario> lotes = loteUsuarioService.listaLotesUsuario(nombreAnalista, pageRequest);
			
			Paginador <LoteUsuario> pageRender = new Paginador<>("/EstadoPlanosAnalista" , lotes);
			
			//List<LoteUsuario> lotesUsuario = loteUsuarioService.MostrarLotesUsuario(nombreAnalista);
		
			LoteUsuario lote = new LoteUsuario();
			
			mp.put("lotes", lote);
			mp.put("lote", lotes);
			mp.put("page", pageRender);
			
			DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

			String date = dateFormat.format(Calendar.getInstance().getTime()); // hora en la que se carga el lote para validar
							
			String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ consulto estado de planos ]";
							
			registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);
		 
		 return "EstadoLotes";
	 }
	 
	 @RequestMapping(value = "/EstadoPlanos" , method = RequestMethod.GET)
	 public String verEstadoPlanos (@RequestParam(name = "page" , defaultValue = "0")  int page ,ModelMap mp , Principal principal) { // ESTE CONTTROLADOR ES PARA LOS OTROS ROLES FUERA DE ANALISTA Y AUTORIZADOR
		 
		 	String NombreUsuarioLogeado = principal.getName();
			
			Usuarios usuario = usuarioService.buscarUsuario(NombreUsuarioLogeado);
			
			String Rol = usuario.getRol().get(0).getName();
			
			mp.put("Area", usuario.getArea().getArea());
			
			mp.put("rol", Rol);
			
			String NombreUsuario = principal.getName();
			
			Usuarios Usuario = usuarioservice.buscarUsuario(NombreUsuario);
			
			String nombreAnalista = Usuario.getNombre();
			
			List<SolicitudesCrearUsuario> cantidadSolicitudes = SolicitudService.cantidadSolicitudes("Enviada");
			
			List<Usuarios> ListaUsuarioCambioCon = usuarioService.listaUsuariosCambio("Olvidada");
			
			int CatidadSolicitudes = cantidadSolicitudes.size();
			
			mp.put("CatidadSolicitudes", CatidadSolicitudes);
			
			int cambiodecontra = ListaUsuarioCambioCon.size();
			
			mp.put("CatidadSolicitudesCambio", cambiodecontra);

			
			//System.out.println("Nombre de persona logeada " + nombreAnalista);
			
			Pageable pageRequest = PageRequest.of(page, 5);
			
			Page <LoteUsuario> lotes = loteUsuarioService.listaLotes(pageRequest);
			
			Paginador <LoteUsuario> pageRender = new Paginador<>("/EstadoPlanos" , lotes);
			
			//List<LoteUsuario> lotesUsuario = loteUsuarioService.MostrarLotesUsuario(nombreAnalista);
		
			LoteUsuario lote = new LoteUsuario();
			
			mp.put("lotes", lote);
			mp.put("lote", lotes);
			mp.put("page", pageRender);
			
			DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

			String date = dateFormat.format(Calendar.getInstance().getTime()); // hora en la que se carga el lote para validar
							
			String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ consulto estado de planos ]";
							
			registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);
		 
		 return "EstadoLotes";
	 }
	 
	
	 
	 @PostMapping("/BuscarLote")
	 public String BuscarLote (@RequestParam(value = "dato", required = false) String dato , RedirectAttributes redirectAttributes , Principal principal) {
		 
		 String NombreUsuarioLogeado = principal.getName();

		 DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

		 String date = dateFormat.format(Calendar.getInstance().getTime()); // hora en la que se carga el lote para validar
		 
		 String datoentrada = dato;
		 
		 //System.out.println("el dato es : " + dato);
		 
		 boolean isNumeric = (datoentrada != null && datoentrada.matches("[0-9]+"));

			//System.out.println("el dato es " + datoentrada);

			if (isNumeric == true) {
				
				String idLote = datoentrada;
				

				redirectAttributes.addFlashAttribute("idLote", idLote);
				
				String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ filtro la busqueda por " + idLote + "]";

				registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);

				return "redirect:/BuscarEstadoIdLote";
			}

			else {
				
				NombreAnalista = datoentrada;

				//System.out.println("Nombre de usuario a buscar es : " + NombreUsuario);
				
				String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ filtro la busqueda por " + NombreAnalista + "]";

				registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);

				redirectAttributes.addFlashAttribute("NombreUsuario", NombreAnalista);

				return "redirect:/BuscarEstadoNombreUsuario";
			}

	 }
	
	 
	 @RequestMapping(value = "/BuscarEstadoNombreUsuario" , method = RequestMethod.GET)
	 public String BuscarEstadoNombreUsuario (@RequestParam(name = "page" , defaultValue = "0")  int page , ModelMap mp , Model model , Principal principal , RedirectAttributes redirectAttributes) {
		 
		
		String NombreUsuario = principal.getName();
		
		Usuarios Usuario = usuarioservice.buscarUsuario(NombreUsuario);
			
		String Rol = Usuario.getRol().get(0).getName();
			
		mp.put("Area", Usuario.getArea().getArea());
			
		mp.put("rol", Rol);
			
		String Nombre = (String) model.asMap().get("NombreUsuario");
		
		Pageable pageRequest = PageRequest.of(page, 5);
		
		Page <LoteUsuario> lotes = loteUsuarioService.listaLotesUsuario(NombreAnalista, pageRequest);
		
		Paginador <LoteUsuario> pageRender = new Paginador<>("/BuscarEstadoNombreUsuario" , lotes);
		
		mp.put("NombreUsuario", NombreAnalista);
		mp.put("lote", lotes);
		mp.put("page", pageRender);
		 
		return "BuscarLoteUsuario";
		
		}


	 
	 @RequestMapping(value = "/BuscarEstadoIdLote" , method = RequestMethod.GET)
	 public String BuscarEstadoLoteId (@RequestParam(name = "page" , defaultValue = "0")  int page , ModelMap mp , Model model , Principal principal , RedirectAttributes redirectAttributes) {
		 
		String NombreUsuarioLogeado = principal.getName();
			
		Usuarios usuario = usuarioService.buscarUsuario(NombreUsuarioLogeado);
			
		String Rol = usuario.getRol().get(0).getName();
			
		mp.put("Area", usuario.getArea().getArea());
			
		mp.put("rol", Rol);
			
		String idlote = (String) model.asMap().get("idLote");
		 
		Pageable pageRequest = PageRequest.of(page, 5);
			
		Page <LoteUsuario> lotes = loteUsuarioService.listaLotesUsuarioIdlote(idlote, pageRequest);
		
		if (lotes.isEmpty() && Rol.contains("Autorizador nivel 1") || lotes.isEmpty() && Rol.contains("Autorizador nivel 2") || lotes.isEmpty() && Rol.contains("Director Contable")) {
			
			redirectAttributes.addFlashAttribute("notificacion", "El lote con el id " + idlote + " No se encontró en el sistema");
			
			return "redirect:/EstadoPlanosAutorizador";
			
		}
		
		if (lotes.isEmpty() && Rol.contains("Analista Contable")) {
			
			redirectAttributes.addFlashAttribute("notificacion", "El lote con el id " + idlote + " No se encontró en el sistema");
			
			return "redirect:/EstadoPlanosAnalista";
			
		}
		
		
		
		if (lotes.isEmpty() && Rol.contains("Centralizador") || lotes.isEmpty() && Rol.contains("Administrador")) {
			
			redirectAttributes.addFlashAttribute("notificacion", "El lote con el id " + idlote + " No se encontró en el sistema");
			
			return "redirect:/EstadoPlanos";
		}
		
		
		if (lotes.isEmpty() && Rol.contains("Auditor")) {
			
			redirectAttributes.addFlashAttribute("notificacion", "El lote con el id " + idlote + " No se encontró en el sistema");
			
			return "redirect:/listaLotesInforme";
		}
		
		if (lotes.isEmpty() && Rol.contains("Consulta")) {
			
			redirectAttributes.addFlashAttribute("notificacion", "El lote con el id " + idlote + " No se encontró en el sistema");
			
			return "redirect:/verTrazabilidadlotes";
		}
		
		
		
		Paginador <LoteUsuario> pageRender = new Paginador<>("/BuscarEstadoIdLote" , lotes);
		
		mp.put("NombreUsuario", idlote);
		mp.put("lote", lotes);
		mp.put("page", pageRender);
		 
		 return "BuscarLoteIdLote";
	 }
	 
	 	@RequestMapping(value = "/GuardarFechaProceso" , method = RequestMethod.GET)
		public String GuardarFechaProceso (@RequestParam(value = "dato", required = false) String dato , RedirectAttributes redirectAttributes) {
		 
		fechaProceso = dato ;
		 
		 redirectAttributes.addFlashAttribute("FechaProceso", fechaProceso);
		 
		 return "redirect:/BuscarEstadoFechaProceso";
	 }
	 
	 @RequestMapping(value = "/BuscarEstadoFechaProceso" , method = RequestMethod.GET)
	 public String BuscarEstadoLoteFechaProceso ( @RequestParam(name = "page" , defaultValue = "0")  int page , ModelMap mp , Model model , Principal principal , RedirectAttributes redirectAttributes) {
		 
		String NombreUsuarioLogeado = principal.getName();
			
		Usuarios usuario = usuarioService.buscarUsuario(NombreUsuarioLogeado);
			
		String Rol = usuario.getRol().get(0).getName();
		
		String NombreCompleto = usuario.getNombre();
			
		mp.put("Area", usuario.getArea().getArea());
			
		mp.put("rol", Rol);
		
		System.out.println("Nombre autorizador " + NombreCompleto);
		 
		Pageable pageRequest = PageRequest.of(page, 5);
		
		if (Rol.contains("Autorizador nivel 1") || Rol.contains("Autorizador nivel 2") || Rol.contains("Director Contable")) {
						
			Page <LoteUsuario> lotes = loteUsuarioService.LotesAutorizadorByfechaProceso(NombreCompleto,fechaProceso, pageRequest);
			
			Paginador <LoteUsuario> pageRender = new Paginador<>("/BuscarEstadoFechaProceso" , lotes);
			
			mp.put("NombreUsuario", fechaProceso);
			mp.put("lote", lotes);
			mp.put("page", pageRender);
			 
			 return "BuscarLoteFechaProceso";
			 
			
		}
		
		if (Rol.contains("Analista Contable")) {
			
			Page <LoteUsuario> lotes = loteUsuarioService.LotesAnalistaByfechaProceso(NombreCompleto,fechaProceso, pageRequest);
			
			Paginador <LoteUsuario> pageRender = new Paginador<>("/BuscarEstadoFechaProceso" , lotes);
			
			mp.put("NombreUsuario", fechaProceso);
			mp.put("lote", lotes);
			mp.put("page", pageRender);
			 
			 return "BuscarLoteFechaProceso";
			 
			
		}

		else {
			
			Page <LoteUsuario> lotes = loteUsuarioService.listaLotesUsuarioFecha(fechaProceso, pageRequest);
			
			Paginador <LoteUsuario> pageRender = new Paginador<>("/BuscarEstadoFechaProceso" , lotes);
			
			mp.put("NombreUsuario", fechaProceso);
			mp.put("lote", lotes);
			mp.put("page", pageRender);
			 
			 return "BuscarLoteFechaProceso";
			
		}
			

	 }
	 

	 
}
