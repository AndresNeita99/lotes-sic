package com.example.demo.controller;

import java.security.Principal;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.example.demo.RegistrosLog;
import com.example.demo.Entity.CentroCostos;
import com.example.demo.Entity.HorasCorte;
import com.example.demo.Entity.Moneda;
import com.example.demo.Entity.SolicitudesCrearUsuario;
import com.example.demo.Entity.TRM;
import com.example.demo.Entity.Usuarios;
import com.example.demo.Paginador.Paginador;
import com.example.demo.service.SolicitudesCrearUsuarioService;
import com.example.demo.service.TrmServices;
import com.example.demo.service.UsuariosService;

import jakarta.servlet.http.HttpServletRequest;


@Controller
public class ControllerTRM {
	
	@Autowired
	private UsuariosService usuarioService;
	
	@Autowired
	private SolicitudesCrearUsuarioService SolicitudService;
	
	@Autowired
	private TrmServices trmService;
	
	private String notificacion = "";
	
	private RegistrosLog registrolog = new RegistrosLog();
	
	@RequestMapping(value = "/listaTrm")
	public String ListaTRM (@RequestParam(name = "page" , defaultValue = "0")  int page  ,ModelMap mp , RedirectAttributes redirectAttributes , Model model , Principal principal) {
		
		String notificacion = (String) model.asMap().get("notificacion");
		
		String NombreUsuarioLogeado = principal.getName();
		
		Usuarios usuario = usuarioService.buscarUsuario(NombreUsuarioLogeado);
		
		String Rol = usuario.getRol().get(0).getName();
		
		mp.put("Area", usuario.getArea().getArea());
		
		mp.put("rol", Rol);

		Pageable pageRequest = PageRequest.of(page, 5);
		
		Page<TRM> ListaValoreTrm = trmService.ListaTrmPagin(pageRequest);
		
		Paginador <TRM> pageRender = new Paginador<>("/listaTrm" , ListaValoreTrm);
		
		List<SolicitudesCrearUsuario> cantidadSolicitudes = SolicitudService.cantidadSolicitudes("Enviada");
		
		List<Usuarios> ListaUsuarioCambioCon = usuarioService.listaUsuariosCambio("Olvidada");
		
		int CatidadSolicitudes = cantidadSolicitudes.size();
		
		mp.put("CatidadSolicitudes", CatidadSolicitudes);
		
		int cambiodecontra = ListaUsuarioCambioCon.size();
		
		mp.put("CatidadSolicitudesCambio", cambiodecontra);

		long TotalListaValoreTrm = ListaValoreTrm.getTotalElements();
		
		mp.put("notificacion", notificacion);
		
		mp.put("totalAreas", TotalListaValoreTrm);
		
		mp.put("ListaTrm", ListaValoreTrm);
		
		mp.put("page", pageRender);

		DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

		String date = dateFormat.format(Calendar.getInstance().getTime());

		String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ Accedio a la lista de TRM ]";

		registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);
		
		return "ParametroTRMlista";
	}
	
	@GetMapping(value = "/eliminarTrm/{id}")
	public String EliminarTrm (@PathVariable("id") long id , RedirectAttributes redirectAttributes, Model model, ModelMap mp , Principal principal) {
		
		String NombreUsuarioLogeado = principal.getName();

		DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

		String date = dateFormat.format(Calendar.getInstance().getTime());
		
		trmService.eliminarTrm(id);
		
		notificacion = "La Trm ha sido eliminada correctamente";
		
		redirectAttributes.addFlashAttribute("notificacion" , notificacion);
		
		String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ " + "Elimino la TRM con el id : " + id + "]";

		registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);
		
		return "redirect:/listaTrm";
		
		
	}
	
	@GetMapping("/crearTrm")
	public String CrearTrm (ModelMap mp , Model model , Principal principal , RedirectAttributes redirectAttributes) {
		
		
		
		if (trmService.buscarTrmActiva("Activo") != null) {
			
			notificacion = "por favor deshabilite la Trm Activa, para poder crear un nuevo parámetro o edite el parámetro existente";
			
			redirectAttributes.addFlashAttribute("notificacion" , notificacion);
			
			return "redirect:/listaTrm";
			
		}
		
		String NombreUsuarioLogeado = principal.getName();
		
		DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

		String date = dateFormat.format(Calendar.getInstance().getTime());
		
		Usuarios usuario = usuarioService.buscarUsuario(NombreUsuarioLogeado);
		
		String Rol = usuario.getRol().get(0).getName();
		
		mp.put("Area", usuario.getArea().getArea());
		
		mp.put("rol", Rol);
		
		TRM trm = new TRM();
		
		mp.put("Trm", trm);
		
		String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ " + "Ingreso al formulario crear TRM ]";

		registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);
		
		return "ParametroTRMCrear";
	}
	
	@PostMapping("/guardarTrm")
	public String GuardarTRM  (TRM trm , RedirectAttributes redirectAttributes , Principal principal) {
		
		String NombreUsuarioLogeado = principal.getName();

		DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

		String date = dateFormat.format(Calendar.getInstance().getTime());
		
		String Piso = trm.getPiso();
		
		String Techo = trm.getTecho();
		
		trm.setPiso(Piso);
		
		trm.setTecho(Techo);
		
		//System.out.println("el valor del pueso es : " + Piso);
		
		//System.out.println("el valor del techo es : " + Techo);
		
		Piso = Piso.replace(",", "");
		
		Techo = Techo.replace(",", "");
		
		double ValorPisco = Double.parseDouble(Piso);
		
		double valorTecho = Double.parseDouble(Techo);
		
		trm.setValorpiso(ValorPisco);
		
		trm.setValortecho(valorTecho);

		trm.setEstado("Activo");
		
		trmService.guardarTrm(trm);
		
		notificacion = "La TRM se ha guardado con éxito";
		
		redirectAttributes.addFlashAttribute("notificacion",notificacion);
		
		String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ guardo la TRM de manera correcta ]";

		registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);
		
		return "redirect:/listaTrm";
	}
	
	@GetMapping("/editarTrm/{id}")
	public String EditarTrm (@PathVariable ("id") long id , TRM trm , ModelMap mp , Principal principal) {
		

		String NombreUsuarioLogeado = principal.getName();
		
		DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

		String date = dateFormat.format(Calendar.getInstance().getTime());
		
		Usuarios usuario = usuarioService.buscarUsuario(NombreUsuarioLogeado);
		
		String Rol = usuario.getRol().get(0).getName();
		
		mp.put("Area", usuario.getArea().getArea());
		
		mp.put("rol", Rol);
		
		 TRM TrmEditar = trmService.BuscaarTrm(id);
		
		 mp.put("Trm", TrmEditar);
		 
		 String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ " + "Edito la TRM con el id : " + id + "]";

		 registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);
		
		return "ParametroTRMeditar";
	}
	
	@PostMapping ("/guardarCambiosTrm")
	public String GuardarCambios (TRM trm , RedirectAttributes redirectAttributes , Principal principal) {
		
		String NombreUsuarioLogeado = principal.getName();

		DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

		String date = dateFormat.format(Calendar.getInstance().getTime());
		
		String Piso = trm.getPiso();
		
		String Techo = trm.getTecho();
		
		trm.setPiso(Piso);
		
		trm.setTecho(Techo);
		
		Piso = Piso.replace(",", "");
		
		Techo = Techo.replace(",", "");
		
		double ValorPisco = Double.parseDouble(Piso);
		
		double valorTecho = Double.parseDouble(Techo);
		
		trm.setValorpiso(ValorPisco);
		
		trm.setValortecho(valorTecho);
		
		trm.setEstado("Activo");
		
		trmService.guardarTrm(trm);

		notificacion = "Cambios guardados correctamente";
		
		String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ " + notificacion + " ]";

		registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);
		
		return "redirect:/listaTrm";
	}
	
	@RequestMapping(value = "/DesabilitarTRM/{id}", method = RequestMethod.GET)
	public String DesabilitarTRM(@PathVariable("id") long id, HttpServletRequest request, ModelMap mp , Principal principal) {

		String NombreUsuarioLogeado = principal.getName();

		DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

		String date = dateFormat.format(Calendar.getInstance().getTime());
		
		TRM TRM = null;

		TRM = trmService.BuscaarTrm(id);

		TRM.setEstado("inactivo");

		trmService.guardarTrm(TRM);
		
		String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ " + "Desabilito la TRM con el id : " + id + "]";

		registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);

		return "redirect:/listaTrm";
	}

	@RequestMapping(value = "/HabilitarTRM/{id}", method = RequestMethod.GET)
	public String HabilitarTRM(@PathVariable("id") long id, HttpServletRequest request, ModelMap mp , Principal principal) {

		String NombreUsuarioLogeado = principal.getName();

		DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

		String date = dateFormat.format(Calendar.getInstance().getTime());
		
		TRM TRM = null;

		TRM = trmService.BuscaarTrm(id);

		TRM.setEstado("Activo");

		trmService.guardarTrm(TRM);
		
		String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ " + "Habilito la TRM con el id : " + id + "]";

		registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);

		return "redirect:/listaTrm";
	}
	
	@GetMapping("/activarParametroTrm")
	public String activarParametroTrm(ModelMap mp , RedirectAttributes redirectAttributes) {
		
		
		List<TRM> TRM = trmService.listaValoresTrm();
		
		for (int i = 0; i < TRM.size(); i++) {
			
			long id = TRM.get(i).getId();
			
			TRM TrmActual = trmService.BuscaarTrm(id);
			
			TrmActual.setEstado("Activo");
			
			trmService.guardarTrm(TrmActual);
			
		}
		
		redirectAttributes.addFlashAttribute("notificacion", "Todos los parámetros se han activado");
		
		return "redirect:/listaTrm";
		
	}
	
	
	@GetMapping("/DesactivarParametroTrm")
	public String DesactivarParametroMoneda(ModelMap mp , RedirectAttributes redirectAttributes) {
		
		List<TRM> TRM = trmService.listaValoresTrm();
		
		for (int i = 0; i < TRM.size(); i++) {
			
			long id = TRM.get(i).getId();
			
			TRM TrmActual = trmService.BuscaarTrm(id);
			
			TrmActual.setEstado("desactivado");
			
			trmService.guardarTrm(TrmActual);
			
		}
		
		redirectAttributes.addFlashAttribute("notificacion", "Todos los parámetros se han sido desactivados, este parámetro no se tomará en cuenta al momento de validar el lote");
		
		return "redirect:/listaTrm";
		
	}

}
