package com.example.demo.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.Principal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.poi.openxml4j.util.ZipSecureFile;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.example.demo.RegistrosLog;
import com.example.demo.Entity.CentroCostos;
import com.example.demo.Entity.SolicitudesCrearUsuario;
import com.example.demo.Entity.Usuarios;
import com.example.demo.Paginador.Paginador;
import com.example.demo.service.CargarArchivoServer;
import com.example.demo.service.CentroCostosService;
import com.example.demo.service.SolicitudesCrearUsuarioService;
import com.example.demo.service.UsuariosService;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;


@Controller
public class ControllerCentroCostos {
	
	@Autowired
	private UsuariosService usuarioService;
	
	@Autowired
	private SolicitudesCrearUsuarioService SolicitudService;
	
	@Autowired
	private CentroCostosService serviceCentroCostos;
	
	private String notificacion = "";
	
	@Autowired
	private CargarArchivoServer cargarArchivo;
	
	private RegistrosLog registrolog = new RegistrosLog();
	
	private Date date = new Date();
	
	// ---------------------------- controller centro costos // -------------------------------- //

	
		@GetMapping("/CrearCentroCostos")
		public String CrearCentroCostos(ModelMap mp , Principal principal) {
			
			String NombreUsuarioLogeado = principal.getName();
			
			DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

			String date = dateFormat.format(Calendar.getInstance().getTime());
			// hora en la que se carga el lote para validar
							
			String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ Crear centro costos ]";
							
			registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);
			
			mp.put("centro", new CentroCostos());

			return "ParametroCentroCostosCrear";
		}
		
		@RequestMapping(value = "/ListaCentroCostos" , method = RequestMethod.GET)
		public String ListaCentroCostos(@RequestParam(name = "page" , defaultValue = "0")  int page ,  ModelMap mp , Principal principal , Model model) {

			String NombreUsuarioLogeado = principal.getName();
			
			String notificacion = (String) model.asMap().get("notificacion");
			
			DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

			String date = dateFormat.format(Calendar.getInstance().getTime()); // hora en la que se carga el lote para validar
			
			Usuarios usuario = usuarioService.buscarUsuario(NombreUsuarioLogeado);
			
			String Rol = usuario.getRol().get(0).getName();
			
			mp.put("Area", usuario.getArea().getArea());
			
			mp.put("rol", Rol);
			
			
			Pageable pageRequest = PageRequest.of(page,10);
			

			Page <CentroCostos> centroCostos = serviceCentroCostos.listaCentroCostosPagina(pageRequest);
			
			Paginador <CentroCostos> pageRender = new Paginador<>("/ListaCentroCostos" , centroCostos);
			
			List<SolicitudesCrearUsuario> cantidadSolicitudes = SolicitudService.cantidadSolicitudes("Enviada");
			
			List<Usuarios> ListaUsuarioCambioCon = usuarioService.listaUsuariosCambio("Olvidada");
			
			int CatidadSolicitudes = cantidadSolicitudes.size();
			
			mp.put("CatidadSolicitudes", CatidadSolicitudes);
			
			int cambiodecontra = ListaUsuarioCambioCon.size();
			
			mp.put("CatidadSolicitudesCambio", cambiodecontra);

			
			mp.put("notificacion", notificacion);
			mp.put("totalRegistros", serviceCentroCostos.listaCostos().size());
			mp.put("centroCostos", centroCostos);
			mp.put("page", pageRender);
			
			String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ lista centro costos ]";
			
			registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);

			return "ParametroCentroCostosLista";
		}

		@RequestMapping("/guardarCentroCostos")
		public String GuardarCentroCostos(Model model, CentroCostos centrocostos) {

			try {

				centrocostos.setEstado("Activo");
				serviceCentroCostos.GuardarCentroCostos(centrocostos);
				//System.out.println("guardado exitosamente");

			} catch (Exception e) {

				//System.out.println("error" + e);

			}

			return "redirect:/ListaCentroCostos";
		}
		
		@RequestMapping(value = "/EditarCentroCostos/{id}", method = RequestMethod.GET)
		public String EditarCentroCostos (@PathVariable("id") long id , HttpServletRequest request, ModelMap mp , CentroCostos centro) {
			
			mp.put("centro", centro);
			
			return "ParametroCentroCostosEditar";
		}
		
		@PostMapping("/actualizarCentroCostos")
		public String ActualizarCentroCostos (ModelMap mp , CentroCostos centro) {

			//System.out.println("nuevo fuente");
			//System.out.println("fuente " + fuentes.getFuente());
			//System.out.println("Nombre fuente " + fuentes.getFuente());
			//System.out.println("Codifo fuente:  " + fuentes.getId());
			
			String NuevoNombre = centro.getNombre();
			int NuevoCentroCostos= centro.getCentroCostos();
			
			if(NuevoNombre.isEmpty() == true) {
				
				//System.out.println("El nombre no puede estar vacion");
				
				return "ParametroCentroCostosEditar";
				
			}else {
				
				centro.setCentroCostos(NuevoCentroCostos);
				centro.setNombre(NuevoNombre);
				centro.setEstado("Activo");

				serviceCentroCostos.GuardarCentroCostos(centro);
				
				//System.out.println("dato actualizado exitosamente");
				
			}

			
			return "redirect:/ListaCentroCostos";
		} 
		
		
		@RequestMapping(value = "/EliminarCentroCosto/{id}", method = RequestMethod.GET)
		public String EliminarCentroCostos(@PathVariable("id") long id, HttpServletRequest request, ModelMap mp , Principal principal , RedirectAttributes redirectAttributes) {
			
			String NombreUsuarioLogeado = principal.getName();

			DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

			String date = dateFormat.format(Calendar.getInstance().getTime()); // hora en la que se carga el lote para validar
							
			String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ elimino el centro de costos " + id + "]";
							
			registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);
			
			serviceCentroCostos.EliminarCentroCosto(id);
			
			redirectAttributes.addFlashAttribute("notificacion", "El centro de costos de ha eliminado");
			
			return "redirect:/ListaCentroCostos";
		}

		@RequestMapping(value = "/DesabilitarCostos/{id}", method = RequestMethod.GET)
		public String DesabilitarCentroCostos(@PathVariable("id") long id, HttpServletRequest request, ModelMap mp , Principal principal , RedirectAttributes redirectAttributes) {

			String NombreUsuarioLogeado = principal.getName();

			DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

			String date = dateFormat.format(Calendar.getInstance().getTime());; // hora en la que se carga el lote para validar
							
			String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ elimino el centro de costos " + id + "]";
							
			registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);
			
			CentroCostos centroCostos = null;

			centroCostos = serviceCentroCostos.findOne(id);

			centroCostos.setEstado("inactivo");

			serviceCentroCostos.GuardarCentroCostos(centroCostos);
			
			redirectAttributes.addFlashAttribute("notificacion", "El centro de costos ha sido deshabitado");

			return "redirect:/ListaCentroCostos";
		}

		@RequestMapping(value = "/HabilitarCostos/{id}", method = RequestMethod.GET)
		public String HabilitarCentroCostos(@PathVariable("id") long id, HttpServletRequest request, ModelMap mp , Principal principal , RedirectAttributes redirectAttributes) {
			
			String NombreUsuarioLogeado = principal.getName();

			DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

			String date = dateFormat.format(Calendar.getInstance().getTime()); // hora en la que se carga el lote para validar

			CentroCostos centroCostos = null;

			centroCostos = serviceCentroCostos.findOne(id);

			centroCostos.setEstado("Activo");
			
			String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ Habilito el centro de constos con id  " + id + "]";
			
			registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);

			serviceCentroCostos.GuardarCentroCostos(centroCostos);
			
			redirectAttributes.addFlashAttribute("notificacion", "El centro de costos ha sido Habilitado");

			return "redirect:/ListaCentroCostos";
		}
		
		@GetMapping("/CargarMasivaCentroCostos")
		public String CargaMasivaCentroCostos (ModelMap mp , Model model , RedirectAttributes redirectAttributes , Principal principal) {
			
			String NombreUsuarioLogeado = principal.getName();

			DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

			String date = dateFormat.format(Calendar.getInstance().getTime()); // hora en la que se carga el lote para validar
			
			String NombreArchivoCentro = (String) model.asMap().get("NombreArchivo");
			
			String ruta = "..\\CentroCostos\\" + NombreArchivoCentro;

			FileInputStream file;

			try {

				file = new FileInputStream(new File(ruta));
				Workbook workbook = new XSSFWorkbook(file);

				org.apache.poi.ss.usermodel.Sheet sheet = workbook.getSheetAt(0);
				ZipSecureFile.setMinInflateRatio(0);
				
				

				for (Row row : sheet) {

					int contador = 0; // contador de columna


					System.out.println(sheet.getHeader().getCenter());
					
			
					

					if (row.getRowNum() == 0) {

						//System.out.println("saltar fila ya que es la cabezera del archivo " + row.);
						System.out.println("");

					} else {
						
						CentroCostos Centrocostos = new CentroCostos();
						

						for (Cell cell : row) {
							
							 
							
							contador = contador + 1;

							switch (cell.getCellType()) {
							
							case STRING:


								if (contador == 2) { // Columna nombre de cuenta

									String NombreCentroCostos = cell.getRichStringCellValue().getString();
									
									//System.out.println("Nombre de centro costos " + NombreCentroCostos);
									
									Centrocostos.setNombre(NombreCentroCostos);
									

								}
		
								break;

							case NUMERIC:


								if (contador == 1) { // COLUMNA CODIGO FUENTE NUMERICA

									double CentroCostos = cell.getNumericCellValue();
									
									//double Centro = lote.LimpiarDatoNumerico(CentroCostos);
									
									int Centro = (int) CentroCostos;
			
									//System.out.println("Centro Costos: " + Centro);
									
									Centrocostos.setCentroCostos(Centro);
			
								}
			
							default:
								break;

							}
							
					
							
						

						}
						
						//System.out.println("el contaodor es : " + contador);
						
						if (contador != 4) {
							
							notificacion = "El archivo " + NombreArchivoCentro + " no cumple con la estructura";
							
							redirectAttributes.addFlashAttribute("notificacion",notificacion);
							
							return "redirect:/ListaCentroCostos";
							
						}
						
						Centrocostos.setEstado("Activo");

						serviceCentroCostos.GuardarCentroCostos(Centrocostos);
						
						/*List<CentroCostos> centroCostos = serviceCentroCostos.BuscarCentroCostos(Centrocostos.getCentroCostos());
						
						int centroCosto = centroCostos.get(0).getCentroCostos();
						
						String NombreCentroCostos = centroCostos.get(0).getNombre();
						
						if(centroCostos == null) {
							
							Centrocostos.setEstado("Activo");

							serviceCentroCostos.GuardarCentroCostos(Centrocostos);
							
						}
						
						if(centroCostos != null) {
							
							if (centroCosto != Centrocostos.getCentroCostos()) {
								
								centroCostos.set(centroCosto, Centrocostos)
								
							}
							
						}*/
						
						
						
						workbook.close();
					}
	

				}
				
				//System.out.println("Guardado exitosamente");

			} catch (FileNotFoundException e) {

				//System.out.print("No se a cargado ninguna lote para validar por favor seleccionar un lote");
				
				String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ No se pudo cargar el archivo en la ruta " + ruta + " de manera exitosa ] ";
								
				registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);

			} catch (IOException e) {

				e.printStackTrace();
				
				//System.out.print("No se a cargado ninguna lote para validar por favor seleccionar un lote");
				
				String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ No se pudo cargar el archivo en la ruta " + ruta + " de manera exitosa ] ";
								
				registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);

			}

			redirectAttributes.addFlashAttribute("notificacion", "Centros de costos cargados correctamente");
			
			String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ Cargo el archivo  " + ruta + " de maenra exitosa ]";
			
			registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);
			
			return "redirect:/ListaCentroCostos";
		}
		
		@GetMapping("/ActualizarCentroCostosMasivo")
		public String ActualizarCargaMasivaCentroCostos (Model model , RedirectAttributes redirectAttributes , Principal principal) {
		
		String NombreUsuarioLogeado = principal.getName();

		DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

		String date = dateFormat.format(Calendar.getInstance().getTime());
			
		String NombreArchivo = (String) model.asMap().get("NombreArchivo");
			
		serviceCentroCostos.ActualizarCargarMasivaCentroCostos();
		
		redirectAttributes.addFlashAttribute("NombreArchivo",NombreArchivo);
		
		
		String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ Actualizo Centro de costos]";
						
		registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);
			
			return "redirect:/CargarMasivaCentroCostos";
		}
		
		
		

		@PostMapping("/ValidarExistenciaCentroCostosCarpeta")
		public String validarExistenciaCarpeta(@RequestParam("file") MultipartFile file , RedirectAttributes redirectAttributes , Model model) throws IOException {
			
			DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

			String date = dateFormat.format(Calendar.getInstance().getTime()); // hora en la que se carga el lote para validar
			
			String NombreArchivo = file.getOriginalFilename();
			
			//System.out.println("el archivo de area es : " + NombreArchivo);
			
			//String NombreArchivo =  dato.replaceAll("[xlsx]", "");

			String ruta = "..\\CentroCostos";

			// esta carpeta se creara cada vez que se descargue y se llene un plano
			File directorio = new File(ruta);
			

			// ---------------- validamos que en la carpeta areas no exista el archivo si existe se elimina para la llegada del nuevo archivo areas // -----------//
			


			if (directorio.exists()) {

				File directory = new File(ruta);
					
				FileUtils.cleanDirectory(directory);
					
				cargarArchivo.save(file,ruta);
					
				redirectAttributes.addFlashAttribute("NombreArchivo",NombreArchivo);
				
				String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + "sistema" + "] " + "Tarea" + " [ Creo la carpeta centroCostos dentro del servidor ]";
				
				registrolog.guardarRegistro("sistema", tarea);

				return "redirect:/ActualizarCentroCostosMasivo";

			}
			
			else {
				
				 cargarArchivo.init(ruta);
				 
				 cargarArchivo.save(file,ruta);
				 
				 redirectAttributes.addFlashAttribute("NombreArchivo",NombreArchivo);
				 
				 String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + "sistema" + "] " + "Tarea" + " [ Creo la carpeta centroCostos dentro del servidor ]";
					
				registrolog.guardarRegistro("sistema", tarea);
				 
				 return "redirect:/CargarMasivaCentroCostos";
				
			}

		}
		
		
		
		
		@GetMapping("/notificacionPlantillaCentroCostos")
		public String NotificacionPlantilla (Model model , RedirectAttributes redirectAttributes) {
			
			notificacion = "El archivo CentrosCostos.xlsx no existe en la ruta, por favor guardar el archivo en la carpeta insumos, para poder cargar o actualizar los centros de costos";
			
			redirectAttributes.addFlashAttribute("notificacion", notificacion);
			
			return "redirect:/ListaCentroCostos";
		}
		
		
		@PostMapping("/BuscarCentroCosto")
		public String BuscarCuenta(@RequestParam(value = "dato", required = false) String dato ,ModelMap mp , Principal principal , RedirectAttributes redirectAttributes) {

			String NombreUsuarioLogeado = principal.getName();
			
			DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

			String date = dateFormat.format(Calendar.getInstance().getTime()); // hora en la que se carga el lote para validar
			
			Usuarios usuario = usuarioService.buscarUsuario(NombreUsuarioLogeado);
			
			String Rol = usuario.getRol().get(0).getName();
			
			mp.put("Area", usuario.getArea().getArea());
			
			mp.put("rol", Rol);
			
			int CantroCosto = Integer.parseInt(dato);
			
			List<CentroCostos> CentroCosto = serviceCentroCostos.BuscarCentroCostos(CantroCosto);
			
			if (CentroCosto.isEmpty()) {
				
				redirectAttributes.addFlashAttribute("notificacion", "El centro de costos " + CantroCosto + " No existe dentro del sistema");
				
				return "redirect:/ListaCentroCostos";
			}

			//System.out.println( "Numero de centro de costo a buscar  : " + dato);
			
			mp.put("centroCostos", CentroCosto);
			
			String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ Busco centro de costos " + dato + "] " ;
			
			registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);

			return "ParametroCentroCostosBuscar";

		}
		
		@GetMapping("/activarParametroCentroCostos")
		public String activarParametroCentroCostos(ModelMap mp , RedirectAttributes redirectAttributes) {
			
			
			List<CentroCostos> CentroCostos = serviceCentroCostos.listaCostos();
			
			for (int i = 0; i < CentroCostos.size(); i++) {
				
				long id = CentroCostos.get(i).getId();
				
				CentroCostos CentroCostosActual = serviceCentroCostos.findOne(id);
				
				CentroCostosActual.setEstado("Activo");
				
				serviceCentroCostos.GuardarCentroCostos(CentroCostosActual);
				
			}
			
			redirectAttributes.addFlashAttribute("notificacion", "Todos los parámetros han sido activados");
			
			return "redirect:/ListaCentroCostos";
			
		}
		
		
		@GetMapping("/DesactivarParametroCentroCostos")
		public String DesactivarParametroCentroCostos(ModelMap mp , RedirectAttributes redirectAttributes) {
			
			
			List<CentroCostos> CentroCostos = serviceCentroCostos.listaCostos();
			
			for (int i = 0; i < CentroCostos.size(); i++) {
				
				long id = CentroCostos.get(i).getId();
				
				CentroCostos CentroCostosActual = serviceCentroCostos.findOne(id);
				
				CentroCostosActual.setEstado("desactivado");
				
				serviceCentroCostos.GuardarCentroCostos(CentroCostosActual);
				
			}
			
			redirectAttributes.addFlashAttribute("notificacion", " Todos los parámetros han sido desactivados, este parámetro no se tomará en cuenta al momento de validar el lote  ");
			
			return "redirect:/ListaCentroCostos";
			
		}
		
		
		

}
