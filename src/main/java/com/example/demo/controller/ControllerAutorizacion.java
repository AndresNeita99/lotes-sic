package com.example.demo.controller;

import java.io.File;
import java.io.FileInputStream;

import java.io.OutputStream;
import java.security.Principal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.example.demo.RegistrosLog;

import com.example.demo.Entity.LoteUsuario;
import com.example.demo.Entity.Mensaje;
import com.example.demo.Entity.ResumenLote;
import com.example.demo.Entity.Usuarios;
import com.example.demo.Paginador.Paginador;
import com.example.demo.service.LoteUsuerioService;
import com.example.demo.service.MensajeService;
import com.example.demo.service.ResumenLoteService;
import com.example.demo.service.UsuariosService;
import com.example.demo.service.emailEnviar;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@Controller
public class ControllerAutorizacion {
	
	@Autowired
	private MensajeService mensajeServi;
	
	@Autowired
	private emailEnviar emailSender;

	@Autowired
	private UsuariosService usuarioService;

	@Autowired
	private LoteUsuerioService loteUsuarioservice;

	@Autowired
	private ResumenLoteService resumenLoteService;

	private String Notificacion = "";
	
	private RegistrosLog registrolog = new RegistrosLog();
	
	private Date date = new Date();

	
	@RequestMapping(value = "/bandejaEntradaLote" , method = RequestMethod.GET)
	public String BandelaLotes(@RequestParam(name = "page" , defaultValue = "0")  int page , ModelMap mp, Principal principal, RedirectAttributes redirectAttributes, Model model) {

		String NombreUsuarioLogeado = principal.getName();
		
		Usuarios usuario = usuarioService.buscarUsuario(NombreUsuarioLogeado);
		
		String Rol = usuario.getRol().get(0).getName();
		
		mp.put("Area", usuario.getArea().getArea());
		
		mp.put("rol", Rol);

		String NombreAutorizador = usuario.getNombre();
		 
		String notificacion = (String) model.asMap().get("notificacion");
		
		String notificacion2 = (String) model.asMap().get("notificacion2");
		
		String idlote = (String) model.asMap().get("idLote");
		
		if (idlote != null) {
			
			LoteUsuario loteActual = loteUsuarioservice.buscarLoteId(idlote);
			
			mp.addAttribute("loteActual", loteActual);
		}
		
		Pageable pageRequest = PageRequest.of(page, 10);
		
		Page <LoteUsuario> LotesAutorizar = loteUsuarioservice.listaLotesAutorizador(NombreAutorizador, "Enviado Autorizar" ,pageRequest);

		Paginador <LoteUsuario> pageRender = new Paginador<>("/bandejaEntradaLote" , LotesAutorizar);
		
		mp.put("total", LotesAutorizar.getTotalElements());

		mp.put("page", pageRender);
		
		mp.put("notificacion", notificacion);
		
		mp.put("notificacion2", notificacion2);

		mp.put("lotesAutorizador", LotesAutorizar);
		

		DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");
		
		String date = dateFormat.format(Calendar.getInstance().getTime());
						
		String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ Lista de lotes para autorizar ]";
						
		registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);

		return "AutorizarLoteBandeja";
	}

	
	@RequestMapping(value = "/lotesAprobados" , method = RequestMethod.GET)
	public String LotesAprobado(@RequestParam(name = "page" , defaultValue = "0")  int page , ModelMap mp, Principal princiapl, RedirectAttributes redirectAttributes, Model model) {

		String NombreUsuarioLogeado = princiapl.getName();
		
		DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");
		
		String date = dateFormat.format(Calendar.getInstance().getTime());
		
		Usuarios usuario = usuarioService.buscarUsuario(NombreUsuarioLogeado);
		
		String NombreAutorizador = usuario.getNombre();
		
		String Rol = usuario.getRol().get(0).getName();
		
		mp.put("Area", usuario.getArea().getArea());
		
		mp.put("rol", Rol);
		
		//System.out.println("nombre del autorizador " + NombreAutorizador);

		Pageable pageRequest = PageRequest.of(page, 5);
		
		Page <LoteUsuario> LotesAutorizar = loteUsuarioservice.listaLotesAutorizador(NombreAutorizador, "Autorizado" ,pageRequest);

		Paginador <LoteUsuario> pageRender = new Paginador<>("/lotesAprobados" , LotesAutorizar);


		if (LotesAutorizar.isEmpty()) {

			Notificacion = "No tienes actualmente lotes aprobados";

			redirectAttributes.addFlashAttribute("notificacion", Notificacion);
			
			String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ No tiene lotes aprobados ]";
			
			registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);

			return "redirect:/bandejaEntradaLote";
		}
		
		mp.put("total", LotesAutorizar.getTotalElements());
		
		mp.put("page", pageRender);

		mp.put("lotesAutorizador", LotesAutorizar);
						
		String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ Realizo busqueda de lotes aprobados ]";
		
		registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);

		return "AutorizarLoteAprobados";

	}

	@RequestMapping(value = "/lotesRechazados" , method = RequestMethod.GET)
	public String LotesRechazados(@RequestParam(name = "page" , defaultValue = "0")  int page , ModelMap mp, Principal principal, RedirectAttributes redirectAttributes) {
		
		String NombreUsuarioLogeado = principal.getName();
		
		DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");
		
		String date = dateFormat.format(Calendar.getInstance().getTime());
		
		Usuarios usuario = usuarioService.buscarUsuario(NombreUsuarioLogeado);
		
		String Rol = usuario.getRol().get(0).getName();
		
		mp.put("Area", usuario.getArea().getArea());
		
		mp.put("rol", Rol);

		String NombreAutorizador = usuario.getNombre();
		
		Pageable pageRequest = PageRequest.of(page, 5);
		
		Page <LoteUsuario> LotesAutorizar = loteUsuarioservice.listaLotesAutorizador(NombreAutorizador, "Rechazado" ,pageRequest);

		Paginador <LoteUsuario> pageRender = new Paginador<>("/lotesRechazados" , LotesAutorizar);

		if (LotesAutorizar.isEmpty()) {

			Notificacion = "No tienes actualmente lotes rechazados";

			redirectAttributes.addFlashAttribute("notificacion", Notificacion);
			
			String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ No tiene lotes rechazados ]";
			
			registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);

			return "redirect:/bandejaEntradaLote";
		}

		mp.put("total", LotesAutorizar.getTotalElements());
		
		mp.put("page", pageRender);

		mp.put("lotesAutorizador", LotesAutorizar);
		
		String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ Realizo busqueda lotes rechazados ]";
		
		registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);


		return "AutorizarLoteRechazados";
	}

	@GetMapping("/listaLotesAutorizar")
	public String MostrarLotesAprobados(ModelMap mp, Principal principal, RedirectAttributes redirectAttributes, Model model) {

		// String NombreAutorizador = princiapl.getName(); // este nombre es traido del
		// login de autorizador ya sea nivel 1 o nivel 2
		
		String NombreUsuarioLogeado = principal.getName();
		
		Usuarios usuario = usuarioService.buscarUsuario(NombreUsuarioLogeado);
		
		String Rol = usuario.getRol().get(0).getName();
		
		mp.put("Area", usuario.getArea().getArea());
		
		mp.put("rol", Rol);

		String NombreAutorizador = usuario.getNombre();


		List<LoteUsuario> lotesAutorizador = loteUsuarioservice.BuscarLotesAutorizador(NombreAutorizador);

		mp.put("lotesAutorizador", lotesAutorizador);

		return "AutorizarLote";
	}

	@RequestMapping(value = "/verDesallesLote/{id}", method = RequestMethod.GET)
	public String verDetallesLote(@PathVariable("id") String id, ModelMap mp, RedirectAttributes redirectAttributes , Principal principal) {
		
		String NombreUsuarioLogeado = principal.getName();
		
		Usuarios usuario = usuarioService.buscarUsuario(NombreUsuarioLogeado);
				
		String Rol = usuario.getRol().get(0).getName();
				
		mp.put("Area", usuario.getArea().getArea());
				
		mp.put("rol", Rol);

		LoteUsuario LoteUsuario = loteUsuarioservice.buscarLoteId(id);
		
		DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");
		
		String date = dateFormat.format(Calendar.getInstance().getTime());

		if (LoteUsuario == null) {

			Notificacion = "El lote no se encuentra registrado";

			redirectAttributes.addFlashAttribute("notificacion", Notificacion);
			
			String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ El lote no se encuentra registrado " + id + "]";
					
			registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);

			return "redirect:/bandejaEntradaLote";
		}
		
		String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ consulta detalles del lote  " + id + "]";
		
		registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);

		mp.put("lotesAutorizador", LoteUsuario);

		return "AutorizarLote";

	}

	@RequestMapping(value = "/devolverlote/{id}", method = RequestMethod.GET)
	public String MotivoRechazo(@PathVariable("id") String id, ModelMap mp, RedirectAttributes redirectAttributes , Principal principal) {

		String NombreUsuarioLogeado = principal.getName();
		
		LoteUsuario LoteUsuario = loteUsuarioservice.buscarLoteId(id);
		
		DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");
		
		String date = dateFormat.format(Calendar.getInstance().getTime());

		if (LoteUsuario == null) {

			Notificacion = "El lote no se encuentra registrado";

			redirectAttributes.addFlashAttribute("notificacion", Notificacion);
			
			String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ El lote " + id + " no se ecuentra en db ]";
			
			registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);

			return "redirect:/bandejaEntradaLote";
		}

		mp.put("loteUsuario", LoteUsuario);
		
		String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ El lote " + id + " A sido rechazado]";
		
		registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);

		return "AutorizarLoteDevolver";

	}

	@PostMapping("/guardarMotivoRechazo")
	public String GuardarMotivoRechazo(ModelMap mp, LoteUsuario loteUsuario, RedirectAttributes redirectAttributes , Principal principal) {
		
		String NombreUsuarioLogeado = principal.getName();
		
		Mensaje mensaje = new Mensaje();

		DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");
		
		String date = dateFormat.format(Calendar.getInstance().getTime());

		String IdLote = loteUsuario.getIdLote();
		
		LoteUsuario loteRechazado = loteUsuarioservice.buscarLoteId(IdLote);
		
		String NombreAnalista = loteRechazado.getAnalista();
		
		Usuarios AnalistaResponsable = usuarioService.buscarPorNombre(NombreAnalista);
		
		String CorreoAnalista = AnalistaResponsable.getCorreo();
		
		String RutaLoteRechazar = loteRechazado.getRutaArchivo();
		
		String ComentarioRechazo = loteUsuario.getComentario();

		LoteUsuario LoteActual = loteUsuarioservice.buscarLoteId(IdLote);

		LoteActual.setComentario(ComentarioRechazo);

		LoteActual.setEstadoLote("Rechazado");

		loteUsuarioservice.guardarLote(LoteActual);

		Notificacion = "El lote " + IdLote + " ha sido rechazado y su estado ha cambiado a No autorizado";
		
		String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ " + IdLote + " rechazado , motivo de rechazo " + ComentarioRechazo + "]";
		
		registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);

		redirectAttributes.addFlashAttribute("notificacion", Notificacion);
		
		emailSender.sendEmail("Estado de Lote SIC", "Test Message : El estado de su lote  : " + IdLote + "ha cambiado a rechazado, por el motivo de : " + ComentarioRechazo, "administracionlotes@pichincha.com.co" , CorreoAnalista /* PONER del nuevo usuario */ , true );

		/* envio por mensaje que el lote a sido rechazado*/
		
		mensaje.setComentario("el lote " + IdLote + "ha sido sido rechazado por el autorizador : " + NombreUsuarioLogeado);
		mensaje.setDestinatario(NombreAnalista);
		mensaje.setFecha(date);
		
		
		mensajeServi.enviarMensaje(mensaje);
		

		return "redirect:/bandejaEntradaLote";
	}

	@GetMapping(value = "/AprobarLote/{id}")
	public String AprobarLote(@PathVariable("id") String id, ModelMap mp, RedirectAttributes redirectAttributes , Principal principal) {
		
		Mensaje mensaje = new Mensaje();
		
		Mensaje mensaje2 = new Mensaje();
		
		Usuarios Centralizador = usuarioService.listaUsuariosPorIdRol(7);
		
		String nombreCentralizador = Centralizador.getNombre();
		
		String correoCentralizador = Centralizador.getCorreo();
		
		String NombreUsuarioLogeado = principal.getName();

		DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");
		
		String date = dateFormat.format(Calendar.getInstance().getTime()); // hora en la que se carga el lote para validar

		LoteUsuario loteActual = loteUsuarioservice.buscarLoteId(id);
		
		String NombreAnalista = loteActual.getAnalista();
		
		Usuarios AnalistaResponsable = usuarioService.buscarPorNombre(NombreAnalista);
		
		String CorreoAnalista = AnalistaResponsable.getCorreo();
		
		String idLote = id ;
		
		if(loteActual.getContrario() != 0 ) {
			
			String Notificacion2 = "Este lote presenta saldo contrario en algunas cuentas está seguro de autorizar";
			
			redirectAttributes.addFlashAttribute("notificacion2", Notificacion2);
			
			redirectAttributes.addFlashAttribute("idLote", idLote);
			
			return "redirect:/bandejaEntradaLote";
			
		}

		String Estado = "Autorizado";

		loteActual.setEstadoLote(Estado);
		
		loteActual.setComentario("Ninguno");

		loteUsuarioservice.guardarLote(loteActual);

		Notificacion = "El lote " + idLote + " fue aprobado y enviado para cargue en AS-400";

		redirectAttributes.addFlashAttribute("notificacion", Notificacion);
		
		emailSender.sendEmail("Estado de Lote SIC", "Test Message : El estado de su lote  : " + idLote + " ha cambiado a Autorizado", "administracionlotes@pichincha.com.co" , CorreoAnalista /* PONER del nuevo usuario */ , true );

		emailSender.sendEmail("Estado de Lote SIC", "Test Message : Hay lotes para generar plano ", "administracionlotes@pichincha.com.co" , correoCentralizador /* PONER del nuevo usuario */ , true );

		
		String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ El lote " + id + " fue aprobado]";
		
		registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);
		
		/* enviamos mensaje para analista */
		
		mensaje.setComentario("el lote " + id + " fue aprobado por el autorizador " + loteActual.getAutorizador() + "  y ha sido enviado para cargue en AS400");
		mensaje.setDestinatario(NombreAnalista);
		mensaje.setEstado("No Leido");
		mensaje.setFecha(date);
		
		/* enviamos mensaje para centralizador */
		
		mensaje2.setComentario("el lote " + id + " fue aprobado por el autorizador " + loteActual.getAutorizador() + "  y ha sido enviado para cargue en AS400");
		mensaje2.setDestinatario(nombreCentralizador);
		mensaje.setEstado("No Leido");
		mensaje2.setFecha(date);
		
		mensajeServi.enviarMensaje(mensaje);

		mensajeServi.enviarMensaje(mensaje2);
		
		return "redirect:/bandejaEntradaLote";
	}
	
	
	@RequestMapping(value = "/AbrirLote/{id}")
	public void download(@PathVariable("id") String id ,  HttpServletResponse response , Principal principal) throws Exception {
		// Dirección del archivo, el entorno real se almacena en la base de datos
		
		String NombreUsuarioLogeado = principal.getName();

		DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");
		
		String date = dateFormat.format(Calendar.getInstance().getTime());

		 // hora en la que se carga el lote para validar
		
		LoteUsuario loteDescargar = loteUsuarioservice.buscarLoteId(id);
		
		String rutaLote = loteDescargar.getRutaArchivo();
		
		String NombreArchivo = loteDescargar.getNombreArchivo();
		
		File file = new File(rutaLote);
		
		String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [Desacargo el archivo " + NombreArchivo + "]";

		registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);
		
		//File file = new File("..\\Planos\\siig11ajc.txt");
		 // Llevando objeto de entrada
		FileInputStream fis = new FileInputStream(file);
		 // Establecer el formato relevante
		response.setContentType("application/force-download");
		 // Establecer el nombre y el encabezado del archivo descargado
		response.addHeader("Content-disposition", "attachment;fileName=" + NombreArchivo);
		 // Crear objeto de salida
		OutputStream os = response.getOutputStream();
		 // operación normal
		byte[] buf = new byte[1024];
		int len = 0;
		while((len = fis.read(buf)) != -1) {
			os.write(buf, 0, len);
		}
		fis.close();
	}

	/*@RequestMapping(value = "/AbrirLote/{id}")  // ESTE METOO SE ABRE CON LA CONSOLA DE COMANDOS PERO DENTRO DEL MISMO COMPUTADOR
	public String AbrirLote(@PathVariable("id") String id) {

		LoteUsuario loteAbrir = loteUsuarioservice.buscarLoteId(id);

		String NombreLote = loteAbrir.getRutaArchivo();

		System.out.println("el archivo para descargar es :" + id);

		// String file = new String("..\\lote2023-04-14\\lote100120230414.xlsx");

		String file = new String(NombreLote);

		System.out.println(file.toString());

		try {

			Runtime run = Runtime.getRuntime();

			run.exec("cmd /c start " + file);

			// run.exec("cmd /c start
			// C:\\\"Users\\andnei662\\eclipse-workspace\\insumos\"\\Balance.xls");

			run.freeMemory();

		} catch (IOException e) {
			e.printStackTrace();

			System.out.println(e);
		}

		return "redirect:/bandejaEntradaLote";
	}*/

	@RequestMapping(value = "/verDesallesLote/MostrarResumen/{id}", method = RequestMethod.GET)
	public String MostrarDetallesLote(@PathVariable("id") String id, HttpServletRequest request, ModelMap mp , Principal principal) {

		
		String NombreUsuarioLogeado = principal.getName();
		
		DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");
		
		String date = dateFormat.format(Calendar.getInstance().getTime());

		 // hora en la que se carga el lote para validar

		Usuarios usuario = usuarioService.buscarUsuario(NombreUsuarioLogeado);
		
		String Rol = usuario.getRol().get(0).getName();
		
		mp.put("Area", usuario.getArea().getArea());
		
		mp.put("rol", Rol);
		
		List<ResumenLote> ListaCuentaLote = resumenLoteService.mostrarResumen(id);

		//System.out.println("el tamaño de cuentas es de : " + ListaCuentaLote.size());

		mp.put("resumen", ListaCuentaLote);
		
		String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ consulto los detalles del lote " + id + "]";

		registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);

		return "CargarLoteResume";
	}
	
	@RequestMapping("/buscarLoteRechazado")
	public String BuscarLoteRechazadoId(@RequestParam(value = "dato", required = false) String dato, ModelMap mp , Principal principal) {
			
		String NombreUsuarioLogeado = principal.getName();
		
		DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");
		
		String date = dateFormat.format(Calendar.getInstance().getTime()); // hora en la que se carga el lote para validar
		
		Usuarios usuario = usuarioService.buscarUsuario(NombreUsuarioLogeado);
		
		String Rol = usuario.getRol().get(0).getName();
		
		mp.put("Area", usuario.getArea().getArea());
		
		mp.put("rol", Rol);

		String NombreAutorizador = usuario.getNombre();
		
		//System.out.println("El lote a bsucar es :" + dato);
		
		String EstadoLote = "Rechazado";

		List<LoteUsuario> lotesRechazados = loteUsuarioservice.BuscarLotesNoAutorizados(EstadoLote, NombreAutorizador);
		
		if (lotesRechazados.isEmpty()) {
			
			Notificacion = "El lote " + dato + "no se encuentra Rechazado";
		
			String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ Buscar el lote  rechazado" + dato + "]";
			
			registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);
			
			return "lotesRechazados";
		}

		LoteUsuario loteRechazadoId = new LoteUsuario();
		
		for(int i = 0 ; i < lotesRechazados.size() ; i ++) {
			
			String id = lotesRechazados.get(i).getIdLote();
			
			//System.out.println(id);
			
			if (id.contains(dato)) {
				
				loteRechazadoId = lotesRechazados.get(i);
			}
			
			//System.err.println(loteRechazadoId.getIdLote());
			
			
		}
		
		String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ Buscar el lote  rechazado" + dato + "]";
		
		registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);
		
		mp.put("loteRechazadoId", loteRechazadoId);
		
		return "AutorizarLoteBuscarIdRechazados";
	}
	
	
	@RequestMapping("/buscarLoteAprobado")
	public String BuscarLoteAprobadoId(@RequestParam(value = "dato", required = false) String dato, ModelMap mp , Principal principal) {
		
		//String Autorizador = principal.getName();
		
		String NombreUsuarioLogeado = principal.getName();
		
		DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");
		
		String date = dateFormat.format(Calendar.getInstance().getTime()); // hora en la que se carga el lote para validar
				
		Usuarios usuario = usuarioService.buscarUsuario(NombreUsuarioLogeado);
		
		String Rol = usuario.getRol().get(0).getName();
		
		mp.put("Area", usuario.getArea().getArea());
		
		mp.put("rol", Rol);

		String NombreAutorizador = usuario.getNombre();
		
		//System.out.println("El lote a bsucar es :" + dato);
		
		String EstadoLote = "Autorizado";

		List<LoteUsuario> LotesAprobados = loteUsuarioservice.BuscarLotesNoAutorizados(EstadoLote, NombreAutorizador);
		
		if (LotesAprobados.isEmpty()) {
			
			Notificacion = "El lote " + dato + "no se encuentra aprobado";
			
			String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [Busqueda lote aprobado " + dato + "]";

			registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);
			
			return "AutorizarLoteAprobados";
		}

		LoteUsuario loteAprobadoId = new LoteUsuario();
		
		for(int i = 0 ; i < LotesAprobados.size() ; i ++) {
			
			String id = LotesAprobados.get(i).getIdLote();
			
			//System.out.println(id);
			
			if (id.contains(dato)) {
				
				loteAprobadoId = LotesAprobados.get(i);
			}
			
			//System.err.println(loteAprobadoId.getIdLote());
			
			
		}
		
		String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [Busqueda lote aprobado " + dato + "]";

		registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);
		
		mp.put("loteAprobadoId", loteAprobadoId);
		
		return "AutorizarLoteBuscarIdAprobados";
	}
	
	@RequestMapping(value = "/continuarFlujoAuto/{id}", method = RequestMethod.GET)
	public String ConfirmarLoteAuto (@PathVariable("id") String id , RedirectAttributes redirectAttributes , ModelMap mp ) {
		

		Mensaje mensaje = new Mensaje();
		
		Mensaje mensaje2 = new Mensaje();
		
		System.out.println("el lote aprobasdo es " + id);
		
		Usuarios Centralizador = usuarioService.listaUsuariosPorIdRol(7);
		
		String correoCentralizador = Centralizador.getCorreo();
		
		LoteUsuario loteActual = loteUsuarioservice.buscarLoteId(id);
		
		String Estado = "Autorizado";

		loteActual.setEstadoLote(Estado);
		
		loteActual.setComentario("Ninguno");
		
		loteUsuarioservice.guardarLote(loteActual);
		
		String NombreAnalista = loteActual.getAnalista();
		
		Usuarios AnalistaResponsable = usuarioService.buscarPorNombre(NombreAnalista);
		
		String CorreoAnalista = AnalistaResponsable.getCorreo();
		
		emailSender.sendEmail("Estado de Lote SIC", "Test Message : El estado de su lote  : " + id + " ha cambiado a Autorizado", "administracionlotes@pichincha.com.co" , CorreoAnalista /* PONER del nuevo usuario */ , true );

		emailSender.sendEmail("Estado de Lote SIC", "Test Message : Hay lotes para generar plano ", "administracionlotes@pichincha.com.co" , correoCentralizador /* PONER del nuevo usuario */ , true );

		


/* enviamos mensaje para analista */
		
		mensaje.setComentario("el lote " + id + " fue aprobado por el autorizador " + loteActual.getAutorizador() + "  y ha sido enviado para cargue en AS400");
		mensaje.setDestinatario(loteActual.getAnalista());
		mensaje.setFecha(date.toString());
		mensaje.setEstado("No Leido");
		
		/* enviamos mensaje para centralizador */
		
		mensaje2.setComentario("El lote " + id + " fue aprobado por el autorizador " + loteActual.getAutorizador() + " Y ha sido enviado para cargue en AS400");
		mensaje2.setDestinatario(loteActual.getCentralizador());
		mensaje2.setFecha(date.toString());
		mensaje2.setEstado("No Leido");
		
		mensajeServi.enviarMensaje(mensaje);

		mensajeServi.enviarMensaje(mensaje2);
		
		
		
		redirectAttributes.addFlashAttribute("notificacion", "El lote " + id + " fue aprobado y enviado para cargue en AS-400");
		
		return "redirect:/bandejaEntradaLote";
	}
	
	@RequestMapping(value = "/DenegarFlujoAuto/{id}", method = RequestMethod.GET)
	public String negarLoteAuto (@PathVariable("id") String id , RedirectAttributes redirectAttributes) {
		
		Mensaje mensaje = new Mensaje();
		
		Notificacion =  "Has rechazado el lote " + id + " por saldos contrarios";
		
		LoteUsuario loteActual = loteUsuarioservice.buscarLoteId(id);
		
		System.out.println("el lote rechazado es : " + id);
		
		redirectAttributes.addFlashAttribute("notificacion" , Notificacion );
		
		String Estado = "Rechazado";

		loteActual.setEstadoLote(Estado);
		
		loteActual.setComentario("Saldos contrarios");
		
		loteUsuarioservice.guardarLote(loteActual);
		
		String NombreAnalista = loteActual.getAnalista();
		
		Usuarios AnalistaResponsable = usuarioService.buscarPorNombre(NombreAnalista);
		
		String CorreoAnalista = AnalistaResponsable.getCorreo();
		
		emailSender.sendEmail("Estado de Lote SIC", "Test Message : El estado de su lote  : " + id + " ha cambiado a Rechazado por motivo de saldos contrarios", "administracionlotes@pichincha.com.co" , CorreoAnalista /* PONER del nuevo usuario */ , true );

		mensaje.setComentario("el lote " + id + " fue rechazado por el autorizador " + loteActual.getAutorizador() + "  y ha sido enviado para cargue en AS400" + loteActual.getAutorizador());
		mensaje.setDestinatario(loteActual.getAnalista());
		mensaje.setFecha(date.toString());
		mensaje.setEstado("No Leido");
		
		redirectAttributes.addFlashAttribute("notificacion", "Has rechazado el lote " + id );
		
		return "redirect:/bandejaEntradaLote";
	}
	
	
	

}
