package com.example.demo.controller;

import java.security.Principal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.example.demo.RegistrosLog;
import com.example.demo.Entity.LoteUsuario;
import com.example.demo.Entity.Usuarios;
import com.example.demo.Paginador.Paginador;
import com.example.demo.service.LogService;
import com.example.demo.service.LoteUsuerioService;
import com.example.demo.service.UsuariosService;

@Controller
public class ControllerConsulta {

	@Autowired
	private LoteUsuerioService loteservice;
	
	@Autowired
	private UsuariosService usuarioservice;
	
	private String Analista = "";
	
	private String Fecha1 = "";
	
	private String Fecha2 = "";
	
	private String Autorizador = "";

	
	@GetMapping("/verTrazabilidadlotes")
	public String verTrazabilidadlotes (@RequestParam(name = "page" , defaultValue = "0")  int page , ModelMap mp , Principal principal , Model model) {
		
		Analista = "";
		
		Fecha1 = "";
		
		Fecha2 = "";
		
		Autorizador = "";
		
		String notificacion = (String) model.asMap().get("notificacion");
		
		Pageable pageRequest = PageRequest.of(page, 5);
		
		Page <LoteUsuario> lotes = loteservice.listaLotes(pageRequest);
		
		Paginador <LoteUsuario> pageRender = new Paginador<>("/verTrazabilidadlotes" , lotes);
		
		String NombreUsuarioLogeado = principal.getName();
		
		Usuarios usuario = usuarioservice.buscarUsuario(NombreUsuarioLogeado);
		
		String Rol = usuario.getRol().get(0).getName();
		
		mp.put("Area", usuario.getArea().getArea());
		
		mp.put("rol", Rol);
		
		mp.put("lote", lotes);
		
		mp.put("page", pageRender);
		
		mp.put("notificacion", notificacion);
		
		
		return "Consulta";
		
	}
	
	@RequestMapping(value = "/GuardarDatosBusquedaTrazabilidad" , method = RequestMethod.GET)
	public String guardarDatosBusquedaTrazabilidad (@RequestParam(value = "analista", required = false) String analista , @RequestParam(value = "autorizador", required = false) String autorizador , @RequestParam(value = "fecha1", required = false) String fecha1 , @RequestParam(value = "fecha2", required = false) String fecha2, ModelMap mp , Principal principal , RedirectAttributes redirectAttributes) {
		

		Analista = analista;
		
		Autorizador = autorizador;
		
		Fecha1 = fecha1;
		
		Fecha2 = fecha2;

		
		return "redirect:/filtroInformeTrazabilidad";
		
	}
	
	
	@RequestMapping(value = "/filtroInformeTrazabilidad" , method = RequestMethod.GET)
	public String FiltrarInformacionTrazabilidad( @RequestParam(name = "page" , defaultValue = "0")  int page , ModelMap mp , Principal principal , Model model , RedirectAttributes redirectAttributes ) {
		

		/*System.out.println("el anlaista es : " + Analista);
		System.out.println("en autorizador es : " + Autorizador);
		System.out.println("la fecha 1 es : " + Fecha1);
		System.out.println("la fecha 2 es :" + Fecha2);*/

		
		Pageable pageRequest = PageRequest.of(page, 5);
		
		Page <LoteUsuario> lotes = loteservice.loteusuarioFitral(Analista, Autorizador, Fecha1, Fecha2, pageRequest);
		
		if (lotes.isEmpty()) {
			
			redirectAttributes.addFlashAttribute("notificacion", "No existen lotes con esas características, por favor revisar los datos ingresados en los filtros");
			
			return "redirect:/verTrazabilidadlotes";
		}
		
		Paginador <LoteUsuario> pageRender = new Paginador<>("/filtroInformeTrazabilidad" , lotes);
		
		String NombreUsuarioLogeado = principal.getName();
		
		Usuarios usuario = usuarioservice.buscarUsuario(NombreUsuarioLogeado);
		
		String Rol = usuario.getRol().get(0).getName();
		
		mp.put("Area", usuario.getArea().getArea());
		
		mp.put("analista", Analista);
		mp.put("Autorizador", Autorizador);
		mp.put("fecha1", Fecha1);
		mp.put("fecha2", Fecha2);
		mp.put("rol", Rol);
		mp.put("lote", lotes);
		mp.put("page", pageRender);
	
		return "ConsultaFiltro";
	}



}
