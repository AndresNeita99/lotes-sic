package com.example.demo.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.Principal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.poi.openxml4j.util.ZipSecureFile;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.example.demo.RegistrosLog;
import com.example.demo.Entity.Moneda;
import com.example.demo.Entity.SolicitudesCrearUsuario;
import com.example.demo.Entity.Usuarios;
import com.example.demo.Entity.ValorCuentasPYG;
import com.example.demo.Paginador.Paginador;
import com.example.demo.service.CargarArchivoServer;
import com.example.demo.service.MonedaService;
import com.example.demo.service.SolicitudesCrearUsuarioService;
import com.example.demo.service.UsuariosService;

import jakarta.servlet.http.HttpServletRequest;

@Controller
public class ControllerMoneda {
	
	@Autowired
	private UsuariosService usuarioService;
	
	@Autowired
	private SolicitudesCrearUsuarioService SolicitudService;
	
	@Autowired
	private MonedaService serviceMonedaService;
	
	@Autowired
	private CargarArchivoServer cargarArchivo;
	
	String notificacion = "";
	
	private RegistrosLog registrolog = new RegistrosLog();
	
	// ---------------------------- controller Modena // -------------------------------- //
	
	
	
		@RequestMapping("/CrearMoneda")
		public String CrearMoneda (ModelMap mp , Principal principal , Model model) {

			String NombreUsuarioLogeado = principal.getName();
			
			DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");
			
			String date = dateFormat.format(Calendar.getInstance().getTime());
			
			Usuarios usuario = usuarioService.buscarUsuario(NombreUsuarioLogeado);
			
			String notificacion = (String) model.asMap().get("notificacion");
			
			String Rol = usuario.getRol().get(0).getName();
			
			mp.put("Area", usuario.getArea().getArea());
			
			mp.put("rol", Rol);
			
			mp.put("notificacion", notificacion);
			
			mp.put("moneda", new Moneda());
			
			String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ Accedio al formulario crear moneda ]";

			registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);
			
			return "ParametroMonedaCrear";
		}
		
		@PostMapping("/guardarMoneda")
		public String GuardarMoneda (ModelMap mp , Moneda moneda , RedirectAttributes redirectAttributes , Principal principal) {
	
			//System.out.println("el valor de la moneda a guardar es : " + moneda.getMoneda());
			
			moneda.setEstado("Activo");
			
			serviceMonedaService.GuardarMoneda(moneda);
			
			String NombreUsuarioLogeado = principal.getName();

			DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

			String date = dateFormat.format(Calendar.getInstance().getTime());

			String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ Registro el valor de la moneda en el sistema ]";

			registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);
			
			return "redirect:/listaMonedas";
		}
		
		

		@RequestMapping(value = "/listaMonedas")
		public String ListaModenas(@RequestParam(name = "page" , defaultValue = "0")  int page ,ModelMap mp , Principal principal , Model model) {

			String NombreUsuarioLogeado = principal.getName();
			
			Usuarios usuario = usuarioService.buscarUsuario(NombreUsuarioLogeado);
			
			String Rol = usuario.getRol().get(0).getName();
			
			mp.put("Area", usuario.getArea().getArea());
			
			mp.put("rol", Rol);
			
			
			Pageable pageRequest = PageRequest.of(page, 5);
			
			Page <Moneda> moneda = serviceMonedaService.listaMonedasPagin(pageRequest);

			Paginador <Moneda> pageRender = new Paginador<>("/listaMonedas" , moneda);
			
			List<SolicitudesCrearUsuario> cantidadSolicitudes = SolicitudService.cantidadSolicitudes("Enviada");
			
			List<Usuarios> ListaUsuarioCambioCon = usuarioService.listaUsuariosCambio("Olvidada");
			
			int CatidadSolicitudes = cantidadSolicitudes.size();
			
			mp.put("CatidadSolicitudes", CatidadSolicitudes);
			
			int cambiodecontra = ListaUsuarioCambioCon.size();
			
			mp.put("CatidadSolicitudesCambio", cambiodecontra);

			
			long Totalmoneda = moneda.getTotalElements();
			
			String notificacion = (String) model.asMap().get("notificacion");
			
			mp.put("moneda", moneda);
			
			mp.put("totalAreas", Totalmoneda);
			
			mp.put("page", pageRender);
			
			mp.put("notificacion",notificacion);
			
			DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

			String date = dateFormat.format(Calendar.getInstance().getTime());

			String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ Accedio a lista de monedas ]";

			registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);

			return "ParametroMonedaLista";
		}

		
		@RequestMapping(value = "/EditarMoneda/{id}", method = RequestMethod.GET)
		public String EditarMoneda(@PathVariable("id") long id, HttpServletRequest request, ModelMap mp , Moneda moneda , Principal principal) {
			
			Moneda monedaEditar = serviceMonedaService.findOne(id);
			
			String NombreUsuarioLogeado = principal.getName();
			
			Usuarios usuario = usuarioService.buscarUsuario(NombreUsuarioLogeado);
			
			String Rol = usuario.getRol().get(0).getName();
			
			mp.put("Area", usuario.getArea().getArea());
			
			mp.put("rol", Rol);
			
			mp.put("moneda", monedaEditar);
			
			DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

			String date = dateFormat.format(Calendar.getInstance().getTime());

			String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ edito el valor de la moneda con el id " + id + " ]";

			registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);
			
			return "ParametroMonedaEditar";
		}
		
		@PostMapping("/actualizarMoneda")
		public String ActualizarMoneda(ModelMap mp , Moneda moneda , Principal principal) {

			String NombreUsuarioLogeado = principal.getName();
			
			//System.out.println("nuevo fuente");
			//System.out.println("fuente " + fuentes.getFuente());
			//System.out.println("Nombre fuente " + fuentes.getFuente());
			//System.out.println("Codifo fuente:  " + fuentes.getId());
			
			String NuevoNombre = moneda.getNombreMoneda();
			int NuevaMoneda= moneda.getMoneda();
			
			if(NuevoNombre.isEmpty() == true) {
				
				//System.out.println("El nombre no puede estar vacion");
				
				return "ParametroCentroCostosEditar";
				
			}else {
				
				moneda.setMoneda(NuevaMoneda);
				moneda.setNombreMoneda(NuevoNombre);
				moneda.setEstado("Activo");

				serviceMonedaService.GuardarMoneda(moneda);
				
				//System.out.println("dato actualizado exitosamente");
				
				DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

				String date = dateFormat.format(Calendar.getInstance().getTime());

				String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ Actualizo el valoe de la moneda corretamente ]";

				registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);
				
			}

			
			return "redirect:/listaMonedas";
		} 
		
		

		@RequestMapping(value = "/DesabilitarMoneda/{id}", method = RequestMethod.GET)
		public String DesabilitarMoneda(@PathVariable("id") long id, HttpServletRequest request, ModelMap mp , Principal principal , RedirectAttributes redirectAttributes) {

			Moneda modena = null;
			
			String NombreUsuarioLogeado = principal.getName();

			modena = serviceMonedaService.findOne(id);

			modena.setEstado("Inactivo");

			serviceMonedaService.GuardarMoneda(modena);
			
			DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

			String date = dateFormat.format(Calendar.getInstance().getTime());

			String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ Desabilito la moneda con el id " + id + " ]";

			registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);
			
			redirectAttributes.addFlashAttribute("notificacion", "El parámetro moneda ha sido deshabilitado");

			return "redirect:/listaMonedas";
		}

		@RequestMapping(value = "/HabilitarMoneda/{id}", method = RequestMethod.GET)
		public String HabilitarMoneda(@PathVariable("id") long id, HttpServletRequest request, ModelMap mp , RedirectAttributes redirectAttributes) {

			Moneda modena = null;

			modena = serviceMonedaService.findOne(id);

			modena.setEstado("Activo");

			serviceMonedaService.GuardarMoneda(modena);
			
			redirectAttributes.addFlashAttribute("notificacion", "El parámetro moneda ha sido habilitado");

			return "redirect:/listaMonedas";
		}
		
		@RequestMapping(value = "/EliminarMoneda/{id}", method = RequestMethod.GET)
		public String EliminarMoneda(@PathVariable("id") long id, HttpServletRequest request, ModelMap mp , Principal principal , RedirectAttributes redirectAttributes) {

			serviceMonedaService.EliminarMoneda(id);
			
			String NombreUsuarioLogeado = principal.getName();
			
			DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

			String date = dateFormat.format(Calendar.getInstance().getTime());

			String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ Desabilito la moneda con el id " + id + " ]";

			registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);
			
			redirectAttributes.addFlashAttribute("notificacion", "El parámetro moneda ha sido eliminado");

			return "redirect:/listaMonedas";
		}
		
		@PostMapping("/ValidarExistenciaArchivoMoneda")
		public String downloadFile(@RequestParam(value = "dato", required = false) String dato ,@RequestParam("file") MultipartFile file , RedirectAttributes redirectAttributes) throws Exception {
			
			DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");
			
			String date = dateFormat.format(Calendar.getInstance().getTime());
			
			String NombreArchivo = file.getOriginalFilename();
			
			//System.out.println("el archivo de area es : " + NombreArchivo);
			
			//String NombreArchivo =  dato.replaceAll("[xlsx]", "");

			String ruta = "..\\Monedas";

			// esta carpeta se creara cada vez que se descargue y se llene un plano
			File directorio = new File(ruta);
			

			// ---------------- validamos que en la carpeta areas no exista el archivo si existe se elimina para la llegada del nuevo archivo areas // -----------//
			


			if (directorio.exists()) {

				File directory = new File(ruta);
					
				FileUtils.cleanDirectory(directory);
					
				cargarArchivo.save(file,ruta);
					
				redirectAttributes.addFlashAttribute("NombreArchivo",NombreArchivo);
				
				String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + "sistema" + "] " + "Tarea" + " [ Creo la carpeta Areas dentro del servidor ]";
				
				registrolog.guardarRegistro("sistema", tarea);

				return "redirect:/ActualizarMonedaMasiva";

			}
			
			else {
				
				 cargarArchivo.init(ruta);
				 
				 cargarArchivo.save(file,ruta);
				 
				 redirectAttributes.addFlashAttribute("NombreArchivo",NombreArchivo);
				 
				 String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + "sistema" + "] " + "Tarea" + " [ Creo la carpeta Areas dentro del servidor ]";
					
				 registrolog.guardarRegistro("sistema", tarea);
				 
				 return "redirect:/cargarMonedaMasiva";
				
			}
			

		}
		

		@GetMapping("/cargarMonedaMasiva")
		public String CargarFuenteMasiva(Model model , RedirectAttributes redirectAttributes , Principal principal) {

			//String NombreArchivo = "Fuentes";

			String NombreArchivoFuentes = (String) model.asMap().get("NombreArchivo");
			
			String NombreUsuarioLogeado = principal.getName();
			
			//String ruta = "..\\insumos\\" + NombreArchivoFuentes;
			
			//C:\Users\andnei662\eclipse-workspace\insumos\Fuentes.xlsx
			
			String ruta = "..\\Monedas\\" + NombreArchivoFuentes;
			
			//System.out.println(ruta);

			FileInputStream file;

			try {

				file = new FileInputStream(new File(ruta));
				Workbook workbook = new XSSFWorkbook(file);

				org.apache.poi.ss.usermodel.Sheet sheet = workbook.getSheetAt(0);
				ZipSecureFile.setMinInflateRatio(0);

				for (Row row : sheet) {

					int contador = 0; // contador de columna

					//System.out.println("Numero de fila: " + row.getRowNum());
					//System.out.println("");
					

					if (row.getRowNum() == 0) {

						//System.out.println("saltar fila ya que es la cabezera del archivo ");
						//System.out.println("");

					} else {
						
						Moneda moneda = new Moneda();
						

						for (Cell cell : row) {
							
							contador = contador + 1;

							switch (cell.getCellType()) {
							
							case STRING:

								if (contador == 1) { // COLUMNA CODIGO FUENTE STRING

									String NombreMoneda = cell.getRichStringCellValue().getString();
									
									//System.out.println("El nombre de la moneda es " + NombreMoneda);
									
									moneda.setNombreMoneda(NombreMoneda);
								

								}

								break;

							case NUMERIC:


								if (contador == 2) { // COLUMNA CODIGO FUENTE NUMERICA

									double ValorMoneda = cell.getNumericCellValue();

									int inum=(int)ValorMoneda;;
									
									moneda.setMoneda(inum);
			
								}
			
							default:
								break;

							}

						}
						
						if (contador != 2) {
							
							notificacion = "El archivo " + NombreArchivoFuentes + " no cumple con la estructura";
							
							redirectAttributes.addFlashAttribute("notificacion",notificacion);
							
							return "redirect:/listaMonedas";
							
						}
						
						moneda.setEstado("Activo");

						serviceMonedaService.GuardarMoneda(moneda);
					}
					
					
					

				}
				
				workbook.close();
				//System.out.println("Guardado exitosamente");

			} catch (FileNotFoundException e) {
				
				DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

				String date = dateFormat.format(Calendar.getInstance().getTime());
						
				String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ No se pudo leer la ruta " + ruta + " ]";
										
				registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);

				//System.out.print("No se a cargado ninguna lote para validar por favor seleccionar un lote");

			} catch (IOException e) {
				
				DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

				String date = dateFormat.format(Calendar.getInstance().getTime());
						
				String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ No se pudo leer la ruta " + ruta + " ]";
										
				registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);

				e.printStackTrace();
			}
			
			DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

			String date = dateFormat.format(Calendar.getInstance().getTime());
					
			String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ el archivo de moneda con la ruta  " + ruta + " fue cargado correctamente ]";
									
			registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);
			
			redirectAttributes.addFlashAttribute("notificacion", "El parámetro moneda ha sido cargado correctamente");

			return "redirect:/listaMonedas";
		}
		
		
		@GetMapping("/ActualizarMonedaMasiva")
		public String ActualizarCargaMasivaCentroCostos (Model model , RedirectAttributes redirectAttributes , Principal principal) {
		
		String NombreUsuarioLogeado = principal.getName();

		DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'a las' HH:mm:ss");

		String date = dateFormat.format(Calendar.getInstance().getTime());
			
		String NombreArchivo = (String) model.asMap().get("NombreArchivo");
			
		serviceMonedaService.ActualizarModenaCargaMasiva();
		
		redirectAttributes.addFlashAttribute("NombreArchivo",NombreArchivo);
		
		
		String tarea =  "Fecha Y hora [" + date + "]  Usuario [" + NombreUsuarioLogeado + "] " + "Tarea" + " [ Actualizo Centro de costos]";
						
		registrolog.guardarRegistro(NombreUsuarioLogeado, tarea);
			
			return "redirect:/cargarMonedaMasiva";
		}
		
		@GetMapping("/activarParametroMoneda")
		public String activarParametroMoneda(ModelMap mp , RedirectAttributes redirectAttributes) {
			
			
			List<Moneda> Monedas = serviceMonedaService.listaMonedas();
			
			for (int i = 0; i < Monedas.size(); i++) {
				
				long id = Monedas.get(i).getId();
				
				Moneda MonedaActual = serviceMonedaService.findOne(id);
				
				MonedaActual.setEstado("Activo");
				
				serviceMonedaService.GuardarMoneda(MonedaActual);
				
			}
			
			redirectAttributes.addFlashAttribute("notificacion", "Todos los parámetros han sido activados");
			
			return "redirect:/listaMonedas";
			
		}
		
		
		@GetMapping("/DesactivarParametroMoneda")
		public String DesactivarParametroMoneda(ModelMap mp , RedirectAttributes redirectAttributes) {
			
			List<Moneda> Monedas = serviceMonedaService.listaMonedas();
			
			for (int i = 0; i < Monedas.size(); i++) {
				
				long id = Monedas.get(i).getId();
				
				Moneda MonedaActual = serviceMonedaService.findOne(id);
				
				MonedaActual.setEstado("desactivado");
				
				serviceMonedaService.GuardarMoneda(MonedaActual);
				
			}
			
			redirectAttributes.addFlashAttribute("notificacion", "Todos los parámetros se han sido desactivados, este parámetro no se tomará en cuenta al momento de validar el lote");
			
			return "redirect:/listaMonedas";
			
		
		}
		
		

}
