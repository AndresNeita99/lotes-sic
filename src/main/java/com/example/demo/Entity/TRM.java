package com.example.demo.Entity;

import java.io.Serializable;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;

@Entity(name = "trm")
public class TRM implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1396488049526363035L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id ;
	
	private String piso;
	
	private double valorpiso ;
	
	private String techo;
	
	private double valortecho;
	
	private String estado;

	public String getPiso() {
		return piso;
	}

	public void setPiso(String piso) {
		this.piso = piso;
	}

	public double getValorpiso() {
		return valorpiso;
	}

	public void setValorpiso(double valorpiso) {
		this.valorpiso = valorpiso;
	}

	public String getTecho() {
		return techo;
	}

	public void setTecho(String techo) {
		this.techo = techo;
	}

	public double getValortecho() {
		return valortecho;
	}

	public void setValortecho(double valortecho) {
		this.valortecho = valortecho;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}
	
	
	
}
