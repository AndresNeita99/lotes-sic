package com.example.demo.Entity;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name = "fechabalance")
public class FechaBalance {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	private String FechaBalance;

	public String getFechaBalance() {
		return FechaBalance;
	}

	public void setFechaBalance(String fechaBalance) {
		FechaBalance = fechaBalance;
	}
	
	
	
	

}
