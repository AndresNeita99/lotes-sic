package com.example.demo.Entity;

import java.io.Serializable;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Table(name = "Areas")
@Entity
public class Area implements Serializable{


	/**
	 * 
	 */
	private static final long serialVersionUID = -1461300665877361903L;

	/**
	 * 
	 */
	

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long Idarea;

	private int codVice;

	private String presidencia;

	private int codDirec;

	private String Direccion; // visepresidencia

	private int codCoor;

	private String Area;
	
	private String Estado;
	
	private int CantidadUsuarios;
	
	private String AutorizadorNivel1;
	
	private String AutorizadorNivel2;
	

	public Long getIdarea() {
		return Idarea;
	}

	public void setIdarea(Long idarea) {
		Idarea = idarea;
	}

	public int getCodVice() {
		return codVice;
	}

	public void setCodVice(int codVice) {
		this.codVice = codVice;
	}

	

	public String getPresidencia() {
		return presidencia;
	}

	public void setPresidencia(String presidencia) {
		this.presidencia = presidencia;
	}

	public int getCodDirec() {
		return codDirec;
	}

	public void setCodDirec(int codDirec) {
		this.codDirec = codDirec;
	}

	public String getDireccion() {
		return Direccion;
	}

	public void setDireccion(String direccion) {
		Direccion = direccion;
	}

	public int getCodCoor() {
		return codCoor;
	}

	public void setCodCoor(int codCoor) {
		this.codCoor = codCoor;
	}

	public String getArea() {
		return Area;
	}

	public void setArea(String area) {
		Area = area;
	}

	public String getEstado() {
		return Estado;
	}

	public void setEstado(String estado) {
		Estado = estado;
	}

	public int getCantidadUsuarios() {
		return CantidadUsuarios;
	}

	public void setCantidadUsuarios(int cantidadUsuarios) {
		CantidadUsuarios = cantidadUsuarios;
	}

	public String getAutorizadorNivel1() {
		return AutorizadorNivel1;
	}

	public void setAutorizadorNivel1(String autorizadorNivel1) {
		AutorizadorNivel1 = autorizadorNivel1;
	}

	public String getAutorizadorNivel2() {
		return AutorizadorNivel2;
	}

	public void setAutorizadorNivel2(String autorizadorNivel2) {
		AutorizadorNivel2 = autorizadorNivel2;
	}



}
