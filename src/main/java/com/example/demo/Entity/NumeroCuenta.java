package com.example.demo.Entity;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Table(name = "cuentas")
@Entity
public class NumeroCuenta {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	private String NoCuenta;
	private String NombreCuenta;
	private String SignoCuenta;
	private String Estado;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNoCuenta() {
		return NoCuenta;
	}

	public void setNoCuenta(String noCuenta) {
		NoCuenta = noCuenta;
	}

	public String getEstado() {
		return Estado;
	}

	public void setEstado(String estado) {
		Estado = estado;
	}

	public String getNombreCuenta() {
		return NombreCuenta;
	}

	public void setNombreCuenta(String nombreCuenta) {
		NombreCuenta = nombreCuenta;
	}

	public String getSignoCuenta() {
		return SignoCuenta;
	}

	public void setSignoCuenta(String signoCuenta) {
		SignoCuenta = signoCuenta;
	}

}
