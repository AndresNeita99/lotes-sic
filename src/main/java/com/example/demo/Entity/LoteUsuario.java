package com.example.demo.Entity;

import java.io.Serializable;
import java.time.LocalDate;

import org.springframework.data.relational.core.mapping.Column;

import jakarta.persistence.CascadeType;

import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToOne;

@Entity(name = "lote")
public class LoteUsuario implements Serializable {

	private static final long serialVersionUID = 56331248345661605L;

	@Id
	private String IdLote;

	private int catidadaregistros;

	private double ValorDebito;

	private double ValorCredito;

	private String EstadoLote;

	private int NumeroErrores;

	private String TipoLote;

	private LocalDate FechaProceso;

	private String NombreArchivo;
	
	private double pesos;
	
	private String ValorDebitoNumerico;
	
	private String ValorCreditoNumerico;
	
	private String ValorPesosNumerico;
	
	private String autorizador;
	
	private String autorizadorArea;
	
	private String Comentario;
	
	private String HoraCargue;
	
	private String analista;
	
	private String analistaArea;
	
	private String RutaArchivo;
	
	private String Centralizador;
	
	private String centralizadorArea;
	
	private int contrario;

	@OneToOne(cascade = CascadeType.REFRESH, fetch = FetchType.LAZY)
	private Usuarios usuario;
	
	@ManyToOne(fetch = FetchType.LAZY)
	private Plano plano;

	public String getIdLote() {
		return IdLote;
	}

	public void setIdLote(String idLote) {
		IdLote = idLote;
	}

	public String getAutorizador() {
		return autorizador;
	}

	public void setAutorizador(String autorizador) {
		this.autorizador = autorizador;
	}

	public String getAnalista() {
		return analista;
	}

	public void setAnalista(String analista) {
		this.analista = analista;
	}

	public int getCatidadaregistros() {
		return catidadaregistros;
	}

	public void setCatidadaregistros(int catidadaregistros) {
		this.catidadaregistros = catidadaregistros;
	}

	public double getValorDebito() {
		return ValorDebito;
	}

	public void setValorDebito(double valorDebito) {
		ValorDebito = valorDebito;
	}

	public double getValorCredito() {
		return ValorCredito;
	}

	public void setValorCredito(double valorCredito) {
		ValorCredito = valorCredito;
	}

	public Usuarios getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuarios usuario) {
		this.usuario = usuario;
	}

	public String getEstadoLote() {
		return EstadoLote;
	}

	public void setEstadoLote(String estadoLote) {
		EstadoLote = estadoLote;
	}

	public int getNumeroErrores() {
		return NumeroErrores;
	}

	public void setNumeroErrores(int numeroErrores) {
		NumeroErrores = numeroErrores;
	}

	public String getTipoLote() {
		return TipoLote;
	}

	public void setTipoLote(String tipoLote) {
		TipoLote = tipoLote;
	}

	public LocalDate getFechaProceso() {
		return FechaProceso;
	}

	public void setFechaProceso(LocalDate fechaProceso) {
		FechaProceso = fechaProceso;
	}

	public String getNombreArchivo() {
		return NombreArchivo;
	}

	public void setNombreArchivo(String nombreArchivo) {
		NombreArchivo = nombreArchivo;
	}

	public double getPesos() {
		return pesos;
	}

	public void setPesos(double pesos) {
		this.pesos = pesos;
	}

	public String getValorDebitoNumerico() {
		return ValorDebitoNumerico;
	}

	public void setValorDebitoNumerico(String valorDebitoNumerico) {
		ValorDebitoNumerico = valorDebitoNumerico;
	}

	public String getValorCreditoNumerico() {
		return ValorCreditoNumerico;
	}

	public void setValorCreditoNumerico(String valorCreditoNumerico) {
		ValorCreditoNumerico = valorCreditoNumerico;
	}

	public String getValorPesosNumerico() {
		return ValorPesosNumerico;
	}

	public void setValorPesosNumerico(String valorPesosNumerico) {
		ValorPesosNumerico = valorPesosNumerico;
	}

	public String getNombreAutorizador() {
		return autorizador;
	}

	public void setNombreAutorizador(String nombreAutorizador) {
		autorizador = nombreAutorizador;
	}

	public String getComentario() {
		return Comentario;
	}

	public void setComentario(String comentario) {
		Comentario = comentario;
	}



	public String getRutaArchivo() {
		return RutaArchivo;
	}

	public void setRutaArchivo(String rutaArchivo) {
		RutaArchivo = rutaArchivo;
	}

	public String getHoraCargue() {
		return HoraCargue;
	}

	public void setHoraCargue(String horaCargue) {
		HoraCargue = horaCargue;
	}

	public Plano getPlano() {
		return plano;
	}

	public void setPlano(Plano plano) {
		this.plano = plano;
	}

	public int getContrario() {
		return contrario;
	}

	public void setContrario(int contrario) {
		this.contrario = contrario;
	}

	public String getAutorizadorArea() {
		return autorizadorArea;
	}

	public void setAutorizadorArea(String autorizadorArea) {
		this.autorizadorArea = autorizadorArea;
	}

	public String getAnalistaArea() {
		return analistaArea;
	}

	public void setAnalistaArea(String analistaArea) {
		this.analistaArea = analistaArea;
	}

	public String getCentralizador() {
		return Centralizador;
	}

	public void setCentralizador(String centralizador) {
		Centralizador = centralizador;
	}

	public String getCentralizadorArea() {
		return centralizadorArea;
	}

	public void setCentralizadorArea(String centralizadorArea) {
		this.centralizadorArea = centralizadorArea;
	}

	
	
	
	
	
}
