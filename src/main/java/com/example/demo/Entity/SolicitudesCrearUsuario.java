package com.example.demo.Entity;

import java.io.Serializable;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;

@Entity (name = "solicitudes")
public class SolicitudesCrearUsuario implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -9044499487239400728L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	private String tipo;
	
	private String nombre;

	private String username;
	
	private String correo;
	
	private String EstadoUsuario;

	private String EstadoContra;

	private String CorreoAutorizador;
	
	private int documento; 
	
	private String Area;
	
	private long idArea;
	
	private String Rol;

	private String Estado;
	
	private String usuario;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getCorreo() {
		return correo;
	}
	public void setCorreo(String correo) {
		this.correo = correo;
	}
	public String getEstadoUsuario() {
		return EstadoUsuario;
	}
	public void setEstadoUsuario(String estadoUsuario) {
		EstadoUsuario = estadoUsuario;
	}
	public String getEstadoContra() {
		return EstadoContra;
	}
	public void setEstadoContra(String estadoContra) {
		EstadoContra = estadoContra;
	}
	public String getCorreoAutorizador() {
		return CorreoAutorizador;
	}
	public void setCorreoAutorizador(String correoAutorizador) {
		CorreoAutorizador = correoAutorizador;
	}
	public int getDocumento() {
		return documento;
	}
	public void setDocumento(int documento) {
		this.documento = documento;
	}
	public String getArea() {
		return Area;
	}
	public void setArea(String area) {
		Area = area;
	}
	public long getIdArea() {
		return idArea;
	}
	public void setIdArea(long idArea) {
		this.idArea = idArea;
	}
	public String getEstado() {
		return Estado;
	}
	public void setEstado(String estado) {
		Estado = estado;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getRol() {
		return Rol;
	}
	public void setRol(String rol) {
		Rol = rol;
	}
	
	
	

	
	
	
}
