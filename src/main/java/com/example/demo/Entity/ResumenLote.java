package com.example.demo.Entity;

import java.io.Serializable;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;


@Entity(name = "ResumenLote")
public class ResumenLote implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3822846848603444151L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	private String NoCuenta;

	private String ResumenDebito;

	private String ResumenCredito;

	private String TotalGeneral;
	
	private double Debito;
	
	private double Credito;
	
	private double ValorTotal;
	
	@ManyToOne (cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private LoteUsuario lote;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNoCuenta() {
		return NoCuenta;
	}

	public void setNoCuenta(String noCuenta) {
		NoCuenta = noCuenta;
	}

	

	public String getResumenDebito() {
		return ResumenDebito;
	}

	public void setResumenDebito(String resumenDebito) {
		ResumenDebito = resumenDebito;
	}

	public String getResumenCredito() {
		return ResumenCredito;
	}

	public void setResumenCredito(String resumenCredito) {
		ResumenCredito = resumenCredito;
	}

	public String getTotalGeneral() {
		return TotalGeneral;
	}

	public void setTotalGeneral(String totalGeneral) {
		TotalGeneral = totalGeneral;
	}

	

	public double getDebito() {
		return Debito;
	}

	public void setDebito(double debito) {
		Debito = debito;
	}

	public double getCredito() {
		return Credito;
	}

	public void setCredito(double credito) {
		Credito = credito;
	}

	public double getValorTotal() {
		return ValorTotal;
	}

	public void setValorTotal(double valorTotal) {
		ValorTotal = valorTotal;
	}

	public void setValorTotal(long valorTotal) {
		ValorTotal = valorTotal;
	}

	public LoteUsuario getLote() {
		return lote;
	}

	public void setLote(LoteUsuario lote) {
		this.lote = lote;
	}
	
	

}
