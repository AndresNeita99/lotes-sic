package com.example.demo.Entity;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name = "moneda")
public class Moneda {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private int moneda;
	private String NombreMoneda;
	private String Estado;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getMoneda() {
		return moneda;
	}

	public void setMoneda(int moneda) {
		this.moneda = moneda;
	}

	public String getEstado() {
		return Estado;
	}

	public void setEstado(String estado) {
		Estado = estado;
	}

	public String getNombreMoneda() {
		return NombreMoneda;
	}

	public void setNombreMoneda(String nombreMoneda) {
		NombreMoneda = nombreMoneda;
	}
	
	

}
