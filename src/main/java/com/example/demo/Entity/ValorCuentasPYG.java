package com.example.demo.Entity;

import java.io.Serializable;



import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;

@Entity(name = "ValorCuentasPYG")
public class ValorCuentasPYG implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7909157215429722269L;

	@GeneratedValue(strategy = GenerationType.AUTO)
	@Id
	private long id;
	
	private int cuenta;
	
	private double valorNivel1;
	
	private double valorNivel2;
	
	private String valorNivel1Unidades;
	
	private String valorNivel2Unidades;
	
	private String Estado;
	
	

	

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
	
	

	public int getCuenta() {
		return cuenta;
	}

	public void setCuenta(int cuenta) {
		this.cuenta = cuenta;
	}

	public double getValorNivel1() {
		return valorNivel1;
	}

	public void setValorNivel1(double valorNivel1) {
		this.valorNivel1 = valorNivel1;
	}

	public double getValorNivel2() {
		return valorNivel2;
	}

	public void setValorNivel2(double valorNivel2) {
		this.valorNivel2 = valorNivel2;
	}

	public String getValorNivel1Unidades() {
		return valorNivel1Unidades;
	}

	public void setValorNivel1Unidades(String valorNivel1Unidades) {
		this.valorNivel1Unidades = valorNivel1Unidades;
	}

	public String getValorNivel2Unidades() {
		return valorNivel2Unidades;
	}

	public void setValorNivel2Unidades(String valorNivel2Unidades) {
		this.valorNivel2Unidades = valorNivel2Unidades;
	}

	public String getEstado() {
		return Estado;
	}

	public void setEstado(String estado) {
		Estado = estado;
	}
	
	
	
	

}
