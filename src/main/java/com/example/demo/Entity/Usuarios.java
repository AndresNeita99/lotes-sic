package com.example.demo.Entity;


import java.io.Serializable;
import java.util.Date;
import java.util.List;

import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

import java.util.Calendar;


@Entity
@Table(name = "usuarios")
public class Usuarios implements Serializable{

	
	private static final long serialVersionUID = -1612701551986788291L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private String nombre;

	private String username;
	
	
	private String correo;

	
	private String password;

	
	private Integer numcelular; // se llena en registro por primera vez
	
	
	private int documento; //
	
	private String fechanacimiento; // se llena por primera vez en preguntas
	
	private String fechaexpedicion; // se llena por primera vez en preguntas

	private String EstadoUsuario;

	private String EstadoContra;
	
	private Date fechacontra;
	
	private Date fechalimite;
	

	@ManyToOne(cascade = CascadeType.REFRESH , fetch = FetchType.LAZY)
	private Area area;

	@JoinTable(name = "Usuario_role",
			joinColumns = @JoinColumn(name = "usuario_id" , referencedColumnName = "id"),
			inverseJoinColumns = @JoinColumn(name = "rol_id", referencedColumnName = "id")
			)
	@ManyToMany(fetch = FetchType.LAZY , cascade = CascadeType.REFRESH)
	private List<Rol> rol;


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Integer getNumcelular() {
		return numcelular;
	}

	public void setNumcelular(Integer numcelular) {
		this.numcelular = numcelular;
	}

	public int getDocumento() {
		return documento;
	}

	public void setDocumento(int documento) {
		this.documento = documento;
	}


	public List<Rol> getRol() {
		return rol;
	}

	public void setRol(List<Rol> rol) {
		this.rol = rol;
	}

	public String getFechanacimiento() {
		return fechanacimiento;
	}

	public void setFechanacimiento(String fechanacimiento) {
		this.fechanacimiento = fechanacimiento;
	}

	public String getFechaexpedicion() {
		return fechaexpedicion;
	}

	public void setFechaexpedicion(String fechaexpedicion) {
		this.fechaexpedicion = fechaexpedicion;
	}

	public String getEstadoUsuario() {
		return EstadoUsuario;
	}

	public void setEstadoUsuario(String estadoUsuario) {
		EstadoUsuario = estadoUsuario;
	}

	public String getEstadoContra() {
		return EstadoContra;
	}

	public void setEstadoContra(String estadoContra) {
		EstadoContra = estadoContra;
	}

	public Area getArea() {
		return area;
	}

	public void setArea(Area area) {
		this.area = area;
	}

	public Date getFechacontra() {
		return fechacontra;
	}

	public void setFechacontra(Date fechacontra) {
		this.fechacontra = fechacontra;
	}

	public Date getFechalimite() {
		return fechalimite;
	}

	public void setFechalimite(Date fechalimite) {
		this.fechalimite = fechalimite;
	}

	
	
	
	

}
