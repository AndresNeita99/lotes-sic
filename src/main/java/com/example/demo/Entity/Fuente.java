package com.example.demo.Entity;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name = "fuentes")
public class Fuente {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	private String NombreFuente;
	private String FuenteParametro;
	private String Estado;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNombreFuente() {
		return NombreFuente;
	}

	public void setNombreFuente(String nombreFuente) {
		NombreFuente = nombreFuente;
	}

	public String getFuenteParametro() {
		return FuenteParametro;
	}

	public void setFuenteParametro(String fuenteParametro) {
		FuenteParametro = fuenteParametro;
	}

	public String getEstado() {
		return Estado;
	}

	public void setEstado(String estado) {
		Estado = estado;
	}

}
