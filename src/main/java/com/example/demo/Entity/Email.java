package com.example.demo.Entity;

public class Email {
	
	String Asunto ;
	String Mensaje;
	String CorreoDestino;
	
	public String getAsunto() {
		return Asunto;
	}
	public void setAsunto(String asunto) {
		Asunto = asunto;
	}
	public String getMensaje() {
		return Mensaje;
	}
	public void setMensaje(String mensaje) {
		Mensaje = mensaje;
	}
	public String getCorreoDestino() {
		return CorreoDestino;
	}
	public void setCorreoDestino(String correoDestino) {
		CorreoDestino = correoDestino;
	}
	
	
	

}
