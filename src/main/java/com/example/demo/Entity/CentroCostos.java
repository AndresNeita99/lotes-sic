package com.example.demo.Entity;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name = "centroCostos")
public class CentroCostos {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private int CentroCostos;
	private String Nombre;
	private String estado;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getCentroCostos() {
		return CentroCostos;
	}

	public void setCentroCostos(int centroCostos) {
		CentroCostos = centroCostos;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getNombre() {
		return Nombre;
	}

	public void setNombre(String nombre) {
		Nombre = nombre;
	}
	
	

}
