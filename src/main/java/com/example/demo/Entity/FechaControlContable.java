package com.example.demo.Entity;



import java.sql.Date;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Table(name = "fechascontables")
@Entity
public class FechaControlContable {

	@GeneratedValue(strategy = GenerationType.AUTO)
	@Id
	private long id;
	private int dias;

	private Date fecha_Inicial;

	private Date fecha_final;
	
	private Date Fecha_Cierre;
	
	private String Tipo;

	private String HoraMaxima;

	private String Modo;
	
	private String estado;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public int getDias() {
		return dias;
	}

	public void setDias(int dias) {
		this.dias = dias;
	}

	

	public String getHoraMaxima() {
		return HoraMaxima;
	}

	public void setHoraMaxima(String horaMaxima) {
		HoraMaxima = horaMaxima;
	}

	public String getModo() {
		return Modo;
	}

	public void setModo(String modo) {
		Modo = modo;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public Date getFecha_Inicial() {
		return fecha_Inicial;
	}

	public void setFecha_Inicial(Date fecha_Inicial) {
		this.fecha_Inicial = fecha_Inicial;
	}

	public Date getFecha_final() {
		return fecha_final;
	}

	public void setFecha_final(Date fecha_final) {
		this.fecha_final = fecha_final;
	}

	public Date getFecha_Cierre() {
		return Fecha_Cierre;
	}

	public void setFecha_Cierre(Date fecha_Cierre) {
		Fecha_Cierre = fecha_Cierre;
	}

	public String getTipo() {
		return Tipo;
	}

	public void setTipo(String tipo) {
		Tipo = tipo;
	}



}
