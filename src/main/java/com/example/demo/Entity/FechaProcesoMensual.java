package com.example.demo.Entity;

import java.sql.Date;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Table(name = "FechaProcesoMensual")
@Entity
public class FechaProcesoMensual {

	@GeneratedValue(strategy = GenerationType.AUTO)
	@Id
	private long id;

	@Column(nullable = true)
	private Date fecha_Inicial;

	@Column(nullable = true)
	private Date fecha_final;

	private String HoraMaxima;
	
	private String TipoCargue;

	private String UltimoDia;
	
	private int dias;
	
	private String Modo;
	
	private String Estado;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Date getFecha_Inicial() {
		return fecha_Inicial;
	}

	public void setFecha_Inicial(Date fecha_Inicial) {
		this.fecha_Inicial = fecha_Inicial;
	}

	public Date getFecha_final() {
		return fecha_final;
	}

	public void setFecha_final(Date fecha_final) {
		this.fecha_final = fecha_final;
	}

	public String getHoraMaxima() {
		return HoraMaxima;
	}

	public void setHoraMaxima(String horaMaxima) {
		HoraMaxima = horaMaxima;
	}

	public String getModo() {
		return Modo;
	}

	public void setModo(String modo) {
		Modo = modo;
	}

	public String getEstado() {
		return Estado;
	}

	public void setEstado(String estado) {
		Estado = estado;
	}

	public String getUltimoDia() {
		return UltimoDia;
	}

	public void setUltimoDia(String ultimoDia) {
		UltimoDia = ultimoDia;
	}

	public int getDias() {
		return dias;
	}

	public void setDias(int dias) {
		this.dias = dias;
	}

	public String getTipoCargue() {
		return TipoCargue;
	}

	public void setTipoCargue(String tipoCargue) {
		TipoCargue = tipoCargue;
	}
	
	

}
