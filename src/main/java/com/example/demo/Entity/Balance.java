package com.example.demo.Entity;

import java.io.Serializable;
import java.util.jar.Attributes.Name;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;

@Entity(name = "Balance")
public class Balance implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5413150167674591661L;
	
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Id
	private Long id;
	
	private String Cuenta;
	
	private String Descripcion;
	private String SaldoAterior;
	private String Debitos;
	private String Creditos;
	private String SaldoFinal;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCuenta() {
		return Cuenta;
	}

	public void setCuenta(String cuenta) {
		Cuenta = cuenta;
	}

	public String getDescripcion() {
		return Descripcion;
	}

	public void setDescripcion(String descripcion) {
		Descripcion = descripcion;
	}

	public String getSaldoAterior() {
		return SaldoAterior;
	}

	public void setSaldoAterior(String saldoAterior) {
		SaldoAterior = saldoAterior;
	}

	public String getDebitos() {
		return Debitos;
	}

	public void setDebitos(String debitos) {
		Debitos = debitos;
	}

	public String getCreditos() {
		return Creditos;
	}

	public void setCreditos(String creditos) {
		Creditos = creditos;
	}

	public String getSaldoFinal() {
		return SaldoFinal;
	}

	public void setSaldoFinal(String saldoFinal) {
		SaldoFinal = saldoFinal;
	}

}
