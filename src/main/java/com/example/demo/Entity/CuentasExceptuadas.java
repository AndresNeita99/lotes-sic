package com.example.demo.Entity;

import java.io.Serializable;



import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;

@Entity(name = "CuentasExceptuadas")
public class CuentasExceptuadas implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6173961819394662566L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id ;
	
	private String Cuenta;
	
	private String Descripcion;

	private String Estado;
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getCuenta() {
		return Cuenta;
	}

	public void setCuenta(String cuenta) {
		Cuenta = cuenta;
	}

	public String getDescripcion() {
		return Descripcion;
	}

	public void setDescripcion(String descripcion) {
		Descripcion = descripcion;
	}

	public String getEstado() {
		return Estado;
	}

	public void setEstado(String estado) {
		Estado = estado;
	}
	
	
	
	

}
