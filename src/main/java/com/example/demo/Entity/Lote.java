package com.example.demo.Entity;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

// modificar nombre a clase validar 
// Esta clase es de validar estructura del lote 

public class Lote {

	List<String> ErroresEstructura = null; // lista donde se almacenan todos los errores de estructura encontrados

	public String limpiarEspacios(String dato) {

		String str = dato;
		String charsToRemove = ",.][";

		for (char c : charsToRemove.toCharArray()) {
			str = str.replace(String.valueOf(c), "");
		}

		return str.replaceAll(" ", "");

	}

	// metodo para limpiar dato Consecutivo

	public int LimpiarDatoNumerico(Double dato) {

		String cadena = dato.toString();

		String charsToRemove = ".0";

		for (char c : charsToRemove.toCharArray()) {
			cadena = cadena.replace(String.valueOf(c), "");
		}

		int datonumerico = Integer.parseInt(cadena);

		return datonumerico;

	}

	public String limpiarErroresE(Double dato) {

		String valor = dato.toString();

		if (valor.contains("E7") && valor.contains(".")) {

			valor = valor.replace("E7", "");
			valor = valor.replace(".", "");

		}

		if (valor.contains("E9") && valor.contains(".")) {

			valor = valor.replace("E9", "");
			valor = valor.replace(".", "");
		}

		if (valor.contains("E8") && valor.contains(".")) {

			valor = valor.replace("E8", "");
			valor = valor.replace(".", "");
		}

		return valor;
	}

	// metodolo para limpiar numero de cuenta

	public String LimpiarNumeroCuenta(Double dato) {

		String cadena = dato.toString();

		String charsToRemove = "E9";
		
		

		if (cadena.contains(charsToRemove)) {

			cadena = cadena.replace("E9", "");
			cadena = cadena.replace(".", "");

			int Longitud = cadena.length();

			if (Longitud < 10) {

				for (int i = cadena.length(); i <= 9; i++) {

					cadena = cadena.concat("0");

				}

			}
			

		} else {
			
			String Numero = cadena ;
			
			//System.out.print("el dato no cumple con la longitud " + Numero); // almacenar en lista de errores
		}

		String Numero = cadena;

		return Numero;

	}

	public String EliminarPuntos(Double dato) {

		String cadena3 = dato.toString();

		cadena3 = cadena3.replace(".0", "");

		//System.out.println(cadena3);
		
		return cadena3;
		
	}

	public void estructurarValorDivisa(Double valorDivida) {

		String cadena3 = valorDivida.toString();

		if ((cadena3.contains(".5") && cadena3.length() < 12) || (cadena3.contains(".9") && cadena3.length() < 12)
				|| (cadena3.contains(".4") && cadena3.length() < 12)
				|| (cadena3.contains(".3") && cadena3.length() < 12)
				|| (cadena3.contains(".8") && cadena3.length() < 12)
				|| (cadena3.contains(".1") && cadena3.length() < 12)) {

			if (cadena3.contains("E7") == false) {

				cadena3 = cadena3.concat("0");
				cadena3 = cadena3.replace(".", "");

				//System.out.print("el resultado es : " + cadena3);

			}

		} else {

			cadena3 = cadena3.replace("E7", "");
			cadena3 = cadena3.replace(".", "");

			//System.out.print("el resultado es : " + cadena3);

		}

	}

	public void validarUnidades(Double dato) {

		String cadena3 = dato.toString();

		if ((cadena3.contains(".5") && cadena3.length() < 10) || (cadena3.contains(".9") && cadena3.length() < 10)
				|| (cadena3.contains(".4") && cadena3.length() < 10)
				|| (cadena3.contains(".3") && cadena3.length() < 10)
				|| (cadena3.contains(".8") && cadena3.length() < 10)
				|| (cadena3.contains(".1") && cadena3.length() < 10)) {

			if (cadena3.contains("E7") == false) {

				cadena3 = cadena3.concat("0");
				cadena3 = cadena3.replace(".", "");

				//System.out.print("el resultado es :" + cadena3);

			}

		}

		if (cadena3.contains("E9")) {

			cadena3 = cadena3.replace("E9", "");
			LimpiarErrorE9(cadena3);

		}

		if (cadena3.contains("E8")) {

			cadena3 = cadena3.replace("E8", "");
			LimpiarErrorE8(cadena3);
		}

		if (cadena3.contains("E7")) {

			cadena3 = cadena3.replace("E7", "");
			LimpiarErrorE7(cadena3);
		}

		if (cadena3.contains("E10")) {

			cadena3 = cadena3.replace("E10", "");
			organizarCeros(cadena3);

		}

	}

	private void LimpiarErrorE7(String cadena3) { // is l;a oongitud es <8 es dato esta mal

		int longitud = cadena3.length();

		if (longitud == 6) {

			cadena3 = cadena3 + "000.00";

		}

		if (longitud == 3) {

			cadena3 = cadena3 + "00.000.000.00";
		}

		if (longitud == 4) {

			cadena3 = cadena3 + "0.000.000.00";
		}

		if (longitud == 7) {

			cadena3 = cadena3 + "00.00";
		}

		if (longitud == 5) {

			cadena3 = cadena3 + "0.000.00";
		}

		if (longitud == 2) {

			cadena3 = cadena3 + "0.000.000.00";
		}

		if (longitud == 11) {

			cadena3 = cadena3 + "0";
		}

		organizarCeros(cadena3);

	}

	private void LimpiarErrorE8(String cadena3) {

		int longitud = cadena3.length(); // min 10 CON . max 12 CON .

		if (longitud == 2) {

			cadena3 = cadena3 + "0.000.00";
		}

		if (longitud == 9) {

			cadena3 = cadena3 + "00.00";
		}

		if (longitud == 3) {

			cadena3 = cadena3 + "00.000.000.00";
		}

		if (longitud == 4) {

			cadena3 = cadena3 + "0.000.000.00";
		}

		if (longitud == 7) {

			cadena3 = cadena3 + ".000.00";
		}

		if (longitud == 8) {

			cadena3 = cadena3 + "00.00";
		}

		if (longitud == 5) {

			cadena3 = cadena3 + "00.000.00";
		}

		organizarCeros(cadena3);

	}

	public void LimpiarErrorE9(String cadena3) { // 14 max longitud del dato con puntos y sin puntos 12

		int longitud = cadena3.length();

		if (longitud == 6) {

			cadena3 = cadena3 + "00.000.00";
		}

		if (longitud == 9) {

			cadena3 = cadena3 + "00.00";
		}

		if (longitud == 3) {

			cadena3 = cadena3 + "00.000.000.00";
		}

		if (longitud == 4) {

			cadena3 = cadena3 + "0.000.000.00";
		}

		if (longitud == 7) {

			cadena3 = cadena3 + "0.000.00";
		}

		if (longitud == 8) {

			cadena3 = cadena3 + ".000.00";
		}

		if (longitud == 5) {

			cadena3 = cadena3 + ".000.000.00";
		}

		organizarCeros(cadena3);

	}

	public void organizarCeros(String dato) {

		long pesos = 0;

		dato = dato.replace(".", "");

		pesos = Long.parseLong(dato);

		System.out.println(pesos);

	}

	public long valorTotalPesos(long valorPesos) {

		return 0;
	}

	public void TrasformarValorPesos(String pesos) {

		if (pesos.length() >= 12) {

			char[] chars = pesos.toCharArray();

			pesos = chars[0] + "." + chars[1] + chars[2] + chars[3] + "." + chars[4] + chars[5] + chars[6] + "."
					+ chars[7] + chars[8] + chars[9] + "." + chars[10] + chars[11];

			//System.out.print("el dato es : " + pesos);

		}
	}

	public String EstructurarFecha(String fecha) {

		String FechaLote = fecha.toString();
		
		//System.out.println("La longitud de la fecha de entrada es de : " + FechaLote.length());
		
		if (FechaLote.length() == 7) {
			
			FechaLote = FechaLote + "0";
		}

		LocalDate FechaAplicativo = LocalDate.now();

		int añoAplicativo = FechaAplicativo.getYear();

		String StringAño = String.valueOf(añoAplicativo);

		if (FechaLote.length() == 8 && FechaLote.contains(StringAño)) { // validamos que la fecha del registro sea del
																		// presente año

			char[] DatosFecha = FechaLote.toCharArray();

			String mes = DatosFecha[4] + "" + DatosFecha[5];

			String dia = DatosFecha[6] + "" + DatosFecha[7];

			String año = DatosFecha[0] + "" + DatosFecha[1] + "" + DatosFecha[2] + "" + DatosFecha[3];

			FechaLote = DatosFecha[0] + "" + DatosFecha[1] + "" + DatosFecha[2] + "" + DatosFecha[3] + "/" + DatosFecha[4] + DatosFecha[5] + "/" + DatosFecha[6] + DatosFecha[7];

			//validarFechaEstructura(FechaLote , tipoLote);

		} else {

			
			//System.out.println("la fecha es incorrecta : " + FechaLote);
		}
		
		return FechaLote;

	}

	public static Date EstablecerFormatoFechaProcesoLote(String fecha) { // valida
																													// el
																													// formato
		// de la fecha que
		// aaaa/mm/dd

		try {
			SimpleDateFormat formatoFecha = new SimpleDateFormat("yyyy/MM/dd");
			formatoFecha.setLenient(false);
			formatoFecha.parse(fecha);

		} catch (ParseException e) {
			//System.out.println("la fecha es incorrecta , No cumple el formato aaaa/mm/dd" + fecha);
		}


		DateFormat format = new SimpleDateFormat("yyyy/MM/dd");
		
		try {
            
			Date fechaLote = format.parse(fecha);
           
			//System.out.println(format.format(fechaLote));
			
			return fechaLote;
        }
        catch (ParseException e) {
            e.printStackTrace();
        }
		
		return null;

	}

	public boolean validarFechaAplicativo(LocalDate fecha) {

		LocalDate FechaAplicativo = LocalDate.now();
		
		boolean fechacorrecta = true;

		if (fecha.equals(FechaAplicativo)) {
			
			//System.out.println("Fecha 1 es igual a fecha2 la fecha esta mal " + fecha);

			fechacorrecta = false;
			
			return fechacorrecta;
			
			
		} else if (fecha.isAfter(FechaAplicativo)) {
			
			//System.out.println("Fecha 1 es mayor a fecha2 esta mal la fecha " + fecha);
			
			fechacorrecta = false;
			
			return fechacorrecta;

		} else if (fecha.isBefore(FechaAplicativo)) {

			//System.out.println("Fecha 1 " + fecha + " es menor " + FechaAplicativo);

			//validarDiasHabiles(fecha, mes, dia, año, TipoLote);

		}

		return fechacorrecta;

	}

	private static void validarDiasHabiles(LocalDate fechaLote, String mes, String dia, String año, String TipoLote) { // validamos
																														// que

		Calendar c = Calendar.getInstance();

		Calendar e = Calendar.getInstance();

		LocalDate FechaAplicativo = LocalDate.now();

		int mesLote = Integer.parseInt(mes);
		int diaLote = Integer.parseInt(dia);
		int añoLote = Integer.parseInt(año);

		int mesAplicativo = FechaAplicativo.getMonthValue();
		int diaAplicativo = FechaAplicativo.getMonthValue();
		int añoAplicativo = FechaAplicativo.getMonthValue();

		c.set(añoLote, mesLote, diaLote); // año, mes, día

		e.set(añoAplicativo, mesAplicativo, diaAplicativo); // año, mes, día

		int diaSemanaAplicativo = e.get(Calendar.DAY_OF_WEEK);

		int diaSemanaLote = c.get(Calendar.DAY_OF_WEEK);

		// este dato se extrae del combo box dependiendo
		// el cargue que se alla hecho

		// String TipoLote = "Mensual";

		switch (TipoLote) {
		case "Diario": {

			// dia 1 domingo
			// dia 7 sabado
			// dia 2 lunes

			// el que la fecha de cargue sea un lunes en ese caso se restan 3 dias
			// el caso de ser de martes a viernes se restara solo 1 dia

			if (diaSemanaAplicativo != 7 || diaSemanaAplicativo != 1 || diaSemanaLote != 7 || diaSemanaLote != 1) {

				if (diaSemanaAplicativo == 2) { // si es lunes se an de restar 2 dias debido a que se carga lo del dia
												// viernes

					int diaCargueDiario = FechaAplicativo.getDayOfMonth() - 3; // comparo esta fecha con la del lote
																				// Excel

					//System.out.println("Dia de cargue " + diaCargueDiario);
					//validarfechaLoteDiario(fechaLote, diaCargueDiario, diaLote);

				} else {

					int diaCargueDiario = FechaAplicativo.getDayOfMonth() - 1;

					//System.out.println("Dia de cargue " + diaCargueDiario);
					//validarfechaLoteDiario(fechaLote, diaCargueDiario, diaLote);

				}

			} else {

				//System.out.println("la fecha del registro es incorrecta , los dias son No habiles: " + fechaLote);

			}

			break;
		}

		case "Mensual": {

			// validar el dia del mes anterior a el cargue que se esta haciendo sea el
			// ultimo de ese mes y que sea habil ese dia

			SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
			// Fecha actual
			Calendar calendar = Calendar.getInstance();
			System.out.println("Fecha Actual:" + sdf.format(calendar.getTime()));

			// Se le resta 1 mes
			calendar.set(Calendar.MONTH, calendar.get(Calendar.MONTH) - 1);
			String FechaMesAnterior = sdf.format(calendar.getTime());
			// System.out.println("Fecha del mes anterio :" + FechaMesAnterior);
			int ultimoDiaMes = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);

			// System.out.println("1-Último día del mes anterior " + ultimoDiaMes);

			validarFechaLoteMensual(fechaLote, ultimoDiaMes, FechaMesAnterior);

			break;
		}

		default:
			throw new IllegalArgumentException("Unexpected value: " + TipoLote);
		}

		/* traer la fecha de procesamiento dia de cargue para ese mes */

		// validar dias habiles
		// tomar la fecha del ultimo dia habil del mes anterior

	}

	public static void validarFechaLoteMensual(LocalDate fecha, int DiaCargue, String fechaMesAnterior) {

		LocalDate fechaCargueParametria = LocalDate.parse("2023-01-31"); // variable que toca extrar de parametria

		if (fecha == fechaCargueParametria) {

			//System.out.println("la fecha es correcta " + fecha);

		} else {

			//System.out.println("la fecha es incorrecta " + fecha);
		}

	}

	public boolean validarfechaLoteDiarioManual(Date fechaProceso, Date fechaInicial, Date fechaFinal) {

		String FechaProcesoString= fechaProceso.toString();
		
		int diaHabil = fechaProceso.getDay();

		
		boolean fechacorrecta = true ; 
		

		if (diaHabil != 6 && diaHabil != 7 && fechaProceso.after(fechaInicial) && fechaProceso.before(fechaFinal) || diaHabil != 6 && diaHabil != 7 && fechaProceso.equals(fechaInicial) || diaHabil != 6 && diaHabil != 7 && fechaProceso.equals(fechaFinal)) {
	
			fechacorrecta = true;
			
			return fechacorrecta;

			
		
		}else {

			fechacorrecta = false;
			
			return fechacorrecta;

		}


	}
	

	
	
	public boolean validarfechaLoteDiarioAuto (int DiasFechaProceso , int diaProcesoLote , int mesFechaProcesoLote) {
		
		//System.out.println("Dias de proceso " + DiasFechaProceso);
	
		LocalDate Fechacargue = LocalDate.now();
		
		Calendar c = Calendar.getInstance();

		int contador = DiasFechaProceso - 1 ;
		
		int contadorDiasHabiles = 1 ;
		
		int fechaFinal = Fechacargue.getDayOfMonth() ; // primer dia habil 
		
		int fechaFinalMes = Fechacargue.getMonthValue();
		
		int fechaInicial = 0 ;
		
		int MesFechaInicial = 0 ;
		
		boolean fechaProcesoCorrecta = true ;
		
		// sacar la fecha inicial CUANDO ES 1 SOLO DIA HABIL
		
		if (DiasFechaProceso == 1) {
			
			
			c.add(Calendar.DATE,-1) ; // en este caso son 2 dias que se restna ya que el numero de dias es traido por parametria 
			
			Date date = c.getTime() ;
			
			int Dia = date.getDate();
			
			//System.out.println("el primer dia de la semana es :"  + Dia);
			
			int DiaSemana = c.get(Calendar.DAY_OF_WEEK);
			
			MesFechaInicial = date.getMonth();
			
			//System.out.println("dia de la semana " + DiaSemana);
			
			if (DiaSemana == Calendar.SUNDAY || DiaSemana == Calendar.SATURDAY) {
				
				//System.out.println("Entra a el condicional ");
				
				c.add(Calendar.DATE, -2) ; // en este caso son 2 dias que se restna ya que el numero de dias es traido por parametria 
				
				Date date2 = c.getTime() ;
				
				int Dia2 = date2.getDate();

				fechaInicial = Dia2;
				
				
			}
			
			else {
				
				fechaInicial = Dia;
				
			}
			
		}
		
		else {
			
			// condicionar para cuando los dias son 2 o mas dias habiles 
			
			//SACAR LA FECHA INICIAL CUANDO SON MAS DE 2 DIAS HABILES 
			
			while (contadorDiasHabiles <= contador) {
		
				c.add(Calendar.DATE, -1) ; // en este caso son 2 dias que se restna ya que el numero de dias es traido por parametria 
				
				Date date = c.getTime() ;
				
				int Dia = date.getDate();
				
				int Mes = date.getMonth();
				
				int DiaSemana = date.getDay();
				
				if (DiaSemana != 6 && DiaSemana != 0) {
					
					//System.out.println("dia atras: "+ date.getDate() + " Dia de la semana " + DiaSemana + " " + contadorDiasHabiles + " Dia de lote " + diaProcesoLote);

					fechaInicial = Dia;
					
					//System.out.println("Fecha final es : " + fechaFinal);
					
					contadorDiasHabiles = contadorDiasHabiles + 1 ;
					
				}

			}
			
			
		}
		
		
		
		if (diaProcesoLote >= fechaInicial && diaProcesoLote <= fechaFinal  ||  diaProcesoLote >= fechaInicial && diaProcesoLote >=  fechaFinal && fechaFinalMes >= mesFechaProcesoLote && MesFechaInicial >= mesFechaProcesoLote) {
			
			//System.out.println("primer fecha incial " + fechaInicial + " Fecha final " + fechaFinal + " Dia de proceso del lote " +  " mes fecha final " + fechaFinalMes + " mes fecha lote " + mesFechaProcesoLote /*+ diaProcesoLote*/);
			
			return true;
		}
		
		else {
			
			//System.out.println("primer fecha incial " + fechaInicial + " Fecha final " + fechaFinal + " Dia de proceso del lote " + diaProcesoLote);

			
			return false;
		}
		
		
	}
	
	public boolean ValidarFechaLoteMensualCierre (Date fechaFinalMensual, Date fechaProceso) {
		
		int diaHabil = fechaProceso.getDate();
		
		boolean fechacorrecta = true ; 
		

		if (diaHabil != 6 && diaHabil != 7 && fechaProceso.equals(fechaFinalMensual)) {
	
			fechacorrecta = true;
			
			return fechacorrecta;

			
		
		}else {

			fechacorrecta = false;
			
			return fechacorrecta;

		}

	}
	
	public boolean ValidarFechaLoteDiarioCierre (Date fechaInicialDiario, Date fechaFinalDiario, Date fechaProceso) {
			
		int diaHabil = fechaProceso.getDay();
		
		boolean fechacorrecta = true ; 
		

		if (diaHabil != 6 && diaHabil != 7 && fechaProceso.after(fechaInicialDiario) && fechaProceso.before(fechaFinalDiario) || diaHabil != 6 && diaHabil != 7 && fechaProceso.equals(fechaInicialDiario) || diaHabil != 6 && diaHabil != 7 && fechaProceso.equals(fechaFinalDiario)) {
	
			fechacorrecta = true;
			
			return fechacorrecta;

			
		
		}else {

			fechacorrecta = false;
			
			return fechacorrecta;

		}

	}

	public String validarNit(String Nit) {

		if (Nit.length() < 9 || Nit.length() > 15) {

			//System.out.print(" El nit " + Nit + " es erroneo ");

		} else {

			//System.out.print(" El nit " + Nit + " es correcto ");

		}

		return Nit;

	}
	
	public String completarCeros (long CuentaBalance) {
		
		String Cuenta = String.valueOf(CuentaBalance);
		
		if (Cuenta.length() < 10) {

			for (int i = Cuenta.length(); i <= 9; i++) {

				Cuenta = Cuenta.concat("0");

			}

		}
		
		return Cuenta;
	}
	
	

	public String GuardarCuentaTemporal (String cuenta) {
		
		
		return cuenta;
	}
	
	public boolean ValidarCuentaValor (double TotalCuentaLote , double valorNivel1 , double valorNivel2 , char Digito) {
		
		if (Digito == '1') {
			
			return true;
		}
		
		return false;
	}

}
