package com.example.demo.Entity;


import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;


@Table(name = "HorasCorte")
@Entity
public class HorasCorte{

	@GeneratedValue(strategy = GenerationType.AUTO)
	@Id
	private long id;
	
	private String horaCorte;
	
	private String Estado;
	
	private String Tipo ;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getHoraCorte() {
		return horaCorte;
	}

	public void setHoraCorte(String horaCorte) {
		this.horaCorte = horaCorte;
	}

	public String getEstado() {
		return Estado;
	}

	public void setEstado(String estado) {
		Estado = estado;
	}

	public String getTipo() {
		return Tipo;
	}

	public void setTipo(String tipo) {
		Tipo = tipo;
	}
	
	
}
