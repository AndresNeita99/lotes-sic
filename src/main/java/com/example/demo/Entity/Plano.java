package com.example.demo.Entity;

import java.io.Serializable;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;

@Entity(name = "Planos")
public class Plano implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7534166424270545188L;

	@Id
	private String id;
	
	private String rutaPlano;
	
	private String Estado;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getRutaPlano() {
		return rutaPlano;
	}

	public void setRutaPlano(String rutaPlano) {
		this.rutaPlano = rutaPlano;
	}

	public String getEstado() {
		return Estado;
	}

	public void setEstado(String estado) {
		Estado = estado;
	}
	
	
	
	

}
