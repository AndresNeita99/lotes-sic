package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.example.demo.Dao.AreaDao;
import com.example.demo.Dao.AreaPaginDao;
import com.example.demo.Entity.Area;

@Service
public class AreaServiceImpl implements AreaService{

	@Autowired
	private AreaDao areaDao;
	
	@Autowired
	private AreaPaginDao areaDaoPagin;
	
	
	@Override
	public List<Area> listaAreas() {
		// TODO Auto-generated method stub
		return (List<Area>) areaDao.findAll();
	}

	@Override
	public Area crearAreas(Area area) {
		// TODO Auto-generated method stub
		return areaDao.save(area);
	}

	@Override
	public void eliminarArea(Long id) {
		
		areaDao.deleteById(id);
		
	}

	@Override
	public Area buscarAreaId(Long id) {
		// TODO Auto-generated method stub
		return areaDao.findById(id).orElse(null);
	}

	@Override
	public Area GuardarArea(Area area) {
		// TODO Auto-generated method stub
		return areaDao.save(area);
	}

	@Override
	public void ActualizarCargarMasivaArea() {
		
		 areaDao.deleteAll();
		
	}

	

	@Override
	public Page<Area> ListaAreaPagina(Pageable pageable) {
		// TODO Auto-generated method stub
		return areaDaoPagin.findAll(pageable);
	}


	

	@Override
	public List<Area> BuscarPorPresidencia(String presidencia) {
		// TODO Auto-generated method stub
		return areaDao.findBypresidencia(presidencia);
	}

	@Override
	public List<Area> BuscarPorArea(String Area) {
		// TODO Auto-generated method stub
		return areaDao.findByArea(Area);
	}

	@Override
	public List<Area> BuscarPorDireccion(String Direccion) {
		// TODO Auto-generated method stub
		return areaDao.finByDireccion(Direccion);
	}

	@Override
	public Area BuscarAreaAntigua(String area) {
		// TODO Auto-generated method stub
		return areaDao.BuscarArea(area);
	}

	@Override
	public Page<Area> FiltrarPorArea(String area, Pageable pageable) {
		// TODO Auto-generated method stub
		return areaDaoPagin.filtrarArea(area, pageable);
	}

	@Override
	public Page<Area> FiltrarPorVisepresidencia(String Vicepresidencia, Pageable pageable) {
		// TODO Auto-generated method stub
		return areaDaoPagin.filtrarVisepresidencia(Vicepresidencia, pageable);
	}

	@Override
	public Page<Area> FiltrarPorPresidencia(String presidencia, Pageable pageable) {
		// TODO Auto-generated method stub
		return areaDaoPagin.filtrarPresidencia(presidencia, pageable);
	}

	@Override
	public List<Area> search(String Area) {
		// TODO Auto-generated method stub
		return areaDao.findByArea(Area);
	}

	



}
