package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.example.demo.Dao.CentroCostosDao;
import com.example.demo.Dao.CentroCostosPaginDao;
import com.example.demo.Entity.CentroCostos;

@Service
@Repository
public class CentroCostosImple implements CentroCostosService{
	
	@Autowired
	private CentroCostosDao centroDao;
	
	@Autowired
	private CentroCostosPaginDao centroPaginDao;

	@Override
	public List<CentroCostos> listaCostos() {
		// TODO Auto-generated method stub
		return (List<CentroCostos>) centroDao.findAll();
	}

	@Override
	public CentroCostos GuardarCentroCostos(CentroCostos centrocostos) {
		// TODO Auto-generated method stub
		return centroDao.save(centrocostos);
	}

	@Override
	public CentroCostos findOne(long id) {
		// TODO Auto-generated method stub
		return centroDao.findById(id).orElse(null);
	}

	@Override
	public List<CentroCostos> BuscarCentroCostos(int CentroCostos) {
		// TODO Auto-generated method stub
		return centroDao.ObtenerCentroCostos(CentroCostos);
	}

	@Override
	public void EliminarCentroCosto(long id) {
		
		centroDao.deleteById(id);
		
	}

	@Override
	public void ActualizarCargarMasivaCentroCostos() {
		
		centroDao.deleteAll();
		
	}

	@Override
	public Page<CentroCostos> listaCentroCostosPagina(Pageable pageable) {
		// TODO Auto-generated method stub
		return centroPaginDao.findAll(pageable);
	}

	@Override
	public List<CentroCostos> listaCentroCostosHabilitados(String Estado, int centroCostos) {
		// TODO Auto-generated method stub
		return centroDao.CentroCostosHabilitados(Estado, centroCostos);
	}

	@Override
	public List<CentroCostos> listaCentrocostosActivos(String Estado) {
		// TODO Auto-generated method stub
		return centroDao.centroCostosActivos(Estado);
	}

	@Override
	public CentroCostos CentroCostosBuscar(int centroCostos) {
		// TODO Auto-generated method stub
		return centroDao.BuscarCentroCostos(centroCostos);
	}

}
