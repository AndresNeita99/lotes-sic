package com.example.demo.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.example.demo.Entity.NumeroCuenta;


public interface CuentasService {

	public List<NumeroCuenta> listaCuentas();
	
	public NumeroCuenta GuardarCuenta (NumeroCuenta numero);
	
	public NumeroCuenta findOne(long id);
	
	public void EliminarCuenta (Long id);
	
	List<NumeroCuenta> BuscarCuenta (String cuenta);
	
	public void ActualizarCuentaCargaMasiva();
	
	public Page <NumeroCuenta> ListaCuentasPagin (Pageable pageable);
	
	public List<NumeroCuenta> listaCuentasHabilitadas (String Estado , String NumeroCuenta);
	
	public List<NumeroCuenta> cuentasActivadas (String Estado);
	
	public NumeroCuenta TraerCuenta (String cuenta);
	
}
