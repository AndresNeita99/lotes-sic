package com.example.demo.service;

import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.example.demo.Entity.Usuarios;

public interface UsuariosService {
	
	public List<Usuarios> listaUsuarios();
	
	public Usuarios crearUsuario (Usuarios usuario);
	
	public void eliminarUsuario (Long id);
	
	public Usuarios buscarUsuarioId(Long id);
	
	public Usuarios GuardarUsuario (Usuarios usuario);
	
	public Usuarios buscarUsuario(String username);
	
	public Usuarios buscarPorCedula(int documento);
	
	public Usuarios buscarPorNombre(String nombre);
	
	public List<Usuarios> buscarUsuarioArea(long id);
	
	public Usuarios buscarPorEmail (String emial);
	
	public Page<Usuarios> listaUsuariosPagin(Pageable pageable);
	
	public Page<Usuarios> listaUsuariosPorArea (long id_Area , Pageable pageable);
	
	public List<Usuarios> listaUsuariosPorAreaLista (long id_Area);
	
	public Page<Usuarios> listaPeticionesCrearUsuarioadminPagin(String EstadoUsuario , Pageable pageable);
	
	public Usuarios listaUsuariosPorIdRol (long rol);
	
	public List<Usuarios> ListaDeAutorizadoresNivel1(long rol);
	
	public List<Usuarios> ListaDeAutorizadoresNivel2 (long rol);
	
	public List<Usuarios> listaUsuariosCambio(String estadoContra);
	
	List<Usuarios> listaPeticionesCrearUsuario(String EstadoUsuario);
	
	public Page<Usuarios> listaPeticionesContra(String estadoContra , Pageable pageable);
	
}
