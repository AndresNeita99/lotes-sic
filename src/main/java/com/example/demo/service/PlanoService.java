package com.example.demo.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.example.demo.Entity.Plano;

public interface PlanoService {
	
	List<Plano> listaPlanos ();
	
	Plano buscarPlano (String id);
	
	Plano GuardarPlano (Plano plano);
	
	List<Plano> BuscarPlanoEstado(String Estado);
	
	Page<Plano> TraerPlanosCrgardos(String Estado , Pageable pageable);
	
	

}
