package com.example.demo.service;


import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.example.demo.Dao.UsuariosDao;
import com.example.demo.Dao.UsuariosPaginDao;
import com.example.demo.Entity.Usuarios;

@Service
@Repository
public class UsuariosServiceImpl implements UsuariosService{

	@Autowired
	private UsuariosDao usuarioDao;
	
	@Autowired
	private UsuariosPaginDao usuarioDaoPagin;
	
	@Override
	public List<Usuarios> listaUsuarios() {
		// TODO Auto-generated method stub
		return (List<Usuarios>) usuarioDao.findAll();
	}

	@Override
	public Usuarios crearUsuario(Usuarios usuario) {
		// TODO Auto-generated method stub
		return usuarioDao.save(usuario);
	}

	@Override
	public void eliminarUsuario(Long id) {
		
		usuarioDao.deleteById(id);
		
	}

	@Override
	public Usuarios buscarUsuarioId(Long id) {
		// TODO Auto-generated method stub
		return usuarioDao.findById(id).orElse(null);
	}

	@Override
	public Usuarios GuardarUsuario(Usuarios usuario) {
		// TODO Auto-generated method stub
		return usuarioDao.save(usuario);
	}

	@Override
	public Usuarios buscarUsuario(String username) {
		// TODO Auto-generated method stub
		return usuarioDao.findByusername(username);
	}

	@Override
	public Usuarios buscarPorCedula(int documento) {
		// TODO Auto-generated method stub
		return usuarioDao.findBydocumento(documento);
	}

	@Override
	public Usuarios buscarPorNombre(String nombre) {
		// TODO Auto-generated method stub
		return usuarioDao.findBynombre(nombre);
	}

	@Override
	public List<Usuarios> buscarUsuarioArea(long id) {
		// TODO Auto-generated method stub
		return usuarioDao.findByarea(id);
	}

	@Override
	public Usuarios buscarPorEmail(String emial) {
		// TODO Auto-generated method stub
		return usuarioDao.findBycorreo(emial);
	}

	@Override
	public Page<Usuarios> listaUsuariosPagin(Pageable pageable) {
		// TODO Auto-generated method stub
		return usuarioDaoPagin.findAll(pageable);
	}

	

	@Override
	public List<Usuarios> listaPeticionesCrearUsuario(String EstadoUsuario) {
		// TODO Auto-generated method stub
		return usuarioDao.findByEstadoUsuario(EstadoUsuario);
	}

	@Override
	public Page<Usuarios> listaUsuariosPorArea(long id_Area , Pageable pageable) {
		// TODO Auto-generated method stub
		return usuarioDaoPagin.filtrarUsuariosArea(id_Area, pageable);
	}

	@Override
	public Usuarios listaUsuariosPorIdRol(long rol) {
		// TODO Auto-generated method stub
		return usuarioDao.findByrol(rol);
	}

	@Override
	public List<Usuarios> listaUsuariosCambio(String estadoContra) {
		// TODO Auto-generated method stub
		return usuarioDao.findByEstadoContra(estadoContra);
	}

	@Override
	public List<Usuarios> ListaDeAutorizadoresNivel1(long rol) {
		// TODO Auto-generated method stub
		return usuarioDao.findByrolAutorizador(rol);
	}

	@Override
	public List<Usuarios> ListaDeAutorizadoresNivel2(long rol) {
		// TODO Auto-generated method stub
		return usuarioDao.findByrolAutorizador(rol);
	}

	@Override
	public List<Usuarios> listaUsuariosPorAreaLista(long id_Area) {
		// TODO Auto-generated method stub
		return usuarioDao.findByarea(id_Area);
	}

	@Override
	public Page<Usuarios> listaPeticionesCrearUsuarioadminPagin(String EstadoUsuario, Pageable pageable) {
		// TODO Auto-generated method stub
		return usuarioDaoPagin.traerPeticionesCrearUsuario(EstadoUsuario, pageable);
	}

	@Override
	public Page<Usuarios> listaPeticionesContra(String estadoContra, Pageable pageable) {
		// TODO Auto-generated method stub
		return usuarioDaoPagin.traerPeticionesContra(estadoContra, pageable);
	}

	
	



	



}
