package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.example.demo.Dao.RolDao;
import com.example.demo.Entity.Rol;

@Repository
@Service
public class RolServiceImpl implements RolService{
	
	@Autowired
	private RolDao roldao;

	@Override
	public List<Rol> listaRoles() {
		// TODO Auto-generated method stub
		return (List<Rol>) roldao.findAll();
	}

	@Override
	public Rol GuardarRol(Rol rol) {
		// TODO Auto-generated method stub
		return roldao.save(rol);
	}

	@Override
	public Rol BuscarRol(long id) {
		// TODO Auto-generated method stub
		return roldao.findById(id).orElse(null);
	}

	@Override
	public void EliminarRol(Long id) {
		
		roldao.deleteById(id);
		
	}

	@Override
	public List<Rol> BuscarRolNombre(String rol) {
		// TODO Auto-generated method stub
		return roldao.BuscarRolNombre(rol);
	}



}
