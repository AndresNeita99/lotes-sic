package com.example.demo.service;


import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.example.demo.Entity.ValorCuentasPYG;


public interface ValorCuentasPYGService {
	
	public List<ValorCuentasPYG> listaCuentasPYG ();
	
	public void  eliminarCuentaPYG (long id);
	
	public ValorCuentasPYG BuscarCuentaPYG (long id);
	
	public ValorCuentasPYG GuardarCuentaPYG (ValorCuentasPYG ValorCuentasPYG);
	
	public Page<ValorCuentasPYG> listaCuentasPYpPagin (Pageable pageable);
	
	public ValorCuentasPYG valorcuentaPyg (String Estado , int tipoCuenta);
	
	public List<ValorCuentasPYG> listaPYGactivas (String estado);

}
