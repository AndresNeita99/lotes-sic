package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.example.demo.Dao.LogDao;
import com.example.demo.Dao.LogPaginDao;
import com.example.demo.Entity.Log;

@Service
public class LogServiceImpl implements LogService{
	
	@Autowired
	private LogDao logdao;
	
	@Autowired
	private LogPaginDao logPagin;

	@Override
	public List<Log> listaLogs() {
		// TODO Auto-generated method stub
		return (List<Log>) logdao.findAll();
	}

	@Override
	public Log BuscarLog(String id) {
		// TODO Auto-generated method stub
		return logdao.findById(id).orElse(null);
	}

	@Override
	public Log guardarLog(Log Log) {
		// TODO Auto-generated method stub
		return logdao.save(Log);
	}

	@Override
	public Page<Log> listaLosg(Pageable pageable) {
		// TODO Auto-generated method stub
		return logPagin.findAll(pageable);
	}

	

}
