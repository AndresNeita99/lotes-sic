package com.example.demo.service;

import java.util.List;

import com.example.demo.Entity.ResumenLote;

public interface ResumenLoteService {
	
	public List<ResumenLote> mostrarCuentasLote ();
	
	public ResumenLote guardarResumenCuenta(ResumenLote resumenCuenta);
	
	public ResumenLote buscarCuentaResumen (String cuenta);
	
	public void EliminarCuenta (long id);
	
	public List<ResumenLote> mostrarResumen (String IdLote);
	
	public ResumenLote TraerCuentaResumen (String cuenta , String id);
	
	
}
