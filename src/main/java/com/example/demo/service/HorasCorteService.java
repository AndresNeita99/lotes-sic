package com.example.demo.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.example.demo.Entity.HorasCorte;



public interface HorasCorteService {
	
	public List<HorasCorte> listasHorasCorte();
	
	public HorasCorte GuardarHoraCorte (HorasCorte HorasCorte);
	
	public HorasCorte BuscarHoraid(Long id);
	
	public void EliminarHoraCorte (Long id );
	
	public int NumeroHorasActivas (String estado);
	
	public List<HorasCorte> horasCorte ();
	
	public Page<HorasCorte> ListaHorasCortePagin(Pageable pageable);

}
