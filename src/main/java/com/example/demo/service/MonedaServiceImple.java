package com.example.demo.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import com.example.demo.Dao.MonedaParametroDao;
import com.example.demo.Dao.MonedaParametroDaoPagin;
import com.example.demo.Entity.Moneda;

@Service
@Repository
public class MonedaServiceImple implements MonedaService{
	
	
	@Autowired
	private MonedaParametroDao monedaDao;
	
	@Autowired
	private MonedaParametroDaoPagin monedaPaginDao;

	@Override
	public List<Moneda> listaMonedas() {
		// TODO Auto-generated method stub
		return (List<Moneda>) monedaDao.findAll();
	}

	@Override
	public Moneda GuardarMoneda(Moneda moneda) {
		// TODO Auto-generated method stub
		return monedaDao.save(moneda);
	}

	@Override
	public Moneda findOne(long id) {
		// TODO Auto-generated method stub
		return monedaDao.findById(id).orElse(null);
	}

	@Override
	public void EliminarMoneda(Long id) {
		
		monedaDao.deleteById(id);
		
	}

	@Override
	public void ActualizarModenaCargaMasiva() {
		
		monedaDao.deleteAll();
		
	}

	@Override
	public List<Moneda> BuscarMoneda(int moneda) {
		// TODO Auto-generated method stub
		return monedaDao.findByMoneda(moneda);
	}

	@Override
	public Page<Moneda> listaMonedasPagin(Pageable pageable) {
		// TODO Auto-generated method stub
		return monedaPaginDao.findAll(pageable);
	}

	@Override
	public List<Moneda> listaMonedaHabilitadas(String Estado, int Moneda) {
		// TODO Auto-generated method stub
		return monedaDao.ListaMonedasHabiilta(Estado, Moneda);
	}

	@Override
	public List<Moneda> listaMonedasActivas(String Estado) {
		// TODO Auto-generated method stub
		return monedaDao.listaMonedasActivas(Estado);
	}

	@Override
	public Moneda buscarMoneda(int moneda) {
		// TODO Auto-generated method stub
		return monedaDao.encontrarMoneda(moneda);
	}

	
}
