package com.example.demo.service;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.example.demo.Dao.ValorCuentasPYGDao;
import com.example.demo.Dao.ValoresCuentaPYGDaoPagin;
import com.example.demo.Entity.ValorCuentasPYG;

@Service
@Repository
public class ValorCuentasPYGServiceImpl implements ValorCuentasPYGService{
	
	@Autowired
	private ValorCuentasPYGDao valorCuentaPYGdao;
	
	@Autowired
	private ValoresCuentaPYGDaoPagin valorCuentasPYGpagin;

	@Override
	public List<ValorCuentasPYG> listaCuentasPYG() {
		// TODO Auto-generated method stub
		return   (List<ValorCuentasPYG>) valorCuentaPYGdao.findAll();
	}

	@Override
	public void eliminarCuentaPYG(long TipoCuenta) {
		// TODO Auto-generated method stub
		 valorCuentaPYGdao.deleteById(TipoCuenta);
	}

	@Override
	public ValorCuentasPYG BuscarCuentaPYG(long id) {
		// TODO Auto-generated method stub
		return valorCuentaPYGdao.findByid(id);
	}

	@Override
	public ValorCuentasPYG GuardarCuentaPYG(ValorCuentasPYG ValorCuentasPYG) {
		// TODO Auto-generated method stub
		return valorCuentaPYGdao.save(ValorCuentasPYG);
	}

	@Override
	public Page<ValorCuentasPYG> listaCuentasPYpPagin(Pageable pageable) {
		// TODO Auto-generated method stub
		return valorCuentasPYGpagin.findAll(pageable);
	}

	@Override
	public ValorCuentasPYG valorcuentaPyg(String Estado, int tipoCuenta) {
		// TODO Auto-generated method stub
		return valorCuentaPYGdao.listaCuetas(Estado, tipoCuenta);
	}

	@Override
	public List<ValorCuentasPYG> listaPYGactivas(String estado) {
		// TODO Auto-generated method stub
		return valorCuentaPYGdao.listaCuentasActivas(estado);
	}

	

	

}
