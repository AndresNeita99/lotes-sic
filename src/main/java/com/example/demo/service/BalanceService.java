package com.example.demo.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.example.demo.Entity.Balance;


public interface BalanceService {
	
	public List<Balance> ListaCuentasBalance();

	public  Balance GuardarCuentaBalance (Balance balance);
	
	public Page <Balance> ListaBalancePagin (Pageable pageable);
	
	public List<Balance> BuscarCuentaBalance (String cuenta);
	
	public void eliminarBalanceAnterior ();
	
	

}
