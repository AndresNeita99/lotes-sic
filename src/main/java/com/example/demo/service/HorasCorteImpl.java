package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import com.example.demo.Dao.HoraCorteoDao;
import com.example.demo.Dao.HorasCorteDaoPagin;
import com.example.demo.Entity.HorasCorte;


@Service
@Repository
public class HorasCorteImpl implements HorasCorteService{

	@Autowired
	private HoraCorteoDao horaCorteDao;
	
	@Autowired
	private HorasCorteDaoPagin horascortePagin;
	
	@Override
	public List<HorasCorte> listasHorasCorte() {
		// TODO Auto-generated method stub
		return (List<HorasCorte>) horaCorteDao.findAll();
	}

	@Override
	public HorasCorte GuardarHoraCorte(HorasCorte HorasCorte) {
		// TODO Auto-generated method stub
		return horaCorteDao.save(HorasCorte);
	}

	@Override
	public HorasCorte BuscarHoraid(Long id) {
		// TODO Auto-generated method stub
		return horaCorteDao.findById(id).orElse(null);
	}

	@Override
	public void EliminarHoraCorte(Long id) {
		
		horaCorteDao.deleteById(id);
		
	}

	@Override
	public int NumeroHorasActivas(String estado) {
		// TODO Auto-generated method stub
		return horaCorteDao.numerHorasoActivas(estado);
	}

	@Override
	public List<HorasCorte> horasCorte() {
		// TODO Auto-generated method stub
		return horaCorteDao.HorasCorte();
	}

	@Override
	public Page<HorasCorte> ListaHorasCortePagin(Pageable pageable) {
		// TODO Auto-generated method stub
		return horascortePagin.findAll(pageable);
	}

	

}
