package com.example.demo.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;

import com.example.demo.Entity.SolicitudesCrearUsuario;

public interface SolicitudesCrearUsuarioService {
	
	public Page<SolicitudesCrearUsuario> ListaSolicitudesUsuario(String Usuario , Pageable pageable);
	
	public Page<SolicitudesCrearUsuario> ListaSolicitudesRechazadasUsua(String usuario , String estado , Pageable pageable);
	
	public Page<SolicitudesCrearUsuario> ListaSolicitudesAproUsu(String usuario , String estado , Pageable pageable);
	
	public Page<SolicitudesCrearUsuario> listasolicitudesUsuario(Pageable pageable);
	
	public Page <SolicitudesCrearUsuario> listaSolcitudesRechazadas (String estado , Pageable pageable);
	
	public Page <SolicitudesCrearUsuario> listaSolicitudesAproba(String estado , Pageable pageable);
	
	public Page<SolicitudesCrearUsuario> listaSolicitudesBloquear(String Tipo , String estado , Pageable pageable);
	
	public Page<SolicitudesCrearUsuario> listaSolicitudesBloquearAutori(String Tipo , String estado , String usuario , Pageable pageable);
	
	public void eliminarPeticion (Long id);
	
	public void GuardarSolicitud (SolicitudesCrearUsuario solicitudesCrearUsuario);
	
	public SolicitudesCrearUsuario VerDetalles (Long id);
	
	public SolicitudesCrearUsuario traerSolicitud(Long id);
	
	public List<SolicitudesCrearUsuario> cantidadSolicitudes (String estado);

}
