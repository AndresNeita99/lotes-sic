package com.example.demo.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.example.demo.Entity.Log;

public interface LogService {
	
	public List<Log> listaLogs ();
	
	public Log BuscarLog (String id);
	
	public Log guardarLog(Log Log);
	
	public Page<Log> listaLosg (Pageable pageable);
	

}
