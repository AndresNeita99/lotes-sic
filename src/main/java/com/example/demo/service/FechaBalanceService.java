package com.example.demo.service;

import java.util.List;

import com.example.demo.Entity.FechaBalance;

public interface FechaBalanceService {
	
	public FechaBalance guardarFecha(FechaBalance fechaBalance);
	
	public List<FechaBalance> MostrarFechaBalance();

}
