package com.example.demo.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.example.demo.Entity.CentroCostos;
import com.example.demo.Entity.CuentasExceptuadas;

public interface CuentasExceptuadasService {
	
	public CuentasExceptuadas guardarCuentaExcep(CuentasExceptuadas cuentaexep);
	
	public CuentasExceptuadas BuscarCuentaExep (long id);
	
	public Page<CuentasExceptuadas> listaCuentasExcepci(Pageable pageable);
	
	public CuentasExceptuadas TraerCuentaExcep (String cuenta);
	
	public List<CuentasExceptuadas> listaCuentasExcep();
	
	public void ActualizarCuentasExcep();
	
	public void EliminarCuentaExcep (Long id);
	
	public List<CuentasExceptuadas> listaCuentasExcepHabilitadas (String Estado , String NumeroCuenta);
	
	public List<CuentasExceptuadas> listaCuentasActivas (String Estado);
	
	
	

}
