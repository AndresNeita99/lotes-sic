package com.example.demo.service;

import java.util.List;

import com.example.demo.Entity.Mensaje;


public interface MensajeService {

	
	public Mensaje BuscarMensajeActual(long id);
	
	public void EliminarMensaje(Long id);
	
	public Mensaje enviarMensaje(Mensaje mensaje);
	
	public List <Mensaje> ListaMisMensajes(String usuario);
	
	public List<Mensaje> listaMensajesNoLeidos(String usuario , String estado);
	
	public List<Mensaje> listaMensajes();
	
	public void EliminarTodosMensajes();
	
	
	
	
}
