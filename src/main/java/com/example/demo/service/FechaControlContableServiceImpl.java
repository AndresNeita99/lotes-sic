package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.example.demo.Dao.FechaControlContableDao;
import com.example.demo.Dao.FechaControlContableDaoPagin;
import com.example.demo.Entity.FechaControlContable;



@Repository
@Service
public class FechaControlContableServiceImpl implements FechaControlContableService{
	
	
	@Autowired
	private FechaControlContableDao fechaContableDao;
	
	@Autowired
	private FechaControlContableDaoPagin fechaControlContablePagin;

	@Override
	public FechaControlContable GuardarFechaContable(FechaControlContable fechaContable) {
		// TODO Auto-generated method stub
		return fechaContableDao.save(fechaContable);
	}

	@Override
	public List<FechaControlContable> listaFechasContables() {
		// TODO Auto-generated method stub
		return (List<FechaControlContable>) fechaContableDao.findAll();
	}

	@Override
	public FechaControlContable buscarFechaContable(long id) {
		// TODO Auto-generated method stub
		return fechaContableDao.findById(id).orElse(null);
	}

	@Override
	public void EliminarFechaContable(long id) {
		// TODO Auto-generated method stub
		fechaContableDao.deleteById(id);
		
	}

	@Override
	public List<FechaControlContable> buscarFechasActivas(String estado) {
		// TODO Auto-generated method stub
		return fechaContableDao.findByEstado(estado);
	}

	@Override
	public Page<FechaControlContable> ListaFechasContablesPagin(Pageable pageable) {
		// TODO Auto-generated method stub
		return fechaControlContablePagin.findAll(pageable);
	}

	

	
	
	





	

}
