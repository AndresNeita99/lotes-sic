package com.example.demo.service;

import java.util.List;

import com.example.demo.Entity.FechaProcesoMensual;

public interface FechaProcesoMensualService {
	
	public List<FechaProcesoMensual> listarFechasProcesoMensual ();
	
	public FechaProcesoMensual guardarFechaProcesoMensual (FechaProcesoMensual fechaProcesoMensual);
	
	public void EliminarFechaProcesoMensual (Long id);
	
	public FechaProcesoMensual BuscarFechaProcesoMensualId (Long id);

}
