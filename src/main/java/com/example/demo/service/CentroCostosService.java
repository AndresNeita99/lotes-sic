package com.example.demo.service;

import java.util.List;

import com.example.demo.Entity.CentroCostos;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


public interface CentroCostosService {
	
	public List<CentroCostos> listaCostos();

	public CentroCostos GuardarCentroCostos(CentroCostos centrocostos);

	public CentroCostos findOne(long id);
	
	public List<CentroCostos> BuscarCentroCostos(int CentroCostos);
	
	public void EliminarCentroCosto (long id);
	
	public CentroCostos CentroCostosBuscar (int centroCostos);
	
	public void ActualizarCargarMasivaCentroCostos ();
	
	public Page <CentroCostos> listaCentroCostosPagina(Pageable pageable);
	
	public List<CentroCostos> listaCentroCostosHabilitados(String Estado , int centroCostos);
	
	public List<CentroCostos> listaCentrocostosActivos (String Estado);
	
	

}
