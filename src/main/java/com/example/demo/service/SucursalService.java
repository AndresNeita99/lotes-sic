package com.example.demo.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.example.demo.Entity.Sucursal;

public interface SucursalService {

	public List<Sucursal> listaSucursales();

	public Sucursal GuardarSucursal(Sucursal sucursal);

	public Sucursal findOne(long id);
	
	public void eliminarSucursal (Long id);
	
	public void ActualizarCargaMasivaSucursal ();

	public List<Sucursal> BuscarSucursal(int sucursal);
	
	public Page <Sucursal> ListaAreaPagina (Pageable pageable);
	
	public List<Sucursal> listaSucursalesActivas(String Estado);
	
	public Sucursal TraerSucursal (int Sucursal);

}
