package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.example.demo.Dao.MensajeDao;
import com.example.demo.Entity.Mensaje;


@Service
@Repository
public class MensajeServiceImpl implements MensajeService{
	
	@Autowired
	private MensajeDao mensajeDao;

	

	@Override
	public void EliminarMensaje(Long id) {
		
		mensajeDao.deleteById(id);
		
	}

	@Override
	public Mensaje enviarMensaje(Mensaje mensaje) {
		// TODO Auto-generated method stub
		return mensajeDao.save(mensaje);
	}

	@Override
	public List<Mensaje> ListaMisMensajes(String usuario) {
		// TODO Auto-generated method stub
		return mensajeDao.MisMensajes(usuario);
	}

	@Override
	public List<Mensaje> listaMensajesNoLeidos(String usuario, String estado) {
		// TODO Auto-generated method stub
		return mensajeDao.MisMensajesNoleidos(usuario, estado);
	}

	@Override
	public Mensaje BuscarMensajeActual(long id) {
		// TODO Auto-generated method stub
		return mensajeDao.findById(id).orElseThrow();
	}

	@Override
	public List<Mensaje> listaMensajes() {
		// TODO Auto-generated method stub
		return (List<Mensaje>) mensajeDao.findAll();
	}

	@Override
	public void EliminarTodosMensajes() {
		
		mensajeDao.deleteAll();
		
	}

}
