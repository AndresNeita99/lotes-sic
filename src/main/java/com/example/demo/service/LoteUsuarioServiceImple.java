package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import com.example.demo.Dao.LoteUsuarioDao;
import com.example.demo.Dao.LoteUsuarioPaginDao;
import com.example.demo.Entity.LoteUsuario;

@Service
@Repository
public class LoteUsuarioServiceImple implements LoteUsuerioService{

	@Autowired
	private LoteUsuarioDao loteUsuarioDao;
	
	@Autowired
	private LoteUsuarioPaginDao loteUsuarioPaginDao;
	
	@Override
	public List<LoteUsuario> listaLoteUsuario() {
		// TODO Auto-generated method stub
		return (List<LoteUsuario>) loteUsuarioDao.findAll();
	}

	@Override
	public void EliminarLote(String id) {
		
		loteUsuarioDao.deleteById(id);
		
	}

	@Override
	public LoteUsuario guardarLote(LoteUsuario loteUsuario) {
		// TODO Auto-generated method stub
		return loteUsuarioDao.save(loteUsuario);
	}

	@Override
	public LoteUsuario buscarLoteId(String idLote) {
		// TODO Auto-generated method stub
		return loteUsuarioDao.findById(idLote).orElse(null);
	}

	@Override
	public List<LoteUsuario> BuscarLotesAutorizador(String nombreAutorizador) {
		// TODO Auto-generated method stub
		return loteUsuarioDao.BuscarNombreAutorizador(nombreAutorizador);
	}

	@Override
	public List<LoteUsuario> BuscarLotesAprobados(String EstadoLote, String NombreAutorizador) {
		// TODO Auto-generated method stub
		return loteUsuarioDao.BuscarEstadoLote(EstadoLote, NombreAutorizador);
	}

	@Override
	public List<LoteUsuario> BuscarLotesNoAutorizados(String EstadoLote, String NombreAutorizador) {
		// TODO Auto-generated method stub
		return loteUsuarioDao.BuscarEstadoLote(EstadoLote, NombreAutorizador);
	}

	@Override
	public List<LoteUsuario> MostrarLotesUsuario(String nombreAnalista) {
		// TODO Auto-generated method stub
		return loteUsuarioDao.BuscarNombreNombreAnalista(nombreAnalista);
	}

	@Override
	public List<LoteUsuario> BuscarEstadoLote(String EstadoLote, String NombreAnalista) {
		// TODO Auto-generated method stub
		return loteUsuarioDao.BuscarEstadoLoteUsuario(EstadoLote, NombreAnalista);
	}

	@Override
	public List<LoteUsuario> LotesAutorizado(String Estado) {
		// TODO Auto-generated method stub
		return loteUsuarioDao.lotesAutorizados(Estado);
	}

	@Override
	public Page<LoteUsuario> listaLotesUsuario(String AnalistaContable, Pageable pageable) {
		// TODO Auto-generated method stub
		return loteUsuarioPaginDao.findAllByanalista(AnalistaContable, pageable);
	}

	@Override
	public LoteUsuario TraerLoteAutorizado(String EstadoLote) {
		// TODO Auto-generated method stub
		return loteUsuarioDao.TraerLoteTrasformar(EstadoLote);
	}
	
	/* ------------------- consulta modulo EstadoLote ------------- */

	@Override
	public Page<LoteUsuario> listaLotes(Pageable pageable) { // consulta para roles administrador, centralizadores y Director Contable 
		// TODO Auto-generated method stub
		return loteUsuarioPaginDao.findAll(pageable);
	}

	@Override
	public Page<LoteUsuario> listaLotesUsuarioFecha(String Fecha,  Pageable pageable) { 
		// TODO Auto-generated method stub
		return loteUsuarioPaginDao.BuscarLotesfechaProceso(Fecha,  pageable);
	}

	@Override
	public Page<LoteUsuario> listaLotesUsuarioIdlote(String idlote, Pageable pageable) {
		// TODO Auto-generated method stub
		return loteUsuarioPaginDao.BuscarLotesId(idlote, pageable);
	}



	@Override
	public List<LoteUsuario> TraerLotesAutorizadosHora(String Estadolote) {
		// TODO Auto-generated method stub
		return loteUsuarioDao.TrearLotesAutorizados(Estadolote);
	}

	@Override
	public List<LoteUsuario> TrearLotePlano(String idlote) {
		// TODO Auto-generated method stub
		return loteUsuarioDao.TraerLostesPlano(idlote);
	}

	@Override
	public Page<LoteUsuario> listaLotesAutorizador(String AutorizadorContable, String estado, Pageable pageable) {
		// TODO Auto-generated method stub
		return loteUsuarioPaginDao.findAllByautorizador(AutorizadorContable, estado, pageable);
	}

	@Override
	public Page<LoteUsuario> listaLotesAutorizadorfecha(String AutorizadorContable, Pageable pageable) {
		// TODO Auto-generated method stub
		return loteUsuarioPaginDao.lotesautorizadorfecha(AutorizadorContable, pageable);
	}

	@Override
	public Page<LoteUsuario> LostesAprobados(String Estado, Pageable pageable) {
		// TODO Auto-generated method stub
		return loteUsuarioPaginDao.findAllByEstadoLote(Estado, pageable);
	}

	@Override
	public Page<LoteUsuario> loteusuarioFitral(String analista, String Autorizador, String fecha1, String fecha2, Pageable pageable) {
		// TODO Auto-generated method stub
		return loteUsuarioPaginDao.FiltradoLote(analista, Autorizador, fecha1, fecha2, pageable);
	}

	@Override
	public Page<LoteUsuario> LotesFecha(String fecha1, String fecha2, Pageable pageable) {
		// TODO Auto-generated method stub
		return loteUsuarioPaginDao.LotesFecha(fecha1, fecha2, pageable);
	}

	@Override
	public Page<LoteUsuario> lostesByAutorizadorInforme(String Autorizador, Pageable pageable) {
		// TODO Auto-generated method stub
		return loteUsuarioPaginDao.loteAutorizadorInforme(Autorizador, pageable);
	}

	@Override
	public Page<LoteUsuario> lostesByAnalistaInforme(String Autorizador, Pageable pageable) {
		// TODO Auto-generated method stub
		return loteUsuarioPaginDao.loteAnalistaInforme(Autorizador, pageable);
	}

	@Override
	public Page<LoteUsuario> LotesAutorizadorByfechaProceso(String Autorizador, String FechaProceso, Pageable pageable) {
		// TODO Auto-generated method stub
		return loteUsuarioPaginDao.LoteAutorizadorByFechaProceso(Autorizador, FechaProceso, pageable);
	}

	@Override
	public Page<LoteUsuario> LotesAnalistaByfechaProceso(String Analista, String FechaProceso, Pageable pageable) {
		// TODO Auto-generated method stub
		return loteUsuarioPaginDao.LoteAAnalistaByFechaProceso(Analista, FechaProceso, pageable);
	}

	


}
