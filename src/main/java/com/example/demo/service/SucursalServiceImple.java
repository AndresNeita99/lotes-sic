package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
//import org.springframework.security.core.userdetails.UserDetails;
//import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.example.demo.Dao.SucursalDao;
import com.example.demo.Dao.SucursalPaginDao;
import com.example.demo.Entity.Sucursal;

@Service
@Repository
public class SucursalServiceImple implements SucursalService{
	
	@Autowired
	private SucursalDao sucursalDao;
	
	@Autowired
	private SucursalPaginDao sucursalPaginDao;

	@Override
	public List<Sucursal> listaSucursales() {
		// TODO Auto-generated method stub
		return (List<Sucursal>) sucursalDao.findAll();
	}

	@Override
	public Sucursal GuardarSucursal(Sucursal sucursal) {
		// TODO Auto-generated method stub
		return sucursalDao.save(sucursal);
	}

	@Override
	public Sucursal findOne(long id) {
		// TODO Auto-generated method stub
		return sucursalDao.findById(id).orElse(null);
	}

	@Override
	public void eliminarSucursal(Long id) {
		
		sucursalDao.deleteById(id);
		
	}

	@Override
	public void ActualizarCargaMasivaSucursal() {
		
		sucursalDao.deleteAll();
		
	}

	@Override
	public List<Sucursal> BuscarSucursal(int sucursal) {
		
		
		return sucursalDao.findBySucursal(sucursal);
	}

	@Override
	public Page<Sucursal> ListaAreaPagina(Pageable pageable) {
		// TODO Auto-generated method stub
		return sucursalPaginDao.findAll(pageable);
	}

	@Override
	public List<Sucursal> listaSucursalesActivas(String Estado) {
		// TODO Auto-generated method stub
		return sucursalDao.listaSucursalesActivas(Estado);
	}

	@Override
	public Sucursal TraerSucursal(int Sucursal) {
		// TODO Auto-generated method stub
		return sucursalDao.BuscarSucursal(Sucursal);
	}


}
