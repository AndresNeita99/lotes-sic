package com.example.demo.service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


import com.example.demo.Entity.Fuente;

public interface fuenteService {
	
	// guardar fuente
	public Fuente GuardarFuente (Fuente fuente);
	
	// listar fuentes 
	public List<Fuente> listarFuente();
	
	// actualizar fuentes
	public Optional <Fuente> actualizarFuente (Long id);
	
	// desabilitar 
	
	public void eliminarFuente (Long id);

	public List<Fuente> encontrarFuente (String fuente);
	
	public Fuente findOne(Long id);
	
	public Fuente BuscarFuente (String Fuente);

	void ActualizarCargarMasivaFuente();
	
	public Page <Fuente> ListaFuentePagina (Pageable pageable);
	
	public List<Fuente> listafuentesHabilitadas (String Estado , String Fuente);
	
	public List<Fuente> listaFuentesActivas (String Estado);
	
	

}
