package com.example.demo.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.example.demo.Entity.TRM;

public interface TrmServices {
	
	public List<TRM> listaValoresTrm ();
	
	public TRM guardarTrm (TRM trm);
	
	public void eliminarTrm (Long id);
	
	public TRM BuscaarTrm (Long id);
	
	public TRM buscarTrmActiva (String estado);
	
	public Page<TRM> ListaTrmPagin (Pageable pageable);

}
