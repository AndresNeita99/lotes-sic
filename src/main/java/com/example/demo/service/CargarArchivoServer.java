package com.example.demo.service;


import java.nio.file.Path;
import java.util.stream.Stream;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

public interface CargarArchivoServer {

	public void init(String rutaArchivo);

	  public void save(MultipartFile file , String NombreArchivo);

	  public Resource load(String filename , String NombreArchivo);

	  public void deleteAll(String Rutacarpeta);

	  public Stream<Path> loadAll(String RutaCarpeta);
}
