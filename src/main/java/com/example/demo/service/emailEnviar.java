package com.example.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import jakarta.mail.internet.MimeMessage;

@Service
public class emailEnviar {

	@Autowired
	private JavaMailSender mailSender;

	public void sendEmail(final String subject, final String message, final String fromEmailAddress , final String toEmailAddresses, final boolean isHtmlMail) {
		
		try {
			
			MimeMessage mimeMessage = mailSender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);
			helper.setFrom(fromEmailAddress);
			helper.setTo(toEmailAddresses);
			helper.setSubject(subject);
			
			if (isHtmlMail) {
				helper.setText("<html><body>" + message + "</html></body>", true);
				//helper.setText("<html><body><img src='cid:respuesta'></body></html>", true);
				//helper.addInline("respuesta", new ClassPathResource("/CargaLotesMasivosAppweb/src/main/resources/static/Img/logo2.PNG"));
			} else {
				helper.setText(message);
			}
			
			// attach the file into email body
			
			
			mailSender.send(mimeMessage);
			
			System.out.println("Email sending complete.");
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
