package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.example.demo.Dao.DebitoCreditoParametroDao;
import com.example.demo.Dao.DebitoCreditoParametroDaoPagin;
import com.example.demo.Entity.DebitoCredito;

@Repository
@Service
public class DebitoCreditoImple implements DebitoCreditoService{
	
	@Autowired
	private DebitoCreditoParametroDao debitoCreditoDao;
	
	@Autowired
	private DebitoCreditoParametroDaoPagin debitocreditoPagin;

	@Override
	public List<DebitoCredito> listaDebitoCredito() {
		// TODO Auto-generated method stub
		return (List<DebitoCredito>) debitoCreditoDao.findAll();
	}

	@Override
	public DebitoCredito guardarDebitoCredito(DebitoCredito debitocredito) {
		// TODO Auto-generated method stub
		return debitoCreditoDao.save(debitocredito);
	}

	@Override
	public DebitoCredito encontrarId(Long id) {
		// TODO Auto-generated method stub
		return debitoCreditoDao.findById(id).orElse(null);
	}

	@Override
	public void EliminarDebitoCredito(Long id) {
		
		debitoCreditoDao.deleteById(id);
		
	}

	@Override
	public void ActualizarDebitoCreditoCargaMasiva() {
		
		debitoCreditoDao.deleteAll();
		
	}

	@Override
	public List<DebitoCredito> buscarDebitoCredito(int debitoCretido) {
		// TODO Auto-generated method stub
		return debitoCreditoDao.findByvalor(debitoCretido);
	}

	@Override
	public Page<DebitoCredito> listaDebitoCreditoPagin(Pageable pageable) {
		// TODO Auto-generated method stub
		return debitocreditoPagin.findAll(pageable);
	}

	@Override
	public DebitoCredito traerDebitoCredito(int valor) {
		// TODO Auto-generated method stub
		return debitoCreditoDao.findByvalor2(valor);
	}


}
