package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.example.demo.Dao.CuentasExceptuadasDao;
import com.example.demo.Dao.CuentasExceptuadasDaoPagin;
import com.example.demo.Entity.CuentasExceptuadas;

@Service
public class CuentasExceptuadasImpl implements CuentasExceptuadasService{
	
	@Autowired
	private CuentasExceptuadasDao cuentasDao;
	
	@Autowired
	private CuentasExceptuadasDaoPagin cuentasExepcioPagin;

	@Override
	public CuentasExceptuadas guardarCuentaExcep(CuentasExceptuadas cuentaexep) {
		// TODO Auto-generated method stub
		return cuentasDao.save(cuentaexep);
	}

	@Override
	public CuentasExceptuadas BuscarCuentaExep(long id) {
		// TODO Auto-generated method stub
		return cuentasDao.findById(id).orElse(null);
	}

	@Override
	public Page<CuentasExceptuadas> listaCuentasExcepci(Pageable pageable) {
		// TODO Auto-generated method stub
		return cuentasExepcioPagin.findAll(pageable);
	}

	@Override
	public CuentasExceptuadas TraerCuentaExcep(String cuenta) {
		// TODO Auto-generated method stub
		return cuentasDao.TreaerCuenta(cuenta);
	}

	@Override
	public List<CuentasExceptuadas> listaCuentasExcep() {
		// TODO Auto-generated method stub
		return cuentasDao.listaCuentas();
	}

	@Override
	public void ActualizarCuentasExcep() {
		
		cuentasDao.deleteAll();
		
	}

	@Override
	public void EliminarCuentaExcep(Long id) {
		
		cuentasDao.deleteById(id);
		
	}

	@Override
	public List<CuentasExceptuadas> listaCuentasExcepHabilitadas(String Estado, String NumeroCuenta) {
		// TODO Auto-generated method stub
		return cuentasDao.listaCuentasExcepHabili(Estado, NumeroCuenta);
	}

	@Override
	public List<CuentasExceptuadas> listaCuentasActivas(String Estado) {
		// TODO Auto-generated method stub
		return cuentasDao.listaCuentasExcepActivas(Estado);
	}



}
