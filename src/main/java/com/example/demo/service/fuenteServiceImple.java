package com.example.demo.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.example.demo.Dao.FuentePaginDao;
import com.example.demo.Dao.FuenteParametroDao;
import com.example.demo.Entity.Fuente;

import jakarta.transaction.Transactional;

@Service
@Repository
public class fuenteServiceImple implements fuenteService{
	
	
	@Autowired
	private FuenteParametroDao fuenteParametroDao;
	
	@Autowired
	private FuentePaginDao fuentePaginDao;

	@Override
	public Fuente GuardarFuente(Fuente fuente) {
		// TODO Auto-generated method stub
		return fuenteParametroDao.save(fuente);
	}

	@Override
	public List<Fuente> listarFuente() {
		// TODO Auto-generated method stub
		return (List<Fuente>) fuenteParametroDao.findAll();
	}

	@Override
	public Optional<Fuente> actualizarFuente(Long id) {
		// TODO Auto-generated method stub
		return fuenteParametroDao.findById(id);
	}

	@Override
	@Transactional()
	public Fuente findOne(Long id) {
		// TODO Auto-generated method stub
		return fuenteParametroDao.findById(id).orElse(null);
	}

	@Override
	public void eliminarFuente(Long id) {
		// TODO Auto-generated method stub
		fuenteParametroDao.deleteById(id);
	}

	@Override
	public void ActualizarCargarMasivaFuente() {
		
		fuenteParametroDao.deleteAll();
		
	}

	@Override
	public List<Fuente> encontrarFuente(String fuente) {
		// TODO Auto-generated method stub
		return fuenteParametroDao.findByFuenteParametro(fuente);
	}

	@Override
	public Page<Fuente> ListaFuentePagina(Pageable pageable) {
		// TODO Auto-generated method stub
		return fuentePaginDao.findAll(pageable);
	}

	@Override
	public List<Fuente> listafuentesHabilitadas(String Estado, String Fuente) {
		// TODO Auto-generated method stub
		return fuenteParametroDao.ListaFuentesHabilitadas(Estado, Fuente);
	}

	@Override
	public List<Fuente> listaFuentesActivas(String Estado) {
		// TODO Auto-generated method stub
		return fuenteParametroDao.fuentesActivas(Estado);
	}

	@Override
	public Fuente BuscarFuente(String Fuente) {
		// TODO Auto-generated method stub
		return fuenteParametroDao.findByFuente(Fuente);
	}
	
	

	





}
