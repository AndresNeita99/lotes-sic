package com.example.demo.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.example.demo.Entity.DebitoCredito;

public interface DebitoCreditoService {

	public List<DebitoCredito> listaDebitoCredito();
	
	public DebitoCredito guardarDebitoCredito (DebitoCredito debitocredito);
	
	public DebitoCredito encontrarId (Long id);
	
	public void EliminarDebitoCredito (Long id);
	
	public void ActualizarDebitoCreditoCargaMasiva ();
	
	public List <DebitoCredito> buscarDebitoCredito(int debitoCretido);
	
	public Page<DebitoCredito> listaDebitoCreditoPagin (Pageable pageable);
	
	public DebitoCredito traerDebitoCredito (int valor);

	
}
