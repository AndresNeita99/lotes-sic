package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.example.demo.Dao.TrmDao;
import com.example.demo.Dao.TrmDaoPagin;
import com.example.demo.Entity.TRM;

@Service
public class TRMServiceImpl implements TrmServices{
	
	@Autowired
	private TrmDao trmDao;
	
	@Autowired
	private TrmDaoPagin trmDaoPagin;

	@Override
	public List<TRM> listaValoresTrm() {
		// TODO Auto-generated method stub
		return (List<TRM>) trmDao.findAll();
	}

	@Override
	public TRM guardarTrm(TRM trm) {
		// TODO Auto-generated method stub
		return trmDao.save(trm);
	}

	@Override
	public void eliminarTrm(Long id) {
		
		trmDao.deleteById(id);
		
	}

	@Override
	public TRM BuscaarTrm(Long id) {
		// TODO Auto-generated method stub
		return trmDao.findById(id).orElse(null);
	}

	@Override
	public TRM buscarTrmActiva(String estado) {
		// TODO Auto-generated method stub
		return trmDao.trmActiva(estado);
	}

	@Override
	public Page<TRM> ListaTrmPagin(Pageable pageable) {
		// TODO Auto-generated method stub
		return trmDaoPagin.findAll(pageable);
	}



}
