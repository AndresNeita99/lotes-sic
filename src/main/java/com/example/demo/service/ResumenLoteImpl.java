package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import com.example.demo.Dao.ResumenLoteDao;
import com.example.demo.Entity.ResumenLote;

@Service
@Repository
public class ResumenLoteImpl implements ResumenLoteService{

	@Autowired
	private ResumenLoteDao resumenLoteDao;
	
	@Override
	public List<ResumenLote> mostrarCuentasLote() {
		// TODO Auto-generated method stub
		return (List<ResumenLote>) resumenLoteDao.findAll();
	}

	@Override
	public ResumenLote guardarResumenCuenta(ResumenLote resumenCuenta) {
		// TODO Auto-generated method stub
		return resumenLoteDao.save(resumenCuenta);
	}

	@Override
	public ResumenLote buscarCuentaResumen(String cuenta) {
		// TODO Auto-generated method stub
		return resumenLoteDao.findByCuenta(cuenta);
	}

	@Override
	public void EliminarCuenta(long id) {
		// TODO Auto-generated method stub
		resumenLoteDao.deleteById(id);
	}

	@Override
	public List<ResumenLote> mostrarResumen(String IdLote) {
		// TODO Auto-generated method stub
		return resumenLoteDao.findBylote(IdLote);
	}

	@Override
	public ResumenLote TraerCuentaResumen(String cuenta, String id) {
		// TODO Auto-generated method stub
		return resumenLoteDao.TraerCuenta(cuenta, id);
	}

	

}
