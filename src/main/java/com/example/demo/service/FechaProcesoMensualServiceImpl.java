package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import com.example.demo.Dao.FechaProcesoMensualDao;
import com.example.demo.Entity.FechaProcesoMensual;


@Repository
@Service
public class FechaProcesoMensualServiceImpl implements FechaProcesoMensualService{

	@Autowired
	private FechaProcesoMensualDao fechaprocesomensualdao;
	
	@Override
	public List<FechaProcesoMensual> listarFechasProcesoMensual() {
		// TODO Auto-generated method stub
		return (List<FechaProcesoMensual>) fechaprocesomensualdao.findAll();
	}

	@Override
	public FechaProcesoMensual guardarFechaProcesoMensual(FechaProcesoMensual fechaProcesoMensual) {
		// TODO Auto-generated method stub
		return fechaprocesomensualdao.save(fechaProcesoMensual);
	}

	@Override
	public void EliminarFechaProcesoMensual(Long id) {
		
		fechaprocesomensualdao.deleteById(id);
		
	}

	@Override
	public FechaProcesoMensual BuscarFechaProcesoMensualId(Long id) {
		// TODO Auto-generated method stub
		return fechaprocesomensualdao.findById(id).orElse(null);
	}

	
	

}
