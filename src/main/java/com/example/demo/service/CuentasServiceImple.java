package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.example.demo.Dao.CuentasParametroDao;
import com.example.demo.Dao.CuentasParametroDaoPagin;
import com.example.demo.Entity.NumeroCuenta;


@Service
@Repository
public class CuentasServiceImple implements CuentasService{
	
	@Autowired
	private CuentasParametroDao cuentasDao;
	
	@Autowired
	private CuentasParametroDaoPagin cuentasDaoPagin;

	@Override
	public List<NumeroCuenta> listaCuentas() {
		// TODO Auto-generated method stub
		return (List<NumeroCuenta>) cuentasDao.findAll();
	}

	@Override
	public NumeroCuenta GuardarCuenta(NumeroCuenta numero) {
		// TODO Auto-generated method stub
		return cuentasDao.save(numero);
	}

	@Override
	public NumeroCuenta findOne(long id) {
		// TODO Auto-generated method stub
		return cuentasDao.findById(id).orElse(null);
	}

	@Override
	public List<NumeroCuenta> BuscarCuenta(String cuenta) {
		// TODO Auto-generated method stub
		return cuentasDao.traerCuenta(cuenta);
	}

	@Override
	public void EliminarCuenta(Long id) {
		
		cuentasDao.deleteById(id);
		
	}

	@Override
	public void ActualizarCuentaCargaMasiva() {
		
		cuentasDao.deleteAll();
		
	}

	@Override
	public Page<NumeroCuenta> ListaCuentasPagin(Pageable pageable) {
		// TODO Auto-generated method stub
		return cuentasDaoPagin.findAll(pageable);
	}

	@Override
	public List<NumeroCuenta> listaCuentasHabilitadas(String Estado, String NumeroCuenta) {
		// TODO Auto-generated method stub
		return cuentasDao.CuentasHabilitadas(Estado, NumeroCuenta);
	}

	@Override
	public List<NumeroCuenta> cuentasActivadas(String Estado) {
		// TODO Auto-generated method stub
		return cuentasDao.cuentasActivas(Estado);
	}

	@Override
	public NumeroCuenta TraerCuenta(String cuenta) {
		// TODO Auto-generated method stub
		return cuentasDao.BuscarCuenta(cuenta);
	}

	
}
