package com.example.demo.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.example.demo.Entity.LoteUsuario;

public interface LoteUsuerioService {
	
	public List<LoteUsuario> listaLoteUsuario ();
	
	public void EliminarLote(String id);
	
	public LoteUsuario guardarLote (LoteUsuario loteUsuario);
	
	public LoteUsuario buscarLoteId (String idLote);
	
	public List<LoteUsuario> BuscarLotesAutorizador (String nombreAutorizador);
	
	public List<LoteUsuario> BuscarLotesAprobados (String EstadoLote , String NombreAutorizador);
	
	public List<LoteUsuario> BuscarLotesNoAutorizados (String EstadoLote , String NombreAutorizador);
	
	public List<LoteUsuario> MostrarLotesUsuario (String nombreAnalista);
	
	public List<LoteUsuario> BuscarEstadoLote(String EstadoLote , String NombreAnalista);
	
	public List<LoteUsuario> LotesAutorizado (String Estado);
	
	Page<LoteUsuario> listaLotes (Pageable pageable);
	
	Page<LoteUsuario> LostesAprobados (String Estado , Pageable pageable);
	
	Page<LoteUsuario> listaLotesUsuario (String AnalistaContable , Pageable pageable);
	
	Page<LoteUsuario> listaLotesAutorizador (String AutorizadorContable , String estado , Pageable pageable);
	
	Page<LoteUsuario> listaLotesAutorizadorfecha (String AutorizadorContable , Pageable pageable);
	
	Page<LoteUsuario> listaLotesUsuarioFecha (String Fecha , Pageable pageable);
	
	Page<LoteUsuario> listaLotesUsuarioIdlote (String idlote , Pageable pageable);
	
	public LoteUsuario TraerLoteAutorizado (String EstadoLote);
	
	public List<LoteUsuario> TraerLotesAutorizadosHora (String Estadolote);
	
	public List<LoteUsuario> TrearLotePlano (String idlote);
	
	Page<LoteUsuario> loteusuarioFitral (String analista , String Autorizador , String fecha1 , String fecha2 , Pageable pageable);
	
	Page<LoteUsuario> LotesFecha (String fecha1 , String fecha2 , Pageable pageable);
	
	Page<LoteUsuario> lostesByAutorizadorInforme (String Autorizador , Pageable pageable);
	
	Page<LoteUsuario> lostesByAnalistaInforme (String Autorizador , Pageable pageable);
	
	Page<LoteUsuario> LotesAutorizadorByfechaProceso (String Autorizador , String FechaProceso , Pageable pageable);
	
	Page<LoteUsuario> LotesAnalistaByfechaProceso (String Analista , String FechaProceso , Pageable pageable);

}
