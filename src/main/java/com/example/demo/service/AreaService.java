package com.example.demo.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.example.demo.Entity.Area;



public interface AreaService {
	
	public List<Area> listaAreas();
	
	public Area crearAreas (Area area);
	
	public void eliminarArea (Long id);
	
	public Area buscarAreaId(Long id);
	
	public Area GuardarArea (Area area);
	
	void ActualizarCargarMasivaArea();
	
	public List<Area> BuscarPorDireccion (String Direccion);
	
	public Page <Area> ListaAreaPagina (Pageable pageable);

	public List<Area> BuscarPorArea (String Area);
	
	public List<Area> BuscarPorPresidencia (String presidencia);
	
	public Area BuscarAreaAntigua (String area);
	
	public Page<Area> FiltrarPorArea (String area , Pageable pageable);
	
	public Page<Area> FiltrarPorVisepresidencia (String Vicepresidencia , Pageable pageable);
	
	public Page<Area> FiltrarPorPresidencia (String presidencia , Pageable pageable);
	
	public List<Area> search (String Area);

}
