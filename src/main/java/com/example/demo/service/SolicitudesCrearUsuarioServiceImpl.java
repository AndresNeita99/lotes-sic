package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.example.demo.Dao.SolicitudesCrearUsuarioDao;
import com.example.demo.Dao.SolicitudesCrearUsuarioDaoPagin;
import com.example.demo.Entity.SolicitudesCrearUsuario;

@Service
public class SolicitudesCrearUsuarioServiceImpl implements SolicitudesCrearUsuarioService{
	
	@Autowired
	private SolicitudesCrearUsuarioDao SolicitudCrearDao;
	
	@Autowired
	private SolicitudesCrearUsuarioDaoPagin SolicitudCrearDaoPagin;
	
	

	@Override
	public Page<SolicitudesCrearUsuario> ListaSolicitudesUsuario(String Usuario, Pageable pageable) {
		// TODO Auto-generated method stub
		return SolicitudCrearDaoPagin.ListaSolicitudesUsuario(Usuario, pageable);
	}

	@Override
	public Page<SolicitudesCrearUsuario> ListaSolicitudesRechazadasUsua(String usuario, String estado,Pageable pageable) {
		// TODO Auto-generated method stub
		return SolicitudCrearDaoPagin.listaSolicitudesRechazadasUsuario(usuario, estado, pageable);
	}

	@Override
	public Page<SolicitudesCrearUsuario> ListaSolicitudesAproUsu(String usuario, String estado, Pageable pageable) {
		// TODO Auto-generated method stub
		return SolicitudCrearDaoPagin.listaSolicitudesAprobadasUsuario(usuario, estado, pageable);
	}

	@Override
	public Page<SolicitudesCrearUsuario> listasolicitudesUsuario(Pageable pageable) {
		// TODO Auto-generated method stub
		return SolicitudCrearDaoPagin.listaSolicitudes(pageable);
	}

	@Override
	public Page<SolicitudesCrearUsuario> listaSolcitudesRechazadas(String estado, Pageable pageable) {
		// TODO Auto-generated method stub
		return SolicitudCrearDaoPagin.listaSolicitudesRechazadas(estado, pageable);
	}

	@Override
	public Page<SolicitudesCrearUsuario> listaSolicitudesAproba(String estado, Pageable pageable) {
		// TODO Auto-generated method stub
		return SolicitudCrearDaoPagin.listaSolicitudesAprobadas(estado, pageable);
	}

	@Override
	public void eliminarPeticion(Long id) {
		
		SolicitudCrearDao.deleteById(id);
		
	}

	@Override
	public void GuardarSolicitud(SolicitudesCrearUsuario solicitudesCrearUsuario) {
		
		
		SolicitudCrearDao.save(solicitudesCrearUsuario);
	}

	@Override
	public SolicitudesCrearUsuario VerDetalles(Long id) {
		// TODO Auto-generated method stub
		return SolicitudCrearDao.findById(id).orElse(null);
	}

	@Override
	public SolicitudesCrearUsuario traerSolicitud(Long id) {
		// TODO Auto-generated method stub
		return SolicitudCrearDao.findById(id).orElse(null);
	}

	@Override
	public List<SolicitudesCrearUsuario> cantidadSolicitudes(String estado) {
		// TODO Auto-generated method stub
		return SolicitudCrearDao.listaSolicitudes(estado);
	}

	@Override
	public Page<SolicitudesCrearUsuario> listaSolicitudesBloquear(String Tipo, String estado, Pageable pageable) {
		// TODO Auto-generated method stub
		return SolicitudCrearDaoPagin.listaSolicitudesBloquear(Tipo, estado, pageable);
	}

	@Override
	public Page<SolicitudesCrearUsuario> listaSolicitudesBloquearAutori(String Tipo, String estado, String usuario,
			Pageable pageable) {
		// TODO Auto-generated method stub
		return SolicitudCrearDaoPagin.listaSolicitudesBloquearEnviadasAuto(Tipo, estado, usuario, pageable);
	}

}
