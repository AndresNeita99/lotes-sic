package com.example.demo.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.example.demo.Entity.Moneda;

public interface MonedaService {

	public List<Moneda> listaMonedas();

	public Moneda GuardarMoneda(Moneda moneda);

	public Moneda findOne(long id);
	
	public void EliminarMoneda (Long id);
	
	public void ActualizarModenaCargaMasiva ();
	
	public List<Moneda> BuscarMoneda(int moneda);
	
	public Page<Moneda> listaMonedasPagin (Pageable pageable);
	
	public List<Moneda> listaMonedaHabilitadas (String Estado , int Moneda);
	
	public List<Moneda> listaMonedasActivas (String Estado);
	
	public Moneda buscarMoneda (int moneda);
	
	

}
