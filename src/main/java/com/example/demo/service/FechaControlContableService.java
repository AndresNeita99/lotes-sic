package com.example.demo.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.example.demo.Entity.FechaControlContable;



public interface FechaControlContableService {
	
	public FechaControlContable  GuardarFechaContable (FechaControlContable fechaContable);
	
	public List<FechaControlContable> listaFechasContables ();
	
	public FechaControlContable buscarFechaContable (long id);
	
	public void EliminarFechaContable (long id);
	
	public List<FechaControlContable> buscarFechasActivas (String estado);
	
	public Page<FechaControlContable> ListaFechasContablesPagin (Pageable pageable);


}
