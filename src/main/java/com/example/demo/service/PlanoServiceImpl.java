package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.example.demo.Dao.PlanoDao;
import com.example.demo.Dao.PlanoPaginDao;
import com.example.demo.Entity.Plano;

@Service
public class PlanoServiceImpl implements PlanoService{

	@Autowired
	private PlanoPaginDao paginplanodao;
	
	@Autowired
	private PlanoDao planodao; 
	
	@Override
	public List<Plano> listaPlanos() {
		// TODO Auto-generated method stub
		return (List<Plano>) planodao.findAll();
	}

	@Override
	public Plano buscarPlano(String id) {
		// TODO Auto-generated method stub
		return planodao.findById(id).orElse(null);
	}

	@Override
	public Plano GuardarPlano(Plano plano) {
		// TODO Auto-generated method stub
		return planodao.save(plano);
	}

	@Override
	public List<Plano> BuscarPlanoEstado(String Estado) {
		// TODO Auto-generated method stub
		return planodao.PlanosPorEstado(Estado);
	}

	@Override
	public Page<Plano> TraerPlanosCrgardos(String Estado, Pageable pageable) {
		// TODO Auto-generated method stub
		return paginplanodao.PlanosCargados(Estado, pageable);
	}

}
