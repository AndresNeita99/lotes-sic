package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.example.demo.Dao.BalanceDao;
import com.example.demo.Dao.BalancePaginDao;
import com.example.demo.Entity.Balance;

@Service
public class BalanceImpl implements BalanceService{

	@Autowired
	private BalancePaginDao balancePaginDao;
	
	@Autowired
	private BalanceDao balanceDao;
	
	@Override
	public List<Balance> ListaCuentasBalance() {
		// TODO Auto-generated method stub
		return (List<Balance>) balanceDao.findAll();
	}

	@Override
	public Balance GuardarCuentaBalance(Balance balance) {
		// TODO Auto-generated method stub
		return balanceDao.save(balance);
	}

	@Override
	public Page<Balance> ListaBalancePagin(Pageable pageable) {
		// TODO Auto-generated method stub
		return balancePaginDao.findAll(pageable);
	}

	@Override
	public List<Balance> BuscarCuentaBalance(String cuenta) {
		// TODO Auto-generated method stub
		return balanceDao.traerCuenta(cuenta);
	}

	@Override
	public void eliminarBalanceAnterior() {
		
		balanceDao.deleteAll();
		
	}
	
	

}
