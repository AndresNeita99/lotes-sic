package com.example.demo.service;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.Dao.FechaBalanceDao;
import com.example.demo.Entity.FechaBalance;

@Service
public class FechaBalanceImpl implements FechaBalanceService{

	@Autowired
	public FechaBalanceDao fechaBalanceDao;
	
	@Override
	public FechaBalance guardarFecha(FechaBalance fechaBalance) {
		// TODO Auto-generated method stub
		return fechaBalanceDao.save(fechaBalance);
	}

	@Override
	public List<FechaBalance> MostrarFechaBalance() {
		// TODO Auto-generated method stub
		return (List<FechaBalance>) fechaBalanceDao.findAll();
	}

}
