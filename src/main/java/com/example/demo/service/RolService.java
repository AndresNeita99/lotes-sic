package com.example.demo.service;

import java.util.List;

import com.example.demo.Entity.Rol;

public interface RolService {
	
	public List<Rol> listaRoles();
	
	public Rol GuardarRol (Rol rol);
	
	public Rol BuscarRol(long id);
	
	public void EliminarRol (Long id);
	
	public List <Rol> BuscarRolNombre(String rol);
	

}
