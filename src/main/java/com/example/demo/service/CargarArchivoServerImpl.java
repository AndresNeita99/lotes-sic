package com.example.demo.service;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.stream.Stream;

import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;
import org.springframework.web.multipart.MultipartFile;


@Service
public class CargarArchivoServerImpl implements CargarArchivoServer{
	
    //private static String filePath = System.getProperty("catalina.home") + File.separator + "webapps"+File.separator+"images"+File.separator;                

	LocalDate Fechacargue = LocalDate.now();
	
	//String ruta = "..\\Lotes\\" + "lote" + Fechacargue;
	
	private Path root = Paths.get("");

	  @Override
	  public void init(String RutaCarpeta) { 
		  
		 root = Paths.get(RutaCarpeta);
		  
	    try {
	      Files.createDirectories(root);
	    } catch (IOException e) {
	      throw new RuntimeException("Could not initialize folder for upload!");
	    }
	  }

	  @Override
	  public void save(MultipartFile file , String RutaArchivo) {
		  
		 root = Paths.get(RutaArchivo);
		  
	    try {
	    	
	      Files.copy(file.getInputStream(), this.root.resolve(file.getOriginalFilename()));
	    } catch (Exception e) {
	      if (e instanceof FileAlreadyExistsException) {
	        throw new RuntimeException("A file of that name already exists.");
	      }

	      throw new RuntimeException(e.getMessage());
	    }
	  }

	  @Override
	  public Resource load(String filename , String nombreArhcivo) {
		  
		 root = Paths.get(nombreArhcivo);
		  
	    try {
	      Path file = root.resolve(filename);
	      Resource resource = new UrlResource(file.toUri());

	      if (resource.exists() || resource.isReadable()) {
	        return resource;
	      } else {
	        throw new RuntimeException("Could not read the file!");
	      }
	    } catch (MalformedURLException e) {
	      throw new RuntimeException("Error: " + e.getMessage());
	    }
	  }

	  @Override
	  public void deleteAll(String RutaCarpeta) {
		  
		root = Paths.get(RutaCarpeta);
		  
	    FileSystemUtils.deleteRecursively(root.toFile());
	  }

	  @Override
	  public Stream<Path> loadAll(String RutaArchivo) {
		  
		 root = Paths.get(RutaArchivo);
		  
	    try {
	      return Files.walk(this.root, 1).filter(path -> !path.equals(this.root)).map(this.root::relativize);
	    } catch (IOException e) {
	      throw new RuntimeException("Could not load the files!");
	    }
	  }
	

}
