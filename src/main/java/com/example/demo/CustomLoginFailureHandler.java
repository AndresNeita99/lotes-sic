package com.example.demo;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.stereotype.Component;
import com.example.demo.Entity.Usuarios;
import com.example.demo.service.UsuariosService;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@Component
public class CustomLoginFailureHandler extends SimpleUrlAuthenticationFailureHandler {

	private int contadorValidar = 4;

	@Autowired
	private UsuariosService usuarioservice;

	@Override
	public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response,
			AuthenticationException exception) throws IOException, ServletException {

		String email = request.getParameter("username");

		System.out.println("el correo es : " + email);

		Usuarios usuarioBloquear = usuarioservice.buscarPorEmail(email);

		if (usuarioBloquear == null) {

			String Notificacion = "El usuario " + email + " No esta registrado en el sistema";

			exception = new LockedException(Notificacion);

		}

		else {

			if (usuarioBloquear.getEstadoContra().contains("Olvidada")) {

				String Notificacion = "Tu usuario " + email + " Esta bloqueado ";

				exception = new LockedException(Notificacion);

			} else {

				if (usuarioBloquear != null ) {

					contadorValidar = contadorValidar - 1;

					if (contadorValidar < 1) {

						usuarioBloquear.setEstadoContra("Olvidada");

						exception = new LockedException( "Tu cuenta ha sido bloqueda por favor intenta restablecerla o ponte en contacto con el administrador");

						usuarioservice.GuardarUsuario(usuarioBloquear);

						contadorValidar = 4;

					}

					if (contadorValidar >= 1) {

						System.out.println("Entro a la condicion");

						String Notificacion = "tienes " + contadorValidar + " intentos";

						exception = new LockedException(Notificacion);

					}
					
					if (contadorValidar == 4) {
						
						exception = new LockedException( "Tu cuenta ha sido bloqueda por favor intenta restablecerla o ponte en contacto con el administrador");

						
					}

				}

			}

		}

		super.setDefaultFailureUrl("/login?error");
		super.onAuthenticationFailure(request, response, exception);
	}

}
