package com.example.demo;

/* en esta clase se valida el tipo de dato y su escrutura que el tipo de dato sea 
 * o numero o alfanumerico o caracter */

public class ValidarEscruturaLote {


	public boolean EstruturaFuente(String fuenteParametro) {
		
		if (fuenteParametro.length() == 3 ) {
			
			return true;
		}
		
		return false;
		
		
	}


	public static boolean validarEstructuraFactura(String factura) {
		

		if (factura.matches("^[a-zA-Z0-9]*$") && factura.length() == 3) { // validamos longitud del dato

			System.out.println("El dato es alfanumerico");
			
			return true;

		} else {

			
			System.out.println("El dato no es alfanumerico");
			
			return false;
		}
		
		
	}


	public static boolean validarEstructuraReferencia(String referencia) {
		
		System.out.print("columna referencia :" + referencia);
		 System.out.println("");
		
		if (referencia.matches("^[a-zA-Z0-9]*$")) { // validamos estructura del dato

			System.out.println("El dato es alfanumerico");

		} else {

			System.out.println("El dato no es alfanumerico");
		}
		
		return false;
	}


	public static boolean validarEstructuraCuenta(String numero) {
		
		if (numero.matches("[0-9]+") && numero.length() == 10) { // validamos estructura del dato

			//System.out.println("El dato es numerico y su longitud es de " + numero.length());
			
			return true;

		} else {

			//System.out.println("El dato no es numerico");
			
			return false;
		}

	}
	
	

}
