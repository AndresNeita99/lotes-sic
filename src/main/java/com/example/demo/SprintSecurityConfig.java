package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.server.header.ReferrerPolicyServerHttpHeadersWriter.ReferrerPolicy;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;



@EnableMethodSecurity(securedEnabled = true, prePostEnabled = true)
@Configuration
public class SprintSecurityConfig{
	
	
	@Autowired
    private BCryptPasswordEncoder passwordEncoder;
	
	@Autowired
	private UserDetailsService userDetailsService;
	
	
	 @Bean
     public UserDetailsService userDetailsService(AuthenticationManagerBuilder build) throws Exception {
        build.userDetailsService(userDetailsService)
        .passwordEncoder(passwordEncoder);
        return build.getDefaultUserDetailsService();
     }
	

		@Bean
		public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
			
			http.authorizeHttpRequests()
			.requestMatchers("/css/**","/icons/**","/Img/**","/js/**","/h2-console/**","/Plantilla/**","/Planos/**","Lotes/**","/insumos/**","/webapps/**").permitAll()
			/*------------Modulo login---------------*/
			.requestMatchers("/login").permitAll()
			.requestMatchers("/preguntasSeguridad").permitAll()
			.requestMatchers("/preguntasRestableContra").permitAll()
			.requestMatchers("/recuperarContra").permitAll()
			.requestMatchers("/crearContraseña").permitAll()
			.requestMatchers("/nuevaContraseña").permitAll()
			.requestMatchers("/preguntasOlvidadas").permitAll()
			.requestMatchers("/CargarAreasMasiva").permitAll()
			.requestMatchers("/guardarpreguntas").permitAll()
			.requestMatchers("/crearcontra").permitAll()
			.requestMatchers("/guardarcontra").permitAll()
			.requestMatchers("/validarPreguntas").permitAll()
			.requestMatchers("/EnviarCorreoContra").permitAll()
			.requestMatchers("/notificaciones").permitAll()
			.requestMatchers(new AntPathRequestMatcher("/h2-console/**")).permitAll()
					
			/*-------------Modulo Parametria -------------- */
			
			/* ----------- Areas  ------------------------ */
			
			.requestMatchers("/listaAreas").hasAuthority("Administrador")
			.requestMatchers("/crearArea").hasAuthority("Administrador")
			.requestMatchers("/guardarArea").hasAuthority("Administrador")
			.requestMatchers("/BuscarArea").hasAuthority("Administrador")
			.requestMatchers("/BuscarUsuariosArea").hasAuthority("Administrador")
			.requestMatchers("/EditarArea/**").hasAuthority("Administrador")
			.requestMatchers("/BuscarUsuariosArea/**").hasAuthority("Administrador")
			.requestMatchers("/ValidarExistenciaArchivoArea").hasAuthority("Administrador")
			.requestMatchers("/notificacionPlantillaArea").hasAuthority("Administrador")
			.requestMatchers("/DesabilitarArea/**").hasAuthority("Administrador")
			.requestMatchers("/habilitarArea/**").hasAuthority("Administrador")
			.requestMatchers("/EliminarArea/**").hasAuthority("Administrador")
			.requestMatchers("/CargarAreasMasiva").hasAuthority("Administrador")
			.requestMatchers("/ActualizarAreaCargaMasiva").hasAuthority("Administrador")
			.requestMatchers("/BuscarPresidencia").hasAuthority("Administrador")
			.requestMatchers("/BuscarVicepresidencia").hasAuthority("Administrador")
			.requestMatchers("/ActualizarUsuarios/**").hasAuthority("Administrador")
			.requestMatchers("/barrabusquedapresidencia").hasAuthority("Administrador")
			.requestMatchers("/guardarDato").hasAuthority("Administrador")
			.requestMatchers("/barrabusquedaArea").hasAuthority("Administrador")
			.requestMatchers("/guardarDatoArea").hasAuthority("Administrador")
			.requestMatchers("/GenerarSoporteAreas").hasAuthority("Administrador")
			.requestMatchers("/DescargarSoporteAreaActual").hasAuthority("Administrador")
			
			
			
			
			
			
			/* ----------- Usuarios ---------------------- */
			
			.requestMatchers("/listaUsuarios").hasAuthority("Administrador")
			.requestMatchers("/crearUsuario").hasAuthority("Administrador")
			.requestMatchers("/guardarUsuario").hasAuthority("Administrador")
			.requestMatchers("/BuscarUsuario").hasAuthority("Administrador")
			.requestMatchers("/BuscarUsuarioCorreo").hasAuthority("Administrador")
			.requestMatchers("/editarUsuario/**").hasAuthority("Administrador")
			.requestMatchers("/actualizarUsuario/**").hasAuthority("Administrador")
			.requestMatchers("/EliminarUsuario/**").hasAuthority("Administrador")
			.requestMatchers("/DesabilitarUsuario/**").hasAuthority("Administrador")
			.requestMatchers("/HabilitarUsuario/**").hasAuthority("Administrador")
			.requestMatchers("/BuscarUsuarioCedula").hasAuthority("Administrador")
			.requestMatchers("/BuscarUsuarioCorreo").hasAuthority("Administrador")
			.requestMatchers("/BuscarUsuarioNombre").hasAuthority("Administrador")
			.requestMatchers("/BuscarUsuarioUser").hasAuthority("Administrador")
			.requestMatchers("/GenerarSoporteUsuarios").hasAuthority("Administrador")
			
			
			
			/*------------ solicitudes -------------------- */
			
			.requestMatchers("/SolicitudUsuariosCrear").hasAnyAuthority("Autorizador nivel 1","Autorizador nivel 2","Director Contable" , "Centralizador")
			.requestMatchers("/guardarSolicitudCrearUsu").hasAnyAuthority("Autorizador nivel 1","Autorizador nivel 2","Director Contable" , "Centralizador")
			.requestMatchers("/listaSolicitudesCrearUsuario").hasAuthority("Administrador")
			.requestMatchers("/AprobarUsuario/**").hasAuthority("Administrador")
			.requestMatchers("/RechazarSolicitudUsuario/**").hasAuthority("Administrador")
			.requestMatchers("/listaSolicitudesCambioCon").hasAuthority("Administrador")
			.requestMatchers("/CambiarEstadoContra/**").hasAuthority("Administrador")
			.requestMatchers("/verdetallesSolicitud/**").hasAnyAuthority("Administrador" ,"Autorizador nivel 1","Autorizador nivel 2","Director Contable" , "Centralizador")
			.requestMatchers("/SolicitudesRechazadas").hasAuthority("Administrador")
			.requestMatchers("/SolicitudesAprobadas").hasAuthority("Administrador")
			.requestMatchers("/listaSolicitudesCrearUsuarioAuto").hasAnyAuthority("Autorizador nivel 1","Autorizador nivel 2","Director Contable" , "Centralizador")
			.requestMatchers("/SolicitudesRechazadasAuto").hasAnyAuthority("Autorizador nivel 1","Autorizador nivel 2","Director Contable" , "Centralizador")
			.requestMatchers("/SolicitudesAprobadasAuto").hasAnyAuthority("Autorizador nivel 1","Autorizador nivel 2","Director Contable" , "Centralizador")
			
			.requestMatchers("/listaSolicitudesBloquearUsu").hasAuthority("Administrador")
			
			

			
			
			/*-------------- fechas contables ----------------- */
			
			.requestMatchers("/ListaFechasContable").hasAnyAuthority("Administrador","Centralizador")
			.requestMatchers("/crearFechaContableManual").hasAnyAuthority("Centralizador")
			.requestMatchers("/crearFechaContableAuto").hasAnyAuthority("Centralizador")
			.requestMatchers("/GuardarFechaContableAuto").hasAnyAuthority("Centralizador")
			.requestMatchers("/GuardarFechaContableManu").hasAnyAuthority("Centralizador")
			.requestMatchers("/EditarFechaContable/**").hasAnyAuthority("Centralizador")
			.requestMatchers("/DesabilitarFechaContable/**").hasAnyAuthority("Centralizador")
			.requestMatchers("/habilitarFechaContable/**").hasAnyAuthority("Centralizador")
			.requestMatchers("/EliminarFechaContable/**").hasAnyAuthority("Centralizador")
			.requestMatchers("/actualizarFechaContable").hasAnyAuthority("Centralizador")
			.requestMatchers("/desactivarFechas").hasAnyAuthority("Centralizador")
			.requestMatchers("/crearFechaContableCierre").hasAnyAuthority("Centralizador")
			.requestMatchers("/GuardarFechaContableCierre").hasAnyAuthority("Centralizador")
			
			/*-------------- cuentasPYG ------------------------------------ */
			
			.requestMatchers("/crearCuentaPYG").hasAuthority("Administrador")
			.requestMatchers("/GuardarCuentaPYG").hasAuthority("Administrador")
			.requestMatchers("/listarCuentasPYG").hasAuthority("Administrador")
			.requestMatchers("/EditarCuentaPYG/**").hasAuthority("Administrador")
			.requestMatchers("/actualizarCuentaPYG").hasAuthority("Administrador")
			.requestMatchers("/EliminarCuentaPYG/**").hasAuthority("Administrador")
			.requestMatchers("/activarParametroPyG").hasAuthority("Administrador")
			.requestMatchers("/DesactivarParametroPyG").hasAuthority("Administrador")

			
			/* ------------- balance ------------------------------------------- */
			
			.requestMatchers("/MostrarBalance").hasAnyAuthority("Administrador","Autorizador nivel 1","Autorizador nivel 2","Director Contable")
			.requestMatchers("/ActualizarBalance").hasAuthority("Administrador")
			.requestMatchers("/CargarBalance").hasAuthority("Administrador")
			.requestMatchers("/BuscarCuentaBalance/**").hasAnyAuthority("Administrador","Autorizador nivel 1","Autorizador nivel 2","Director Contable")
			.requestMatchers("/validarArchivoBalance").hasAuthority("Administrador")
			.requestMatchers("/notificacionBalance").hasAnyAuthority("Administrador","Autorizador nivel 1","Autorizador nivel 2","Director Contable")
			
			

			/*------------------- cuentas contables ------------------------------*/
			
			.requestMatchers("/listarCuentas").hasAuthority("Administrador")
			.requestMatchers("/CargarCuentasMasivas").hasAuthority("Administrador")
			.requestMatchers("/BuscarCuenta").hasAuthority("Administrador")
			.requestMatchers("/CrearCapetaInsumos").hasAuthority("Administrador")
			.requestMatchers("/ValidarExistenciaCuentaCarpeta").hasAuthority("Administrador")
			.requestMatchers("/notificacionPlantillaCuentas").hasAuthority("Administrador")
			.requestMatchers("/notificacionCuentasCargadas").hasAuthority("Administrador")
			.requestMatchers("/DesabilitarCuenta/**").hasAuthority("Administrador")
			.requestMatchers("/HabilitarCuenta/**").hasAuthority("Administrador")
			.requestMatchers("/DesactivarParametroCuenta").hasAuthority("Administrador")
			.requestMatchers("/activarParametroCuenta").hasAuthority("Administrador")
	
			
			
			
			
			/*------------------- sucursales -------------------------------------*/
			
			.requestMatchers("/CrearSucursal").hasAuthority("Administrador")
			.requestMatchers("/guardarSucursal").hasAuthority("Administrador")
			.requestMatchers("/listaSucursales").hasAuthority("Administrador")
			.requestMatchers("/editarSucursal/**").hasAuthority("Administrador")
			.requestMatchers("/actualizarSucursal").hasAuthority("Administrador")
			.requestMatchers("/EliminarSucursal/**").hasAuthority("Administrador")
			.requestMatchers("/HabilitarSucursal/**").hasAuthority("Administrador")
			.requestMatchers("/DesabilitarSucursal/**").hasAuthority("Administrador")
			.requestMatchers("/activarParametroSucursal").hasAuthority("Administrador")
			.requestMatchers("/DesactivarParametroSucursal").hasAuthority("Administrador")

			
			
			
			/*---------------- centro costos -------------------------------------*/
			
			.requestMatchers("/ListaCentroCostos").hasAuthority("Administrador")
			.requestMatchers("/CargarMasivaCentroCostos").hasAuthority("Administrador")
			.requestMatchers("/notificacionPlantillaCentroCostos").hasAuthority("Administrador")
			.requestMatchers("/ValidarExistenciaCentroCostosCarpeta").hasAuthority("Administrador")
			.requestMatchers("/CrearCapetaInsumosCentroCostos").hasAuthority("Administrador")
			.requestMatchers("/HabilitarCostos/**").hasAuthority("Administrador")
			.requestMatchers("/DesabilitarCostos/**").hasAuthority("Administrador")
			.requestMatchers("/BuscarCentroCosto").hasAuthority("Administrador")
			.requestMatchers("/activarParametroCentroCostos").hasAuthority("Administrador")
			.requestMatchers("/DesactivarParametroCentroCostos").hasAuthority("Administrador")
			
			

			
			/*------------------ parametria fuentes ---------------*/

			.requestMatchers("/listarFuentes").hasAuthority("Administrador")
			.requestMatchers("/cargarFuenteMasiva").hasAuthority("Administrador")
			.requestMatchers("/BuscarCentroCosto").hasAuthority("Administrador")
			.requestMatchers("/notificacionPlantillaFuentes").hasAuthority("Administrador")
			.requestMatchers("/ValidarExistenciaFuenteCarpeta").hasAuthority("Administrador")
			.requestMatchers("/CrearCapetaInsumosFuente").hasAuthority("Administrador")
			.requestMatchers("/activarParametroFuente").hasAuthority("Administrador")
			.requestMatchers("/DesactivarParametroFuente").hasAuthority("Administrador")


			/*------------------ parametria Moneda ---------------*/

			.requestMatchers("/listaMonedas").hasAuthority("Administrador")
			.requestMatchers("/CrearMoneda").hasAuthority("Administrador")
			.requestMatchers("/guardarMoneda").hasAuthority("Administrador")
			.requestMatchers("/EditarMoneda/**").hasAuthority("Administrador")
			.requestMatchers("/DesabilitarMoneda/**").hasAuthority("Administrador")
			.requestMatchers("/HabilitarMoneda/**").hasAuthority("Administrador")
			.requestMatchers("/EliminarMoneda/**").hasAuthority("Administrador")
			.requestMatchers("/actualizarMoneda").hasAuthority("Administrador")
			.requestMatchers("/activarParametroMoneda").hasAuthority("Administrador")
			.requestMatchers("/DesactivarParametroMoneda").hasAuthority("Administrador")
			
			
			
			/*------------------ parametria Debito cretido ---------------*/
			
			.requestMatchers("/listarDebitoCredito").hasAuthority("Administrador")
			.requestMatchers("/crearDebitoCredito").hasAuthority("Administrador")
			.requestMatchers("/GuardarDebitoCredito").hasAuthority("Administrador")
			.requestMatchers("/EditarCreditoDebito/**").hasAuthority("Administrador")
			.requestMatchers("/EliminarDebitoCredito/**").hasAuthority("Administrador")
			.requestMatchers("/actualizarDebtioCredito").hasAuthority("Administrador")
			.requestMatchers("/HabilitarDebitoCredito/**").hasAuthority("Administrador")
			.requestMatchers("/DesabilitarDebitoCredito/**").hasAuthority("Administrador")
			
		/*------------------ Modulo horas corte ---------------*/
			
			.requestMatchers("/listaHorasCorte").hasAnyAuthority("Administrador" , "Centralizador")
			.requestMatchers("/crearHoraCorte").hasAnyAuthority("Administrador" , "Centralizador")
			.requestMatchers("/GuardarHoraCorte").hasAnyAuthority("Administrador" , "Centralizador")
			.requestMatchers("/EliminarHoraCorte/**").hasAnyAuthority("Administrador" , "Centralizador")
			.requestMatchers("/EditarHoraCorte/**").hasAnyAuthority("Administrador" , "Centralizador")
			.requestMatchers("/actualizarHoraCorte").hasAnyAuthority("Administrador" , "Centralizador")
			.requestMatchers("/DesabilitarHoraCorte/**").hasAnyAuthority("Administrador" , "Centralizador")
			.requestMatchers("/habilitarHoraCorte/**").hasAnyAuthority("Administrador" , "Centralizador")
			

			/* ---------------- Modulo lista cuentas Excep ---------------- */
			
			.requestMatchers("/listaCuentasExcep").hasAnyAuthority("Administrador")
			.requestMatchers("/editarCuentaExcep/**").hasAnyAuthority("Administrador")
			.requestMatchers("/guardarCambiosCuentaExcep").hasAnyAuthority("Administrador")
			.requestMatchers("/CargarAreasMasivaCuentaExcep").hasAnyAuthority("Administrador")
			.requestMatchers("/ValidarExistenciaArchivoCuentasExcep").hasAnyAuthority("Administrador")
			.requestMatchers("/activarParametroCuentaExcep").hasAnyAuthority("Administrador")
			.requestMatchers("/DesactivarParametroCuentaExcep").hasAnyAuthority("Administrador")


			/* ------------- modulo lotes ------------------------------------------- */
			
			.requestMatchers("/lote").hasAnyAuthority("Analista Contable","Centralizador")
			.requestMatchers("/validarFechaProceso").hasAnyAuthority("Analista Contable","Centralizador")
			.requestMatchers("/validarlote").hasAnyAuthority("Analista Contable","Centralizador")
			.requestMatchers("/descargarPlantilla").hasAnyAuthority("Analista Contable","Centralizador")
			.requestMatchers("/EliminarLote/**").hasAnyAuthority("Analista Contable","Centralizador")
			.requestMatchers("/MostrarResumen/**").hasAnyAuthority("Autorizador nivel 1","Autorizador nivel 2","Director Contable" , "Centralizador","Analista Contable","Administrador" , "Auditor" , "Consulta")
			.requestMatchers("/ValidarCuentas").hasAnyAuthority("Analista Contable","Centralizador")
			.requestMatchers("/notificacion/**").permitAll()
			.requestMatchers("/validarFlujoAprobacion").hasAnyAuthority("Analista Contable","Centralizador")
			.requestMatchers("/verDesallesLote/**").hasAnyAuthority("Analista Contable" , "Autorizador nivel 1","Autorizador nivel 2","Director Contable" , "Centralizador")
			.requestMatchers("/actualizarEstado").hasAnyAuthority("Analista Contable","Centralizador")
			.requestMatchers("/ValidarCuentas").hasAnyAuthority("Analista Contable","Centralizador")
			.requestMatchers("/GuardarFechaProceso").hasAnyAuthority("Analista Contable" , "Autorizador nivel 1","Autorizador nivel 2","Director Contable" , "Centralizador" , "Administrador")
			.requestMatchers("/BuscarEstadoFechaProceso").hasAnyAuthority("Analista Contable" , "Autorizador nivel 1","Autorizador nivel 2","Director Contable" , "Centralizador" , "Administrador")
			.requestMatchers("/download").permitAll()
			.requestMatchers("/upload").hasAnyAuthority("Analista Contable","Centralizador")
			.requestMatchers("/actualirEstadoLoteAdmin").hasAuthority("Administrador")
			
			

			/*-------------- modulo autorizacion ------------------------- */
			
			.requestMatchers("/listaLotesAutorizar").hasAnyAuthority("Autorizador nivel 1","Autorizador nivel 2","Director Contable")
			.requestMatchers("/devolverlote/**").hasAnyAuthority("Autorizador nivel 1","Autorizador nivel 2","Director Contable")
			.requestMatchers("/guardarMotivoRechazo").hasAnyAuthority("Autorizador nivel 1","Autorizador nivel 2","Director Contable")
			.requestMatchers("/AbrirLote/**").hasAnyAuthority("Autorizador nivel 1","Autorizador nivel 2","Director Contable" , "Auditor")
			.requestMatchers("/AprobarLote").hasAnyAuthority("Autorizador nivel 1","Autorizador nivel 2","Director Contable")
			.requestMatchers("/lotesRechazados").hasAnyAuthority("Autorizador nivel 1","Autorizador nivel 2","Director Contable")
			.requestMatchers("/lotesAprobados").hasAnyAuthority("Autorizador nivel 1","Autorizador nivel 2","Director Contable")
			.requestMatchers("/bandejaEntradaLote").hasAnyAuthority("Autorizador nivel 1","Autorizador nivel 2","Director Contable")
			.requestMatchers("/AprobarLote/**").hasAnyAuthority("Autorizador nivel 1","Autorizador nivel 2","Director Contable")
			.requestMatchers("/buscarLoteRechazado").hasAnyAuthority("Autorizador nivel 1","Autorizador nivel 2","Director Contable")
			.requestMatchers("/buscarLoteAprobado").hasAnyAuthority("Autorizador nivel 1","Autorizador nivel 2","Director Contable")
			.requestMatchers("/SolicitudUsuariosCrear").hasAnyRole("Autorizador nivel 1","Autorizador nivel 2")
			.requestMatchers("/guardarSolicitudCrearUsu").hasAnyAuthority("Autorizador nivel 1","Autorizador nivel 2","Director Contable")
			.requestMatchers("/listaSolicitudesCrearUsuario").hasAnyAuthority("Autorizador nivel 1","Autorizador nivel 2","Director Contable")
			.requestMatchers("/AprobarUsuario/**").hasAnyAuthority("Autorizador nivel 1","Autorizador nivel 2","Director Contable")
			.requestMatchers("/RechazarSolicitudUsuario/**").hasAnyAuthority("Autorizador nivel 1","Autorizador nivel 2","Director Contable")
			.requestMatchers("/verDesallesLote/**").hasAnyAuthority("Autorizador nivel 1","Autorizador nivel 2","Director Contable" , "Centralizador")
			.requestMatchers("/devolverlote/**").hasAnyAuthority("Autorizador nivel 1","Autorizador nivel 2","Director Contable")
			.requestMatchers("/continuarFlujoAuto/**").hasAnyAuthority("Autorizador nivel 1","Autorizador nivel 2","Director Contable")
			.requestMatchers("/DenegarFlujoAuto/**").hasAnyAuthority("Autorizador nivel 1","Autorizador nivel 2","Director Contable")
			
			
			
			
			
			
			/*------------------ Modulo Planos ---------------*/
			
			.requestMatchers("/listaLotesAutorizados").hasAnyAuthority("Centralizador","Administrador")
			
			/* roles que pueden acceder administrador y centralizador */
			.requestMatchers("/CrearCapetaPlanos").hasAnyAuthority("Centralizador","Administrador")
			.requestMatchers("/IniciarProcesoAutomatico").hasAnyAuthority("Centralizador","Administrador")	
			.requestMatchers("/DetenerProcesoAutomatico").hasAnyAuthority("Centralizador","Administrador")
			.requestMatchers("/ContinuarProceso").hasAnyAuthority("Centralizador","Administrador")			
			.requestMatchers("/traformar").hasAnyAuthority("Centralizador","Administrador")
			.requestMatchers("/descargarPlano").hasAuthority("Centralizador")
			.requestMatchers("/ListaPlanos").hasAuthority("Centralizador")
			
			/*------------------ Modulo Divisa ---------------- */
			
			
			.requestMatchers("/listaTrm").hasAuthority("Administrador")
			.requestMatchers("/eliminarTrm/**").hasAuthority("Administrador")
			.requestMatchers("/crearTrm").hasAuthority("Administrador")
			.requestMatchers("/guardarTrm").hasAuthority("Administrador")
			.requestMatchers("/editarTrm/**").hasAuthority("Administrador")
			.requestMatchers("/guardarCambiosTrm").hasAuthority("Administrador")
			.requestMatchers("/DesabilitarTRM/**").hasAuthority("Administrador")
			.requestMatchers("/HabilitarTRM/**").hasAuthority("Administrador")
			.requestMatchers("/activarParametroTrm").hasAuthority("Administrador")
			.requestMatchers("/DesactivarParametroTrm").hasAuthority("Administrador")
			
			/*------------------ modulo informes auditoria ------------ */
			
			
			.requestMatchers("/listaLotesInforme").hasAuthority("Auditor")
			.requestMatchers("/filtroInforme").hasAuthority("Auditor")
			.requestMatchers("/logs").hasAuthority("Auditor")
			.requestMatchers("/downloadlog/**").hasAuthority("Auditor")
			.requestMatchers("/GuardarDatosBusqueda").hasAuthority("Auditor")
			
			/* ------------------ m
			
			
			/*------------------ modulo consuta ------------ */
			
			
			.requestMatchers("/verTrazabilidadlotes").hasAuthority("Consulta")
			.requestMatchers("/GuardarDatosBusquedaTrazabilidad").hasAuthority("Consulta")
			.requestMatchers("/filtroInformeTrazabilidad").hasAuthority("Consulta")
		
			/*---------------------------- permisos generales ------------ */
			
			.anyRequest().authenticated()
			.and()
            .formLogin()
            		.loginPage("/login")
            		.permitAll()
            		.usernameParameter("username")
            		.defaultSuccessUrl("/Home", true)
            		//.failureUrl("/login?error=true")
            		.failureHandler(loginFailureHandler)
            .and()
            .logout()
            .logoutUrl("/logout")
            .permitAll()
            .logoutSuccessUrl("/login")
            
            .and()
            .exceptionHandling().accessDeniedPage("/error403")
            
			.and()
			.csrf().ignoringRequestMatchers("/h2-console/**")
			.and()
			.headers()
				.frameOptions()
				.sameOrigin() // X-Frame-Options: 
				.defaultsDisabled()
				.contentTypeOptions();
					//X-Content-Type-Options
				
			return http.build();
		}
		
		
		@Autowired
	    private CustomLoginFailureHandler loginFailureHandler;
		
	}



