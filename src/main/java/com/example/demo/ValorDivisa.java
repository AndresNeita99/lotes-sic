package com.example.demo;

public class ValorDivisa {
	
	private String valorcreditoDivisa;

	private double valorCreditoDivisaNumero;

	private String valorDebitoDivisa;
	
	private double valorDebitoDivisaNumero;

	public String getValorcreditoDivisa() {
		return valorcreditoDivisa;
	}

	public void setValorcreditoDivisa(String valorcreditoDivisa) {
		this.valorcreditoDivisa = valorcreditoDivisa;
	}

	public double getValorCreditoDivisaNumero() {
		return valorCreditoDivisaNumero;
	}

	public void setValorCreditoDivisaNumero(double valorCreditoDivisaNumero) {
		this.valorCreditoDivisaNumero = valorCreditoDivisaNumero;
	}

	public String getValorDebitoDivisa() {
		return valorDebitoDivisa;
	}

	public void setValorDebitoDivisa(String valorDebitoDivisa) {
		this.valorDebitoDivisa = valorDebitoDivisa;
	}

	public double getValorDebitoDivisaNumero() {
		return valorDebitoDivisaNumero;
	}

	public void setValorDebitoDivisaNumero(double valorDebitoDivisaNumero) {
		this.valorDebitoDivisaNumero = valorDebitoDivisaNumero;
	}
	
	
	
	
	
}
