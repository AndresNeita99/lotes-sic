package com.example.demo;

import java.io.File;
import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.example.demo.Entity.Mensaje;
import com.example.demo.service.MensajeService;

@Component
@EnableScheduling
public class EliminarMensajes {

	@Autowired
	public MensajeService mensajeService;

	// 0 15 15 2 4 ?
	
	// esta funcion se ejcuta a las 4:16 del dia 2 del 6 mes
	
	@Scheduled(cron = "0 16 16 2 6 ?")
	public void verificaPorHora() {

		// System.out.println(LocalDateTime.now());

		guardarMensajes();

	}

	public void guardarMensajes() {

		String ruta = "..\\LogsMensajes";

		// esta carpeta se creara cada vez que se descargue y se llene un plano
		File directorio = new File(ruta);

		if (!directorio.exists()) {

			if (directorio.mkdirs()) {

				//System.out.println("Directorio creado" + ruta);

			}
		}

		try {

			String rutaMensajes = "../LogsMensajes/LogMensajesSic.txt";

			File file = new File(rutaMensajes); // Si el archivo no existe es creado

			if (!file.exists()) {

				file.createNewFile();

			}

			else {

				

				if (mensajeService.listaMensajes().isEmpty() == false) {
					
					List<Mensaje> TodosMensajes = mensajeService.listaMensajes();

					for (int i = 0; i < TodosMensajes.size(); i++) {

						Mensaje Mensaje = TodosMensajes.get(i);

						String mensajeCompleto = Mensaje.getComentario() + Mensaje.getDestinatario()
								+ Mensaje.getEstado() + Mensaje.getFecha();

					}

					mensajeService.EliminarTodosMensajes();

				}else {
					
					System.out.println("No hay mensjaes ");
				}

			}

		} catch (Exception e) {

			e.printStackTrace();

		}

	}

}
