package com.example.demo.Dao;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.example.demo.Entity.DebitoCredito;

public interface DebitoCreditoParametroDaoPagin extends PagingAndSortingRepository<DebitoCredito, Long>{

}
