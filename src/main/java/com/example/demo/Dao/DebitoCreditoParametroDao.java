package com.example.demo.Dao;

import java.util.List;


import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.jpa.repository.Query;

import com.example.demo.Entity.DebitoCredito;

public interface DebitoCreditoParametroDao extends CrudRepository<DebitoCredito, Long>{
	
	@Query("SELECT m FROM DebitoCredito m where m.valor=?1")
	public List<DebitoCredito> findByvalor(int valor);
	
	@Query("SELECT m FROM DebitoCredito m where m.id=?1")
	public DebitoCredito findByid(int valor);
	
	@Query(value = "select * from debitos_creditos where valor = :valor" , nativeQuery = true)
	public DebitoCredito findByvalor2 (@Param("valor") int valor);

}
