package com.example.demo.Dao;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.example.demo.Entity.FechaControlContable;

public interface FechaControlContableDaoPagin extends PagingAndSortingRepository<FechaControlContable, Long>{

}
