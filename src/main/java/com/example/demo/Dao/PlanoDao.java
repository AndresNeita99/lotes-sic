package com.example.demo.Dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.example.demo.Entity.Plano;

public interface PlanoDao extends CrudRepository<Plano, String>{
	
	@Query(value = "select * from planos where estado = :Estado" , nativeQuery = true)
	List<Plano> PlanosPorEstado (@Param("Estado") String estado);


}
