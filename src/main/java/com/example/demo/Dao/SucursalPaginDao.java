package com.example.demo.Dao;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.example.demo.Entity.Sucursal;

public interface SucursalPaginDao extends PagingAndSortingRepository<Sucursal, Long>{

}
