package com.example.demo.Dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.example.demo.Entity.Area;

public interface AreaDao extends CrudRepository<Area, Long>{
	
	@Query("SELECT c FROM Area c where c.Direccion=?1")
	public List<Area> finByDireccion(String Direccion);
	
	/*@Query("SELECT c FROM Area c where c.Area=?1")
	public List<Area> findByArea(String Area);*/
	
	@Query("SELECT c FROM Area c where c.presidencia=?1")
	public 	List<Area> findBypresidencia(String presidencia);
	
	@Query(value = "select * from areas where area = :nombreArea" , nativeQuery = true)
	public Area BuscarArea (@Param("nombreArea") String nombreArea);
	
	@Query(value = "select * from areas where autorizador_nivel1 = :autorizador" , nativeQuery = true)
	public Area BuscarAreaAutorizador1 (@Param("autorizador") String Autorizador);
	
	@Query(value = "select a from Area a where a.Area like %?1%")
	public List<Area> findByArea(String  term);
	
	
}
