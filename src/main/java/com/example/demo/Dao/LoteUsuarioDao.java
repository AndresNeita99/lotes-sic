package com.example.demo.Dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import com.example.demo.Entity.LoteUsuario;


@Repository
public interface LoteUsuarioDao extends CrudRepository<LoteUsuario, String>{

	@Query(value = "select * from lote where autorizador = :NombreAutorizador" , nativeQuery = true)
	public List<LoteUsuario> BuscarNombreAutorizador (@Param("NombreAutorizador") String NombreAutorizador);
	
	@Query(value = "select * from lote where analista = :NombreAnalista" , nativeQuery = true)
	public List<LoteUsuario> BuscarNombreNombreAnalista (@Param("NombreAnalista") String NombreAnalista);
	
	
	@Query(value = "select * from lote where (estado_lote = :EstadoLote and autorizador = :NombreAutorizador)" , nativeQuery = true)
	public List<LoteUsuario> BuscarEstadoLote (@Param("EstadoLote") String EstadoLote , @Param("NombreAutorizador") String NombreAutorizador);
	
	@Query(value = "select * from lote where (estado_lote = :EstadoLote and analista = :NombreAnalista)" , nativeQuery = true)
	public List<LoteUsuario> BuscarEstadoLoteUsuario (@Param("EstadoLote") String EstadoLote , @Param("NombreAnalista") String NombreAnalista);
	
	@Query(value = "select * from lote where estado_lote = :EstadoLote" , nativeQuery = true)
	public List<LoteUsuario> lotesAutorizados (@Param("EstadoLote") String EstadoLote);
	
	@Query(value = "select * from lote WHERE analista = :nombreAnalista" , nativeQuery = true)
	public List<LoteUsuario> loteDeUsuario (@Param ("nombreAnalista") String nombreAnalista);
	
	@Query(value = "select * from lote where estado_lote = :EstadoLote" , nativeQuery = true )
	public LoteUsuario TraerLoteTrasformar (@Param("EstadoLote") String EstadoLote);
	
	@Query(value = "select * from lote where estado_lote = :EstadoLote order by hora_cargue asc" , nativeQuery = true)
    public List<LoteUsuario> TrearLotesAutorizados (@Param ("EstadoLote") String EstadoLote);
	
	@Query(value = "select * FROM lote INNER JOIN planos on  lote.plano_id = id where id = :idlote" , nativeQuery = true)
	List<LoteUsuario> TraerLostesPlano (@Param("idlote") String idlote);

	
	
}
