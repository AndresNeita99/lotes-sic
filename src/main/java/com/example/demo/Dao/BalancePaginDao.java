package com.example.demo.Dao;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.example.demo.Entity.Balance;

public interface BalancePaginDao extends PagingAndSortingRepository<Balance, Long>{

}
