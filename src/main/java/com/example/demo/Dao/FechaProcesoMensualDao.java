package com.example.demo.Dao;

import org.springframework.data.repository.CrudRepository;

import com.example.demo.Entity.FechaProcesoMensual;

public interface FechaProcesoMensualDao extends CrudRepository<FechaProcesoMensual, Long>{

}
