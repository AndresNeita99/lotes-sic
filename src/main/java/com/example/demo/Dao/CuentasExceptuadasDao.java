package com.example.demo.Dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.example.demo.Entity.CuentasExceptuadas;

public interface CuentasExceptuadasDao extends CrudRepository<CuentasExceptuadas, Long>{
	
	@Query(value = "select * from cuentas_exceptuadas where cuenta = :Nocuenta" , nativeQuery = true)
	public CuentasExceptuadas TreaerCuenta (@Param("Nocuenta") String cuenta);
	
	@Query(value = "select * from cuentas_exceptuadas" , nativeQuery =  true)
	public List<CuentasExceptuadas> listaCuentas();
	
	@Query(value = "select * from cuentas_exceptuadas where estado = :Estado and cuenta = :Nocuenta" , nativeQuery =  true)
	public List<CuentasExceptuadas> listaCuentasExcepHabili(@Param("Estado") String Estado , @Param("Nocuenta") String cuenta);
	
	@Query(value = "select * from cuentas_exceptuadas where estado = :Estado" , nativeQuery = true)
	public List<CuentasExceptuadas> listaCuentasExcepActivas (@Param("Estado") String estado);

}
