package com.example.demo.Dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.example.demo.Entity.Area;

public interface AreaPaginDao extends PagingAndSortingRepository<Area, Long>{
	
	@Query(value = "select * from areas where area = :area" , nativeQuery = true)
	Page<Area> filtrarArea (@Param("area") String area , Pageable pageable);
	
	@Query(value = "select * from areas where direccion = :direccion" , nativeQuery = true)
	Page<Area> filtrarVisepresidencia (@Param("direccion") String direccion , Pageable pageable);
	
	@Query(value = "select * from areas where presidencia = :presidencia" , nativeQuery = true)
	Page<Area> filtrarPresidencia (@Param("presidencia") String presidencia , Pageable pageable);

}
