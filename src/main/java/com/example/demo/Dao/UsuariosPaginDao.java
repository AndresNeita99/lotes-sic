package com.example.demo.Dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.example.demo.Entity.Usuarios;

public interface UsuariosPaginDao extends PagingAndSortingRepository<Usuarios, Long>{
	

	@Query(value = "select * from usuarios INNER JOIN areas On usuarios.area_idarea = areas.idarea where area_idarea = :id" , nativeQuery = true)
	public Page<Usuarios> filtrarUsuariosArea(@Param ("id") long id_area , Pageable pageable);
	
	@Query(value = "select * from usuarios where estado_usuario = :estadoUsuario" , nativeQuery = true)
	public Page<Usuarios> traerPeticionesCrearUsuario  (@Param("estadoUsuario") String estado , Pageable pageable);

	@Query(value = "select * from usuarios where estado_contra = :estadoContrase" , nativeQuery = true)
	public Page<Usuarios> traerPeticionesContra  (@Param("estadoContrase") String estadoContra , Pageable pageable);
	
}
