package com.example.demo.Dao;


import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


import com.example.demo.Entity.ResumenLote;

@Repository
public interface ResumenLoteDao extends CrudRepository<ResumenLote, Long>{

	@Query("SELECT c FROM ResumenLote c where c.NoCuenta=?1")
	public ResumenLote findByCuenta(String cuenta);
	
	
	@Query(value = "select * from resumen_lote where lote_id_lote = :id order by no_cuenta ASC", nativeQuery = true)
    public List <ResumenLote> findBylote (@Param("id") String id);
	
	@Query(value = "select * from resumen_lote where no_cuenta = :cuenta and lote_id_lote = :id" , nativeQuery = true )
	public ResumenLote TraerCuenta (@Param("cuenta") String cuenta ,@Param ("id") String id);
	
}
