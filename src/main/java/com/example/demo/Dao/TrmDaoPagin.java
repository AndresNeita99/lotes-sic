package com.example.demo.Dao;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.example.demo.Entity.TRM;

public interface TrmDaoPagin extends PagingAndSortingRepository<TRM, Long>{

}
