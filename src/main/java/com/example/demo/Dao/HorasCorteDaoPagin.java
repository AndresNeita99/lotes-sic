package com.example.demo.Dao;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.example.demo.Entity.HorasCorte;

public interface HorasCorteDaoPagin extends PagingAndSortingRepository<HorasCorte, Long>{

}
