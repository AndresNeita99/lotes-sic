package com.example.demo.Dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.example.demo.Entity.Rol;

public interface RolDao extends CrudRepository<Rol, Long>{
	
	@Query(value = "select * from roles where name = :rol" , nativeQuery = true)
	public List<Rol> BuscarRolNombre (@Param ("rol") String rol);

}
