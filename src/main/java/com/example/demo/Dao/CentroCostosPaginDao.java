package com.example.demo.Dao;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.example.demo.Entity.CentroCostos;

public interface CentroCostosPaginDao extends PagingAndSortingRepository<CentroCostos, Long>{

}
