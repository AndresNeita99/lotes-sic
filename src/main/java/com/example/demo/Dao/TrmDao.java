package com.example.demo.Dao;



import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.example.demo.Entity.TRM;

public interface TrmDao extends CrudRepository<TRM, Long>{
	
	@Query(value = "select * from trm where estado = :estadoTRM" , nativeQuery = true)
	public TRM trmActiva (@Param ("estadoTRM") String Estado);
	
	
	

}
