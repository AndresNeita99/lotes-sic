package com.example.demo.Dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.example.demo.Entity.HorasCorte;


public interface HoraCorteoDao extends CrudRepository<HorasCorte, Long>{
	
	@Query(value = "select COUNT (estado) from horas_corte where estado = :estadoHora" , nativeQuery = true )
	int numerHorasoActivas (@Param ("estadoHora") String estadoHora);
	
	@Query(value = "select * from horas_corte  where estado = 'Activo' order by hora_corte" , nativeQuery = true)
	public List<HorasCorte> HorasCorte ();

}
