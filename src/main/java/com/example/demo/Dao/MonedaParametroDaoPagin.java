package com.example.demo.Dao;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.example.demo.Entity.Moneda;

public interface MonedaParametroDaoPagin extends PagingAndSortingRepository<Moneda, Long>{

}
