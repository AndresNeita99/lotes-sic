package com.example.demo.Dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.example.demo.Entity.SolicitudesCrearUsuario;

public interface SolicitudesCrearUsuarioDao extends CrudRepository<SolicitudesCrearUsuario, Long>{

	
	@Query(value = "select * from solicitudes where estado = :Estado" , nativeQuery = true)
	public List<SolicitudesCrearUsuario> listaSolicitudes (@Param("Estado") String solicitud);

	
}
