package com.example.demo.Dao;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.example.demo.Entity.Moneda;

public interface MonedaParametroDao extends CrudRepository<Moneda, Long>{

	@Query("SELECT m FROM Moneda m where m.moneda=?1")
	List<Moneda> findByMoneda(int moneda); 
	
	@Query(value = "Select * from Moneda where estado  = :Estado and moneda  = :Moneda" , nativeQuery = true)
	public List<Moneda> ListaMonedasHabiilta(@Param ("Estado") String Estado , @Param("Moneda") int moneda);
	
	@Query(value = "Select * from Moneda where estado  = :Estado",nativeQuery = true)
	public List<Moneda> listaMonedasActivas (@Param("Estado") String estado);
	
	@Query("SELECT m FROM Moneda m where m.moneda=?1")
	Moneda encontrarMoneda (int Moneda);
	
}
