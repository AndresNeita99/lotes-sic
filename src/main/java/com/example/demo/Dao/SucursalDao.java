package com.example.demo.Dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.example.demo.Entity.Sucursal;

public interface SucursalDao extends CrudRepository<Sucursal, Long>{
	
	@Query("SELECT s FROM Sucursal s where s.Sucursal=?1")
	public List<Sucursal> findBySucursal (int sucursal);
	
	@Query(value = "select * from Sucursal where estado : = Estado and Sucursal : = sucursal" , nativeQuery = true)
	public List<Sucursal> ListaSucursalesHbailitadas (@Param ("estado")  String estado , @Param("sucursal") int sucursal);
	
	@Query(value = "select * from sucursales where estado  = :Estado" , nativeQuery = true)
	public List<Sucursal> listaSucursalesActivas (@Param ("Estado") String estado);
	
	@Query("SELECT s FROM Sucursal s where s.Sucursal=?1")
	public Sucursal BuscarSucursal (int sucursal);

}
