package com.example.demo.Dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.example.demo.Entity.Plano;

public interface PlanoPaginDao extends PagingAndSortingRepository<Plano, String>{
	
	@Query(value = "select * from planos where estado = :Estado" ,nativeQuery = true)
	public Page <Plano> PlanosCargados (@Param("Estado") String estado , Pageable pageable);
	
	
}
