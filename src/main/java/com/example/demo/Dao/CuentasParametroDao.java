package com.example.demo.Dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import com.example.demo.Entity.NumeroCuenta;

@Repository
public interface CuentasParametroDao extends CrudRepository<NumeroCuenta, Long>{
	
	@Query("SELECT c FROM NumeroCuenta c where c.NoCuenta=?1")
	List<NumeroCuenta> traerCuenta(String cuenta);
	
	@Query(value = "Select * from NumeroCuenta where estado  = :Estado and NoCuenta  = :cuenta" , nativeQuery = true)
	public List<NumeroCuenta> CuentasHabilitadas(@Param("Estado") String Estado , @Param("cuenta") String cuenta);
	
	@Query(value = "Select * from cuentas where estado  = :Estado" , nativeQuery = true)
	public List<NumeroCuenta> cuentasActivas (@Param("Estado") String Estado);
	
	@Query("SELECT c FROM NumeroCuenta c where c.NoCuenta=?1")
	public NumeroCuenta BuscarCuenta (String cuenta);
	
	
	
	
}
