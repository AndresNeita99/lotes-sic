package com.example.demo.Dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import com.example.demo.Entity.ValorCuentasPYG;

@Repository
public interface ValorCuentasPYGDao extends CrudRepository<ValorCuentasPYG, Long>{
	
	@Query("SELECT v FROM ValorCuentasPYG v where v.id =?1")
	public ValorCuentasPYG findByid(long id);
	
	@Query(value = "Select * from valor_cuentaspyg where estado  = :Estado and cuenta = :Numero" , nativeQuery = true)
	public ValorCuentasPYG listaCuetas (@Param ("Estado") String estado , @Param("Numero") int cuenta);

	@Query(value = "select * from valor_cuentaspyg where estado  = :Estado" , nativeQuery = true)
	public List<ValorCuentasPYG> listaCuentasActivas (@Param("Estado") String estado);

}
