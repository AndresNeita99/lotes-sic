package com.example.demo.Dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.example.demo.Entity.Fuente;

public interface FuenteParametroDao  extends CrudRepository<Fuente,Long>{

	@Query("SELECT f FROM Fuente f where f.FuenteParametro=?1")
	List<Fuente> findByFuenteParametro(String fuente); 
	
	@Query(value = "Select * from Fuente where  estado = :Estado and fuente_parametro  = :fuente" , nativeQuery = true)
	public List<Fuente> ListaFuentesHabilitadas(@Param ("Estado") String Estado, @Param ("fuente") String fuente);
	
	@Query(value = "Select * from fuentes where estado = :Estado", nativeQuery = true)
	public List<Fuente> fuentesActivas (@Param("Estado") String Estado);
	
	@Query("SELECT f FROM Fuente f where f.FuenteParametro=?1")
	Fuente findByFuente (String fuente);
	
}

