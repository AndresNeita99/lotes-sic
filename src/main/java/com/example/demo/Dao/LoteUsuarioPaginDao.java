package com.example.demo.Dao;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.example.demo.Entity.LoteUsuario;

public interface LoteUsuarioPaginDao extends PagingAndSortingRepository<LoteUsuario, Long> {

	@Query(value = "select * from lote where analista = :analista order by fecha_proceso desc", nativeQuery = true)
	Page<LoteUsuario> findAllByanalista(@Param("analista") String analista, Pageable pageable);

	@Query
	Page<LoteUsuario> findAllByFechaProceso(String FechaProceso, String analista, Pageable pageable);

	@Query(value = "select * from lote where id_lote = :idlote", nativeQuery = true)
	Page<LoteUsuario> BuscarLotesId(@Param("idlote") String idlote, Pageable pageable);

	@Query(value = "select * from lote where fecha_proceso = :fechaProceso", nativeQuery = true)
	Page<LoteUsuario> BuscarLotesfechaProceso(@Param("fechaProceso") String fechaProceso, Pageable pageable);

	Page<LoteUsuario> findAllByIdLote(String Id_lote, Pageable pageable);

	@Query(value = "select * from lote where autorizador = :autorizador and estado_lote = :estadolote order by fecha_proceso desc", nativeQuery = true)
	Page<LoteUsuario> findAllByautorizador(@Param("autorizador") String autorizador, @Param("estadolote") String estado, Pageable pageable);

	@Query(value = "select * from lote where autorizador = :autorizador  order by fecha_proceso desc", nativeQuery = true)
	Page<LoteUsuario> lotesautorizadorfecha(@Param("autorizador") String autorizador, Pageable pageable);

	@Query(value = "select * from lote where estado_lote = :estadolote", nativeQuery = true)
	Page<LoteUsuario> findAllByEstadoLote(@Param("estadolote") String EstadoLote, Pageable pageable);

	/*@Query(value = "select * from lote as l inner join resumen_lote as r on (l.id_lote  = r.lote_id_lote) inner join usuarios as u on (u.id = l.usuario_id) where analista = :analista and autorizador = :autorizador and no_cuenta = :Nocuenta and documento = :documento and fecha_proceso between :Fechaproceso1 and :fechaproceso2", nativeQuery = true)
	Page<LoteUsuario> FiltradoLote(@Param("analista") String analista, @Param("autorizador") String autorizador, @Param("Nocuenta") String nocuenta, @Param("documento") String documento, @Param("Fechaproceso1") String fecha, @Param("fechaproceso2") String fecha2, Pageable pageable);*/ // triple inner join con datos de ingreso 

	@Query(value = "select * from lote where analista = :analista and autorizador = :autorizador and fecha_proceso between :Fechaproceso1 and :fechaproceso2", nativeQuery = true)
	Page<LoteUsuario> FiltradoLote(@Param("analista") String analista, @Param("autorizador") String autorizador, @Param("Fechaproceso1") String fecha, @Param("fechaproceso2") String fecha2, Pageable pageable);
	
	@Query(value = "select * from lote where fecha_proceso between :fechaUno and :fechaDos" ,nativeQuery = true)
	public Page<LoteUsuario> LotesFecha (@Param ("fechaUno") String fecha , @Param("fechaDos") String fecha2 , Pageable pageable);
	
	@Query(value = "select * from lote where autorizador = :autorizador" , nativeQuery = true)
	public Page<LoteUsuario> loteAutorizadorInforme (@Param ("autorizador") String autorizador , Pageable pageable);
	
	@Query(value = "select * from lote where analista = :Analista" , nativeQuery = true)
	public Page<LoteUsuario> loteAnalistaInforme (@Param ("Analista") String autorizador , Pageable pageable);
	
	@Query(value = "Select * from lote where analista = :Autorizador and fecha_proceso = :FechaProceso" , nativeQuery = true)
	public Page <LoteUsuario> LoteAutorizadorByFechaProceso (@Param("Autorizador") String autorizador , @Param("FechaProceso") String fechaProceso , Pageable pageable);

	@Query(value = "Select * from lote where analista = :Analista and fecha_proceso = :FechaProceso" , nativeQuery = true)
	public Page <LoteUsuario> LoteAAnalistaByFechaProceso (@Param("Analista") String autorizador , @Param("FechaProceso") String fechaProceso , Pageable pageable);


}
