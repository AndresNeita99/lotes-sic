package com.example.demo.Dao;

import java.util.List;


import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.Query;


import com.example.demo.Entity.Usuarios;

@Repository
public interface UsuariosDao extends CrudRepository<Usuarios, Long>{
	
	@Query("SELECT t FROM Usuarios t WHERE t.username =?1")
	public Usuarios findByusername(String username);
	
	@Query("SELECT t FROM Usuarios t WHERE t.documento =?1")
	public Usuarios findBydocumento(int documento);
	 
	@Query(value = "select * from usuarios WHERE nombre = :nombre" , nativeQuery = true)
	public Usuarios findBynombre(@Param("nombre") String nombre);
	
	@Query("SELECT t FROM Usuarios t WHERE t.area.id =?1")
	public List<Usuarios> findByarea(long id);
	
	@Query("SELECT t FROM Usuarios t WHERE t.correo =?1")
	public Usuarios findBycorreo(String correo);

	/*@Query("select u.username from Usuarios u inner join Rol r on r.id = u.rol.id where r.id = :id")
	public Usuarios findByid(long id);*/
	
	@Query(value = "select * from usuarios inner join usuario_role ON usuarios.id = usuario_role.usuario_id where rol_id = :rol" , nativeQuery = true)
	public Usuarios findByrol (@Param("rol") long rol);
	
	@Query(value = "select * from usuarios inner join usuario_role ON usuarios.id = usuario_role.usuario_id where rol_id = :rol and estado_usuario = :Estado" , nativeQuery = true)
	public Usuarios BuscarRolEstado (@Param("rol") long rol , @Param ("Estado") String estado);
	
	@Query(value = "select * from usuarios inner join usuario_role ON usuarios.id = usuario_role.usuario_id where rol_id = :rol" , nativeQuery = true)
	public List<Usuarios> findByrolAutorizador (@Param("rol") long rol);
	
	@Query("SELECT t FROM Usuarios t WHERE t.EstadoUsuario =?1")
	public List<Usuarios> findByEstadoUsuario (String EstadoUsuario);
	
	@Query(value = "select * from usuarios INNER JOIN areas On usuarios.area_idarea = areas.idarea where area_idarea = :id" , nativeQuery = true)
	public List<Usuarios> findByUsuariosArea(@Param ("id") long id_area);
	
	@Query(value = "select * from usuarios where estado_contra = :estado" ,  nativeQuery = true)
	public List<Usuarios> findByEstadoContra(@Param ("estado") String estado);

	
}
