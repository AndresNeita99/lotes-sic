package com.example.demo.Dao;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.example.demo.Entity.NumeroCuenta;

public interface CuentasParametroDaoPagin extends PagingAndSortingRepository<NumeroCuenta, Long>{

}
