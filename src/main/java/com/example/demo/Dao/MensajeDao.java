package com.example.demo.Dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.example.demo.Entity.Mensaje;

public interface MensajeDao extends CrudRepository<Mensaje, Long>{
	
	@Query(value = "select * from mensajes where destinatario = :usuario" , nativeQuery = true)
	public List<Mensaje> MisMensajes (@Param("usuario") String  Usuario);
	
	@Query(value = "select * from mensajes where destinatario = :usuario and estado = :estadoMensaje" , nativeQuery = true)
	public List<Mensaje> MisMensajesNoleidos(@Param("usuario") String usuario , @Param("estadoMensaje") String estado);
	
	

}
