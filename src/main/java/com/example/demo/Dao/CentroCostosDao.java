package com.example.demo.Dao;

import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.example.demo.Entity.CentroCostos;


public interface CentroCostosDao extends CrudRepository<CentroCostos, Long>{
	
	@Query("SELECT c FROM CentroCostos c where c.CentroCostos=?1")
	List<CentroCostos> ObtenerCentroCostos(int centroCostos);
	
	@Query(value = "Select * from centro_costos where estado  = :Estado and centro_costos  = :CentroCosto" , nativeQuery = true)
	public List<CentroCostos> CentroCostosHabilitados (@Param("Estado") String Estado , @Param ("CentroCosto") int CentroCosto);
	
	@Query(value = "select * from centro_costos where estado  = :Estado" , nativeQuery = true)
	public List<CentroCostos> centroCostosActivos (@Param ("Estado") String estado);
	
	@Query("SELECT c FROM CentroCostos c where c.CentroCostos=?1")
	public CentroCostos BuscarCentroCostos (int CentroCostos);

}

