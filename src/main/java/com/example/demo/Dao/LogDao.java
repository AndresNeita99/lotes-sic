package com.example.demo.Dao;

import org.springframework.data.repository.CrudRepository;

import com.example.demo.Entity.Log;

public interface LogDao extends CrudRepository<Log, String>{

}
