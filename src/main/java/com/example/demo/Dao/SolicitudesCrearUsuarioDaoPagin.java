package com.example.demo.Dao;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.example.demo.Entity.SolicitudesCrearUsuario;

public interface SolicitudesCrearUsuarioDaoPagin extends PagingAndSortingRepository<SolicitudesCrearUsuario, Long>{

	@Query(value = "select * from solicitudes where usuario = :usuario" , nativeQuery = true)
	Page<SolicitudesCrearUsuario> ListaSolicitudesUsuario (@Param ("usuario") String usuario , Pageable pageable);
	
	@Query(value = "select * from solicitudes where usuario = :usuario and estado = :Estado", nativeQuery = true)
	Page<SolicitudesCrearUsuario> listaSolicitudesRechazadasUsuario (@Param("usuario") String usuario , @Param("Estado") String estado ,Pageable pageable);
	
	@Query(value = "select * from solicitudes where usuario = :usuario and estado = :Estado", nativeQuery = true)
	Page<SolicitudesCrearUsuario> listaSolicitudesAprobadasUsuario (@Param("usuario") String usuario , @Param("Estado") String estado ,Pageable pageable);

	@Query(value = "select * from solicitudes where estado = :Estado", nativeQuery = true)
	Page<SolicitudesCrearUsuario> listaSolicitudesRechazadas( @Param("Estado") String estado ,Pageable pageable);
	
	@Query(value = "select * from solicitudes where estado = :Estado", nativeQuery = true)
	Page<SolicitudesCrearUsuario> listaSolicitudesAprobadas( @Param("Estado") String estado ,Pageable pageable);
	
	
	@Query(value = "select * from solicitudes where tipo = :tipoSolici  and estado = :estado " , nativeQuery = true)
	Page<SolicitudesCrearUsuario> listaSolicitudesBloquear (@Param("tipoSolici") String tipo , @Param("estado") String Estado , Pageable pageable);
	
	@Query(value = "select * from solicitudes", nativeQuery = true)
	Page<SolicitudesCrearUsuario> listaSolicitudes(Pageable pageable);
	
	@Query(value = "select * from solicitudes where tipo = :tipoSolici  and estado = :estado and usuario = :nombreUsuario" , nativeQuery = true)
	Page<SolicitudesCrearUsuario> listaSolicitudesBloquearEnviadasAuto (@Param("tipoSolici") String tipo , @Param("estado") String Estado , @Param("nombreUsuario") String usuario, Pageable pageable);
	
}
