package com.example.demo.Dao;

import java.util.List;

import org.springframework.data.jdbc.repository.query.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.Entity.FechaControlContable;

@Repository
public interface FechaControlContableDao extends CrudRepository<FechaControlContable, Long>{

	@Query("SELECT f FROM FechaControlContable f where f.Estado=?1")
	public List<FechaControlContable> findByEstado(String Estado);
}
