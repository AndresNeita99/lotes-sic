package com.example.demo.Dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.Entity.FechaBalance;

@Repository
public interface FechaBalanceDao extends CrudRepository<FechaBalance, Long>{

}
