package com.example.demo.Dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import com.example.demo.Entity.Balance;


@Repository
public interface BalanceDao extends CrudRepository<Balance, Long>{
	
	@Query("SELECT c FROM Balance c where c.Cuenta=?1")
	List<Balance> traerCuenta(String cuenta);
}
