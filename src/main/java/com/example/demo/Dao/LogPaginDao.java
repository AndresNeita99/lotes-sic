package com.example.demo.Dao;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.example.demo.Entity.Log;

public interface LogPaginDao extends PagingAndSortingRepository<Log, String>{

}
