package com.example.demo;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.time.LocalDate;
import java.util.Date;
import java.util.Iterator;

import org.apache.poi.openxml4j.util.ZipSecureFile;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.util.IOUtils;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;


import com.example.demo.Entity.Lote;
import com.example.demo.Entity.Moneda;



@Controller
public class TrasformarLoteAplano{
	
	public boolean traformarloteOptimizado (String rutalote , int numeroPLano , String usuario , String Autorizador , String idPlano) {
		
		 Moneda moneda = new Moneda();
		  
		 long inicio = System.currentTimeMillis();
		 
		 LocalDate Fechacargue = LocalDate.now();
		 
		 String rutaHistorial = "" ;
		 
		 String NombrePLano = idPlano;
		 
		 String NombreUsuario = usuario; // este dato tiene el nombre del usuario traido de DB
		 
		 String NombreAutorizador = Autorizador; // este dato tiene el nombre del Autorizador traido de DB
		 
		 int lote = 1; // este dato es remplazado por el contaor
		 
		 //System.out.println("proceso iniciado");
		  
		  try {

			  		
				  String ruta = "../Planos/sii11ajc.txt";
				  
				  File file = new File(ruta); // Si el archivo no existe es creado
				  	
				  rutaHistorial = "../HistorialPLanos/" + NombrePLano + ".txt";

				  File file2 = new File(rutaHistorial); // Si el archivo no existe es creado
				  
			  if (!file2.exists()) {

				  file2.createNewFile();
				  file.createNewFile(); 
			  
			  }
		  
		  } catch (Exception e) { 
			  
			  e.printStackTrace(); 
		  
		  }

		String filaDelLote = "";
		
		Lote datoslote = new Lote();

		FileInputStream file;
		
		try {

            FileInputStream inputStream = new FileInputStream(new File(rutalote));
            
            Workbook workbook = new XSSFWorkbook(inputStream);
            
            Sheet firstSheet = workbook.getSheetAt(0);
            
            Iterator<Row> iterator = firstSheet.iterator();
            
            DataFormatter formatter = new DataFormatter();
            
            while (iterator.hasNext()) {
            	
            	Row nextRow = iterator.next();
            	
            	int numeroFila  =nextRow.getRowNum();
            	
            	//System.out.println("NUMERO DE FILA " + numeroFila);
   
            	Iterator<Cell> cellIterator = nextRow.cellIterator();
                
            	while(cellIterator.hasNext()) {

            		Cell cell = cellIterator.next();
            		
            		if (numeroFila!= 0) {

            			int Numerocolumna = cell.getColumnIndex();
            			
            			//System.out.println("EL NUMERO DE COLUMNA ES : " + Numerocolumna);
            			
            				if(Numerocolumna == 0) { // FUENTE REQUIERE PARAMETRIA 
            				
            					String fuente = formatter.formatCellValue(cell);
            					
            					//System.out.println("LA FUENTE ES : " + fuente);
            					
            					String Lote = String.valueOf(lote);
								 
								String LotePlano = lotePlano(Lote);
								
								//System.out.println("lotePlano " + LotePlano + " Longitud " + LotePlano.length());

								filaDelLote = filaDelLote + LotePlano;
								
								String FuentePlano = "";
								
								//System.out.println("Fuente " + fuente);
								
								if (fuente.length() == 3) {
									
									filaDelLote = filaDelLote + fuente;
									
									//System.out.println("Fuente : " + fuente + " Longitud " + fuente.length());
									
								}
								
								else {
									
									FuentePlano = FuentePlano(fuente);
									
									filaDelLote = filaDelLote + FuentePlano;
									
									//System.out.println("Fuente : " + FuentePlano + " Longitud " + FuentePlano.length());
								}
            					
            	
								
							}
            				
            				if(Numerocolumna == 1) {
            					
            					String consecutivo = formatter.formatCellValue(cell);
            					
            					String ConsecutivoPlano = ConsecutivoPlano(consecutivo);
								
								filaDelLote = filaDelLote + ConsecutivoPlano ;
            					
            					//System.out.println("CONSECUTIVO " + consecutivo);
            				
            				}
            			
            				if(Numerocolumna == 2) { // REQUIERE PARAMETRIA NO CUENTA
            					       					
            					String Cuenta = formatter.formatCellValue(cell);
            					
            					String NumeroCuentaPLano = NoCuentaPlano(Cuenta);
								
								//System.out.println("Nuemro Cuenta Plano " + NumeroCuentaPLano + "lONGITUD " + NumeroCuentaPLano.length());
								
								filaDelLote = filaDelLote + NumeroCuentaPLano ;
            					
            					//System.out.println("CUENTA " + Cuenta);
            					
            					
            				}
            				
            				if (Numerocolumna == 3) { // COLUMNA SUCURSAL  ( REQUIERE PARAMETRIA )
            					
            					String Sucursal = formatter.formatCellValue(cell);
            					
            					String SucursalPlano = SucursalPlano(Sucursal);
								
								//System.out.println("Sucursal plano " + SucursalPlano + " Longitud " + SucursalPlano.length());
								
								filaDelLote = filaDelLote + SucursalPlano;
            					
            					//System.out.println("SUCURSAL " + Sucursal);
            					
            				}
            				
            				if (Numerocolumna == 4) { // PARAMETRIA CENTROD DE COSTOS
            					
            					String centroCostos = formatter.formatCellValue(cell);
            					
            					//System.out.println("CENTRO COSTOS " + centroCostos);
            					
            					String CentroCostosPlano =  CentroCostosPlano(centroCostos);
								
								//System.out.println("Centro constos Plano " + CentroCostosPlano + " Longitud " + CentroCostosPlano.length());
								
								filaDelLote = filaDelLote + CentroCostosPlano ;
            					
            				
            				}
            				
            				if(Numerocolumna == 5) {
            					
            					String pesos = formatter.formatCellValue(cell);
            					
            					String ValorPesosPlano = ValorPesosPlano(pesos);
								
								//System.out.println("Valor pesos del plano " + ValorPesosPlano + " longitud " + ValorPesosPlano.length()); 

								filaDelLote = filaDelLote + ValorPesosPlano ;
            					
            					//System.out.println("PESOS " + pesos);
            				}
            				
            				
            				
            				if (Numerocolumna == 6) {// PARAMETRO MONEDA
            					
            					String TipoMoneda = formatter.formatCellValue(cell);
            					
            					int Moneda = Integer.parseInt(TipoMoneda);
								
								moneda.setMoneda(Moneda);
            					
            					
            					//System.out.println("TIPOMODENA " + TipoMoneda);
            					
            					
            					
            					
            				}
            				
            				
            				if (Numerocolumna == 7) { // VALOR DIVISA 
            					
            					String Divisa = formatter.formatCellValue(cell);

								String ValorDivisaPlano = Divisa;
								
								String charsToRemove = ",.";
								
								for (char c : charsToRemove.toCharArray()) {
									ValorDivisaPlano = ValorDivisaPlano.replace(String.valueOf(c), "");
								}

								//System.out.println("valorDivisa:" + ValorDivisa);
																
								if (moneda.getMoneda() == 0) {
									
									String valorCerosDivisa = "000000000000000000";
									
									filaDelLote = filaDelLote + valorCerosDivisa;
									
									//System.out.println("Valor divisa plano : " + valorCerosDivisa + " LONGITUD " + valorCerosDivisa.length());
								}
								
								else {
									
									if (ValorDivisaPlano.length() < 15) {
										
										String ValorMoneda = String.valueOf(moneda.getMoneda());
										
										String ValorFinalDivisa = ValorDivisaPlano(ValorDivisaPlano , ValorMoneda);
										
										filaDelLote = filaDelLote + ValorFinalDivisa;
										
										//System.out.println("Valor divisa plano : " + ValorFinalDivisa + " Longitud " + ValorFinalDivisa.length());
										
									}
									
									
								}
            					
            					
            				}
            				
            				
            				if (Numerocolumna == 8) { //PARAMETRO D/C
            					
            					String debitoCredito = formatter.formatCellValue(cell);
            					
            					//System.out.println("D/C " + debitoCredito);
            					
            					filaDelLote = filaDelLote + debitoCredito;
            					
            				}
            				
            				if(Numerocolumna == 9) {
            					
            					//String fechaEfectiva = formatter.formatCellValue(cell);
            					
            					//System.out.println("FECHA EFECTIVA " + fechaEfectiva);
            					
            					LocalDate FechaEfectiva = LocalDate.now();
								
								String fechaEfectiva = FechaEfectiva.toString();
								
								String FechaEfectivaPlano = fechaEfectivaPLano(fechaEfectiva);
								
								//System.out.println("Fecha Efectiva del plano " + FechaEfectivaPlano + " longitud " + FechaEfectivaPlano.length());
								
								filaDelLote = filaDelLote + FechaEfectivaPlano;
								
								
            				
            				}
            				
            				if (Numerocolumna == 10) {
            					
            					String fechaProceso = formatter.formatCellValue(cell);
            					
            					String FechaProcesoPlano = fechaProcesoPLano(fechaProceso);
								
								//System.out.println("Fecha Proceso del plano " + FechaProcesoPlano + " longitud " + FechaProcesoPlano.length());

								filaDelLote = filaDelLote + FechaProcesoPlano;
								
            					
            					//System.out.println("FECHA PROCESO " + fechaProceso);
            					
            				}
            				
            				if (Numerocolumna == 11) {
            					
            					String identificacion = formatter.formatCellValue(cell);
            					
            					//System.out.println("TIPO IDENTICIACION " + identificacion);
            					
            				}
            				
            				if (Numerocolumna == 12) {
            					
            					String nit = formatter.formatCellValue(cell);
            					
            					//System.out.println("NIT " + nit );
            					
            					String NitPlano = NitPlano(nit);
								
								//System.out.println("El nit del plano es : " + NitPlano + " Longitud " + NitPlano.length());
								
								filaDelLote = filaDelLote + NitPlano;
								
								String Otros = "0000000000000000000000000000";
								
								//System.out.println("Otros del plano es : " + Otros + " Longitud " + Otros.length());
								
								filaDelLote = filaDelLote + Otros;
								
								String UsuarioPlano = UsuariosPlano(NombreUsuario);
								
								//System.out.println("Usuario del plano es : " + UsuarioPlano + " Longitud " + UsuarioPlano.length());
								
								filaDelLote = filaDelLote + UsuarioPlano;
								
								String Autorizadorpl = AutorizadorPlano(NombreAutorizador);
								
								//System.out.println("Autorizador del plano es : " + Autorizadorpl + " Longitud " + Autorizadorpl.length());

								filaDelLote = filaDelLote + Autorizadorpl;
								
								String OtroCodigo = "001";
								
								//System.out.println("Otros Codigo " + OtroCodigo + " Longitud " + OtroCodigo.length());
								
								filaDelLote = filaDelLote + OtroCodigo;
								
								//System.out.println(filaDelLote);
            					
            				}
            				
            				
            				if (Numerocolumna == 13) {
            					
            					String factura = formatter.formatCellValue(cell);
            					
            					//System.out.println("FACTURA "+ factura);
            					
            					String facturaPlanos = FacturaPlano(factura);
								
								//System.out.println("Factura del plano " + facturaPlanos + " Longitud " + facturaPlanos.length());
								
								filaDelLote = filaDelLote + facturaPlanos + "00000000000000000";
            					
            					
            				}
            				
            				if (Numerocolumna == 14) {
            					
            					String referencia = formatter.formatCellValue(cell);
            					
            					//System.out.println("REFERENCIA " + referencia);
            					
            				}

            			
            		}else {
            			
            			//System.out.println("Cabezera del archivo");
            			
            		}
            		                  
            		
                }
            	
            	
            	try {
			        
					 File ArchivoPlano = new File("../Planos/siig11ajc.txt");
					 
					 File ArchivoPlanoHistorial = new File(rutaHistorial);
					 
					    
			         FileWriter escribir = new FileWriter(ArchivoPlano , true);
			         
			         FileWriter escribir2 = new FileWriter(ArchivoPlanoHistorial , true);
			            
			         escribir.write(filaDelLote);
			            
			         escribir.write("\r\n"); 
			            
			         escribir.close();
			         
			         escribir2.write(filaDelLote);
			            
			         escribir2.write("\r\n"); 
			            
			         escribir2.close();
			         
			         filaDelLote = "";
			         
		        } catch (Exception e) {
		        	
		            e.printStackTrace();
		        }
            	
                
            }
        } catch (Exception e) {
            
        	e.printStackTrace();
        }
		
		
		long fin = System.currentTimeMillis();
        
        double tiempo = (double) ((fin - inicio)/1000);
         
        System.out.println(tiempo +" segundos");

		return true;
	}
	
	
	
	
	
	@GetMapping("/traformar")
	public boolean TrasformarExcelTxt(String rutalote , int numeroPLano , String usuario , String Autorizador , String idPlano) {
		
		 Moneda moneda = new Moneda();
		 
		 
		 LocalDate Fechacargue = LocalDate.now();
		 
		 String rutaHistorial = "" ;
		 
		 String NombrePLano = idPlano;
		 
		 String NombreUsuario = usuario; // este dato tiene el nombre del usuario traido de DB
		 
		 String NombreAutorizador = Autorizador; // este dato tiene el nombre del Autorizador traido de DB
		 
		 int lote = 1; // este dato es remplazado por el contaor
		 
		 System.out.println("proceso iniciado");
		  
		  try {

			  		
				  String ruta = "../Planos/sii11ajc.txt";
				  
				  File file = new File(ruta); // Si el archivo no existe es creado
				  	
				  rutaHistorial = "../HistorialPLanos/" + NombrePLano + ".txt";

				  File file2 = new File(rutaHistorial); // Si el archivo no existe es creado
				  
			  if (!file2.exists()) {

				  file2.createNewFile();
				  file.createNewFile(); 
			  
			  }
		  
		  } catch (Exception e) { 
			  
			  e.printStackTrace(); 
		  
		  }

		String filaDelLote = "";
		
		Lote datoslote = new Lote();

		FileInputStream file;
		
		IOUtils.setByteArrayMaxOverride(1000000000);

		try {

			file = new FileInputStream(new File(rutalote));
			Workbook workbook = new XSSFWorkbook(file);

			org.apache.poi.ss.usermodel.Sheet sheet = workbook.getSheetAt(0);
			ZipSecureFile.setMinInflateRatio(0);

			for (Row row : sheet) {

				int contador = 0;

				//System.out.println("Numero de fila: " + row.getRowNum());

				System.out.println("");

				if (row.getRowNum() == 0) {

					//System.out.println("saltar fila ya que es la cabezera del archivo ");
					//System.out.println("");

				} else {

					for (Cell cell : row) {

						if (cell.getCellType() != CellType.BLANK) {
							
							//ContadorRegistros = row.getRowNum() + 1;
						}
		
						contador = contador + 1;
						
						//filaDelLote = filaDelLote + contador2;
						
						//System.out.println("la columna es : " + contador);

						switch (cell.getCellType()) {
						
	

						case STRING:
							
							

							if (contador == 1) { // columna fuente (tiene parametria )
								
								String Lote = String.valueOf(lote);
								 
								String LotePlano = lotePlano(Lote);
								
								//System.out.println("lotePlano " + LotePlano + " Longitud " + LotePlano.length());

								filaDelLote = filaDelLote + LotePlano;
								
								String FuentePlano = "";

								String fuente = cell.getRichStringCellValue().getString();
								
								//System.out.println("Fuente " + fuente);
								
								if (fuente.length() == 3) {
									
									filaDelLote = filaDelLote + fuente;
									
									//System.out.println("Fuente : " + fuente + " Longitud " + fuente.length());
									
								}
								
								else {
									
									FuentePlano = FuentePlano(fuente);
									
									filaDelLote = filaDelLote + FuentePlano;
									
									//System.out.println("Fuente : " + FuentePlano + " Longitud " + FuentePlano.length());
								}
							

							}

							if (contador == 14) { // COLUMNA FACTURA (No tiene parametria)

								String Factura = cell.getRichStringCellValue().getString();
								
								//System.out.println("Factura Lote : " + Factura);
								
								String facturaPlanos = FacturaPlano(Factura);
								
								//System.out.println("Factura del plano " + facturaPlanos + " Longitud " + facturaPlanos.length());
								
								filaDelLote = filaDelLote + facturaPlanos + "00000000000000000";

								
							}

							/*if (contador == 15) { // COLUMNA REFERENCIA (No tiene parametria)
								
								String ReferenciaPlano = "00000000000000000";
								
								//System.out.println("Referencia del plano : " + ReferenciaPlano + " Longitud " + ReferenciaPlano.length());
								
								filaDelLote = filaDelLote + "00000000000000000";

							}*/


							break;
						case NUMERIC:

							if (contador == 2) { // COLUMNA CONSECUTIVO

								double dato1 = cell.getNumericCellValue();
								
								String Consecutivo = datoslote.EliminarPuntos(dato1);
								
								//System.out.println("Consecutivo : " + Consecutivo);
								
								String ConsecutivoPlano = ConsecutivoPlano(Consecutivo);
								
								filaDelLote = filaDelLote + ConsecutivoPlano ;
								
								//System.out.println("Consecutivo Plano " + ConsecutivoPlano + "Longitud " + ConsecutivoPlano.length());

							}

							if (contador == 3) { // COLUMNA NUMERO DE CUENTA (REQUIERE PARAMETRIA)
	
								double Nocuenta = cell.getNumericCellValue();

								String NumeroCuenta = datoslote.LimpiarNumeroCuenta(Nocuenta);
								
								//System.out.println("Numero Cuenta :" + NumeroCuenta);
								
								String NumeroCuentaPLano = NoCuentaPlano(NumeroCuenta);
								
								//System.out.println("Nuemro Cuenta Plano " + NumeroCuentaPLano + "lONGITUD " + NumeroCuentaPLano.length());
								
								filaDelLote = filaDelLote + NumeroCuentaPLano ;
		
							}
							

							if (contador == 4) { // COLUMNA SUCURSAL (REQUIERE PARAMETRIA)

								double dato1 = cell.getNumericCellValue();
	
								int Sucursal = (int) dato1;  
								
								//System.out.println("Sucursal : " + Sucursal);
								
								String SucursalString = String.valueOf(Sucursal);
								
								String SucursalPlano = SucursalPlano(SucursalString);
								
								//System.out.println("Sucursal plano " + SucursalPlano + " Longitud " + SucursalPlano.length());
								
								filaDelLote = filaDelLote + SucursalPlano;
								
								//System.out.println(filaDelLote);
								

							}

							if (contador == 5) { // COLUMNA CENTRO DE COSTOS (REQUIERE PARAMETRIA)

								double dato1 = cell.getNumericCellValue();

								int CentroCostos =  (int) dato1;
								
								String CentroCostosString = String.valueOf(CentroCostos);

								//System.out.println("Centro Costos: " + CentroCostosString);
								
								String CentroCostosPlano =  CentroCostosPlano(CentroCostosString);
								
								//System.out.println("Centro constos Plano " + CentroCostosPlano + " Longitud " + CentroCostosPlano.length());
								
								filaDelLote = filaDelLote + CentroCostosPlano ;
								
								//System.out.println(filaDelLote);


							}

							if (contador == 6) { // COLUMNA PESOS (SUMAR EL TOTAL DEL LOTE )

								
								double ValorPesos =  cell.getNumericCellValue();
								
								DecimalFormat formatea = new DecimalFormat("###,###,##0.00");
				
								String ValorPesosFormato = formatea.format(ValorPesos);

								//System.out.println("valor Pesos del lote : " + ValorPesosFormato);
								
								String ValorPesosPlano = ValorPesosPlano(ValorPesosFormato);
								
								//System.out.println("Valor pesos del plano " + ValorPesosPlano + " longitud " + ValorPesosPlano.length()); 

								filaDelLote = filaDelLote + ValorPesosPlano ;
								
								//System.out.println(filaDelLote);
								
								//System.out.println("");

							}
							
							if (contador == 7) { // COLUMNA MONEDA (REQUIERE PARAMETRIA)

								double dato1 = cell.getNumericCellValue();

								int Moneda = (int) dato1;
								
								moneda.setMoneda(Moneda);
								
								String MonedaString = String.valueOf(Moneda);

								//System.out.println("Moneda:" + Moneda);

								//filaDelLote = filaDelLote + MonedaString;
								
								//System.out.println(filaDelLote);
								

							}

							if (contador == 8) { // COLUMNA VALOR EN DIVISAS (FATA MODULO DIVISAS PARA VALIDAR COLUMNA )

								// metodo personalizado

								double Divisa = cell.getNumericCellValue();
								
								DecimalFormat formatea = new DecimalFormat("###,###,##0.00");
								
								String ValorDivisa = formatea.format(Divisa);
								
								String ValorDivisaPlano = ValorDivisa;
								
								String charsToRemove = ",.";
								
								for (char c : charsToRemove.toCharArray()) {
									ValorDivisaPlano = ValorDivisaPlano.replace(String.valueOf(c), "");
								}

								//System.out.println("valorDivisa:" + ValorDivisa);
																
								if (moneda.getMoneda() == 0) {
									
									String valorCerosDivisa = "000000000000000000";
									
									filaDelLote = filaDelLote + valorCerosDivisa;
									
									//System.out.println("Valor divisa plano : " + valorCerosDivisa + " LONGITUD " + valorCerosDivisa.length());
								}
								
								else {
									
									if (ValorDivisaPlano.length() < 15) {
										
										String ValorMoneda = String.valueOf(moneda.getMoneda());
										
										String ValorFinalDivisa = ValorDivisaPlano(ValorDivisaPlano , ValorMoneda);
										
										filaDelLote = filaDelLote + ValorFinalDivisa;
										
										//System.out.println("Valor divisa plano : " + ValorFinalDivisa + " Longitud " + ValorFinalDivisa.length());
										
									}
									
									
								}
						


							}

							if (contador == 9) { // COLUMNA CREDITO / DEBITO (requiere parametria)

		

								double dato1 = cell.getNumericCellValue();

								int DebitoCredito = (int) dato1;
								
								String DebitoCreditoString = String.valueOf(DebitoCredito);

								//System.out.println("debito/credito es :"  + DebitoCredito);
								
								//System.out.println("Valor debito del plano es : " + DebitoCreditoString + " Longitud " + DebitoCreditoString.length());
								
								filaDelLote = filaDelLote + DebitoCreditoString;
								
								//System.out.println(filaDelLote);
									
									
							}
							
							if (contador == 11) { // COLUMNA FECHA DE PROCESO (REQUIERE PARAMETRIA)

								// metodo personalizado
								
								double dato1 = cell.getNumericCellValue();
								
								String fecha = datoslote.limpiarErroresE(dato1); // limpio la fecha 
								
								String FechaEstrcuturada = datoslote.EstructurarFecha(fecha); // escruturo la fecha
								
								//System.out.println("fecha Proceso: " + fecha);
								
								Date FechaProceso = datoslote.EstablecerFormatoFechaProcesoLote(FechaEstrcuturada); // doy formato dd/mm/yyyy
			
								LocalDate FechaEfectiva = LocalDate.now();
								
								String fechaEfectiva = FechaEfectiva.toString();
								
								String FechaEfectivaPlano = fechaEfectivaPLano(fechaEfectiva);
								
								//System.out.println("Fecha Efectiva del plano " + FechaEfectivaPlano + " longitud " + FechaEfectivaPlano.length());
								
								filaDelLote = filaDelLote + FechaEfectivaPlano;
								
								String FechaProcesoPlano = fechaProcesoPLano(fecha);
								
								//System.out.println("Fecha Proceso del plano " + FechaProcesoPlano + " longitud " + FechaProcesoPlano.length());

								
								filaDelLote = filaDelLote + FechaProcesoPlano;
								
								//System.out.println(filaDelLote);
							}
							
							if (contador == 10) { // COLUMNA FECHA EFECTIVA (REQUIERE PARAMETRIA)

								// metodo personalizado
								
								double dato1 = cell.getNumericCellValue();
								
								String fecha = datoslote.limpiarErroresE(dato1); // limpio la fecha 
								
								String FechaEstrcuturada = datoslote.EstructurarFecha(fecha); // escruturo la fecha
								
								System.out.println("fecha Proceso: " + FechaEstrcuturada);
								
								
							}

							if (contador == 12) { // COLUMNA TIPO DE IDENTIFICACION

								double dato1 = cell.getNumericCellValue();

								int TipoIdentificacion = (int) dato1;
								
								//System.out.println("TipoId: " + TipoIdentificacion);
								
								//System.out.println("");
								
						
								
								//System.out.println(filaDelLote);

							}

							if (contador == 13) { // COLUMNA NIT

								long Nit = (long) cell.getNumericCellValue();
								
								//System.out.println("Nit:" + Nit);
								
								String NitLote = String.valueOf(Nit);
								
								String NitPlano = NitPlano(NitLote);
								
								//System.out.println("El nit del plano es : " + NitPlano + " Longitud " + NitPlano.length());
								
								filaDelLote = filaDelLote + NitPlano;
								
								String Otros = "0000000000000000000000000000";
								
								//System.out.println("Otros del plano es : " + Otros + " Longitud " + Otros.length());
								
								filaDelLote = filaDelLote + Otros;
								
								String UsuarioPlano = UsuariosPlano(NombreUsuario);
								
								//System.out.println("Usuario del plano es : " + UsuarioPlano + " Longitud " + UsuarioPlano.length());
								
								filaDelLote = filaDelLote + UsuarioPlano;
								
								String Autorizadorpl = AutorizadorPlano(NombreAutorizador);
								
								//System.out.println("Autorizador del plano es : " + Autorizadorpl + " Longitud " + Autorizadorpl.length());

								filaDelLote = filaDelLote + Autorizadorpl;
								
								String OtroCodigo = "001";
								
								//System.out.println("Otros Codigo " + OtroCodigo + " Longitud " + OtroCodigo.length());
								
								filaDelLote = filaDelLote + OtroCodigo;
								
								//System.out.println(filaDelLote);

							}

						
							
								if (contador == 1) { // columna fuente (tiene parametria )
								
								String Lote = String.valueOf(lote);
								 
								String LotePlano = lotePlano(Lote);
								
								//System.out.println("lotePlano " + LotePlano + " Longitud " + LotePlano.length());

								filaDelLote = filaDelLote + LotePlano;
								
								String FuentePlano = "";

								double fuente = cell.getNumericCellValue();
								
								int DigitoCheque = (int) fuente;
								
								String DigitoChequeString= String.valueOf(DigitoCheque);

								//System.out.println("Fuente " + fuente);
								
								if (DigitoChequeString.length() == 3) {
									
									filaDelLote = filaDelLote + DigitoChequeString;
									
									//System.out.println("Fuente : " + fuente + " Longitud " + DigitoChequeString.length());
									
								}
								
								else {
									
									FuentePlano = FuentePlano(DigitoChequeString);
									
									filaDelLote = filaDelLote + FuentePlano;
									
									//System.out.println("Fuente : " + FuentePlano + " Longitud " + FuentePlano.length());
								}
							

							}

							
							

							break;

						case BLANK:

							break;

						case ERROR:

							break;

						case FORMULA:

							break;

						default:

							System.out.println("el dato tiene erroe no se puede leer");

						}
						
						
						

					}
					
					
					//System.out.println(filaDelLote);
					//System.out.println("Longitud de la fila " + filaDelLote.length());
					

				}
				
				try {
			        
					 File ArchivoPlano = new File("../Planos/siig11ajc.txt");
					 
					 File ArchivoPlanoHistorial = new File(rutaHistorial);
					 
					    
			         FileWriter escribir = new FileWriter(ArchivoPlano , true);
			         
			         FileWriter escribir2 = new FileWriter(ArchivoPlanoHistorial , true);
			            
			         escribir.write(filaDelLote);
			            
			         escribir.write("\r\n"); 
			            
			         escribir.close();
			         
			         escribir2.write(filaDelLote);
			            
			         escribir2.write("\r\n"); 
			            
			         escribir2.close();
			         
			         filaDelLote = "";
			         
		        } catch (Exception e) {
		        	
		            e.printStackTrace();
		        }
				
				//System.out.print(filaDelLote);
				
				

			}
			
			

			workbook.close();

		} catch (FileNotFoundException e) {

			

			//System.out.print("No se a cargado ninguna lote para validar por favor seleccionar un lote");
			
			return false;


		} catch (IOException e) {

			e.printStackTrace();
			
			System.out.println(e);
			
			return false;

		}

		return true ;

	}
	
	
	
	// ------------------------------------------------- FUNCIONES PARA TRANSFORMAR EXCEL A PLANO ----------------------- 
	
	public String lotePlano(String lote) {
		
		int CantidadaCerosFaltantes = 3 - lote.length();
		
		String CantidadCeros = "";
		
		
		
		for (int i = 0 ; i < CantidadaCerosFaltantes ; i++) {
			
			CantidadCeros = CantidadCeros + "0";
			
		}
		
		String LoteFinal = CantidadCeros + lote; 
		
		return LoteFinal;
		
	}
	
	
	public String FuentePlano(String fuente) {
		
		int CantidadaCerosFaltantes = 3 - fuente.length();
		
		String CantidadCeros = "";
		
		
		
		for (int i = 0 ; i < CantidadaCerosFaltantes ; i++) {
			
			CantidadCeros = CantidadCeros + "0";
			
		}
		
		String FuenteFinal = CantidadCeros + fuente; 
		
		return FuenteFinal;
	}
	
	public String ConsecutivoPlano(String Consecutivo) {
		
		int CantidadaCerosFaltantes = 13 - Consecutivo.length();
		
		String CantidadCeros = "";
		
		for (int i = 0 ; i < CantidadaCerosFaltantes ; i++) {
			
			CantidadCeros = CantidadCeros + "0";
			
		}
		
		String ConsecutivoFinal = CantidadCeros + Consecutivo; 
		
		return ConsecutivoFinal;
	}
	
	public String NoCuentaPlano(String NoCuenta) {
		
		int CantidadaCerosFaltantes = 17 - NoCuenta.length();
		
		String CantidadCeros = "";
		
		for (int i = 0 ; i < CantidadaCerosFaltantes ; i++) {
			
			CantidadCeros = CantidadCeros + "0";
			
		}
		
		String NoCuentaFinal = CantidadCeros + NoCuenta; 
		
		return NoCuentaFinal;
	}
	
	public String SucursalPlano(String Sucursal) {
		
		int CantidadaCerosFaltantes = 3 - Sucursal.length();
		
		String CantidadCeros = "";
		
		for (int i = 0 ; i < CantidadaCerosFaltantes ; i++) {
			
			CantidadCeros = CantidadCeros + "0";
			
		}
		
		String SucursalFinal = CantidadCeros + Sucursal; 
		
		return SucursalFinal;
	}
	
	public String CentroCostosPlano(String CentroCostos) {
		
		int CantidadaCerosFaltantes = 5 - CentroCostos.length();
		
		String CantidadCeros = "";
		
		for (int i = 0 ; i < CantidadaCerosFaltantes ; i++) {
			
			CantidadCeros = CantidadCeros + "0";
			
		}
		
		String CentroCostosFinal = CantidadCeros + CentroCostos; 
		
		//System.out.println("el centro de costos es : " + CentroCostosFinal +  " "+ CentroCostosFinal.length());
		
		return CentroCostosFinal;
	}
	
	
	
	public String ValorPesosPlano(String valoPesos) {

		String charsToRemove = ",.";
		
		for (char c : charsToRemove.toCharArray()) {
			valoPesos = valoPesos.replace(String.valueOf(c), "");
		}
		
		int CantidadaCerosFaltantes = 15 - valoPesos.length();
		
		String CantidadCeros = "";
		
		for (int i = 0 ; i < CantidadaCerosFaltantes ; i++) {
			
			CantidadCeros = CantidadCeros + "0";
			
		}
		
		
		
		String valoPesosFinal = CantidadCeros + valoPesos; 
		
		return valoPesosFinal;
	}
	
		public String ValorDivisaPlano(String valoDivisa , String Moneda) {

		
		int CantidadaCerosFaltantes = 15 - valoDivisa.length();
		
		int CantidadaCerosFaltantesDivisa = 3 - Moneda.length();
		
		String CantidadCeros = "";
		
		String cantidadCerosDivisa = "";
		
		for (int i = 0 ; i < CantidadaCerosFaltantes ; i++) {
			
			CantidadCeros = CantidadCeros + "0";
			
		}
		
		for (int i = 0 ; i < CantidadaCerosFaltantesDivisa ; i++) {
			
			cantidadCerosDivisa = cantidadCerosDivisa + "0";
			
		}
		
		
		
		String valoDivisaFinal = CantidadCeros + valoDivisa; 
		
		String valorMonedaFinal = cantidadCerosDivisa + Moneda;
		
		String ValorDivisaPlano = valorMonedaFinal + valoDivisaFinal;
		
		return ValorDivisaPlano;
	}
	
	public String fechaProcesoPLano (String fechaProceso) {
		
		int CantidadaCerosFaltantes = 9 - fechaProceso.length();
		
		String cantidadCerosFecha = "";
		
		for (int i = 0 ; i < CantidadaCerosFaltantes ; i++) {
			
			cantidadCerosFecha = cantidadCerosFecha + "0";
			
		}
		
		String FechaProcesoFinal = cantidadCerosFecha + fechaProceso;
		
		return FechaProcesoFinal;
	}
	
	public String fechaEfectivaPLano (String fechaEfectiva) {
		
		

		String charsToRemove = "-";

		for (char c : charsToRemove.toCharArray()) {
			fechaEfectiva = fechaEfectiva.replace(String.valueOf(c), "");
		}
		
		int CantidadaCerosFaltantes = 9 - fechaEfectiva.length();
		
		String cantidadCerosFechaEfectiva = "";
		
		for (int i = 0 ; i < CantidadaCerosFaltantes ; i++) {
			
			cantidadCerosFechaEfectiva = cantidadCerosFechaEfectiva + "0";
			
		}
		
		String FechaProcesoFinal = cantidadCerosFechaEfectiva + fechaEfectiva;
		
		return FechaProcesoFinal;
	}
	
	public String NitPlano (String nit) {
		
		int CantidadaCerosFaltantes = 17 - nit.length();
		
		String CantidadCeros = "";
		
		for (int i = 0 ; i < CantidadaCerosFaltantes ; i++) {
			
			CantidadCeros = CantidadCeros + "0";
			
		}
		
		String CentroCostosFinal = CantidadCeros + nit; 
		
		return CentroCostosFinal;
		
		
	}
	
	
	public String FacturaPlano (String factura) {
		
		int CantidadaCerosFaltantes = 17 - factura.length();
		
		String CantidadCeros = "";
		
		for (int i = 0 ; i < CantidadaCerosFaltantes ; i++) {
			
			CantidadCeros = CantidadCeros + "0";
			
		}
		
		String CentroCostosFinal = CantidadCeros + factura; 
		
		return CentroCostosFinal;
		
		
	}
	
	public String UsuariosPlano (String Usuarios) {
		
		int CantidadaCerosFaltantes = 10 - Usuarios.length();
		
		String CantidadCeros = "";
		
		for (int i = 0 ; i < CantidadaCerosFaltantes ; i++) {
			
			CantidadCeros = CantidadCeros + " ";
			
		}
		
		String UsuarioFinal = CantidadCeros + Usuarios; 
		
		return UsuarioFinal;
		
	}
	
	public String AutorizadorPlano (String Autorizador) {
		
		int CantidadaCerosFaltantes = 10 - Autorizador.length();
		
		String CantidadCeros = "";
		
		for (int i = 0 ; i < CantidadaCerosFaltantes ; i++) {
			
			CantidadCeros = CantidadCeros + " ";
			
		}
		
		String AutorizadorFinal = CantidadCeros + Autorizador; 
		
		return AutorizadorFinal;
		
	}
	
	
	
	public String ReferenciaPlano (String Referencia) {
		
		int CantidadaCerosFaltantes = 17 - Referencia.length();
		
		String CantidadCeros = "";
		
		for (int i = 0 ; i < CantidadaCerosFaltantes ; i++) {
			
			CantidadCeros = CantidadCeros + "0";
			
		}
		
		String CentroCostosFinal = CantidadCeros + Referencia; 
		
		return CentroCostosFinal;
		
		
	}

	
}
