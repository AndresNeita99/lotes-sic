package com.example.demo;

import java.io.File;
import java.io.FileWriter;
import java.time.LocalDate;

public class RegistrosLog {
	
	public void guardarRegistro (String usuario , String tarea) {
		
		try {
			
			LocalDate Fechacargue = LocalDate.now();
		  		
			String rutalog = "../Logs/log" + Fechacargue +".txt";
	        
			 File Archivolog = new File(rutalog);
   
	         FileWriter escribir = new FileWriter(Archivolog , true);
	         
	        
	         escribir.write(tarea);
	            
	         escribir.write("\r\n"); 
	            
	         escribir.close();

	         
       } catch (Exception e) {
       	
           e.printStackTrace();
       }
		
		
	}

}
