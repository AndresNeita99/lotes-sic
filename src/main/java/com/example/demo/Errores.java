package com.example.demo;

public class Errores {

	private int numeroError;

	private String TipoError; // 2 tipos de error (Por parametria o por estructura)

	private String DescripError;

	private String DatoErroneo;
	
	private String PosicionError;

	public int getNumeroError() {
		return numeroError;
	}

	public void setNumeroError(int numeroError) {
		this.numeroError = numeroError;
	}

	public String getTipoError() {
		return TipoError;
	}

	public void setTipoError(String tipoError) {
		TipoError = tipoError;
	}

	public String getDescripError() {
		return DescripError;
	}

	public void setDescripError(String descripError) {
		DescripError = descripError;
	}

	public String getDatoErroneo() {
		return DatoErroneo;
	}

	public void setDatoErroneo(String datoErroneo) {
		DatoErroneo = datoErroneo;
	}

	public String getPosicionError() {
		return PosicionError;
	}

	public void setPosicionError(String posicionError) {
		PosicionError = posicionError;
	}

	@Override
	public String toString() {
		return "Errores [numeroError=" + numeroError + ", TipoError=" + TipoError + ", DescripError=" + DescripError
				+ ", DatoErroneo=" + DatoErroneo + ", PosicionError=" + PosicionError + "]";
	}
	
	
	
	


	

}
