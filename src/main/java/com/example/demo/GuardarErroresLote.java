package com.example.demo;

import java.io.File;
import java.io.FileWriter;
import java.util.List;

public class GuardarErroresLote {

	public void GuardarErroresTxt(List<Errores> listaErrores, String idlote) {

		String rutaPlano = "..\\HistorialErrores";

		String rutaHistorial = "../HistorialErrores/" + idlote + ".txt";

		File directorio2 = new File(rutaPlano);

		if (!directorio2.exists()) {

			if (directorio2.mkdirs()) {

			}

		}

		// creamos el archivo txt con en la carpeta previamente creada para guardar los
		// errores TXT

		try {

			File file2 = new File(rutaHistorial); // Si el archivo no existe es creado

			if (!file2.exists()) {

				file2.createNewFile();

			}

		} catch (Exception e) {

			e.printStackTrace();

		}

		try {

			File ArchivoPlanoHistorial = new File(rutaHistorial);

			FileWriter escribir2 = new FileWriter(ArchivoPlanoHistorial, true);

			for (int i = 0; i < listaErrores.size(); i++) {
				

				escribir2.write(listaErrores.get(i).toString());

				escribir2.write("\r\n");
				
			}
			
			//escribir2.write(listaErrores.toString());

			//escribir2.write("\r\n");

			escribir2.close();

		} catch (Exception e) {

			e.printStackTrace();
		}

	}

}
