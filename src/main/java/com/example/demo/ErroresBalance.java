package com.example.demo;

public class ErroresBalance {
	
	private int NumeroError;
	
	private String TipoError;
	
	private String Descripccion;
	
	private String DatoErroneo;
	
	private String saldoloteString;
	
	private String SaldoBalance;

	public int getNumeroError() {
		return NumeroError;
	}

	public void setNumeroError(int numeroError) {
		NumeroError = numeroError;
	}

	public String getTipoError() {
		return TipoError;
	}

	public void setTipoError(String tipoError) {
		TipoError = tipoError;
	}

	public String getDescripccion() {
		return Descripccion;
	}

	public void setDescripccion(String descripccion) {
		Descripccion = descripccion;
	}

	public String getDatoErroneo() {
		return DatoErroneo;
	}

	public void setDatoErroneo(String datoErroneo) {
		DatoErroneo = datoErroneo;
	}

	public String getSaldoloteString() {
		return saldoloteString;
	}

	public void setSaldoloteString(String saldoloteString) {
		this.saldoloteString = saldoloteString;
	}

	public String getSaldoBalance() {
		return SaldoBalance;
	}

	public void setSaldoBalance(String saldoBalance) {
		SaldoBalance = saldoBalance;
	}

	
	

}
